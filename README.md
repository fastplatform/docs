# <img src="https://gitlab.com/fastplatform/website/-/raw/master/assets/images/logo-32x32.svg" /> FaST

_[https://fastplatform.eu](https://fastplatform.eu)_

Welcome to the documentation for the FaST platform. The entry points below present the overall architecture of the platform, from the infrastructure level to the access layer, and the user documentation material.

## The basics

- [What is the FaST platform?](basics/what-is-the-fast-platform.md)
- [High level design](basics/high-level-design.md)
- [Structure of the code repositories](basics/code-repositories.md)

## Reference information

In these sections, we provide reference information about the various components of the platform, and how they interact.

- [Infrastructure](reference/infrastructure.md)
    - [Services Orchestration](reference/orchestration.md)
    - [Continuous Integration & Deployment](reference/ci-cd.md)
    - [Monitoring](reference/monitoring.md)
- [Authentication and authorization](reference/auth.md)
- [Modules and services](reference/services.md)
- [Data model](reference/data-model.md)
- [Mobile and web application](reference/mobile-and-web.md)
- [Development environment](reference/setting-up-dev-env.md)
- [Glossary](reference/glossary.md)

## Deploying the platform

- [Deploy the FaST Platform infrastructure on Flexible Engine](reference/flexible-engine-deployment.md)
- [Deploy the FaST platform infrastructure to another Cloud provider](reference/requirements-deployment.md)
- [Hibernation of the FaST Platform infrastructure on Flexible Engine](reference/flexible-engine-hibernation.md)


## Running the platform

- [Runbook](reference/runbook.md)

## User documentation

- [Administration Portal](https://gitlab.com/fastplatform/core/-/blob/master/services/web/backend/docs/src/index.md) (note: this documentation is also available from within the portal itself)
- Mobile and web application quickstart:
  [bg](quickstart/app-quickstart-bg-bg.pdf)
  [de](quickstart/app-quickstart-be-de.pdf)
  [el](quickstart/app-quickstart-gr-el.pdf)
  [en](quickstart/app-quickstart-en.pdf)
  [es](quickstart/app-quickstart-es-es.pdf)
  [et](quickstart/app-quickstart-ee-et.pdf)
  [fr](quickstart/app-quickstart-be-fr.pdf)
  [it](quickstart/app-quickstart-it-it.pdf)
  [ro](quickstart/app-quickstart-ro-ro.pdf)
  [sk](quickstart/app-quickstart-sk-sk.pdf)

## Project documents

- [Concept of Operations](conops/conops.md)
- [Journey Documentation](journey_doc/journey_doc.md)

## Contact us

You can contact the FaST technical team at [tech@fastplatform.eu](mailto:tech@fastplatform.eu).
