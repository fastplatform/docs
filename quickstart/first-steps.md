# Accessing the FaST platform

The platform is composed of 2 main elements:
- the farmer mobile application (also accessible in the browser)
- the administration portal (browser only)

## Accessing the mobile application

- Uninstall it, if you had installed it in a previous iteration of the project
- Then, on your mobile browser, go to either:
    - **iOS** https://testflight.apple.com/join/7d0Nl9Oa and follow the instructions
    - **Android** https://play.google.com/store/apps/details?id=eu.fastplatform.mobile.farmer and follow the instructions
    - **Web version** https://app.beta.fastplatform.eu
- Once the app is installed and started, select your country / region and click _Log in_
  <br><img src="img/region-selection.png" width="200">
- Choose the FaST identity provider when prompted
  <br><img src="img/fast-idp.png" width="200">
- Enter your username and password, and click _Log in_
- Then, within the app:
    - Read and accept the _Terms and conditions_ and the _Privacy policy_ when prompted
    - Choose: _Use a demo farm_
- You are now in the farmer application, using a _demo farm_, and can proceed to test and [provide your feedback](#providing-feedback).

Quickstart guides are available at:
- Belgium: [fr](app-quickstart-be-fr.pdf) [de](app-quickstart-be-de.pdf)
- Bulgaria: [bg](app-quickstart-bg.pdf)
- Estonia: [et](app-quickstart-ee-et.pdf)
- Spain: [es](app-quickstart-es-es.pdf)
- Greece: [el](app-quickstart-gr-el.pdf)
- Italy: [it](app-quickstart-it-it.pdf)
- Romania: [ro](app-quickstart-ro-ro.pdf)
- Slovakia: [sk](app-quickstart-sk-sk.pdf)

## Accessing the administration portal

- Go to:
    - Wallonia (Belgium): https://portal.beta.be-wal.fastplatform.eu
    - Greece: https://portal.beta.gr.fastplatform.eu
    - Piemonte (Italy): https://portal.beta.it-21.fastplatform.eu
    - Romania: https://portal.beta.ro.fastplatform.eu
    - Slovakia: https://portal.beta.sk.fastplatform.eu
- Choose the FaST identity provider when prompted
  <br><img src="img/fast-idp.png" width="200">
- Enter your username and password, and click _Log in_
- Read and accept the _Terms and conditions_ and the _Privacy policy_, if prompted
- You are now logged in to the administration portal! You have been granted _superuser_ role for testing, so you can see everything (and also break everything, so be careful) ; more granular permissions will be applied in the future, when we exit the test phase.
- Note that there is a documentation available in the portal (_Documentation_ link in the top right corner)
  <br><img src="img/admin-docs.png" width="500">

> **Note:** If you have a recent Android phone, the mobile application is unfortunately not compatible with Android 11. We are working on this and hope to have it resolved in January. You can however use the web version (link above) from your phone's browser.

## Providing feedback

Please send your feedback to support@fastplatform.eu, preferrably with _only one question per email_: each one of your emails gets converted into a support ticket and that helps the development team to better address your remarks.