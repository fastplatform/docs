# Structure of the code repositories

All the FaST source code is currently hosted on [gitlab.com](https://gitlab.com/). The code is open-source and is generally licensed under MIT license; however, some of the code is licensed under other licenses (e.g. GPLv3), so be sure to check the `LICENSE` file of each repository.

The common source code of all the services is hosted in the [gitlab.com/fastplatform](https://gitlab.com/fastplatform) group, that is managed by the European Commission. The code that is private to each Member State (custom services, deployment configurations, encryption keys, etc.) is hosted in private groups such as [gitlab.com/fastplatform-xx](https://gitlab.com/fastplatform-xx), where `xx` is the code of the Member State:
- Wallonia: https://gitlab.com/fastplatform-be-wal
- Bulgaria: https://gitlab.com/fastplatform-bg
- Greece: https://gitlab.com/fastplatform-gr
- Piemonte: https://gitlab.com/fastplatform-it-21
- Romania: https://gitlab.com/fastplatform-ro
- Slovakia: https://gitlab.com/fastplatform-sk

Each private group contains:
- A copy of the source code of the *core services* (the FaST common services). This copy is performed using a [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) of the corresponding [core](https://gitlab.com/fastplatform/core) repository of the main group.
- A copy of the source code of the *infrastructure* (the code that defines the cloud resources used by the FaST platforms). This copy is also performed using a fork of the corresponding [ops](https://gitlab.com/fastplatform/ops) repositories of the main group.
- A copy of the source code of the *mobile application*. This copy is also performed using a fork of the corresponding [farmer-mobile-app](https://gitlab.com/fastplatform/ops) repository of the main group.
- A copy of the source code of the *add-ons* that are used by the Member State. This copy is also performed using a fork of the corresponding [add-ons](https://gitlab.com/fastplatform/addons) repositories of the main group. The add-ons used by each Member State are different, and only the ones actually used in the national platform are copied in the private group. The add-ons currently cover the weather forecasting, the fertilization algorithms and the satellite imagery processing.
- The *custom* services developed for the Member State, mainly:
    - the authentication service, interfacing with national identity providers
    - the IACS data access service, interfacing with the IACS/GSAA registries of the Paying Agency
    - the regulatory/nitrate service, that computes the nitrogen limitations and warnings, based on the local regulations

The copied/forked code repositories are automatically updated when the corresponding source code (from the main public code repositories) is modified. This is done using the [mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/) capability of GitLab, but can be disabled. If it is disabled, the code is not automatically updated, and the user must manually pull the new versions of the code into the private repositories.


```plantuml
title
    Example structure of code repositories for Wallonia

end title

rectangle "gitlab.com/fastplatform" as repo #white {
    label "This is the main (public) group\rof the FaST platform.\rIt contains all the common\rservices used by the national platforms."
    rectangle "infrastructure" as infra #cyan
    rectangle "core services" as core #cyan
    rectangle "mobile app" as app #cyan
    rectangle "add-ons" as addons {
        rectangle "add-on 1" as addon1 #cyan
        rectangle "add-on 2" as addon2 #cyan
    }
}

rectangle "gitlab.com/fastplatform-be-wal" as repo_be_wal #white {
    label "This is the private group\rof <b>Wallonia</b>. It contains a copy (fork)\rof all the common services, as well as\rcustom services specific to Wallonia\rand Wallonian configuration parameters\rfor all the services."
    rectangle "infrastructure" as infra_be_wal #yellow
    rectangle "core services" as core_be_wal #yellow
    rectangle "mobile app" as app_be_wal #yellow
    rectangle "add-ons" as addons_be_wal  {
        rectangle "add-on 1" as addon1_be_wal #yellow
    }
    rectangle "custom services" as custom_be_wal #white {
        rectangle "IACS connector" as custom1_be_wal #yellow
        rectangle "authentication" as custom2_be_wal #yellow
        rectangle "regulatory" as custom3_be_wal #yellow
        rectangle "configuration" as custom4_be_wal #yellow;line.dashed
    }
}

rectangle "FaST Wallonia\ncloud infrastructure" as cloud_be_wal #yellow
rectangle "FaST Wallonia\nCI-CD" as cicd_be_wal #yellow
rectangle "FaST Wallonia\nApp Store account" as apple_be_wal #yellow
rectangle "FaST Wallonia\nPlay Store account" as google_be_wal #yellow

repo -[hidden]- repo_be_wal

infra -> infra_be_wal #black: <color:black>template</color>
core -> core_be_wal #black: <color:black>fork</color>
app -> app_be_wal #black: <color:black>fork</color>
addon1 -> addon1_be_wal #black: <color:black>fork</color>

infra_be_wal --> cicd_be_wal #black: <color:black>build</color>
core_be_wal --> cicd_be_wal #black: <color:black>build</color>
addon1_be_wal --> cicd_be_wal #black: <color:black>build</color>
custom1_be_wal --> cicd_be_wal #black: <color:black>build</color>
custom2_be_wal --> cicd_be_wal #black: <color:black>build</color>
custom3_be_wal --> cicd_be_wal #black: <color:black>build</color>
custom4_be_wal --> cicd_be_wal #black: <color:black>build</color>

cicd_be_wal --> cloud_be_wal #black: <color:black>deploy</color>

app_be_wal --> apple_be_wal #black: <color:black>publish</color>
app_be_wal --> google_be_wal #black: <color:black>publish</color>
app_be_wal --> cicd_be_wal #black: <color:black>build (web)</color>

legend
    In the diagram above, the yellow color denotes the private elements
    that are fully owned by the Member State, while the cyan color denotes
    the common elements that are exposed by the European Commission.
end legend

```

The above structure allows each Member State to:
- have a fully private copy of all the services that are run on their platform, including the common services
- have full ownership of the custom services developed for their platform
- have full ownership of all the configurations (including secrets/certificates) that are applied on their platform
- still have the benefit of being able to get updates from the common services (by pulling changes)
- retain the ability to "unplug" the common services from the main public repository, and perform changes on these services directly in their private repository

#### Deployment

The source code hosted in the each private group is deployed using [CI/CD](../reference/ci-cd.md) pipelines on GitLab. The pipelines are configured to deploy the code to the corresponding private cloud infrastructure.

#### Customizing the common services and the mobile application

As each Member State has a private copy of the common services and of the mobile application source code, it is possible for it to customize these services further by modifying the private copy. This implies:
- that the automatic mirroring (described above) must be disabled
- that all future updates to the common services must be pulled and rebased manually in the private repositories
- that the compatibility of the local changes with the other services must be assured by the Member State (e.g. changes to the data model must be compatible with the API gateway and the mobile application)

#### Contributing changes back to the common repositories

If a Member State chooses to perform private changes on the common services, it can then push these changes back to the main public repository. This is done by opening a [merge request across the initial fork](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html). Merging those changes will have to be approved by the owner of the main public repository.
