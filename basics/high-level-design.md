# Main components


## High level design

The FaST Platform is deployed on standard cloud infrastructure, currently on [Orange Business Services](https://www.orange-business.com/en) and [Sobloo](https://sobloo.eu/) for the satellite images. 

The cloud resources are abstracted from the actual workload and storage by the use of the Kubernetes container orchestration technology. This orchestrator then runs the FaST workloads, automatically and at scale.

## Core, custom and add-ons

The FaST Platform provides a common infrastructure that allows to run load at high scale, for any country or region. Each country has its own cluster of machines serving its data and processing. The countries are isolated in different *cloud subscriptions*, i.e. separate contracts with the cloud provider, therefore ensuring complete isolation between the resources of separate countries. 
More details are provided in the [infrastructure](reference/infrastructure.md) document.

The platform is also modular and therefore allows the reuse of modules across national platforms. The code of the core modules for example, is common to all platforms (e.g. the app or the Admin Portal). The custom modules are specific to a platform (e.g. an IACS connector). Add-ons are also available and can be deployed in one or more platforms. 
See [add-ons](https://gitlab.com/fastplatform/addons) for the list of currently available add-ons on FaST and more detailed [documentation](reference/services.md) about core/custom services.

# Farmer app and Admin Portal

The FaST users access the platform through 2 main channels:
- the Farmer app (web or mobile version)
- the Administration Portal (web only)

The app is accessible to all FaST users. It has the **same** codebase across regions: there is no *Bulgarian version* or *Romanian version*, only one *FaST app* version (however some features might be enabled/disabled in some regions). 

The app and the Admin Portal are localized to the languages currently supported in FaST and received different *configuration* depending on the country being selected by the user. 
More detailed documentation about the Farmer app can be found [here](https://gitlab.com/fastplatform/mobile/farmer-mobile-app).

The Administration Portal is only accessible to designated FaST users that have the adequate permissions.
