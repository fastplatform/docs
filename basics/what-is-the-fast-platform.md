# What is the FaST platform?


## Vision

Supported by the European Commission’s DG Agriculture and Rural Development, by the EU Space Programme (DG DEFIS) and by the EU ISA2 Programme (DG DIGIT), the FaST digital service platform makes available capabilities for agriculture, environment and sustainability to EU farmers, Member State Paying Agencies, farm advisors and developers of digital solutions. The vision is for the FaST to become a world-leading platform for the generation and re-use of solutions for sustainable and competitive agriculture based on space data (Copernicus and Galileo) and other data public and private datasets. The modular platform will support EU agriculture and the Common Agricultural policy by also enabling the use of solutions based on machine learning applied to image recognition, as well as the use and reuse of IoT data, various public sector data, and user generated data.

You can find more details and contact forms on the dedicated [FaST website](https://fastplatform.eu).

## Stakeholders

- **Farmers**
    + Access the platform through a [web and mobile application](../reference/mobile-and-web.md) for standard browsers, iOS and Android
    + All farm data in the palm of their hands
    + Simplified tasks

- **National and Regional CAP agencies**
    + Access the platform through a [web or portal](https://gitlab.com/fastplatform/core/-/blob/master/services/web/backend/docs/src/index.md)
    + Data-informed action
    + Direct communication to farmers

- **Advisors & farmer associations**
    + Access the platform through a [web or mobile application](../reference/mobile-and-web.md) for standard browsers, iOS and Android
    + More information on their members
    + Better support and coordination

- **Policy makers**
    + Access the platform through [secured APIs](../reference/services.md)
    + Data for designing and monitoring policy measures

- **Research centers**
    + Access the platform through [secured APIs](../reference/services.md)
    + Research inputs
    + Link to farms

- **Non-Governmental agencies**
    + Access the platform through [secured APIs](../reference/services.md)
    + Opportunities to engage farmers in innovative environmental action

- **Commercial services providers**
    + Access the platform through [secured APIs](../reference/services.md)
    + Market pull from public sector clients
    + Direct marketing to farmers
