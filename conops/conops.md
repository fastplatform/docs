<img src="https://gitlab.com/fastplatform/website/-/raw/master/assets/images/logo-32x32.svg" />

# FaST Concept Of Operations (CONOPS)

For each Member State, the FaST platform is composed of:
- A common web and mobile application (Android & iOS)
- A Cloud infrastructure deployed on Flexible Engine (Orange Business Services part / DIAS Sobloo) that orchestrates:
    - Common and Custom services
    - Data

The purpose of this document is to provide information about the roles and governance for the operation of this platform.

## Roles

### Users

There are three types of users:
- Farmers and advisors who use the Farmer application to access their farm data, communicate, and receive agronomic advices
- Paying agencies that use the Administration Portal to manage the FaST platform
- Third-party providers that offer new subscribable services that are interfaced with FaST data

More information is available in these [documents](https://gitlab.com/fastplatform/core/-/blob/master/services/web/backend/docs/src/users).

### Platform operators

Three types of expertise are required to operate the technical layers of the platform:
- Infrastructure (Cloud ressources)
- Kubernetes (Container orchestration)
- App & Services (Microservices)

**Tools**

Operators have access to different tools to monitor the platform and to analyze issues:
- Dashboards with metrics
- Logs
- System APIs

**Bug Tracking**

Users can send an email to the support in order to open tickets. The bug tracker is hosted on the GitLab desk service.

When a ticket is raised, following actions need to be done:
- Set ticket severity (blocking, non-blocking)
- Set ticket categories (impacted components, target release, etc...)
- Open new tickets related to the issue if needed
- Assign ticket(s)
- Close ticket(s) when resolved

## Governance

### European Commission

A coordination committee between the DGs concerned must be set up to manage and arbitrate the overall evolution of the platform and the FaST ecosystem.

The committee has the responsibility to:
- Select an IT contractor for the development and support of the FaST platform
- Select the participating Member States
- Decide on a common strategy for the evolution of the common modules of the platform 
- Oversee platform policies, such as privacy and terms of use
- Manage subscriptions to common external services needed for the platform (Domain Name, etc ...)

### Paying Agencies

Paying Agencies are the Regional/National Stakeholders of FaST.

A Paying Agency has the responsibility to:
- Communicate about FaST to their local farmers and support them to use the tool
- Provide Level 1 (Basic help desk resolution and service desk delivery) support for users
- Propose or implement evolutions to their custom services or the common services
- Manage the integration of external third parties to the FaST platform

### IT Contractor

An IT contractor that provides technical maintenance and support for the FaST platform and ecosystem.

The IT contractor has the responsibility to:
- Maintain the code base on GitLab and the CI/CD pipelines
- Ensure the availability and the performance of the FaST platform
- Ensure the security of the platform
- Implement and validate new features
- Maintain the FaST ancillary systems such as the wiki, help desk and marketing [website](https://fastplatform.eu)
- Provide Level 2 (In-depth technical support) and Level 3 (Expert product and service support) support

