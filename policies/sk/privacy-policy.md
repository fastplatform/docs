# Zásady ochrany osobných údajov

_Posledná aktualizácia 17. januára 2022_

Tieto Zásady ochrany osobných údajov opisujú naše zásady a postupy týkajúce sa zhromažďovania, používania a zverejňovania vašich informácií pri používaní Služby a informujú vás o vašich právach na ochranu osobných údajov a o tom, ako vás chráni zákon.

Vaše osobné údaje používame na poskytovanie a zlepšovanie Služby. Používaním Služby súhlasíte so zhromažďovaním a používaním informácií v súlade s týmito Zásadami ochrany osobných údajov.

## Výklad a definície

### Výklad

Slová, ktorých začiatočné písmeno je veľké, majú význam definovaný za nasledujúcich podmienok. Nasledujúce definície majú rovnaký význam bez ohľadu na to, či sú uvedené v jednotnom alebo množnom čísle.

### Definície

Na účely týchto zásad ochrany osobných údajov:

- **Účtom** sa rozumie jedinečné konto vytvorené pre vás na prístup k našej službe alebo jej častiam.

- **Spoločník** znamená subjekt, ktorý kontroluje, je kontrolovaný alebo je pod spoločnou kontrolou so stranou, pričom "kontrola" znamená vlastníctvo 50 % alebo viac akcií, majetkových podielov alebo iných cenných papierov oprávnených hlasovať pri voľbe riaditeľov alebo iných riadiacich orgánov.

- **Aplikácia** znamená softvérový program poskytnutý Európskou komisiou, ktorý ste si stiahli do akéhokoľvek elektronického zariadenia s názvom FaST

- **Európska komisia** (v tejto zmluve označovaná ako "Európska komisia", "My", "Nás" alebo "Naša") sa vzťahuje na Európsku komisiu, GR DEFIS, oddelenie I3 Vesmírne údaje pre spoločenské výzvy a rast, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgicko

- **GDPR** je všeobecné nariadenie o ochrane údajov

- **Platobná agentúra CAP** sa vzťahuje na:
    - Pre používateľov Andalúzie: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Pre používateľov z Kastílie a Leónu: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Pre používateľov z Piemontu: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Pre estónskych používateľov: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Pre používateľov z Valónska: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgique
    - Pre gréckych používateľov: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pre používateľov z Bulharska: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Pre slovenských používateľov: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Pre používateľov v Rumunsku: Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, Bucureşti, România

- **Prevádzkovateľ údajov** (v tejto zmluve označovaný ako "My", "Nás" alebo "Náš") na účely GDPR označuje Európsku komisiu ako právny subjekt, ktorý určuje účely a prostriedky spracúvania osobných údajov.

- **Zariadenie** znamená akékoľvek zariadenie, ktoré má prístup k Službe, ako napríklad počítač, mobilný telefón alebo digitálny tablet.

- **Osobné údaje** sú akékoľvek informácie, ktoré sa týkajú identifikovanej alebo identifikovateľnej osoby. Na účely GDPR Osobné údaje znamenajú akékoľvek informácie, ktoré sa vás týkajú, napríklad meno, identifikačné číslo, údaje o polohe, online identifikátor alebo jeden či viac faktorov špecifických pre fyzickú, fyziologickú, genetickú, mentálnu, ekonomickú, kultúrnu alebo sociálnu identitu.

- **Služba** sa vzťahuje na Aplikáciu.

- **Poskytovateľ služieb** znamená akúkoľvek fyzickú alebo právnickú osobu, ktorá spracúva údaje v mene Európskej komisie. Vzťahuje sa na spoločnosti alebo fyzické osoby tretích strán, ktoré zamestnáva Európska komisia alebo platobná agentúra na uľahčenie Služby, na poskytovanie Služby v mene Európskej komisie, na vykonávanie služieb súvisiacich so Službou alebo na pomoc Európskej komisii a platobnej agentúre pri analýze spôsobu používania Služby. Na účely GDPR sa poskytovatelia služieb považujú za spracovateľov údajov.

- **Overovacia služba tretej strany** sa vzťahuje na akúkoľvek službu, prostredníctvom ktorej sa Používateľ môže prihlásiť alebo si vytvoriť účet na používanie Služby.

- **Tretia strana - služba prihlásenia** sa vzťahuje na akúkoľvek externú službu, ktorá bola zaregistrovaná v rámci Služby a ku ktorej sa môžete prihlásiť.

- **Metriky používania** sa vzťahujú na automaticky zhromažďované údaje, ktoré sú buď generované používaním Služby, alebo pochádzajú zo samotnej infraštruktúry Služby (napríklad trvanie návštevy stránky).

- **Vy** znamená jednotlivca, ktorý pristupuje k Službe alebo ju používa, prípadne spoločnosť alebo iný právny subjekt, v mene ktorého takýto jednotlivec pristupuje k Službe alebo ju používa. Podľa nariadenia GDPR (všeobecné nariadenie o ochrane údajov) vás možno označiť ako subjekt údajov alebo ako používateľa, keďže ste fyzická osoba, ktorá používa Službu.

## Zhromažďovanie a používanie vašich osobných údajov

### Typy zhromažďovaných údajov

#### Osobné údaje

Pri používaní Našej služby Vás môžeme požiadať, aby ste nám poskytli určité osobné údaje, ktoré možno použiť na kontaktovanie alebo identifikáciu Vašej osoby. Osobne identifikovateľné údaje môžu okrem iného zahŕňať:

- e-mailovú adresu
- Meno a priezvisko
- telefónne číslo
- Adresa, štát, provincia, PSČ, mesto
- Správy a prílohy, ktoré si vymieňate s inými používateľmi (vrátane platobnej agentúry CAP)
- Fotografie s geografickým označením, ktoré si vymieňate na platforme (vrátane fotografií s vašou platobnou agentúrou CAP)
- Opis vašej farmy (napr. pozemky, plodiny), poľnohospodárske postupy (napr. odporúčania na hnojenie, ktoré dostávate), údaje o pôde, ktoré ste vložili do služby
- Metriky používania (pozri nižšie)

#### Metriky využívania služieb

Metriky používania sa zhromažďujú automaticky pri používaní služby.

Metriky používania môžu zahŕňať informácie, ako je adresa internetového protokolu vášho zariadenia (napr. IP adresa), typ prehliadača, verzia prehliadača, stránky našej Služby, ktoré ste navštívili, čas a dátum vašej návštevy, čas strávený na týchto stránkach, jedinečné identifikátory zariadenia a iné diagnostické údaje.

Keď pristupujete k Službe prostredníctvom mobilného zariadenia alebo cez mobilné zariadenie, môžeme automaticky zhromažďovať určité informácie, okrem iného typ mobilného zariadenia, ktoré používate, jedinečný identifikátor vášho mobilného zariadenia, IP adresu vášho mobilného zariadenia, váš mobilný operačný systém, typ mobilného internetového prehliadača, ktorý používate, jedinečné identifikátory zariadenia a iné diagnostické údaje.

Môžeme tiež zhromažďovať informácie, ktoré Váš prehliadač odošle vždy, keď navštívite našu službu alebo keď k službe pristupujete prostredníctvom mobilného zariadenia alebo cez mobilné zariadenie.

#### Informácie zhromažďované počas používania aplikácie

Počas používania Našej aplikácie môžeme s vaším predchádzajúcim súhlasom zhromažďovať:

- informácie týkajúce sa vašej polohy
- Obrázky a iné informácie z fotoaparátu a knižnice fotografií vášho Zariadenia

Tieto informácie používame na poskytovanie funkcie geograficky označených fotografií v Našej službe a na lokalizáciu Vašej polohy na našich satelitných mapách. Tieto informácie môžu byť odoslané na Naše servery a/alebo na server Poskytovateľa služieb alebo môžu byť jednoducho uložené vo Vašom zariadení.

Prístup k týmto informáciám môžete kedykoľvek povoliť alebo zakázať prostredníctvom nastavení Vášho zariadenia.

### Používanie vašich osobných údajov

Naša služba je v súčasnosti vo fáze beta verzie, a preto sa vaše Osobné údaje budú používať len na **vývoj, zlepšovanie a testovanie našej Služby**.

Počas vášho používania Služby vás môžeme kontaktovať prostredníctvom e-mailu, telefonických hovorov, SMS alebo iných rovnocenných foriem elektronickej komunikácie, ako sú napríklad push oznámenia mobilnej aplikácie, týkajúce sa aktualizácií alebo informačných oznámení súvisiacich s funkciami, vylepšeniami alebo novinkami o našich Službách vrátane bezpečnostných aktualizácií, ak je to potrebné alebo primerané na ich realizáciu. Vaše osobné údaje budeme uchovávať aj na účely vybavovania a správy žiadostí, ktoré nám predložíte.

### Uchovávanie vašich osobných údajov

Vaše Osobné údaje budeme uchovávať len tak dlho, ako to bude potrebné na účely uvedené v týchto Zásadách ochrany osobných údajov. Vaše Osobné údaje budeme uchovávať a používať v rozsahu potrebnom na splnenie našich zákonných povinností (napríklad ak sme povinní uchovávať vaše údaje na splnenie platných zákonov), na riešenie sporov a na presadzovanie našich právnych dohôd a zásad.

Na účely internej analýzy budeme uchovávať aj metriky používania. Metriky používania sa vo všeobecnosti uchovávajú kratšiu dobu, okrem prípadov, keď sa tieto údaje používajú na posilnenie bezpečnosti alebo zlepšenie funkčnosti Našej služby, alebo ak sme právne povinní uchovávať tieto údaje dlhšiu dobu.

### Prenos vašich osobných údajov

Vaše informácie vrátane osobných údajov sa spracúvajú v Našich prevádzkových kanceláriách a na akýchkoľvek iných miestach, kde sa nachádzajú strany zapojené do spracúvania. To znamená, že tieto informácie môžu byť prenesené do počítačov - a uchovávané v počítačoch - nachádzajúcich sa mimo Vášho štátu, provincie, krajiny alebo inej vládnej jurisdikcie, kde sa zákony o ochrane údajov môžu líšiť od zákonov Vašej jurisdikcie.

Váš súhlas s týmito Zásadami ochrany osobných údajov, po ktorom nasleduje Vaše odoslanie takýchto informácií, predstavuje Váš súhlas s týmto prenosom.

Urobíme všetky kroky, ktoré sú primerane potrebné na zabezpečenie bezpečného zaobchádzania s Vašimi údajmi a v súlade s týmito Zásadami ochrany osobných údajov, a žiadny prenos Vašich osobných údajov sa neuskutoční do organizácie alebo krajiny, ak nie sú zavedené primerané kontrolné mechanizmy vrátane zabezpečenia Vašich údajov a iných osobných informácií.

### Zverejnenie Vašich osobných údajov

#### Vymáhanie práva

Za určitých okolností môžeme byť povinní zverejniť Vaše Osobné údaje, ak to vyžaduje zákon alebo v reakcii na platné žiadosti orgánov verejnej moci (napr. súdu alebo vládneho orgánu).

#### Ďalšie právne požiadavky

Vaše Osobné údaje môžeme zverejniť v dobrej viere, že takéto konanie je nevyhnutné na:

- splnenie zákonnej povinnosti
- Chrániť a obhajovať práva alebo majetok Európskej komisie
- Predchádzanie alebo vyšetrovanie možného protiprávneho konania v súvislosti so Službou
- chrániť osobnú bezpečnosť používateľov služby alebo verejnosti
- ochranu pred právnou zodpovednosťou

### Bezpečnosť vašich osobných údajov

Bezpečnosť Vašich osobných údajov je pre Nás dôležitá, ale nezabúdajte, že žiadny spôsob prenosu cez internet ani spôsob elektronického ukladania nie je stopercentne bezpečný. Hoci sa snažíme používať komerčne prijateľné prostriedky na ochranu Vašich Osobných údajov, nemôžeme zaručiť ich absolútnu bezpečnosť.

## GDPR Ochrana osobných údajov

### Právny základ spracovania osobných údajov podľa GDPR

Osobné údaje môžeme spracúvať za nasledujúcich podmienok:

- **Súhlas:** Poskytli ste svoj súhlas so spracovaním Osobných údajov na jeden alebo viacero konkrétnych účelov.

- **Plnenie zmluvy:** Poskytnutie Osobných údajov je nevyhnutné na plnenie zmluvy s Vami a/alebo na plnenie akýchkoľvek jej predzmluvných povinností.

- **Právne povinnosti:** Spracovanie Osobných údajov je nevyhnutné na splnenie právnej povinnosti, ktorá sa vzťahuje na Európsku komisiu.

- **Životne dôležité záujmy:** Spracúvanie osobných údajov je nevyhnutné na ochranu Vašich životne dôležitých záujmov alebo záujmov inej fyzickej osoby.

- **Verejný záujem:** Spracúvanie osobných údajov súvisí s úlohou, ktorá sa vykonáva vo verejnom záujme alebo pri výkone verejnej moci zverenej Európskej komisii.

- **Oprávnené záujmy:** Spracúvanie osobných údajov je nevyhnutné na účely oprávnených záujmov, ktoré sleduje Európska komisia.

V každom prípade vám radi pomôžeme objasniť konkrétny právny základ, ktorý sa na spracúvanie vzťahuje, a najmä to, či je poskytnutie Osobných údajov zákonnou alebo zmluvnou požiadavkou, alebo požiadavkou potrebnou na uzavretie zmluvy.

### Vaše práva podľa GDPR

Európska komisia sa zaväzuje dodržiavať dôvernosť Vašich osobných údajov a zaručiť Vám možnosť uplatnenia Vašich práv.

Podľa týchto Zásad ochrany osobných údajov a právnych predpisov EÚ máte právo na:

- **požiadať o prístup k Vašim osobným údajom.** Právo na prístup, aktualizáciu alebo vymazanie informácií, ktoré o Vás máme. Vždy, keď je to možné, môžete získať prístup k Vašim Osobným údajom, aktualizovať ich alebo požiadať o ich vymazanie priamo v sekcii nastavení Vášho účtu. Ak tieto úkony nemôžete vykonať sami, kontaktujte Nás, aby sme Vám pomohli. To Vám tiež umožňuje získať kópiu Osobných údajov, ktoré o Vás uchovávame.

- **Žiadosť o opravu Osobných údajov, ktoré o Vás máme.** Máte právo na opravu akýchkoľvek neúplných alebo nepresných informácií, ktoré o Vás máme.

- **Namietať proti spracúvaniu Vašich osobných údajov.** Toto právo existuje, ak sa spoliehame na oprávnený záujem ako právny základ pre Naše spracúvanie a existuje niečo vo Vašej konkrétnej situácii, čo Vás núti namietať proti spracúvaniu Vašich osobných údajov z tohto dôvodu. Právo namietať máte aj v prípade, že Vaše Osobné údaje spracúvame na účely priameho marketingu.

- **Žiadosť o vymazanie Vašich Osobných údajov.** Máte právo nás požiadať o vymazanie alebo odstránenie Osobných údajov, ak neexistuje žiadny dobrý dôvod, aby sme ich naďalej spracúvali.

- **Požiadať o prenos Vašich Osobných údajov.** Poskytneme Vám alebo Vami zvolenej tretej strane Vaše Osobné údaje v štruktúrovanom, bežne používanom, strojovo čitateľnom formáte. Upozorňujeme, že toto právo sa vzťahuje len na automatizované informácie, na ktorých použitie ste nám pôvodne poskytli súhlas alebo ak sme tieto informácie použili na plnenie zmluvy s vami.

- **Odvolanie súhlasu.** Máte právo odvolať svoj súhlas s použitím vašich osobných údajov. Ak svoj súhlas odvoláte, je možné, že Vám nebudeme môcť poskytnúť prístup k určitým špecifickým funkciám Služby.

### Uplatnenie vašich práv na ochranu údajov podľa GDPR

Svoje práva na prístup, opravu, zrušenie a námietku môžete uplatniť tak, že nás kontaktujete. Upozorňujeme, že pred odpoveďou na takéto žiadosti Vás môžeme požiadať o overenie Vašej totožnosti. Ak podáte žiadosť, budeme sa snažiť odpovedať vám čo najskôr.

Máte právo podať sťažnosť na Úrad na ochranu osobných údajov v súvislosti s Naším zhromažďovaním a používaním Vašich osobných údajov. Ďalšie informácie získate od Vášho miestneho orgánu na ochranu údajov v Európskom hospodárskom priestore.

## Odkazy na iné webové stránky a služby

Naša služba môže obsahovať odkazy na iné webové stránky alebo služby, ktoré nie sú prevádzkované nami. Ak kliknete na odkaz tretej strany, budete presmerovaní na stránku tejto tretej strany. Dôrazne Vám odporúčame, aby ste si prečítali Zásady ochrany osobných údajov každej stránky, ktorú navštívite.

Nemáme žiadnu kontrolu nad obsahom, zásadami ochrany osobných údajov alebo postupmi akýchkoľvek stránok alebo služieb tretích strán a nenesieme za ne žiadnu zodpovednosť.

## Zmeny týchto zásad ochrany osobných údajov

Naše Zásady ochrany osobných údajov môžeme z času na čas aktualizovať. O všetkých zmenách vás budeme informovať uverejnením nových Zásad ochrany osobných údajov na tejto stránke.

Pred nadobudnutím účinnosti zmeny vás o tom budeme informovať prostredníctvom e-mailu a/alebo výrazného oznámenia v našej službe a aktualizujeme dátum "Poslednej aktualizácie" v hornej časti týchto Zásad ochrany osobných údajov.

Odporúčame vám, aby ste pravidelne kontrolovali tieto Zásady ochrany osobných údajov, či nedošlo k nejakým zmenám. Zmeny týchto Zásad ochrany osobných údajov nadobúdajú účinnosť ich zverejnením na tejto stránke.

## Kontaktujte nás

Ak máte akékoľvek otázky týkajúce sa týchto Zásad ochrany osobných údajov, môžete nás kontaktovať:

- e-mailom: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Navštívením tejto stránky na našej webovej stránke: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
