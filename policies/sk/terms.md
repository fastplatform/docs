# Podmienky a ustanovenia

_Posledná aktualizácia 17. januára 2022_

Pred používaním našej služby si pozorne prečítajte tieto podmienky.

## Výklad a definície

### Výklad

Slová, ktorých začiatočné písmeno je veľké, majú význam definovaný za nasledujúcich podmienok. Nasledujúce definície majú rovnaký význam bez ohľadu na to, či sú uvedené v jednotnom alebo množnom čísle.

### Definície

Na účely týchto podmienok:

- **Aplikácia** znamená softvérový program poskytnutý Európskou komisiou, ktorý ste si stiahli do akéhokoľvek elektronického zariadenia s názvom FaST

- **Application Store** znamená digitálnu distribučnú službu, ktorú prevádzkuje a vyvíja spoločnosť Apple Inc. (Apple App Store) alebo Google Inc. (Google Play Store), v ktorej bola aplikácia stiahnutá.

- **Pridružená spoločnosť** znamená subjekt, ktorý kontroluje, je kontrolovaný alebo je pod spoločnou kontrolou so stranou, pričom "kontrola" znamená vlastníctvo 50 % alebo viac akcií, majetkových podielov alebo iných cenných papierov oprávnených hlasovať pri voľbe riaditeľov alebo iných riadiacich orgánov.

- **Účtom** sa rozumie jedinečný účet vytvorený pre vás na prístup k našej službe alebo jej častiam.

- Pod pojmom **Platobná agentúra CAP** sa rozumie:
    - Pre používateľov Andalúzie: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Pre používateľov z Kastílie a Leónu: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Pre používateľov z Piemontu: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Pre estónskych používateľov: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Pre používateľov z Valónska: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgique
    - Pre gréckych používateľov: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pre používateľov z Bulharska: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Pre slovenských používateľov: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Pre používateľov v Rumunsku: Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, Bucureşti, România

- **Európska komisia** (v tejto dohode uvádzaná ako "Európska komisia", "My", "Nás" alebo "Naša") sa vzťahuje na Európsku komisiu, GR DEFIS, oddelenie I3 Vesmírne údaje pre spoločenské výzvy a rast, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgicko

- **Obsah** sa vzťahuje na obsah, ako je text, obrázky alebo iné informácie, ktoré môžete zverejniť, nahrať, prepojiť alebo inak sprístupniť, bez ohľadu na formu tohto obsahu.

- **Zariadenie** znamená akékoľvek zariadenie, ktoré má prístup k službe, ako napríklad počítač, mobilný telefón alebo digitálny tablet.

- **Odozva** znamená spätnú väzbu, inovácie alebo návrhy, ktoré ste zaslali v súvislosti s vlastnosťami, výkonom alebo funkciami našej Služby.

- **Služba** sa vzťahuje na Aplikáciu.

- **Podmienky** (ďalej aj "podmienky") znamenajú tieto podmienky, ktoré tvoria úplnú dohodu medzi Vami a Európskou komisiou týkajúcu sa používania Služby.

- **Overovacia služba tretej strany** znamená akúkoľvek službu overovania používateľov poskytovanú treťou stranou (napríklad národným poskytovateľom identifikačných údajov z členského štátu Európskej únie), na ktorú Európska komisia deleguje overovanie používateľov na prístup k aplikácii.

- **Tretia strana - služba prihlásenia** znamená akékoľvek služby alebo obsah (vrátane údajov, informácií, produktov alebo služieb) poskytované treťou stranou, ktoré môžu byť zobrazené, zahrnuté alebo sprístupnené prostredníctvom Služby.

- **Vy** znamená jednotlivca, ktorý pristupuje k Službe alebo ju používa, prípadne spoločnosť alebo iný právny subjekt, v mene ktorého takýto jednotlivec pristupuje k Službe alebo ju používa.

## Potvrdenie

Toto sú podmienky, ktorými sa riadi používanie tejto Služby, a dohoda, ktorá platí medzi vami a nami. Tieto Podmienky stanovujú práva a povinnosti všetkých používateľov týkajúce sa používania Služby.

Váš prístup k Službe a jej používanie je podmienené Vaším súhlasom s týmito Podmienkami a ich dodržiavaním. Tieto Podmienky sa vzťahujú na všetkých návštevníkov, používateľov a iné osoby, ktoré pristupujú k Službe alebo ju používajú.

Prístupom k Službe alebo jej používaním súhlasíte s týmito Podmienkami. Ak nesúhlasíte s ktoroukoľvek časťou týchto Podmienok, nesmiete mať prístup k Službe.

Vyhlasujete, že ste starší ako 18 rokov. Európska komisia nepovoľuje osobám mladším ako 18 rokov používať Službu.

Váš prístup k Službe a jej používanie je tiež podmienené Vaším súhlasom so Zásadami ochrany osobných údajov Európskej komisie a ich dodržiavaním. Naše Zásady ochrany osobných údajov opisujú Naše zásady a postupy týkajúce sa zhromažďovania, používania a zverejňovania Vašich osobných údajov pri používaní Aplikácie alebo Webovej lokality a informujú Vás o Vašich právach na ochranu osobných údajov a o tom, ako Vás chráni zákon. Pred používaním Našej služby si pozorne prečítajte Naše zásady ochrany osobných údajov.

## Používateľské účty

Keď si u Nás vytvoríte účet, musíte nám poskytnúť informácie, ktoré sú vždy presné, úplné a aktuálne. Ak tak neurobíte, ide o porušenie Podmienok, ktoré môže mať za následok okamžité zrušenie Vášho účtu v Našej službe.

Ste zodpovední za ochranu hesla, ktoré používate na prístup k Službe, a za všetky činnosti alebo akcie pod Vaším heslom, bez ohľadu na to, či je Vaše heslo v Našej službe alebo v autentifikačnej službe tretej strany.
Súhlasíte s tým, že svoje heslo neposkytnete žiadnej tretej strane. Po zistení akéhokoľvek narušenia bezpečnosti alebo neoprávneného používania Vášho účtu ste povinní nás o tom okamžite informovať.

Ako používateľské meno nesmiete používať meno inej osoby alebo subjektu alebo meno, ktoré nie je legálne dostupné na používanie, meno alebo ochrannú známku, ktoré je predmetom akýchkoľvek práv inej osoby alebo subjektu než Vás bez príslušného povolenia, alebo meno, ktoré je inak urážlivé, vulgárne alebo obscénne.

## Obsah

### Vaše právo nahrávať obsah

Naša služba vám umožňuje nahrávať Obsah (napr. fotografie s geografickými značkami). Ste zodpovední za Obsah, ktorý zverejníte v Službe, vrátane jeho zákonnosti, spoľahlivosti a vhodnosti.

Vyhlasujete a zaručujete, že: (i) Obsah je váš (ste jeho vlastníkom) alebo máte právo ho používať a udeliť nám práva a licenciu podľa týchto Podmienok a (ii) zverejnenie vášho Obsahu v Službe alebo prostredníctvom nej neporušuje práva na súkromie, práva na publicitu, autorské práva, zmluvné práva alebo akékoľvek iné práva akejkoľvek osoby.

### Obmedzenia obsahu

Európska komisia nezodpovedá za obsah používateľov Služby. Výslovne chápete a súhlasíte s tým, že ste výlučne zodpovední za obsah a za všetky činnosti, ktoré sa uskutočňujú v rámci vášho účtu, či už ich vykonávate vy alebo akákoľvek tretia osoba, ktorá používa váš účet.

Nesmiete prenášať žiadny obsah, ktorý je nezákonný, urážlivý, rozrušujúci, určený na znechutenie, vyhrážanie, ohováranie, hanobenie, obscénny alebo inak nevhodný. Medzi príklady takéhoto nevhodného obsahu patria okrem iného tieto skutočnosti:

- nezákonné alebo propagujúce nezákonnú činnosť.
- hanlivý, diskriminačný alebo zlomyseľný obsah vrátane odkazov alebo komentárov týkajúcich sa náboženstva, rasy, sexuálnej orientácie, pohlavia, národnostného/etnického pôvodu alebo iných cieľových skupín.
- Spam, generovaný strojovo alebo náhodne, ktorý predstavuje neoprávnenú alebo nevyžiadanú reklamu, reťazové listy, akúkoľvek inú formu neoprávneného nabádania alebo akúkoľvek formu lotérie alebo hazardných hier.
- Obsahujúce alebo inštalujúce akékoľvek vírusy, červy, škodlivý softvér, trójske kone alebo iný obsah, ktorý je navrhnutý alebo určený na narušenie, poškodenie alebo obmedzenie fungovania akéhokoľvek softvéru, hardvéru alebo telekomunikačného zariadenia alebo na poškodenie alebo získanie neoprávneného prístupu k akýmkoľvek údajom alebo iným informáciám tretej osoby.
- Porušovanie akýchkoľvek vlastníckych práv ktorejkoľvek strany vrátane patentových práv, práv na ochrannú známku, obchodného tajomstva, autorských práv, práva na publicitu alebo iných práv.
- Vydávanie sa za akúkoľvek osobu alebo subjekt vrátane Európskej komisie a jej zamestnancov alebo zástupcov.
- Porušovanie súkromia akejkoľvek tretej osoby.
- Nepravdivé informácie a funkcie.

Európska komisia si vyhradzuje právo, nie však povinnosť, podľa vlastného uváženia určiť, či je akýkoľvek obsah vhodný a v súlade s týmito Podmienkami, odmietnuť alebo odstrániť tento obsah. Európska komisia si ďalej vyhradzuje právo na formátovanie a úpravy a zmenu spôsobu akéhokoľvek Obsahu. Európska komisia môže tiež obmedziť alebo zrušiť používanie Služby, ak zverejníte takýto nevhodný Obsah.

Keďže Európska komisia nemôže kontrolovať všetok obsah zverejnený používateľmi a/alebo tretími stranami v Službe, súhlasíte s tým, že Službu budete používať na vlastné riziko. Beriete na vedomie, že používaním služby môžete byť vystavení obsahu, ktorý môžete považovať za urážlivý, neslušný, nesprávny alebo nevhodný, a súhlasíte s tým, že Európska komisia za žiadnych okolností nenesie žiadnu zodpovednosť za akýkoľvek obsah vrátane akýchkoľvek chýb alebo opomenutí v akomkoľvek obsahu alebo za akúkoľvek stratu alebo škodu akéhokoľvek druhu, ktorá vznikne v dôsledku vášho používania akéhokoľvek obsahu.

### Zálohovanie obsahu

Hoci sa vykonáva pravidelné zálohovanie obsahu, Európska komisia nezaručuje, že nedôjde k strate alebo poškodeniu údajov.

Poškodenie alebo neplatnosť bodov zálohovania môže byť spôsobená, okrem iného, Obsahom, ktorý je poškodený pred zálohovaním alebo ktorý sa zmení počas vykonávania zálohovania.

Európska komisia poskytne podporu a pokúsi sa vyriešiť všetky známe alebo zistené problémy, ktoré môžu ovplyvniť zálohovanie Obsahu. Beriete však na vedomie, že Európska komisia nenesie žiadnu zodpovednosť súvisiacu s integritou Obsahu alebo s neúspešným obnovením Obsahu do použiteľného stavu.

Súhlasíte s tým, že budete uchovávať úplnú a presnú kópiu akéhokoľvek Obsahu na mieste nezávislom od Služby.

## Politika autorských práv

### Porušenie práv duševného vlastníctva

Rešpektujeme práva duševného vlastníctva ostatných. Našou zásadou je reagovať na akékoľvek tvrdenie, že Obsah zverejnený v Službe porušuje autorské práva alebo iné práva duševného vlastníctva akejkoľvek osoby.

Ak ste vlastníkom autorských práv alebo ste oprávnený v jeho mene a domnievate sa, že dielo chránené autorskými právami bolo skopírované spôsobom, ktorý predstavuje porušenie autorských práv, ku ktorému dochádza prostredníctvom Služby, musíte svoje oznámenie zaslať písomne na adresu nášho zástupcu pre autorské práva prostredníctvom e-mailu na adrese contact@fastplatform.eu a do oznámenia zahrnúť podrobný opis údajného porušenia.

Za uvedenie nepravdivých údajov o tom, že akýkoľvek obsah porušuje vaše autorské práva, môžete byť zodpovední za náhradu škody (vrátane nákladov a poplatkov za právne zastupovanie).

## Vaša spätná väzba pre nás

Všetky práva, vlastnícke práva a záujmy k akejkoľvek Spätnej väzbe, ktorú poskytnete Európskej komisii, postúpite. Ak je takéto postúpenie z akéhokoľvek dôvodu neúčinné, súhlasíte s tým, že Európskej komisii udelíte nevýhradné, trvalé, neodvolateľné, bezplatné, celosvetové právo a licenciu na používanie, reprodukciu, zverejňovanie, sublicenciu, distribúciu, úpravu a využívanie takejto Spätnej väzby bez obmedzenia.

## Odkazy na iné webové stránky

Naša služba môže obsahovať odkazy na webové stránky alebo služby tretích strán, ktoré nevlastní ani nekontroluje Európska komisia.

Európska komisia nemá kontrolu nad obsahom, zásadami ochrany osobných údajov alebo postupmi webových stránok alebo služieb tretích strán a nenesie za ne žiadnu zodpovednosť. Ďalej beriete na vedomie a súhlasíte s tým, že Európska komisia nenesie priamu ani nepriamu zodpovednosť za žiadne škody alebo straty spôsobené alebo údajne spôsobené používaním alebo v súvislosti s používaním alebo spoliehaním sa na akýkoľvek obsah, tovar alebo služby dostupné na takýchto webových stránkach alebo službách alebo ich prostredníctvom.

Dôrazne vám odporúčame, aby ste si prečítali podmienky a zásady ochrany osobných údajov všetkých webových stránok alebo služieb tretích strán, ktoré navštívite.

## Ukončenie

Vaše konto môžeme okamžite zrušiť alebo pozastaviť bez predchádzajúceho upozornenia alebo zodpovednosti, a to z akéhokoľvek dôvodu, okrem iného aj vtedy, ak porušíte tieto Podmienky.

Po ukončení okamžite zanikne vaše právo používať službu. Ak chcete ukončiť svoje konto, môžete jednoducho prestať používať Službu.

## Obmedzenie zodpovednosti

Bez ohľadu na akékoľvek škody, ktoré by vám mohli vzniknúť, je celá zodpovednosť Európskej komisie a všetkých jej dodávateľov podľa akéhokoľvek ustanovenia týchto Podmienok a váš výlučný prostriedok nápravy v prípade všetkých vyššie uvedených skutočností obmedzená na sumu, ktorú ste skutočne zaplatili prostredníctvom Služby, alebo 100 USD, ak ste si prostredníctvom Služby nič nekúpili.

V maximálnom rozsahu povolenom príslušnými právnymi predpismi Európska komisia ani jej dodávatelia v žiadnom prípade nenesú zodpovednosť za akékoľvek osobitné, náhodné, nepriame alebo následné škody (okrem iného vrátane škôd za stratu zisku, stratu údajov alebo iných informácií, za prerušenie činnosti, za zranenie osôb, stratou súkromia, ktoré vznikli v dôsledku alebo v akejkoľvek súvislosti s používaním alebo nemožnosťou používať službu, softvér tretích strán a/alebo hardvér tretích strán používaný so službou alebo inak v súvislosti s akýmkoľvek ustanovením týchto podmienok), a to aj v prípade, že Európska komisia alebo ktorýkoľvek dodávateľ boli upozornení na možnosť vzniku takýchto škôd, a aj v prípade, že náprava nesplní svoj základný účel.

Niektoré štáty nepovoľujú vylúčenie predpokladaných záruk alebo obmedzenie zodpovednosti za náhodné alebo následné škody, čo znamená, že niektoré z vyššie uvedených obmedzení sa nemusia uplatňovať. V týchto štátoch bude zodpovednosť každej strany obmedzená v najväčšom rozsahu povolenom zákonom.

## Vyhlásenie o odmietnutí zodpovednosti "AKO JE" a "AKO JE K DISPOZÍCII"

Služba sa vám poskytuje "AKO JE" a "AKO JE DOSTUPNÁ" a so všetkými chybami a nedostatkami bez akejkoľvek záruky. V maximálnom rozsahu povolenom platnými právnymi predpismi Európska komisia vo svojom mene a v mene svojich pridružených spoločností a svojich a ich príslušných poskytovateľov licencií a služieb výslovne odmieta všetky záruky, či už výslovné, implicitné, zákonné alebo iné, týkajúce sa Služby, vrátane všetkých implicitných záruk predajnosti, vhodnosti na konkrétny účel, vlastníckeho práva a neporušenia práv a záruk, ktoré môžu vyplývať z priebehu obchodovania, spôsobu vykonávania, zvyklostí alebo obchodných zvyklostí. Bez obmedzenia vyššie uvedeného Európska komisia neposkytuje žiadnu záruku ani záväzok a nerobí žiadne vyhlásenie akéhokoľvek druhu, že služba bude spĺňať vaše požiadavky, dosahovať zamýšľané výsledky, bude kompatibilná alebo fungovať s akýmkoľvek iným softvérom, aplikáciami, systémami alebo službami, bude fungovať bez prerušenia, bude spĺňať akékoľvek výkonnostné alebo spoľahlivé normy, bude bezchybná alebo že akékoľvek chyby alebo nedostatky môžu byť alebo budú odstránené.

Bez obmedzenia vyššie uvedeného Európska komisia ani žiadny z poskytovateľov Európskej komisie neposkytuje žiadne výslovné ani implicitné vyhlásenie alebo záruku akéhokoľvek druhu: (i) pokiaľ ide o fungovanie alebo dostupnosť služby alebo informácií, obsahu a materiálov alebo produktov v nej obsiahnutých; (ii) že služba bude nepretržitá alebo bezchybná; (iii) pokiaľ ide o presnosť, spoľahlivosť alebo aktuálnosť akýchkoľvek informácií alebo obsahu poskytovaného prostredníctvom služby; alebo (iv) že služba, jej servery, obsah alebo e-maily odoslané z Európskej komisie alebo v jej mene neobsahujú vírusy, skripty, trójske kone, červy, škodlivý softvér, časové bomby alebo iné škodlivé komponenty.

Niektoré jurisdikcie neumožňujú vylúčenie určitých typov záruk alebo obmedzenie platných zákonných práv spotrebiteľa, takže niektoré alebo všetky vyššie uvedené vylúčenia a obmedzenia sa na vás nemusia vzťahovať. V takom prípade sa však vylúčenia a obmedzenia uvedené v tejto časti uplatnia v najväčšom rozsahu, ktorý je vymožiteľný podľa platných právnych predpisov.

## Rozhodné právo

Tieto Podmienky a vaše používanie Služby sa riadia právom Európskej únie, v prípade potreby doplneným belgickým právom. Vaše používanie aplikácie môže podliehať aj iným miestnym, štátnym, národným alebo medzinárodným zákonom.

## Riešenie sporov

Ak máte akékoľvek obavy alebo spor týkajúci sa Služby, súhlasíte s tým, že sa spor najskôr pokúsite vyriešiť neformálne kontaktovaním Európskej komisie.

## Pre používateľov z Európskej únie (EÚ)

Ak ste spotrebiteľom Európskej únie, budú sa na vás vzťahovať všetky záväzné ustanovenia právnych predpisov krajiny, v ktorej máte bydlisko.

## Oddeliteľnosť a zrieknutie sa práv

### Oddeliteľnosť

Ak sa niektoré ustanovenie týchto Podmienok považuje za nevymáhateľné alebo neplatné, takéto ustanovenie sa zmení a vyloží tak, aby sa dosiahli ciele takéhoto ustanovenia v čo najväčšom možnom rozsahu podľa platných právnych predpisov a ostatné ustanovenia zostanú v plnej platnosti a účinnosti.

### Zrieknutie sa práva

S výnimkou prípadov uvedených v tomto dokumente, neuplatnenie práva alebo nevyžadovanie plnenia záväzku podľa týchto Podmienok nemá vplyv na možnosť strany uplatniť takéto právo alebo vyžadovať takéto plnenie kedykoľvek neskôr, ani vzdanie sa práva na porušenie nepredstavuje vzdanie sa práva na akékoľvek následné porušenie.

## Preklad Výklad

Tieto Podmienky môžu byť preložené, ak sme vám ich sprístupnili v našej Službe.

Súhlasíte s tým, že v prípade sporu má prednosť pôvodný anglický text.

## Zmeny týchto Podmienok

Vyhradzujeme si právo podľa vlastného uváženia tieto Podmienky kedykoľvek upraviť alebo nahradiť. Ak je zmena podstatná, vynaložíme primerané úsilie, aby sme poskytli oznámenie aspoň 30 dní pred nadobudnutím účinnosti akýchkoľvek nových podmienok. O tom, čo predstavuje podstatnú zmenu, rozhodujeme podľa vlastného uváženia.

Pokračovaním v prístupe k Našej službe alebo jej používaním po nadobudnutí účinnosti týchto zmien súhlasíte s tým, že sa budete riadiť revidovanými podmienkami. Ak s novými podmienkami nesúhlasíte, či už úplne alebo čiastočne, prestaňte používať webové stránky a službu.

## Kontaktujte nás

Ak máte akékoľvek otázky týkajúce sa týchto zmluvných podmienok, môžete nás kontaktovať:

- e-mailom: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Navštívením tejto stránky na našej webovej stránke: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
