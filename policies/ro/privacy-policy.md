# Politica de confidențialitate

_Ultima actualizare la 17 ianuarie 2022_

Această Politică de confidențialitate descrie politicile și procedurile Noastre privind colectarea, utilizarea și dezvăluirea informațiilor Dumneavoastră atunci când utilizați Serviciul și vă informează cu privire la drepturile Dumneavoastră la confidențialitate și la modul în care vă protejează legea.

Noi folosim datele Dumneavoastră personale pentru a furniza și îmbunătăți Serviciul. Prin utilizarea Serviciului, sunteți de acord cu colectarea și utilizarea informațiilor în conformitate cu această Politică de confidențialitate.

## Interpretare și definiții

### Interpretare

Cuvintele a căror literă inițială este scrisă cu majusculă au înțelesuri definite în următoarele condiții. Următoarele definiții vor avea același înțeles indiferent dacă apar la singular sau la plural.

### Definiții

În sensul prezentei Politici de confidențialitate:

- **Cont** înseamnă un cont unic creat pentru Dumneavoastră pentru a accesa Serviciul nostru sau părți ale Serviciului nostru.

- **Afiliat** înseamnă o entitate care controlează, este controlată de sau se află sub control comun cu o parte, unde "control" înseamnă deținerea a 50% sau mai mult din acțiunile, participațiile sau alte titluri de valoare cu drept de vot pentru alegerea directorilor sau a altor autorități de conducere.

- **Aplicația** înseamnă programul software furnizat de Comisia Europeană descărcat de dumneavoastră pe orice dispozitiv electronic, denumit FaST

- **Comisia Europeană** (denumită fie "Comisia Europeană", "Noi", "Ne" sau "Al nostru" în prezentul acord) se referă la Comisia Europeană, DG DEFIS, Unitatea I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgia

- **GDPR** este Regulamentul general privind protecția datelor

- **CAP Paying Agency** se referă la:
    - Pentru utilizatorii din Andalucia: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España.
    - Pentru utilizatorii din Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, Spania.
    - Pentru utilizatorii din Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia.
    - Pentru utilizatorii din Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti.
    - Pentru utilizatorii din Valonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgia.
    - Pentru utilizatorii din Grecia: ΟΠΕΚΕΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pentru utilizatorii din Bulgaria: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Pentru utilizatorii din Slovacia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Pentru utilizatorii din România: Agenția de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România

- **Controlerul de date** (denumit fie "Noi", fie "Noi" sau "Al nostru" în prezentul acord), în sensul RGPD, se referă la Comisia Europeană, în calitate de entitate juridică care stabilește scopurile și mijloacele de prelucrare a datelor cu caracter personal.

- **Dispozitiv** înseamnă orice dispozitiv care poate accesa Serviciul, cum ar fi un computer, un telefon mobil sau o tabletă digitală.

- **Date personale** reprezintă orice informație care se referă la o persoană fizică identificată sau identificabilă. În sensul GDPR, Datele cu caracter personal înseamnă orice informație referitoare la Dumneavoastră, cum ar fi un nume, un număr de identificare, date de localizare, un identificator online sau la unul sau mai mulți factori specifici identității fizice, fiziologice, genetice, mentale, economice, culturale sau sociale.

- **Serviciul** se referă la Aplicație.

- **Furnizor de servicii** înseamnă orice persoană fizică sau juridică care prelucrează datele în numele Comisiei Europene. Se referă la companii terțe sau persoane fizice angajate de Comisia Europeană sau de Agenția de Plăți pentru a facilita Serviciul, pentru a furniza Serviciul în numele Comisiei Europene, pentru a efectua servicii legate de Serviciu sau pentru a asista Comisia Europeană și Agenția de Plăți în analiza modului în care este utilizat Serviciul. În sensul GDPR, furnizorii de servicii sunt considerați operatori de date.

- **Serviciul de autentificare al unei terțe părți** se referă la orice serviciu prin intermediul căruia un Utilizator se poate autentifica sau își poate crea un cont pentru a utiliza Serviciul.

- **Serviciu de înscriere terță parte** se referă la orice serviciu extern care a fost înregistrat în Serviciu și la care Dumneavoastră puteți opta.

- **Metricele de utilizare** se referă la datele colectate automat, fie generate de utilizarea Serviciului, fie de infrastructura Serviciului în sine (de exemplu, durata vizitării unei pagini).

- **Voi** se referă la persoana fizică care accesează sau utilizează Serviciul, sau la compania sau altă entitate juridică în numele căreia o astfel de persoană fizică accesează sau utilizează Serviciul, după caz. În conformitate cu GDPR (Regulamentul general privind protecția datelor), Dumneavoastră puteți fi denumit Persoana vizată sau Utilizatorul, deoarece sunteți persoana fizică care utilizează Serviciul.

## Colectarea și utilizarea datelor dumneavoastră cu caracter personal

### Tipuri de date colectate

#### Date cu caracter personal

În timp ce utilizați Serviciul nostru, vă putem solicita să ne furnizați anumite informații personale care pot fi utilizate pentru a vă contacta sau identifica. Informațiile personale identificabile pot include, dar nu se limitează la:

- Adresa de e-mail
- Numele și prenumele
- Numărul de telefon
- Adresă, stat, provincie, cod poștal/cod poștal, oraș
- Mesajele și atașamentele pe care le schimbați cu alți utilizatori (inclusiv cu agenția dumneavoastră de plată PAC)
- Fotografii cu etichetă geografică pe care le schimbați pe platformă (inclusiv cu agenția dumneavoastră de plată CAP)
- Descrierea fermei dvs. (de exemplu, parcele, culturi), practicile agricole (de exemplu, recomandările de fertilizare pe care le primiți), datele privind solul pe care le introduceți în serviciu
- Parametrii de utilizare (a se vedea mai jos)

#### Metrici de utilizare

Usage Metrics sunt colectate automat atunci când utilizați Serviciul.

Metricele de utilizare pot include informații precum adresa de protocol internet a dispozitivului dumneavoastră (de exemplu, adresa IP), tipul de browser, versiunea browserului, paginile Serviciului nostru pe care le vizitați, data și ora vizitei dumneavoastră, timpul petrecut pe acele pagini, identificatorii unici ai dispozitivului și alte date de diagnosticare.

Atunci când accesați Serviciul de către sau prin intermediul unui dispozitiv mobil, este posibil să colectăm anumite informații în mod automat, inclusiv, dar fără a se limita la, tipul de dispozitiv mobil pe care îl utilizați, ID-ul unic al dispozitivului mobil, adresa IP a dispozitivului mobil, sistemul de operare mobil, tipul de browser de internet mobil pe care îl utilizați, identificatorii unici ai dispozitivului și alte date de diagnosticare.

De asemenea, este posibil să colectăm informații pe care browserul dvs. le trimite ori de câte ori vizitați Serviciul nostru sau când accesați Serviciul de către sau prin intermediul unui dispozitiv mobil.

#### Informații colectate în timpul utilizării aplicației

În timpul utilizării Aplicației noastre, putem colecta, cu permisiunea dvs. prealabilă:

- Informații referitoare la locația dvs.
- Imagini și alte informații din aparatul foto și din biblioteca foto a Dispozitivului dvs.

Utilizăm aceste informații pentru a oferi funcția de fotografii geoetichetate a Serviciului nostru și pentru a vă localiza pe hărțile noastre prin satelit. Aceste informații pot fi încărcate pe serverele Noastre și/sau pe serverul unui Furnizor de servicii sau pot fi pur și simplu stocate pe dispozitivul Dumneavoastră.

Puteți activa sau dezactiva accesul la aceste informații în orice moment, prin setările Dispozitivului Dumneavoastră.

### Utilizarea datelor dvs. personale

Serviciul nostru se află în prezent în faza beta și, prin urmare, Datele dvs. personale vor fi utilizate doar pentru **dezvoltarea, îmbunătățirea și testarea Serviciului nostru**.

În timpul utilizării Serviciului, vă putem contacta, prin e-mail, apeluri telefonice, SMS sau alte forme echivalente de comunicare electronică, cum ar fi notificările push ale unei aplicații mobile, cu privire la actualizări sau comunicări informative legate de funcționalități, îmbunătățiri sau noutăți despre Serviciile noastre, inclusiv actualizările de securitate, atunci când este necesar sau rezonabil pentru implementarea acestora. De asemenea, vom stoca Datele dumneavoastră cu caracter personal pentru a participa și a gestiona solicitările pe care ni le adresați.

### Retenția datelor dumneavoastră cu caracter personal

Vom păstra Datele Dumneavoastră cu caracter personal doar atât timp cât este necesar pentru scopurile stabilite în această Politică de confidențialitate. Vom păstra și utiliza Datele Dumneavoastră cu caracter personal în măsura în care este necesar pentru a respecta obligațiile noastre legale (de exemplu, dacă suntem obligați să păstrăm datele Dumneavoastră pentru a respecta legile aplicabile), pentru a rezolva litigiile și pentru a aplica acordurile și politicile noastre legale.

De asemenea, vom reține Datele de măsurare a utilizării în scopuri de analiză internă. Datele de măsurare a utilizării sunt, în general, păstrate pentru o perioadă mai scurtă de timp, cu excepția cazului în care aceste date sunt utilizate pentru a consolida securitatea sau pentru a îmbunătăți funcționalitatea Serviciului nostru, sau suntem obligați din punct de vedere legal să păstrăm aceste date pentru perioade mai lungi de timp.

### Transferul datelor dvs. personale

Informațiile dumneavoastră, inclusiv Datele cu caracter personal, sunt prelucrate la sediile noastre operaționale și în orice alte locuri în care se află părțile implicate în procesare. Aceasta înseamnă că aceste informații pot fi transferate către - și păstrate pe - computere situate în afara statului, provinciei, țării Dumneavoastră sau a altei jurisdicții guvernamentale în care legile privind protecția datelor pot fi diferite de cele din jurisdicția Dumneavoastră.

Consimțământul Dumneavoastră cu privire la această Politică de confidențialitate, urmat de transmiterea de către Dumneavoastră a acestor informații, reprezintă acordul Dumneavoastră cu privire la acest transfer.

Vom lua toate măsurile necesare în mod rezonabil pentru a ne asigura că datele Dumneavoastră sunt tratate în siguranță și în conformitate cu această Politică de confidențialitate și niciun transfer al datelor Dumneavoastră personale nu va avea loc către o organizație sau o țară dacă nu există controale adecvate, inclusiv securitatea datelor Dumneavoastră și a altor informații personale.

### Dezvăluirea datelor dumneavoastră cu caracter personal

#### Aplicarea legii

În anumite circumstanțe, este posibil ca Noi să fim obligați să dezvăluim Datele Dumneavoastră cu caracter personal dacă acest lucru este cerut de lege sau ca răspuns la solicitări valabile din partea autorităților publice (de exemplu, o instanță sau o agenție guvernamentală).

#### Alte cerințe legale

Putem dezvălui Datele dvs. personale în cazul în care credem cu bună credință că o astfel de acțiune este necesară pentru:

- Respectarea unei obligații legale
- Protejați și apărați drepturile sau proprietatea Comisiei Europene
- Prevenirea sau investigarea unor posibile fapte ilegale în legătură cu Serviciul
- Protejarea siguranței personale a Utilizatorilor Serviciului sau a publicului
- protejarea împotriva răspunderii juridice

### Securitatea datelor dumneavoastră personale

Securitatea Datelor Dumneavoastră personale este importantă pentru Noi, dar nu uitați că nicio metodă de transmitere pe Internet sau metodă de stocare electronică nu este 100% sigură. Deși ne străduim să folosim mijloace acceptabile din punct de vedere comercial pentru a proteja Datele Dumneavoastră personale, nu putem garanta securitatea absolută a acestora.

## Confidențialitate GDPR

### Temeiul juridic pentru prelucrarea datelor cu caracter personal în conformitate cu GDPR

Putem prelucra Datele cu caracter personal în următoarele condiții:

- **Consimțământ:** V-ați dat consimțământul pentru prelucrarea Datelor cu caracter personal pentru unul sau mai multe scopuri specifice.

- **Executarea unui contract: ** Furnizarea Datelor cu caracter personal este necesară pentru executarea unui acord cu Dumneavoastră și/sau pentru orice obligații precontractuale ale acestuia.

- **Obligații legale: ** Prelucrarea Datelor cu caracter personal este necesară pentru respectarea unei obligații legale la care este supusă Comisia Europeană.

- **interese vitale: ** Prelucrarea Datelor cu caracter personal este necesară pentru a vă proteja interesele vitale ale Dumneavoastră sau ale unei alte persoane fizice.

- **Interes public:** Prelucrarea Datelor cu caracter personal este legată de o sarcină care este îndeplinită în interes public sau în exercitarea autorității oficiale cu care este învestită Comisia Europeană.

- **interese legitime: ** Prelucrarea Datelor cu caracter personal este necesară în scopul intereselor legitime urmărite de Comisia Europeană.

În orice caz, vă vom ajuta cu plăcere să clarificăm temeiul juridic specific care se aplică prelucrării și, în special, dacă furnizarea de Date cu caracter personal este o cerință legală sau contractuală, sau o cerință necesară pentru încheierea unui contract.

### Drepturile dumneavoastră în temeiul GDPR

Comisia Europeană se angajează să respecte confidențialitatea datelor dvs. personale și să vă garanteze că vă puteți exercita drepturile.

Aveți dreptul, în conformitate cu această Politică de confidențialitate și cu legislația UE, să:

- **Solicitați accesul la Datele Dumneavoastră cu caracter personal.** Dreptul de a accesa, actualiza sau șterge informațiile pe care le avem despre Dumneavoastră. Ori de câte ori este posibil, puteți accesa, actualiza sau solicita ștergerea Datelor Dumneavoastră cu caracter personal direct în secțiunea de setări a contului Dumneavoastră. Dacă nu sunteți în măsură să efectuați aceste acțiuni singuri, vă rugăm să ne contactați pentru a vă ajuta. Acest lucru vă permite, de asemenea, să primiți o copie a datelor personale pe care le deținem despre dumneavoastră.

- **Solicitați corectarea Datelor cu caracter personal pe care le deținem despre Dumneavoastră.** Aveți dreptul de a obține corectarea oricăror informații incomplete sau inexacte pe care le deținem despre Dumneavoastră.

- **Opuneți-vă prelucrării Datelor Dumneavoastră cu caracter personal.** Acest drept există în cazul în care ne bazăm pe un interes legitim ca temei juridic pentru prelucrarea noastră și există ceva în situația Dumneavoastră particulară, care vă face să doriți să vă opuneți prelucrării de către noi a Datelor Dumneavoastră cu caracter personal pe acest motiv. Aveți, de asemenea, dreptul de a obiecta în cazul în care prelucrăm Datele Dumneavoastră cu caracter personal în scopuri de marketing direct.

- **Solicitarea ștergerii Datelor Dumneavoastră cu caracter personal.** Aveți dreptul de a ne cere să ștergem sau să eliminăm Datele Dumneavoastră cu caracter personal atunci când nu există niciun motiv întemeiat pentru ca Noi să continuăm să le prelucrăm.

- **Solicitați transferul Datelor Dumneavoastră cu caracter personal.** Vă vom furniza Dumneavoastră, sau unei terțe părți pe care ați ales-o, Datele Dumneavoastră cu caracter personal într-un format structurat, utilizat în mod obișnuit și care poate fi citit automat. Vă rugăm să rețineți că acest drept se aplică numai informațiilor automatizate pentru care v-ați dat inițial consimțământul ca Noi să le folosim sau în cazul în care am folosit informațiile pentru a executa un contract cu Dumneavoastră.

- **Retragerea consimțământului Dumneavoastră.** Aveți dreptul de a vă retrage consimțământul privind utilizarea datelor dumneavoastră personale. Dacă vă retrageți consimțământul, este posibil să nu vă putem oferi acces la anumite funcționalități specifice ale Serviciului.

### Exercitarea drepturilor Dumneavoastră privind protecția datelor în conformitate cu GDPR

Vă puteți exercita drepturile Dumneavoastră de acces, rectificare, anulare și opoziție contactându-ne. Vă rugăm să rețineți că este posibil să Vă cerem să Vă verificați identitatea înainte de a răspunde la astfel de solicitări. Dacă faceți o solicitare, vom face tot posibilul să vă răspundem cât mai curând posibil.

Aveți dreptul de a depune o plângere la o autoritate de protecție a datelor cu privire la colectarea și utilizarea de către Noi a datelor dumneavoastră personale. Pentru mai multe informații, vă rugăm să contactați autoritatea Dumneavoastră locală de protecție a datelor din Spațiul Economic European.

## Legături către alte site-uri web și servicii

Serviciul nostru poate conține linkuri către alte site-uri web sau servicii care nu sunt operate de Noi. Dacă faceți clic pe un link al unei terțe părți, veți fi direcționat către site-ul acelei terțe părți. Vă sfătuim cu tărie să consultați Politica de confidențialitate a fiecărui site pe care îl vizitați.

Nu avem niciun control asupra și nu ne asumăm nicio responsabilitate pentru conținutul, politicile de confidențialitate sau practicile site-urilor sau serviciilor unor terțe părți.

## Modificări ale acestei Politici de confidențialitate

Putem actualiza Politica noastră de confidențialitate din când în când. Vă vom informa cu privire la orice modificări prin publicarea noii Politici de confidențialitate pe această pagină.

Vă vom anunța prin e-mail și/sau printr-o notificare proeminentă pe Serviciul nostru, înainte ca modificarea să intre în vigoare și vom actualiza data "Ultima actualizare" din partea de sus a acestei Politici de confidențialitate.

Vă sfătuim să revizuiți periodic această Politică de confidențialitate pentru a lua cunoștință de eventualele modificări. Modificările la această Politică de confidențialitate intră în vigoare în momentul în care sunt publicate pe această pagină.

## Contactați-ne

Dacă aveți întrebări cu privire la această Politică de confidențialitate, ne puteți contacta:

- Prin e-mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>.
- Vizitând această pagină de pe site-ul nostru: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
