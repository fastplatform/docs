# Termeni și condiții

_Ultima actualizare la 17 ianuarie 2022_

Vă rugăm să citiți cu atenție acești termeni și condiții înainte de a utiliza Serviciul nostru.

## Interpretare și definiții

### Interpretare

Cuvintele a căror literă inițială este scrisă cu majusculă au înțelesuri definite în următoarele condiții. Următoarele definiții au același înțeles, indiferent dacă apar la singular sau la plural.

### Definiții

În sensul prezentelor Termeni și condiții:

- **Aplicația** înseamnă programul software furnizat de Comisia Europeană descărcat de dumneavoastră pe orice dispozitiv electronic, denumit FaST

- **Application Store** înseamnă serviciul de distribuție digitală operat și dezvoltat de Apple Inc. (Apple App Store) sau Google Inc. (Google Play Store) în care a fost descărcată aplicația.

- **Afiliat** înseamnă o entitate care controlează, este controlată de sau se află sub control comun cu o parte, unde "control" înseamnă deținerea a 50% sau mai mult din acțiunile, participațiile sau alte titluri de valoare cu drept de vot pentru alegerea directorilor sau a altor autorități de conducere.

- **Cont** înseamnă un cont unic creat pentru Dumneavoastră pentru a accesa Serviciul nostru sau părți ale Serviciului nostru.

- **Agenția de plată CAP** se referă la:
    - Pentru utilizatorii Andalucia: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España.
    - Pentru utilizatorii din Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, Spania.
    - Pentru utilizatorii din Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia.
    - Pentru utilizatorii din Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti.
    - Pentru utilizatorii din Valonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgia.
    - Pentru utilizatorii din Grecia: ΟΠΕΚΕΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pentru utilizatorii din Bulgaria: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Pentru utilizatorii din Slovacia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Pentru utilizatorii din România: Agenția de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România

- **Comisia Europeană** (denumită fie "Comisia Europeană", "Noi", "Ne" sau "Al nostru" în prezentul acord) se referă la Comisia Europeană, DG DEFIS, Unitatea I3 Date spațiale pentru provocări societale și creștere, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgia.

- **Contenit** se referă la conținut, cum ar fi text, imagini sau alte informații care pot fi postate, încărcate, legate sau puse la dispoziție în alt mod de către dumneavoastră, indiferent de forma conținutului respectiv.

- **Dispozitiv** se referă la orice dispozitiv care poate accesa Serviciul, cum ar fi un computer, un telefon mobil sau o tabletă digitală.

- **Feedback** înseamnă feedback, inovații sau sugestii trimise de Dumneavoastră cu privire la atributele, performanța sau caracteristicile Serviciului nostru.

- **Serviciul** se referă la Aplicație.

- **Termeni și condiții** (denumite și "Termeni") înseamnă acești Termeni și condiții care constituie întregul acord între Dumneavoastră și Comisia Europeană cu privire la utilizarea Serviciului.

- **Serviciu de autentificare terță parte** înseamnă orice serviciu de autentificare a utilizatorilor furnizat de o terță parte (cum ar fi un furnizor național de identitate dintr-un stat membru al Uniunii Europene) căruia Comisia Europeană îi deleagă autentificarea utilizatorilor pentru a accesa Aplicația.

- **Serviciu de oprire a părților terțe** înseamnă orice servicii sau conținut (inclusiv date, informații, produse sau servicii) furnizate de o terță parte care pot fi afișate, incluse sau puse la dispoziție prin intermediul Serviciului.

- **Voi** înseamnă persoana fizică care accesează sau utilizează Serviciul, sau compania sau altă entitate juridică în numele căreia o astfel de persoană fizică accesează sau utilizează Serviciul, după caz.

## Recunoaștere

Aceștia sunt Termenii și condițiile care guvernează utilizarea acestui Serviciu și acordul care operează între Dumneavoastră și Noi. Acești Termeni și Condiții stabilesc drepturile și obligațiile tuturor utilizatorilor în ceea ce privește utilizarea Serviciului.

Accesul și utilizarea Serviciului de către Dumneavoastră sunt condiționate de acceptarea și respectarea de către Dumneavoastră a acestor Termeni și Condiții. Acești Termeni și Condiții se aplică tuturor vizitatorilor, utilizatorilor și altor persoane care accesează sau utilizează Serviciul.

Prin accesarea sau utilizarea Serviciului, sunteți de acord să vă supuneți acestor Termeni și Condiții. Dacă nu sunteți de acord cu orice parte a acestor Termeni și Condiții, atunci nu puteți accesa Serviciul.

Declarați că aveți peste 18 ani. Comisia Europeană nu permite persoanelor sub 18 ani să utilizeze Serviciul.

Accesul și utilizarea Serviciului sunt condiționate, de asemenea, de acceptarea și respectarea de către Dumneavoastră a Politicii de confidențialitate a Comisiei Europene. Politica noastră de confidențialitate descrie politicile și procedurile noastre privind colectarea, utilizarea și dezvăluirea informațiilor dvs. personale atunci când utilizați Aplicația sau Site-ul web și vă informează cu privire la drepturile dvs. de confidențialitate și la modul în care legea vă protejează. Vă rugăm să citiți cu atenție Politica noastră de confidențialitate înainte de a utiliza Serviciul nostru.

## Conturi de utilizator

Atunci când creați un cont la Noi, trebuie să ne furnizați informații care să fie corecte, complete și actuale în orice moment. Nerespectarea acestui lucru constituie o încălcare a Termenilor, ceea ce poate duce la închiderea imediată a contului Dumneavoastră pe Serviciul Nostru.

Sunteți responsabil pentru protejarea parolei pe care o folosiți pentru a accesa Serviciul și pentru orice activități sau acțiuni efectuate cu parola Dumneavoastră, indiferent dacă parola Dumneavoastră este cu Serviciul Nostru sau cu un Serviciu de autentificare al unei terțe părți.
Sunteți de acord să nu dezvăluiți parola Dumneavoastră niciunei terțe părți. Trebuie să ne anunțați imediat ce luați cunoștință de orice încălcare a securității sau de utilizarea neautorizată a contului Dumneavoastră.

Nu puteți utiliza ca nume de utilizator numele unei alte persoane sau entități sau care nu este disponibil în mod legal pentru utilizare, un nume sau o marcă comercială care face obiectul unor drepturi ale unei alte persoane sau entități decât Dumneavoastră, fără autorizația corespunzătoare, sau un nume care este în alt mod ofensator, vulgar sau obscen.

## Conținut

### Dreptul dumneavoastră de a încărca conținut

Serviciul nostru vă permite să încărcați Conținut (de exemplu, fotografii geoetichetate). Sunteți responsabil pentru Conținutul pe care îl postați pe Serviciu, inclusiv legalitatea, fiabilitatea și caracterul adecvat al acestuia.

Dumneavoastră declarați și garantați că: (i) Conținutul este al Dumneavoastră (vă aparține) sau aveți dreptul de a-l utiliza și de a ne acorda drepturile și licența prevăzute în acești Termeni și (ii) postarea Conținutului Dumneavoastră pe sau prin intermediul Serviciului nu încalcă drepturile de confidențialitate, drepturile de publicitate, drepturile de autor, drepturile contractuale sau orice alte drepturi ale oricărei persoane.

### Restricții privind conținutul

Comisia Europeană nu este responsabilă pentru conținutul utilizatorilor Serviciului. Înțelegeți și sunteți de acord în mod expres că sunteți singurul responsabil pentru Conținut și pentru toate activitățile care au loc în contul dumneavoastră, indiferent dacă acestea sunt realizate de dumneavoastră sau de orice terță persoană care utilizează contul dumneavoastră.

Nu aveți voie să transmiteți niciun Conținut care este ilegal, ofensator, supărător, supărător, destinat să dezguste, amenințător, calomnios, defăimător, obscen sau în orice alt mod inacceptabil. Exemple de astfel de Conținut reprobabil includ, dar nu se limitează la următoarele:

- Ilegalitate sau promovarea unei activități ilegale.
- Conținut defăimător, discriminatoriu sau răutăcios, inclusiv referințe sau comentarii despre religie, rasă, orientare sexuală, sex, origine națională/etnică sau alte grupuri vizate.
- Spam, generat automat sau aleatoriu, care constituie publicitate neautorizată sau nesolicitată, scrisori în lanț, orice altă formă de solicitare neautorizată sau orice formă de loterie sau jocuri de noroc.
- Conține sau instalează viruși, viermi, programe malware, cai troieni sau alte tipuri de conținut conceput sau destinat să întrerupă, să deterioreze sau să limiteze funcționarea oricărui software, hardware sau echipament de telecomunicații sau să deterioreze sau să obțină acces neautorizat la orice date sau alte informații ale unei terțe persoane.
- Încălcarea oricăror drepturi de proprietate ale oricărei părți, inclusiv a drepturilor de brevet, mărci comerciale, secrete comerciale, drepturi de autor, drepturi de publicitate sau alte drepturi.
- Impunerea identității oricărei persoane sau entități, inclusiv a Comisiei Europene și a angajaților sau reprezentanților acesteia.
- Încălcarea vieții private a oricărei terțe persoane.
- Informații și caracteristici false.

Comisia Europeană își rezervă dreptul, dar nu și obligația, de a determina, la propria discreție, dacă un conținut este sau nu adecvat și dacă respectă acești Termeni, de a refuza sau de a elimina acest conținut. Comisia Europeană își rezervă, de asemenea, dreptul de a face formatarea și editarea și de a schimba modul în care orice Conținut. Comisia Europeană poate, de asemenea, să limiteze sau să revoce utilizarea Serviciului în cazul în care publicați un astfel de Conținut inacceptabil.

Deoarece Comisia Europeană nu poate controla tot conținutul postat de utilizatori și/sau de terți pe Serviciu, sunteți de acord să utilizați Serviciul pe propriul risc. Înțelegeți că, prin utilizarea Serviciului, este posibil să fiți expus la conținut pe care îl puteți considera ofensator, indecent, incorect sau reprobabil și sunteți de acord că, în niciun caz, Comisia Europeană nu va fi răspunzătoare în niciun fel pentru orice conținut, inclusiv pentru orice erori sau omisiuni în orice conținut, sau pentru orice pierdere sau daună de orice fel survenită ca urmare a utilizării de către dvs. a oricărui conținut.

### Copii de siguranță ale conținutului

Deși se efectuează în mod regulat copii de siguranță ale conținutului, Comisia Europeană nu garantează că nu vor exista pierderi sau corupții de date.

Punctele de backup corupte sau invalide pot fi cauzate, fără a se limita la acestea, de Conținutul care este corupt înainte de a fi salvat sau care se modifică în timpul efectuării unui backup.

Comisia Europeană va oferi asistență și va încerca să rezolve orice probleme cunoscute sau descoperite care pot afecta copiile de rezervă ale conținutului. Dar recunoașteți că Comisia Europeană nu are nicio responsabilitate legată de integritatea conținutului sau de eșecul de a restaura cu succes conținutul într-o stare utilizabilă.

Sunteți de acord să păstrați o copie completă și exactă a oricărui conținut într-o locație independentă de Serviciu.

## Politica privind drepturile de autor

### Încălcarea proprietății intelectuale

Respectăm drepturile de proprietate intelectuală ale altora. Este politica Noastră să răspundem la orice reclamație conform căreia Conținutul postat pe Serviciu încalcă drepturile de autor sau alte încălcări ale proprietății intelectuale ale oricărei persoane.

Dacă sunteți un proprietar al drepturilor de autor sau sunteți autorizat în numele unuia dintre aceștia și credeți că lucrarea protejată de drepturi de autor a fost copiată într-un mod care constituie o încălcare a drepturilor de autor care are loc prin intermediul Serviciului, trebuie să trimiteți o notificare în scris în atenția agentului nostru de drepturi de autor prin e-mail la contact@fastplatform.eu și să includeți în notificarea dumneavoastră o descriere detaliată a presupusei încălcări.

Puteți fi tras la răspundere pentru daune (inclusiv costuri și onorarii de avocați) pentru că ați declarat în mod eronat că orice Conținut încalcă drepturile dumneavoastră de autor.

## Feedback-ul dumneavoastră către noi

Cedați Comisiei Europene toate drepturile, titlurile și interesele legate de orice Feedback pe care îl furnizați. Dacă, din orice motiv, o astfel de cesiune este ineficientă, sunteți de acord să acordați Comisiei Europene un drept și o licență neexclusivă, perpetuă, irevocabilă, fără redevențe, la nivel mondial, de a utiliza, reproduce, dezvălui, sublicenția, distribui, modifica și exploata aceste Feedback-uri fără restricții.

## Legături către alte site-uri web

Serviciul nostru poate conține linkuri către site-uri web sau servicii ale unor terțe părți care nu sunt deținute sau controlate de Comisia Europeană.

Comisia Europeană nu are niciun control asupra conținutului, politicilor de confidențialitate sau practicilor oricăror site-uri web sau servicii ale terților și nu își asumă nicio responsabilitate pentru acestea. De asemenea, recunoașteți și sunteți de acord că Comisia Europeană nu este responsabilă sau răspunzătoare, direct sau indirect, pentru niciun prejudiciu sau pierdere cauzată sau presupusă a fi cauzată de sau în legătură cu utilizarea sau încrederea în conținutul, bunurile sau serviciile disponibile pe sau prin intermediul unor astfel de site-uri sau servicii web.

Vă sfătuim cu tărie să citiți termenii și condițiile și politicile de confidențialitate ale oricăror site-uri web sau servicii ale terților pe care le vizitați.

## Încetare

Putem rezilia sau suspenda Contul Dumneavoastră imediat, fără notificare prealabilă sau răspundere, din orice motiv, inclusiv, dar fără limitare, dacă încălcați acești Termeni și Condiții.

La reziliere, dreptul Dumneavoastră de a utiliza Serviciul va înceta imediat. Dacă doriți să reziliați Contul Dumneavoastră, puteți pur și simplu să încetați să mai utilizați Serviciul.

## Limitarea răspunderii

Fără a aduce atingere daunelor pe care le-ați putea suferi, întreaga răspundere a Comisiei Europene și a oricăruia dintre furnizorii săi în temeiul oricărei prevederi a acestor Termeni și remediul dumneavoastră exclusiv pentru toate cele menționate mai sus se limitează la suma plătită efectiv de dumneavoastră prin intermediul Serviciului sau la 100 USD dacă nu ați cumpărat nimic prin intermediul Serviciului.

În măsura maximă permisă de legislația aplicabilă, în niciun caz Comisia Europeană sau furnizorii săi nu vor fi răspunzători pentru daune speciale, accidentale, indirecte sau de consecință de orice fel (inclusiv, dar fără a se limita la daune pentru pierderi de profit, pierderi de date sau alte informații, pentru întreruperea activității, pentru vătămări corporale, pierderea vieții private care rezultă din sau sunt legate în vreun fel de utilizarea sau incapacitatea de a utiliza serviciul, software-ul și/sau hardware-ul terților utilizat cu serviciul sau în legătură cu orice dispoziție a acestor condiții), chiar dacă Comisia Europeană sau orice furnizor a fost informat de posibilitatea unor astfel de daune și chiar dacă soluția nu își atinge scopul esențial.

Unele state nu permit excluderea garanțiilor implicite sau limitarea răspunderii pentru daune accidentale sau indirecte, ceea ce înseamnă că unele dintre limitările de mai sus pot să nu se aplice. În aceste state, răspunderea fiecărei părți va fi limitată în cea mai mare măsură permisă de lege.

## Renunțare de răspundere "AS IS" și "AS AVAILABLE"

Serviciul vă este furnizat "AS IS" și "AS AVAILABLE" și cu toate greșelile și defectele, fără niciun fel de garanție. În măsura maximă permisă de legea aplicabilă, Comisia Europeană, în numele său și în numele afiliaților săi și al licențiatorilor și furnizorilor de servicii ai acesteia și ai acestora, renunță în mod expres la toate garanțiile, fie că sunt exprese, implicite, legale sau de altă natură, cu privire la Serviciu, inclusiv toate garanțiile implicite de vandabilitate, de adecvare la un anumit scop, de titlu și de neîncălcare, precum și garanțiile care pot apărea în urma unei tranzacții, a unei performanțe, a unei utilizări sau a unei practici comerciale. Fără a se limita la cele de mai sus, Comisia Europeană nu oferă nicio garanție sau angajament și nu face nicio declarație de niciun fel cu privire la faptul că Serviciul va îndeplini cerințele dumneavoastră, va obține rezultatele dorite, va fi compatibil sau va funcționa cu orice alt software, aplicații, sisteme sau servicii, va funcționa fără întrerupere, va respecta standardele de performanță sau de fiabilitate sau va fi lipsit de erori sau că orice erori sau defecte pot fi sau vor fi corectate.

Fără a limita cele de mai sus, nici Comisia Europeană și nici unul dintre furnizorii Comisiei Europene nu face nicio declarație sau garanție de niciun fel, expresă sau implicită: (i) în ceea ce privește funcționarea sau disponibilitatea Serviciului, sau informațiile, conținutul și materialele sau produsele incluse în acesta; (ii) că Serviciul va fi neîntrerupt sau lipsit de erori; (iii) în ceea ce privește acuratețea, fiabilitatea sau actualitatea oricăror informații sau conținuturi furnizate prin intermediul Serviciului; sau (iv) că Serviciul, serverele sale, conținutul sau e-mailurile trimise de la sau în numele Comisiei Europene sunt lipsite de viruși, scripturi, cai troieni, viermi, programe malware, bombe cu efect întârziat sau alte componente dăunătoare.

Unele jurisdicții nu permit excluderea anumitor tipuri de garanții sau limitări ale drepturilor legale aplicabile ale unui consumator, astfel încât este posibil ca unele sau toate excluderile și limitările de mai sus să nu se aplice în cazul dumneavoastră. Dar, într-un astfel de caz, excluderile și limitările prevăzute în această secțiune se vor aplica în cea mai mare măsură posibilă în conformitate cu legislația aplicabilă.

## Legea aplicabilă

Legile Uniunii Europene, completate, dacă este necesar, de legea Belgiei, vor guverna acești Termeni și utilizarea de către Dumneavoastră a Serviciului. Utilizarea de către Dvs. a Aplicației poate fi, de asemenea, supusă altor legi locale, statale, naționale sau internaționale.

## Soluționarea litigiilor

Dacă aveți orice îngrijorare sau dispută cu privire la Serviciu, sunteți de acord să încercați mai întâi să rezolvați disputa în mod informal, contactând Comisia Europeană.

## Pentru utilizatorii din Uniunea Europeană (UE)

Dacă sunteți un consumator din Uniunea Europeană, veți beneficia de orice prevederi obligatorii ale legislației țării în care sunteți rezident.

## Caracterul excepțional și renunțarea

### Severabilitate

În cazul în care orice prevedere a acestor Termeni este considerată inaplicabilă sau invalidă, respectiva prevedere va fi modificată și interpretată pentru a îndeplini obiectivele respectivei prevederi în cea mai mare măsură posibilă în conformitate cu legea aplicabilă, iar celelalte prevederi vor continua să fie în vigoare și să producă efecte depline.

### Renunțare

Cu excepția celor prevăzute în prezentul document, neexercitarea unui drept sau neexecutarea unei obligații în temeiul acestor Termeni nu va afecta capacitatea unei părți de a exercita un astfel de drept sau de a solicita o astfel de executare în orice moment ulterior și nici renunțarea la o încălcare nu va constitui o renunțare la orice încălcare ulterioară.

## Traducere Interpretare

Este posibil ca acești Termeni și Condiții să fi fost traduși în cazul în care vi i-am pus la dispoziție pe Serviciul nostru.

Sunteți de acord ca textul original în limba engleză să prevaleze în cazul unui litigiu.

## Modificări ale acestor Termeni și condiții

Ne rezervăm dreptul, la discreția Noastră exclusivă, de a modifica sau înlocui acești Termeni în orice moment. În cazul în care o revizuire este materială, vom depune eforturi rezonabile pentru a oferi o notificare cu cel puțin 30 de zile înainte de intrarea în vigoare a noilor termeni. Ceea ce constituie o modificare materială va fi stabilit la discreția Noastră exclusivă.

Continuând să accesați sau să utilizați Serviciul nostru după intrarea în vigoare a acestor revizuiri, sunteți de acord să vă supuneți termenilor revizuiți. Dacă nu sunteți de acord cu noii termeni, în totalitate sau parțial, vă rugăm să nu mai utilizați site-ul web și Serviciul.

## Contactați-ne

Dacă aveți întrebări cu privire la acești Termeni și Condiții, ne puteți contacta:

- Prin e-mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>.
- Vizitând această pagină de pe site-ul nostru: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
