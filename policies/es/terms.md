# Condiciones generales

_Última actualización: 17 de enero de 2022_

Por favor, lea atentamente estas Condiciones Generales antes de utilizar nuestro servicio.

## Interpretación y Definiciones

### Interpretación

Las palabras cuya letra inicial aparece en mayúscula tienen el significado que se define en las siguientes condiciones. Las siguientes definiciones tendrán el mismo significado independientemente de que aparezcan en singular o en plural.

### Definiciones

A los efectos de las presentes Condiciones Generales:

- **Aplicación** significa el programa de software proporcionado por la Comisión Europea descargado por Usted en cualquier dispositivo electrónico, denominado FaST

- **Application Store** significa el servicio de distribución digital operado y desarrollado por Apple Inc. (Apple App Store) o Google Inc. (Google Play Store) en el que se ha descargado la Aplicación.

- **Afiliada** significa una entidad que controla, es controlada por o está bajo control común con una parte, donde "control" significa la propiedad del 50% o más de las acciones, participaciones u otros valores con derecho a voto para la elección de directores u otra autoridad de gestión.

- **Cuenta** se refiere a una cuenta única creada para que Usted pueda acceder a nuestro Servicio o a partes de nuestro Servicio.

- **Organismo Pagador** se refiere a:
    - Para los usuarios de Andalucía: Organismo Pagador de Andalucía, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Para los usuarios de Castilla y León: Organismo Pagador de Castilla y León, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Para los usuarios de Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Para los usuarios de Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Para los usuarios de Valonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Bélgica
    - Para los usuarios de Grecia: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Para los usuarios de Bulgaria: Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България
    - Para los usuarios de Eslovaquia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Para los usuarios de Rumanía: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- **Comisión Europea** (denominada "la Comisión Europea", "nosotros", "nos" o "nuestro" en el presente Acuerdo) se refiere a la Comisión Europea, DG DEFIS, Unidad I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Bélgica

- **Contenido** se refiere al contenido como texto, imágenes u otra información que puede ser publicada, cargada, enlazada o puesta a disposición de otra manera por Usted, independientemente de la forma de ese contenido.

- **Dispositivo** se refiere a cualquier dispositivo que pueda acceder al Servicio, como un ordenador, un teléfono móvil o una tableta digital.

- **Comentarios** se refiere a los comentarios, innovaciones o sugerencias enviados por Usted en relación con los atributos, el rendimiento o las características de nuestro Servicio.

- **Servicio** se refiere a la Aplicación.

- **Condiciones generales** significa estas Condiciones generales que forman el acuerdo completo entre Usted y la Comisión Europea en relación con el uso del Servicio.

- **Servicio de autenticación de terceros** significa cualquier servicio de autenticación de usuarios proporcionado por un tercero (como un proveedor nacional de identidad de un Estado miembro de la Unión Europea) al que la Comisión Europea delega la autenticación de usuarios para acceder a la Aplicación.

- **Servicio de terceros** significa cualquier servicio o contenido (incluidos los datos, la información, los productos o los servicios) proporcionado por un tercero que pueda mostrarse, incluirse o ponerse a disposición por el Servicio.

- **Usted** se refiere a la persona que accede o utiliza el Servicio, o a la empresa u otra entidad jurídica en cuyo nombre dicha persona accede o utiliza el Servicio, según corresponda.

## Reconocimiento

Estas son las Condiciones Generales que rigen el uso de este Servicio y el acuerdo que opera entre Usted y Nosotros. Estas Condiciones Generales establecen los derechos y obligaciones de todos los usuarios en relación con el uso del Servicio.

Su acceso y uso del Servicio está condicionado a su aceptación y cumplimiento de estas Condiciones Generales. Estas Condiciones Generales se aplican a todos los visitantes, usuarios y otras personas que acceden o utilizan el Servicio.

Al acceder o utilizar el Servicio, Usted acepta quedar vinculado por estas Condiciones Generales. Si no está de acuerdo con alguna parte de estas Condiciones Generales, no podrá acceder al Servicio.

Usted declara que es mayor de 18 años. La Comisión Europea no permite que los menores de 18 años utilicen el Servicio.

Su acceso y uso del Servicio también está condicionado a su aceptación y cumplimiento de la Política de Privacidad de la Comisión Europea. Nuestra Política de Privacidad describe nuestras políticas y procedimientos sobre la recopilación, el uso y la divulgación de su información personal cuando utiliza la Aplicación o el Sitio Web y le informa sobre sus derechos de privacidad y cómo le protege la ley. Le rogamos que lea atentamente nuestra Política de Privacidad antes de utilizar nuestro Servicio.

## Cuentas de usuario

Cuando usted crea una cuenta con nosotros, debe proporcionarnos información precisa, completa y actualizada en todo momento. No hacerlo constituye un incumplimiento de las Condiciones, que puede dar lugar a la cancelación inmediata de su cuenta en nuestro Servicio.

Usted es responsable de salvaguardar la contraseña que utiliza para acceder al Servicio y de cualquier actividad o acción realizada con su contraseña, ya sea con nuestro Servicio o con un Servicio de Autenticación de Terceros.
Usted se compromete a no revelar su contraseña a ningún tercero. Debe notificarnos inmediatamente si tiene conocimiento de cualquier violación de la seguridad o del uso no autorizado de su cuenta.

Usted no puede utilizar como nombre de usuario el nombre de otra persona o entidad o que no esté legalmente disponible para su uso, un nombre o marca comercial que esté sujeto a cualquier derecho de otra persona o entidad que no sea Usted sin la autorización correspondiente, o un nombre que sea de otra manera ofensivo, vulgar u obsceno.

## Contenido

### Su derecho a cargar contenidos

Nuestro Servicio le permite cargar Contenido (por ejemplo, fotos geoetiquetadas). Usted es responsable del Contenido que publique en el Servicio, incluyendo su legalidad, fiabilidad y adecuación.

Usted declara y garantiza que (i) el Contenido es suyo (es de su propiedad) o tiene derecho a utilizarlo y a concedernos los derechos y la licencia según lo dispuesto en estas Condiciones, y (ii) la publicación de su Contenido en el Servicio o a través de él no viola los derechos de privacidad, los derechos de publicidad, los derechos de autor, los derechos contractuales o cualquier otro derecho de cualquier persona.

### Restricciones de contenido

La Comisión Europea no es responsable del contenido de los usuarios del Servicio. Usted entiende y acepta expresamente que es el único responsable del Contenido y de toda la actividad que ocurra bajo su cuenta, ya sea hecha por Usted o por cualquier tercera persona que utilice su cuenta.

Usted no puede transmitir ningún Contenido que sea ilegal, ofensivo, molesto, que tenga la intención de disgustar, amenazante, calumnioso, difamatorio, obsceno o de cualquier otra manera objetable. Ejemplos de dicho Contenido objetable incluyen, pero no se limitan a, lo siguiente:

- Ilegal o que promueva una actividad ilegal.
- Contenido difamatorio, discriminatorio o mezquino, incluyendo referencias o comentarios sobre religión, raza, orientación sexual, género, origen nacional/étnico u otros grupos específicos.
- Spam, generado de forma automática o aleatoria, que constituya publicidad no autorizada o no solicitada, cartas en cadena, cualquier otra forma de solicitud no autorizada, o cualquier forma de lotería o juego.
- Que contenga o instale cualquier tipo de virus, gusanos, programas maliciosos, troyanos u otros contenidos que estén diseñados o tengan por objeto interrumpir, dañar o limitar el funcionamiento de cualquier software, hardware o equipo de telecomunicaciones o dañar u obtener acceso no autorizado a cualquier dato u otra información de una tercera persona.
- Infringir cualquier derecho de propiedad de cualquier parte, incluyendo patentes, marcas, secretos comerciales, derechos de autor, derechos de publicidad u otros derechos.
- Suplantar a cualquier persona o entidad, incluida la Comisión Europea y sus empleados o representantes.
- Violar la privacidad de cualquier tercera persona.
- Información y características falsas.

La Comisión Europea se reserva el derecho, pero no la obligación, de, a su sola discreción, determinar si cualquier Contenido es apropiado y cumple con estos Términos, rechazar o eliminar este Contenido. La Comisión Europea se reserva además el derecho de dar formato y editar y cambiar la forma de cualquier Contenido. La Comisión Europea también puede limitar o revocar el uso del Servicio si Usted publica dicho Contenido objetable.

Dado que la Comisión Europea no puede controlar todo el contenido publicado por los usuarios y/o terceros en el Servicio, Usted acepta utilizar el Servicio bajo su propia responsabilidad. Usted entiende que al utilizar el Servicio puede estar expuesto a contenido que puede encontrar ofensivo, indecente, incorrecto o censurable, y usted acepta que bajo ninguna circunstancia la Comisión Europea será responsable de cualquier contenido, incluyendo cualquier error u omisión en cualquier contenido, o cualquier pérdida o daño de cualquier tipo incurrido como resultado de su uso de cualquier contenido.

### Copias de seguridad de los contenidos

Aunque se realizan copias de seguridad periódicas de los contenidos, la Comisión Europea no garantiza que no haya pérdida o corrupción de datos.

Los puntos de copia de seguridad corruptos o inválidos pueden ser causados por, sin limitación, el Contenido que se corrompe antes de ser respaldado o que cambia durante el tiempo que se realiza una copia de seguridad.

La Comisión Europea proporcionará apoyo e intentará solucionar cualquier problema conocido o descubierto que pueda afectar a las copias de seguridad del Contenido. Sin embargo, usted reconoce que la Comisión Europea no tiene ninguna responsabilidad en relación con la integridad de los Contenidos o la imposibilidad de restaurar con éxito los Contenidos a un estado utilizable.

Usted se compromete a mantener una copia completa y precisa de cualquier Contenido en una ubicación independiente del Servicio.

## Política de derechos de autor

### Infracción de la propiedad intelectual

Respetamos los derechos de propiedad intelectual de otros. Nuestra política es responder a cualquier reclamación de que el Contenido publicado en el Servicio infringe un derecho de autor u otra infracción de la propiedad intelectual de cualquier persona.

Si Usted es un propietario de derechos de autor, o está autorizado en nombre de uno, y cree que el trabajo protegido por derechos de autor ha sido copiado de una manera que constituye una infracción de los derechos de autor que está teniendo lugar a través del Servicio, debe enviar su notificación por escrito a la atención de nuestro agente de derechos de autor a través del correo electrónico contact@fastplatform.eu e incluir en su notificación una descripción detallada de la supuesta infracción.

Usted puede ser considerado responsable de los daños y perjuicios (incluidos los costes y los honorarios de los abogados) por haber declarado erróneamente que cualquier Contenido infringe sus derechos de autor.

## Sus comentarios a nosotros

Usted cede todos los derechos, la titularidad y los intereses de cualquier comentario que proporcione a la Comisión Europea. Si, por cualquier motivo, dicha cesión no es efectiva, usted acepta conceder a la Comisión Europea un derecho y una licencia no exclusivos, perpetuos, irrevocables y libres de derechos de autor en todo el mundo para utilizar, reproducir, divulgar, sublicenciar, distribuir, modificar y explotar dichos comentarios sin restricciones.

## Enlaces a otros sitios web

Nuestro Servicio puede contener enlaces a sitios web o servicios de terceros que no son propiedad ni están controlados por la Comisión Europea.

La Comisión Europea no tiene ningún control sobre el contenido, las políticas de privacidad o las prácticas de los sitios web o servicios de terceros, y no asume ninguna responsabilidad al respecto. Además, usted reconoce y acepta que la Comisión Europea no será responsable, directa o indirectamente, de cualquier daño o pérdida causada o supuestamente causada por o en relación con el uso o la confianza en cualquier contenido, bienes o servicios disponibles en o a través de dichos sitios web o servicios.

Le recomendamos encarecidamente que lea las condiciones generales y las políticas de privacidad de los sitios web o servicios de terceros que visite.

## Cancelación

Podemos cancelar o suspender su cuenta inmediatamente, sin previo aviso ni responsabilidad, por cualquier motivo, incluyendo, sin limitación, si usted incumple estas Condiciones Generales.

Tras la rescisión, su derecho a utilizar el Servicio cesará inmediatamente. Si desea cancelar su cuenta, puede simplemente dejar de utilizar el Servicio.

## Limitación de la responsabilidad

Sin perjuicio de los daños y perjuicios en los que pueda incurrir, toda la responsabilidad de la Comisión Europea y de cualquiera de sus proveedores en virtud de cualquier disposición de estas Condiciones y su recurso exclusivo para todo lo anterior se limitará a la cantidad efectivamente pagada por Usted a través del Servicio o a 100 USD si no ha comprado nada a través del Servicio.

En la medida máxima permitida por la ley aplicable, en ningún caso la Comisión Europea o sus proveedores serán responsables de ningún daño especial, incidental, indirecto o consecuente (incluyendo, pero sin limitarse a, daños por pérdida de beneficios, pérdida de datos u otra información, por interrupción del negocio, por daños personales, pérdida de privacidad que surjan o estén relacionados de alguna manera con el uso o la imposibilidad de usar el Servicio, el software de terceros y/o el hardware de terceros utilizado con el Servicio, o que estén relacionados de alguna manera con cualquier disposición de estas Condiciones), incluso si la Comisión Europea o cualquier proveedor ha sido advertido de la posibilidad de tales daños e incluso si el remedio no cumple con su propósito esencial.

Algunos estados no permiten la exclusión de las garantías implícitas o la limitación de la responsabilidad por daños incidentales o consecuentes, lo que significa que algunas de las limitaciones anteriores pueden no ser aplicables. En estos estados, la responsabilidad de cada una de las partes se limitará en la mayor medida permitida por la ley.

## Descargo de responsabilidad "TAL CUAL" y "SEGÚN DISPONIBILIDAD"

El Servicio se le proporciona "TAL CUAL" y "SEGÚN DISPONIBILIDAD" y con todos los fallos y defectos sin garantía de ningún tipo. En la medida máxima permitida por la legislación aplicable, la Comisión Europea, en su propio nombre y en el de sus Afiliados y sus respectivos licenciadores y proveedores de servicios, renuncia expresamente a todas las garantías, ya sean expresas, implícitas, legales o de otro tipo, con respecto al Servicio, incluidas todas las garantías implícitas de comerciabilidad, idoneidad para un fin determinado, titularidad y no infracción, y las garantías que puedan surgir del curso de la negociación, el curso de la ejecución, el uso o la práctica comercial. Sin limitación de lo anterior, la Comisión Europea no ofrece ninguna garantía o compromiso, y no hace ninguna declaración de ningún tipo de que el Servicio cumplirá con sus requisitos, logrará los resultados previstos, será compatible o funcionará con cualquier otro software, aplicaciones, sistemas o servicios, funcionará sin interrupción, cumplirá con las normas de rendimiento o fiabilidad o estará libre de errores o que cualquier error o defecto puede o será corregido.

Sin perjuicio de lo anterior, ni la Comisión Europea ni ninguno de los proveedores de la Comisión Europea ofrecen ninguna declaración o garantía de ningún tipo, expresa o implícita (i) en cuanto al funcionamiento o la disponibilidad del Servicio, o la información, el contenido y los materiales o productos incluidos en él; (ii) que el Servicio será ininterrumpido o estará libre de errores; (iii) en cuanto a la exactitud, la fiabilidad o la actualidad de cualquier información o contenido proporcionado a través del Servicio; o (iv) que el Servicio, sus servidores, el contenido o los mensajes de correo electrónico enviados desde o en nombre de la Comisión Europea están libres de virus, scripts, troyanos, gusanos, malware, bombas de tiempo u otros componentes dañinos.

Algunas jurisdicciones no permiten la exclusión de ciertos tipos de garantías o limitaciones de los derechos legales aplicables a un consumidor, por lo que algunas o todas las exclusiones y limitaciones anteriores pueden no ser aplicables a Usted. Pero en tal caso, las exclusiones y limitaciones establecidas en esta sección se aplicarán en la mayor medida posible según la legislación aplicable.

## Derecho aplicable

Las leyes de la Unión Europea, complementadas si es necesario por la ley de Bélgica, regirán estos Términos y Su uso del Servicio. Su uso de la Aplicación también puede estar sujeto a otras leyes locales, estatales, nacionales o internacionales.

## Resolución de conflictos

Si usted tiene alguna preocupación o disputa sobre el Servicio, usted se compromete a tratar de resolver primero la disputa de manera informal poniéndose en contacto con la Comisión Europea.

## Para los usuarios de la Unión Europea (UE)

Si Usted es un consumidor de la Unión Europea, se beneficiará de las disposiciones obligatorias de la legislación del país en el que resida.

## Divisibilidad y renuncia

### Divisibilidad

Si alguna disposición de estas Condiciones se considera inaplicable o inválida, dicha disposición se modificará e interpretará para cumplir los objetivos de dicha disposición en la mayor medida posible según la legislación aplicable y las disposiciones restantes continuarán en plena vigencia.

### Renuncia

Salvo lo dispuesto en el presente documento, el hecho de no ejercer un derecho o de no exigir el cumplimiento de una obligación en virtud de las presentes Condiciones no afectará a la capacidad de una parte para ejercer dicho derecho o exigir dicho cumplimiento en cualquier momento posterior, ni la renuncia a un incumplimiento constituirá una renuncia a cualquier incumplimiento posterior.

## Traducción Interpretación

Estas Condiciones Generales pueden haber sido traducidos si los hemos puesto a su disposición en nuestro Servicio.

Usted acepta que el texto original en inglés prevalecerá en caso de litigio.

## Cambios en estas condiciones

Nos reservamos el derecho, a nuestra entera discreción, de modificar o sustituir estas Condiciones en cualquier momento. Si una revisión es material, haremos esfuerzos razonables para proporcionar al menos 30 días de aviso antes de que los nuevos términos entren en vigor. Lo que constituye un cambio material se determinará a nuestra entera discreción.

Al continuar accediendo o utilizando nuestro Servicio después de que esas revisiones entren en vigor, usted acepta quedar obligado por los términos revisados. Si usted no está de acuerdo con los nuevos términos, en su totalidad o en parte, por favor deje de usar el sitio web y el Servicio.

## Contacto

Si tiene alguna pregunta sobre estas Condiciones Generales, puede ponerse en contacto con nosotros

- Por correo electrónico: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Visitando esta página de nuestro sitio web: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
