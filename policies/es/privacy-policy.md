# Política de Privacidad

_Última actualización: 17 de enero de 2022_

Esta Política de Privacidad describe nuestras políticas y procedimientos sobre la recopilación, el uso y la divulgación de sus datos cuando utiliza el Servicio y le informa sobre sus derechos de privacidad y cómo le protege la ley.

Utilizamos sus datos personales para proporcionar y mejorar el Servicio. Al utilizar el Servicio, Usted acepta la recopilación y el uso de la información de acuerdo con esta Política de Privacidad.

## Interpretación y definiciones

### Interpretación

Las palabras cuya letra inicial está en mayúscula tienen el significado que se define en las siguientes condiciones. Las siguientes definiciones tendrán el mismo significado independientemente de que aparezcan en singular o en plural.

### Definiciones

A los efectos de la presente Política de Privacidad:

- **Cuenta** significa una cuenta única creada para que Usted pueda acceder a nuestro Servicio o a partes de nuestro Servicio.

- **Afiliada** significa una entidad que controla, es controlada por o está bajo control común con una parte, donde "control" significa la propiedad del 50% o más de las acciones, participación en el capital u otros valores con derecho a voto para la elección de directores u otra autoridad de gestión.

- **Aplicación** significa el programa de software proporcionado por la Comisión Europea descargado por Usted en cualquier dispositivo electrónico, denominado FaST

- **Comisión Europea** (denominada "Comisión Europea", "Nosotros", "Nos" o "Nuestro" en este Acuerdo) se refiere a la Comisión Europea, DG DEFIS, Unidad I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Bélgica

- **GDPR** es el Reglamento General de Protección de Datos

- **El organismo pagador de la PAC** se refiere a:
    - Para los usuarios de Andalucía: Organismo Pagador de Andalucía, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Para los usuarios de Castilla y León: Organismo Pagador de Castilla y León, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Para los usuarios de Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Para los usuarios de Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Para los usuarios de Valonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Bélgica
    - Para los usuarios de Grecia: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Para los usuarios de Bulgaria: Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България
    - Para los usuarios de Eslovaquia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Para los usuarios de Rumanía: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- **Responsable del Tratamiento de Datos** (denominado "Nosotros", "Nos" o "Nuestro" en el presente Acuerdo), a efectos del RGPD, se refiere a la Comisión Europea, como entidad jurídica que determina los fines y los medios del tratamiento de los datos personales.

- **Dispositivo** se refiere a cualquier dispositivo que pueda acceder al Servicio, como un ordenador, un teléfono móvil o una tableta digital.

- **Datos personales** es cualquier información relacionada con un individuo identificado o identificable. A los efectos del RGPD, se entiende por Datos Personales cualquier información relacionada con Usted, como un nombre, un número de identificación, datos de localización, un identificador en línea o uno o más factores específicos de la identidad física, fisiológica, genética, mental, económica, cultural o social.

- **Servicio** se refiere a la Aplicación.

- **Proveedor de servicios** se refiere a cualquier persona física o jurídica que procese los datos en nombre de la Comisión Europea. Se refiere a terceras empresas o personas contratadas por la Comisión Europea o el Organismo Pagador para facilitar el Servicio, prestar el Servicio en nombre de la Comisión Europea, realizar servicios relacionados con el Servicio o ayudar a la Comisión Europea y al Organismo Pagador a analizar cómo se utiliza el Servicio. A efectos del RGPD, los Proveedores de Servicios se consideran Procesadores de Datos.

- **Servicio de autenticación de terceros** se refiere a cualquier servicio a través del cual un usuario puede iniciar sesión o crear una cuenta para utilizar el Servicio.

- **Servicio de inclusión de terceros** se refiere a cualquier servicio externo que se haya registrado en el Servicio y al que el Usuario pueda acceder.

- **Métricas de uso** se refiere a los datos recogidos automáticamente, generados por el uso del Servicio o por la propia infraestructura del Servicio (por ejemplo, la duración de la visita a una página).

- **Usted** se refiere a la persona que accede o utiliza el Servicio, o a la empresa u otra entidad legal en cuyo nombre dicha persona accede o utiliza el Servicio, según corresponda. De acuerdo con el Reglamento General de Protección de Datos (RGPD), se puede hacer referencia a usted como el sujeto de los datos o como el usuario, ya que es la persona que utiliza el Servicio.

## Recogida y uso de sus datos personales

### Tipos de datos recogidos

#### Datos personales

Al utilizar nuestro servicio, es posible que le pidamos que nos proporcione cierta información de identificación personal que pueda utilizarse para ponerse en contacto con usted o identificarle. La información personal identificable puede incluir, pero no está limitada a:

- Dirección de correo electrónico
- Nombre y apellidos
- Número de teléfono
- Dirección, Estado, Provincia, Código Postal, Ciudad
- Mensajes y archivos adjuntos que intercambia con otros usuarios (incluso con su Organismo Pagador)
- Fotos geoetiquetadas que intercambia en la plataforma (incluso con su Organismo Pagador)
- Descripción de su explotación (por ejemplo, parcelas, cultivos), prácticas agrícolas (por ejemplo, recomendaciones de fertilización que recibe), datos sobre el suelo que introduce en el Servicio
- Métricas de uso (véase más abajo)

#### Métricas de uso

Las métricas de uso se recogen automáticamente al utilizar el Servicio.

Las métricas de uso pueden incluir información como la dirección de protocolo de Internet de su dispositivo (por ejemplo, la dirección IP), el tipo de navegador, la versión del navegador, las páginas de nuestro Servicio que visita, la hora y la fecha de su visita, el tiempo que pasa en esas páginas, los identificadores únicos del dispositivo y otros datos de diagnóstico.

Cuando Usted accede al Servicio por o a través de un dispositivo móvil, podemos recopilar cierta información automáticamente, incluyendo, pero sin limitarse a, el tipo de dispositivo móvil que Usted utiliza, la identificación única de su dispositivo móvil, la dirección IP de su dispositivo móvil, su sistema operativo móvil, el tipo de navegador de Internet móvil que Usted utiliza, los identificadores únicos del dispositivo y otros datos de diagnóstico.

También podemos recopilar la información que su navegador envía cada vez que visita nuestro Servicio o cuando accede al Servicio por o a través de un dispositivo móvil.

#### Información recopilada durante el uso de la aplicación

Durante el uso de nuestra aplicación, podemos recopilar, con su permiso previo

- Información relativa a su ubicación
- Imágenes y otra información de la cámara y la biblioteca de fotos de su Dispositivo

Utilizamos esta información para proporcionar la función de fotos geoetiquetadas de Nuestro Servicio, y para localizarle en nuestros mapas por satélite. Esta información puede cargarse en nuestros servidores y/o en el servidor de un proveedor de servicios o simplemente almacenarse en su dispositivo.

Usted puede activar o desactivar el acceso a esta información en cualquier momento, a través de la configuración de su dispositivo.

### Uso de sus datos personales

Nuestro servicio se encuentra actualmente en fase beta y, por tanto, sus Datos Personales sólo se utilizarán para **desarrollar, mejorar y probar nuestro Servicio**.

Durante su uso del Servicio, podremos ponernos en contacto con Usted, por correo electrónico, llamadas telefónicas, SMS u otras formas equivalentes de comunicación electrónica, como las notificaciones push de una aplicación móvil, en relación con actualizaciones o comunicaciones informativas relacionadas con las funcionalidades, mejoras o novedades de nuestros Servicios, incluidas las actualizaciones de seguridad, cuando sea necesario o razonable para su implementación. También almacenaremos sus Datos Personales para atender y gestionar las solicitudes que nos haga.

### Retención de sus datos personales

Conservaremos Sus Datos Personales sólo durante el tiempo necesario para los fines establecidos en esta Política de Privacidad. Conservaremos y utilizaremos sus Datos Personales en la medida necesaria para cumplir con nuestras obligaciones legales (por ejemplo, si estamos obligados a conservar sus datos para cumplir con la legislación aplicable), resolver conflictos y hacer cumplir nuestros acuerdos y políticas legales.

También conservaremos los parámetros de uso con fines de análisis interno. Las métricas de uso se conservan generalmente durante un período de tiempo más corto, excepto cuando estos datos se utilizan para reforzar la seguridad o mejorar la funcionalidad de nuestro servicio, o cuando estamos legalmente obligados a conservar estos datos durante períodos de tiempo más largos.

### Transferencia de sus datos personales

Su información, incluidos los Datos Personales, se procesa en nuestras oficinas operativas y en cualquier otro lugar donde se encuentren las partes implicadas en el procesamiento. Esto significa que esta información puede ser transferida y mantenida en ordenadores situados fuera de su estado, provincia, país u otra jurisdicción gubernamental donde las leyes de protección de datos pueden diferir de las de su jurisdicción.

Su consentimiento a esta Política de Privacidad, seguido de su envío de dicha información, representa su acuerdo con dicha transferencia.

Tomaremos todas las medidas razonablemente necesarias para garantizar que sus datos sean tratados de forma segura y de acuerdo con esta Política de Privacidad y no se realizará ninguna transferencia de sus datos personales a una organización o país a menos que haya controles adecuados que incluyan la seguridad de sus datos y otra información personal.

### Divulgación de sus datos personales

#### Aplicación de la ley

En determinadas circunstancias, podemos estar obligados a revelar sus datos personales si así lo exige la ley o en respuesta a solicitudes válidas de las autoridades públicas (por ejemplo, un tribunal o una agencia gubernamental).

#### Otros requisitos legales

Podemos revelar sus datos personales si creemos de buena fe que dicha acción es necesaria para

- Cumplir con una obligación legal
- Proteger y defender los derechos o la propiedad de la Comisión Europea
- Prevenir o investigar posibles infracciones en relación con el Servicio
- Proteger la seguridad personal de los usuarios del Servicio o del público
- Proteger contra la responsabilidad legal

### Seguridad de sus datos personales

La seguridad de sus datos personales es importante para nosotros, pero recuerde que ningún método de transmisión por Internet o de almacenamiento electrónico es 100% seguro. Aunque nos esforzamos por utilizar medios comercialmente aceptables para proteger sus datos personales, no podemos garantizar su seguridad absoluta.

## Privacidad GDPR

### Base Legal para el Procesamiento de Datos Personales bajo el GDPR

Podemos procesar Datos Personales bajo las siguientes condiciones:

- **Consentimiento:** Usted ha dado Su consentimiento para el procesamiento de Datos Personales para uno o más propósitos específicos.

- **Cumplimiento de un contrato:** El suministro de Datos Personales es necesario para el cumplimiento de un acuerdo con Usted y/o para cualquier obligación precontractual del mismo.

- **Obligaciones legales:** El tratamiento de Datos Personales es necesario para el cumplimiento de una obligación legal a la que está sujeta la Comisión Europea.

- **Intereses vitales:** El tratamiento de Datos Personales es necesario para proteger sus intereses vitales o los de otra persona física.

- **Intereses públicos:** El tratamiento de Datos Personales está relacionado con una tarea que se lleva a cabo en el interés público o en el ejercicio de la autoridad oficial conferida a la Comisión Europea.

- **Intereses legítimos:** El tratamiento de los Datos Personales es necesario para los fines de los intereses legítimos perseguidos por la Comisión Europea.

En cualquier caso, estaremos encantados de ayudar a aclarar la base legal específica que se aplica al procesamiento, y en particular si el suministro de Datos Personales es un requisito legal o contractual, o un requisito necesario para celebrar un contrato.

### Sus derechos según el GDPR

La Comisión Europea se compromete a respetar la confidencialidad de sus Datos Personales y a garantizarle el ejercicio de sus derechos.

Usted tiene derecho en virtud de esta Política de Privacidad, y por la ley de la UE, a:

- **Solicitar el acceso a Sus Datos Personales.** El derecho a acceder, actualizar o eliminar la información que tenemos sobre Usted. Siempre que sea posible, puede acceder, actualizar o solicitar la eliminación de sus datos personales directamente en la sección de configuración de su cuenta. Si no puede realizar estas acciones por sí mismo, póngase en contacto con nosotros para que le ayudemos. Esto también le permite recibir una copia de los Datos Personales que tenemos sobre usted.

- **Solicitar la corrección de los Datos Personales que tenemos sobre usted.** Tiene derecho a que se corrija cualquier información incompleta o inexacta que tengamos sobre usted.

- **Oponerse al tratamiento de sus datos personales.** Este derecho existe cuando nos basamos en un interés legítimo como base legal para nuestro tratamiento y hay algo en su situación particular que hace que quiera oponerse a nuestro tratamiento de sus datos personales por este motivo. También tiene derecho a oponerse cuando tratemos sus datos personales con fines de marketing directo.

- **Solicitar la eliminación de sus datos personales.** Usted tiene derecho a pedirnos que borremos o eliminemos sus datos personales cuando no haya ninguna razón válida para que sigamos tratándolos.

- **Solicitar la transferencia de sus datos personales.** Le proporcionaremos a usted, o a un tercero que usted haya elegido, sus datos personales en un formato estructurado, de uso común y legible por máquina. Tenga en cuenta que este derecho sólo se aplica a la información automatizada para cuyo uso usted dio inicialmente su consentimiento o cuando nosotros utilizamos la información para ejecutar un contrato con usted.

- **Retirar su consentimiento.** Usted tiene derecho a retirar su consentimiento para el uso de sus datos personales. Si retira su consentimiento, es posible que no podamos proporcionarle acceso a ciertas funcionalidades específicas del Servicio.

### Ejercicio de sus Derechos de Protección de Datos GDPR

Puede ejercer sus derechos de acceso, rectificación, cancelación y oposición poniéndose en contacto con nosotros. Tenga en cuenta que podemos pedirle que verifique su identidad antes de responder a dichas solicitudes. Si realiza una solicitud, haremos todo lo posible para responderle lo antes posible.

Tiene derecho a presentar una reclamación ante una autoridad de protección de datos sobre la recopilación y el uso de sus datos personales. Para más información, póngase en contacto con su autoridad local de protección de datos en el Espacio Económico Europeo.

## Enlaces a otros sitios web y servicios

Nuestro Servicio puede contener enlaces a otros sitios web o servicios que no son operados por nosotros. Si hace clic en un enlace de un tercero, será dirigido al sitio de ese tercero. Le recomendamos encarecidamente que revise la Política de Privacidad de cada sitio que visite.

No tenemos ningún control ni asumimos ninguna responsabilidad por el contenido, las políticas de privacidad o las prácticas de los sitios o servicios de terceros.

## Cambios en la Política de Privacidad

Podemos actualizar nuestra Política de Privacidad de vez en cuando. Le notificaremos cualquier cambio publicando la nueva Política de Privacidad en esta página.

Se lo comunicaremos por correo electrónico y/o mediante un aviso destacado en nuestro Servicio, antes de que el cambio entre en vigor y actualicemos la fecha de "Última actualización" en la parte superior de esta Política de Privacidad.

Se le aconseja que revise esta Política de Privacidad periódicamente para ver si hay cambios. Los cambios en esta Política de Privacidad son efectivos cuando se publican en esta página.

## Contacto

Si tiene alguna pregunta sobre esta Política de Privacidad, puede ponerse en contacto con nosotros

- Por correo electrónico: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Visitando esta página en nuestro sitio web: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
