# Termes et conditions

_Dernière mise à jour le 17 janvier 2022_

Veuillez lire attentivement ces termes et conditions avant d'utiliser notre service.

## Interprétation et définitions

### Interprétation

Les mots dont la lettre initiale est en majuscule ont des significations définies dans les conditions suivantes. Les définitions suivantes ont la même signification, qu'elles apparaissent au singulier ou au pluriel.

### Définitions

Aux fins des présentes Conditions Générales :

- **Application** désigne le programme logiciel fourni par la Commission européenne téléchargé par Vous sur tout appareil électronique, nommé FaST.

- **Application Store** désigne le service de distribution numérique exploité et développé par Apple Inc. (Apple App Store) ou Google Inc. (Google Play Store) dans lequel l'Application a été téléchargée.

- **Affilié** désigne une entité qui contrôle, est contrôlée par ou est sous contrôle commun avec une partie, où " contrôle " signifie la propriété de 50 % ou plus des actions, de la participation au capital ou d'autres titres donnant droit de vote pour l'élection des administrateurs ou autre autorité de gestion.

- **Compte** désigne un compte unique créé pour vous afin d'accéder à notre service ou à certaines parties de notre service.

- **Agence de paiement CAP** désigne :
    - Pour les utilisateurs d'Andalucia : Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Pour les utilisateurs de Castilla y Leon : Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Pour les utilisateurs du Piémont : Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia.
    - Pour les utilisateurs estoniens : Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti.
    - Pour les utilisateurs de Wallonie : Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgique
    - Pour les utilisateurs grecs : ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pour les utilisateurs bulgares : Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България
    - Pour les utilisateurs slovaques : Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko.
    - Pour les utilisateurs roumains : Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- La Commission européenne** (désignée par "la Commission européenne", "nous", "notre" ou "nos" dans le présent accord) désigne la Commission européenne, DG DEFIS, Unité I3 Données spatiales pour les défis sociétaux et la croissance, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgique.

- **Contenu** désigne le contenu tel que le texte, les images ou d'autres informations qui peuvent être affichées, téléchargées, liées ou autrement mises à disposition par Vous, quelle que soit la forme de ce contenu.

- **Appareil** désigne tout appareil pouvant accéder au Service tel qu'un ordinateur, un téléphone portable ou une tablette numérique.

- **Rétroaction** désigne les commentaires, les innovations ou les suggestions que vous envoyez concernant les attributs, les performances ou les caractéristiques de notre Service.

- **Service** désigne l'Application.

- **Termes et Conditions** (également dénommés " Termes ") désigne les présents Termes et Conditions qui constituent l'intégralité de l'accord entre Vous et la Commission européenne concernant l'utilisation du Service.

- Service d'authentification tiers** désigne tout service d'authentification de l'utilisateur fourni par un tiers (tel qu'un fournisseur d'identité nationale d'un État membre de l'Union européenne) auquel la Commission européenne délègue l'authentification de l'utilisateur pour accéder à l'Application.

- **Service Opt-In tiers** désigne tout service ou contenu (y compris les données, informations, produits ou services) fourni par un tiers qui peut être affiché, inclus ou rendu disponible par le Service.

- Vous** désigne la personne qui accède au Service ou l'utilise, ou la société ou autre entité juridique pour le compte de laquelle cette personne accède au Service ou l'utilise, selon le cas.

## Reconnaissance

Les présentes conditions générales régissent l'utilisation de ce service et constituent l'accord entre vous et nous. Ces conditions générales définissent les droits et obligations de tous les utilisateurs concernant l'utilisation du service.

Votre accès et votre utilisation du service sont conditionnés par votre acceptation et votre respect de ces conditions générales. Ces conditions générales s'appliquent à tous les visiteurs, utilisateurs et autres personnes qui accèdent ou utilisent le service.

En accédant au service ou en l'utilisant, vous acceptez d'être lié par les présentes conditions générales. Si vous n'êtes pas d'accord avec une partie des présentes conditions générales, vous ne pouvez pas accéder au service.

Vous déclarez que vous êtes âgé de plus de 18 ans. La Commission européenne n'autorise pas les personnes de moins de 18 ans à utiliser le Service.

Votre accès au Service et son utilisation sont également conditionnés par votre acceptation et votre respect de la Politique de confidentialité de la Commission européenne. Notre Politique de confidentialité décrit Nos politiques et procédures sur la collecte, l'utilisation et la divulgation de Vos informations personnelles lorsque Vous utilisez l'Application ou le Site Internet et Vous informe de Vos droits en matière de confidentialité et de la manière dont la loi Vous protège. Veuillez lire attentivement notre politique de confidentialité avant d'utiliser notre service.

## Comptes d'utilisateur

Lorsque vous créez un compte chez nous, vous devez nous fournir des informations qui sont exactes, complètes et à jour à tout moment. Tout manquement à cette obligation constitue une violation des Conditions, qui peut entraîner la résiliation immédiate de votre compte sur Notre Service.

Vous êtes responsable de la sauvegarde du mot de passe que vous utilisez pour accéder au service et de toutes les activités ou actions effectuées sous votre mot de passe, que ce dernier soit avec notre service ou un service d'authentification tiers.
Vous acceptez de ne pas divulguer votre mot de passe à un tiers. Vous devez nous informer immédiatement dès que vous avez connaissance d'une violation de la sécurité ou d'une utilisation non autorisée de votre compte.

Vous ne pouvez pas utiliser comme nom d'utilisateur le nom d'une autre personne ou entité ou qui n'est pas légalement disponible pour être utilisé, un nom ou une marque déposée qui est soumis à des droits d'une autre personne ou entité autre que Vous sans autorisation appropriée, ou un nom qui est autrement offensant, vulgaire ou obscène.

## Content

### Votre droit de télécharger du contenu

Notre service vous permet de télécharger du contenu (par exemple, des photos géolocalisées). Vous êtes responsable du Contenu que vous publiez sur le Service, y compris de sa légalité, de sa fiabilité et de son caractère approprié.

Vous déclarez et garantissez que : (i) le Contenu est le vôtre (vous en êtes le propriétaire) ou vous avez le droit de l'utiliser et de nous accorder les droits et la licence prévus dans les présentes Conditions, et (ii) la publication de votre Contenu sur ou par le biais du Service ne viole pas les droits à la vie privée, les droits de publicité, les droits d'auteur, les droits contractuels ou tout autre droit de toute personne.

### Restrictions relatives au contenu

La Commission européenne n'est pas responsable du contenu des utilisateurs du Service. Vous comprenez et acceptez expressément que vous êtes seul responsable du contenu et de toutes les activités qui se produisent sous votre compte, qu'elles soient le fait de vous ou de toute autre personne utilisant votre compte.

Vous ne pouvez pas transmettre de contenu illégal, offensant, bouleversant, destiné à dégoûter, menaçant, calomnieux, diffamatoire, obscène ou autrement répréhensible. Les exemples d'un tel contenu répréhensible comprennent, sans s'y limiter, les éléments suivants :

- Illégaux ou faisant la promotion d'une activité illégale.
- Contenu diffamatoire, discriminatoire ou mesquin, y compris des références ou des commentaires sur la religion, la race, l'orientation sexuelle, le sexe, l'origine nationale/ethnique ou d'autres groupes ciblés.
- Spam, généré par une machine ou de manière aléatoire, constituant une publicité non autorisée ou non sollicitée, des chaînes de lettres, toute autre forme de sollicitation non autorisée, ou toute forme de loterie ou de jeu.
- Contenant ou installant des virus, des vers, des logiciels malveillants, des chevaux de Troie ou tout autre contenu conçu ou destiné à perturber, endommager ou limiter le fonctionnement de tout logiciel, matériel ou équipement de télécommunications ou à endommager ou obtenir un accès non autorisé à toute donnée ou autre information d'une tierce personne.
- Enfreindre les droits de propriété de toute partie, y compris les brevets, marques, secrets commerciaux, droits d'auteur, droits de publicité ou autres droits.
- Usurper l'identité de toute personne ou entité, y compris la Commission européenne et ses employés ou représentants.
- Violation de la vie privée de toute personne tierce.
- les fausses informations et caractéristiques.

La Commission européenne se réserve le droit, mais non l'obligation, de déterminer, à sa seule discrétion, si un contenu est approprié et conforme aux présentes conditions, de refuser ou de supprimer ce contenu. La Commission européenne se réserve en outre le droit d'effectuer des mises en forme et des modifications et de changer la manière dont le contenu est présenté. La Commission européenne peut également limiter ou révoquer l'utilisation du service si vous publiez un tel contenu répréhensible.

Comme la Commission européenne ne peut pas contrôler tout le contenu publié par les utilisateurs et/ou les tiers sur le Service, vous acceptez d'utiliser le Service à vos propres risques. Vous comprenez qu'en utilisant le Service, vous pouvez être exposé à un contenu que vous pouvez trouver offensant, indécent, incorrect ou répréhensible, et vous acceptez qu'en aucun cas la Commission européenne ne soit responsable de quelque manière que ce soit de tout contenu, y compris toute erreur ou omission dans tout contenu, ou toute perte ou tout dommage de quelque nature que ce soit résultant de votre utilisation de tout contenu.

### Sauvegarde du contenu

Bien que des sauvegardes régulières du contenu soient effectuées, la Commission européenne ne garantit pas l'absence de perte ou de corruption des données.

Des points de sauvegarde corrompus ou invalides peuvent être causés par, sans limitation, un contenu corrompu avant d'être sauvegardé ou qui change pendant la durée de la sauvegarde.

La Commission européenne fournira une assistance et tentera de résoudre tout problème connu ou découvert qui pourrait affecter les sauvegardes du contenu. Vous reconnaissez toutefois que la Commission européenne n'a aucune responsabilité quant à l'intégrité du contenu ou à l'impossibilité de restaurer avec succès le contenu dans un état utilisable.

Vous acceptez de conserver une copie complète et précise de tout contenu dans un endroit indépendant du service.

## Politique en matière de droits d'auteur

### Violation de la propriété intellectuelle

Nous respectons les droits de propriété intellectuelle d'autrui. Notre politique est de répondre à toute réclamation selon laquelle le Contenu publié sur le Service enfreint un droit d'auteur ou toute autre violation de la propriété intellectuelle de toute personne.

Si vous êtes propriétaire d'un droit d'auteur, ou autorisé au nom d'un tel propriétaire, et que vous pensez que l'œuvre protégée par le droit d'auteur a été copiée d'une manière qui constitue une violation du droit d'auteur qui a lieu par le biais du Service, vous devez soumettre votre notification par écrit à l'attention de notre agent de droits d'auteur par e-mail à contact@fastplatform.eu et inclure dans votre notification une description détaillée de la prétendue violation.

Vous pouvez être tenu responsable des dommages (y compris les coûts et les honoraires d'avocat) pour avoir déclaré de manière erronée qu'un contenu enfreint vos droits d'auteur.

## Vos commentaires à notre intention

Vous cédez tous les droits, titres et intérêts relatifs à tout retour d'information que vous fournissez à la Commission européenne. Si, pour quelque raison que ce soit, une telle cession est sans effet, vous acceptez d'accorder à la Commission européenne un droit et une licence non exclusifs, perpétuels, irrévocables, libres de redevances et mondiaux d'utilisation, de reproduction, de divulgation, de sous-licence, de distribution, de modification et d'exploitation de ce feedback sans restriction.

## Liens vers d'autres sites Web

Notre service peut contenir des liens vers des sites Web ou des services de tiers qui ne sont pas détenus ou contrôlés par la Commission européenne.

La Commission européenne n'a aucun contrôle et n'assume aucune responsabilité quant au contenu, aux politiques de confidentialité ou aux pratiques des sites Web ou services de tiers. Vous reconnaissez et acceptez en outre que la Commission européenne ne peut être tenue pour responsable, directement ou indirectement, de tout dommage ou perte causé ou supposé être causé par ou en relation avec l'utilisation ou la confiance accordée à tout contenu, bien ou service disponible sur ou par le biais de ces sites ou services web.

Nous vous conseillons vivement de lire les conditions générales et les politiques de confidentialité de tout site Web ou service tiers que vous visitez.

## Résiliation

Nous pouvons résilier ou suspendre votre compte immédiatement, sans préavis ni responsabilité, pour quelque raison que ce soit, notamment si vous violez les présentes conditions générales.

En cas de résiliation, votre droit d'utiliser le service cessera immédiatement. Si vous souhaitez résilier votre compte, vous pouvez simplement cesser d'utiliser le service.

## Limitation de responsabilité

Nonobstant tout dommage que vous pourriez subir, l'entière responsabilité de la Commission européenne et de ses fournisseurs en vertu de toute disposition des présentes Conditions et votre recours exclusif pour tout ce qui précède seront limités au montant que vous avez effectivement payé par le biais du Service ou à 100 USD si vous n'avez rien acheté par le biais du Service.

Dans la mesure maximale autorisée par la loi applicable, la Commission européenne ou ses fournisseurs ne pourront en aucun cas être tenus responsables de tout dommage spécial, accessoire, indirect ou consécutif, quel qu'il soit (y compris, mais sans s'y limiter, les dommages pour perte de bénéfices, perte de données ou d'autres informations, pour interruption d'activité, pour préjudice personnel, perte de vie privée découlant de ou liée de quelque manière que ce soit à l'utilisation ou à l'incapacité d'utiliser le service, les logiciels de tiers et/ou le matériel de tiers utilisés avec le service, ou autrement en rapport avec toute disposition des présentes conditions), même si la Commission européenne ou tout fournisseur a été informé de la possibilité de tels dommages et même si le remède ne remplit pas son objectif essentiel.

Certains États n'autorisent pas l'exclusion des garanties implicites ou la limitation de la responsabilité pour les dommages accessoires ou indirects, ce qui signifie que certaines des limitations ci-dessus peuvent ne pas s'appliquer. Dans ces États, la responsabilité de chaque partie sera limitée dans toute la mesure permise par la loi.

## Avis de non-responsabilité "TEL QUEL" et "TEL QUE DISPONIBLE".

Le service vous est fourni "TEL QUEL" et "TEL QUE DISPONIBLE", avec tous les défauts et toutes les défaillances, sans garantie d'aucune sorte. Dans toute la mesure permise par la loi applicable, la Commission européenne, en son nom propre et au nom de ses affiliés et de leurs concédants de licence et prestataires de services respectifs, décline expressément toute garantie, qu'elle soit expresse, implicite, statutaire ou autre, concernant le Service, y compris toute garantie implicite de qualité marchande, d'adéquation à un usage particulier, de titre et d'absence de contrefaçon, ainsi que toute garantie pouvant découler de la conduite habituelle des affaires, des performances, des usages ou des pratiques commerciales. Sans limitation de ce qui précède, la Commission européenne ne fournit aucune garantie ou engagement, et ne fait aucune déclaration de quelque nature que ce soit, selon laquelle le Service répondra à Vos exigences, atteindra les résultats escomptés, sera compatible ou fonctionnera avec tout autre logiciel, application, système ou service, fonctionnera sans interruption, répondra à toute norme de performance ou de fiabilité ou sera exempt d'erreurs ou que toute erreur ou tout défaut peut être ou sera corrigé.

Sans limiter ce qui précède, ni la Commission européenne ni aucun de ses fournisseurs ne font de déclaration ou ne donnent de garantie d'aucune sorte, expresse ou implicite : (i) quant au fonctionnement ou à la disponibilité du service ou des informations, du contenu et des matériaux ou produits qui y sont inclus ; (ii) quant au fait que le service sera ininterrompu ou exempt d'erreurs ; (iii) quant à l'exactitude, la fiabilité ou l'actualité de toute information ou de tout contenu fourni par le biais du service ; ou (iv) quant au fait que le service, ses serveurs, le contenu ou les courriers électroniques envoyés par la Commission européenne ou en son nom sont exempts de virus, de scripts, de chevaux de Troie, de vers, de logiciels malveillants, de bombes à retardement ou d'autres éléments nuisibles.

Certaines juridictions n'autorisent pas l'exclusion de certains types de garanties ou la limitation des droits statutaires applicables d'un consommateur, de sorte que tout ou partie des exclusions et limitations susmentionnées peuvent ne pas s'appliquer à vous. Mais dans un tel cas, les exclusions et limitations énoncées dans cette section seront appliquées dans la plus grande mesure possible en vertu de la loi applicable.

## Droit applicable

Les lois de l'Union européenne, complétées si nécessaire par le droit belge, régissent les présentes Conditions et votre utilisation du Service. Votre utilisation de l'Application peut également être soumise à d'autres lois locales, étatiques, nationales ou internationales.

## Résolution des litiges

Si vous avez un quelconque souci ou litige concernant le Service, vous acceptez de tenter d'abord de résoudre le litige de manière informelle en contactant la Commission européenne.

## Pour les utilisateurs de l'Union européenne (UE)

Si Vous êtes un consommateur de l'Union européenne, Vous bénéficierez de toutes les dispositions obligatoires de la loi du pays dans lequel Vous résidez.

### Divisibilité et renonciation

### Divisibilité

Si une disposition des présentes Conditions est jugée inapplicable ou invalide, cette disposition sera modifiée et interprétée de manière à atteindre les objectifs de cette disposition dans toute la mesure du possible en vertu de la loi applicable, et les autres dispositions resteront en vigueur et de plein effet.

### Renonciation

Sauf dans les cas prévus par les présentes, le fait de ne pas exercer un droit ou d'exiger l'exécution d'une obligation en vertu des présentes Conditions n'affecte pas la capacité d'une partie à exercer ce droit ou à exiger cette exécution à tout moment par la suite, et la renonciation à une violation constitue une renonciation à toute violation ultérieure.

## Traduction Interprétation

Les présentes Conditions Générales peuvent avoir été traduites si Nous les avons mises à votre disposition sur notre Service.

Vous acceptez que le texte original en anglais prévale en cas de litige.

## Modifications des présentes conditions générales

Nous nous réservons le droit, à notre seule discrétion, de modifier ou de remplacer ces Conditions à tout moment. Si une révision est importante, Nous ferons des efforts raisonnables pour fournir un préavis d'au moins 30 jours avant l'entrée en vigueur des nouvelles conditions. Ce qui constitue une modification importante sera déterminé à Notre seule discrétion.

En continuant à accéder à notre service ou à l'utiliser après l'entrée en vigueur de ces révisions, vous acceptez d'être lié par les conditions révisées. Si vous n'acceptez pas les nouvelles conditions, en totalité ou en partie, veuillez cesser d'utiliser le site Web et le service.

## Contact

Si vous avez des questions sur les présentes conditions générales, vous pouvez nous contacter :

- Par email : <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- En visitant cette page sur notre site internet : <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
