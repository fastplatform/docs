# Politique de confidentialité

_Dernière mise à jour le 17 janvier 2022_

La présente Politique de confidentialité décrit Nos politiques et procédures en matière de collecte, d'utilisation et de divulgation de Vos informations lorsque Vous utilisez le Service et Vous informe de Vos droits en matière de confidentialité et de la manière dont la loi Vous protège.

Nous utilisons Vos données personnelles pour fournir et améliorer le Service. En utilisant le Service, vous acceptez la collecte et l'utilisation des informations conformément à la présente Politique de confidentialité.

## Interprétation et définitions

### Interprétation

Les mots dont la lettre initiale est en majuscule ont des significations définies dans les conditions suivantes. Les définitions suivantes ont le même sens, qu'elles apparaissent au singulier ou au pluriel.

### Définitions

Aux fins de la présente politique de confidentialité :

- **Compte** désigne un compte unique créé pour Vous afin d'accéder à notre Service ou à certaines parties de notre Service.

- **Affilié** désigne une entité qui contrôle, est contrôlée par ou est sous contrôle commun avec une partie, où "contrôle" signifie la propriété de 50% ou plus des actions, de la participation au capital ou d'autres titres donnant droit de vote pour l'élection des administrateurs ou autre autorité de gestion.

- **Application** désigne le programme logiciel fourni par la Commission européenne que vous avez téléchargé sur tout appareil électronique, dénommé FaST

- **Commission européenne** (désignée par "la Commission européenne", "Nous", "Notre" ou "Nos" dans le présent Accord) désigne la Commission européenne, DG DEFIS, Unité I3 Données spatiales pour les défis sociétaux et la croissance, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgique

- **GDPR** désigne le règlement général sur la protection des données

- **CAP Paying Agency** désigne l'organisme payeur :
    - Pour les utilisateurs d'Andalousie : Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España.
    - Pour les utilisateurs de Castilla y Leon : Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Pour les utilisateurs du Piémont : Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia.
    - Pour les utilisateurs estoniens : Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti.
    - Pour les utilisateurs de Wallonie : Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgique
    - Pour les utilisateurs grecs : ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Pour les utilisateurs bulgares : Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България
    - Pour les utilisateurs slovaques : Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko.
    - Pour les utilisateurs roumains : Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- **Contrôleur de données** (désigné soit par "Nous", "Notre" ou "Nos" dans le présent Accord), aux fins du GDPR, désigne la Commission européenne, en tant qu'entité juridique qui détermine les objectifs et les moyens du traitement des Données personnelles.

- **Appareil** désigne tout appareil permettant d'accéder au Service, tel qu'un ordinateur, un téléphone portable ou une tablette numérique.

- **Données Personnelles** désigne toute information qui se rapporte à un individu identifié ou identifiable. Aux fins du GDPR, les Données Personnelles désignent toute information vous concernant telle qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne ou à un ou plusieurs facteurs spécifiques à l'identité physique, physiologique, génétique, mentale, économique, culturelle ou sociale.

- **Service** désigne l'Application.

- **Fournisseur de service** désigne toute personne physique ou morale qui traite les données pour le compte de la Commission européenne. Il s'agit de sociétés tierces ou de personnes employées par la Commission européenne ou l'Organisme payeur pour faciliter le Service, pour fournir le Service au nom de la Commission européenne, pour effectuer des services liés au Service ou pour aider la Commission européenne et l'Organisme payeur à analyser la façon dont le Service est utilisé. Aux fins du GDPR, les fournisseurs de services sont considérés comme des processeurs de données.

- **Service d'authentification tiers** désigne tout service par lequel un Utilisateur peut se connecter ou créer un compte pour utiliser le Service.

- **Service d'opt-in tiers** désigne tout service externe qui a été enregistré sur le Service et auquel vous pouvez vous inscrire.

- **Métriques d'utilisation** désigne les données collectées automatiquement, soit générées par l'utilisation du Service, soit par l'infrastructure du Service elle-même (par exemple, la durée de la visite d'une page).

- **Vous** désigne la personne physique accédant ou utilisant le Service, ou la société, ou toute autre entité juridique pour le compte de laquelle cette personne physique accède ou utilise le Service, selon le cas. Dans le cadre du RGPD (Règlement général sur la protection des données), Vous pouvez être désigné comme la Personne concernée ou comme l'Utilisateur car vous êtes la personne physique qui utilise le Service.

### Collecte et utilisation de vos données personnelles

### Types de données collectées

#### Données personnelles

Lors de l'utilisation de notre Service, nous pouvons vous demander de nous fournir certaines informations personnellement identifiables qui peuvent être utilisées pour vous contacter ou vous identifier. Les informations personnellement identifiables peuvent inclure, mais ne sont pas limitées à :

- l'adresse électronique
- prénom et nom de famille
- le numéro de téléphone
- adresse, état, province, code postal, ville.
- les messages et pièces jointes que vous échangez avec d'autres utilisateurs (y compris avec votre organisme payeur CAP)
- photos géolocalisées que vous échangez sur la plateforme (y compris avec votre organisme payeur CAP)
- description de votre exploitation (par exemple, parcelles, cultures), pratiques agricoles (par exemple, recommandations de fertilisation que vous recevez), données sur les sols que vous saisissez dans le service.
- paramètres d'utilisation (voir ci-dessous)

#### Paramètres d'utilisation

Les mesures d'utilisation sont recueillies automatiquement lors de l'utilisation du service.

Les mesures d'utilisation peuvent inclure des informations telles que l'adresse de protocole Internet (par exemple, l'adresse IP) de votre appareil, le type de navigateur, la version du navigateur, les pages de notre service que vous visitez, l'heure et la date de votre visite, le temps passé sur ces pages, les identifiants uniques de l'appareil et d'autres données de diagnostic.

Lorsque vous accédez au Service par ou via un appareil mobile, nous pouvons recueillir automatiquement certaines informations, y compris, mais sans s'y limiter, le type d'appareil mobile que vous utilisez, l'identifiant unique de votre appareil mobile, l'adresse IP de votre appareil mobile, votre système d'exploitation mobile, le type de navigateur Internet mobile que vous utilisez, les identifiants uniques des appareils et d'autres données de diagnostic.

Nous pouvons également collecter les informations que votre navigateur envoie chaque fois que vous visitez notre Service ou lorsque vous accédez au Service par ou via un appareil mobile.

#### Informations collectées lors de l'utilisation de l'application

Lors de l'utilisation de notre Application, Nous pouvons collecter, avec votre autorisation préalable :

- Des informations concernant votre localisation.
- Des images et autres informations provenant de l'appareil photo et de la photothèque de votre Appareil.

Nous utilisons ces informations pour fournir la fonctionnalité de photos géolocalisées de Notre Service, et pour vous localiser sur nos cartes satellites. Ces informations peuvent être téléchargées sur nos serveurs et/ou sur le serveur d'un fournisseur de services ou être simplement stockées sur votre appareil.

Vous pouvez activer ou désactiver l'accès à ces informations à tout moment, via les paramètres de votre appareil.

### Utilisation de vos données personnelles

Notre service est actuellement en phase bêta et, par conséquent, vos Données personnelles ne seront utilisées que pour **développer, améliorer et tester notre Service**.

Pendant votre utilisation du Service, Nous pouvons Vous contacter, par email, appels téléphoniques, SMS, ou autres formes équivalentes de communication électronique, telles que les notifications push d'une application mobile concernant des mises à jour ou des communications informatives liées aux fonctionnalités, améliorations ou nouveautés de nos Services, y compris les mises à jour de sécurité, lorsque cela est nécessaire ou raisonnable pour leur mise en œuvre. Nous conserverons également Vos Données Personnelles pour assister et gérer les demandes que Vous Nous adressez.

### Conservation de vos données personnelles

Nous ne conserverons vos Données personnelles que pendant la durée nécessaire aux fins énoncées dans la présente Politique de confidentialité. Nous conserverons et utiliserons Vos Données Personnelles dans la mesure nécessaire pour nous conformer à nos obligations légales (par exemple, si nous sommes tenus de conserver vos données pour nous conformer aux lois applicables), résoudre les litiges et appliquer nos accords et politiques juridiques.

Nous conserverons également les mesures d'utilisation à des fins d'analyse interne. Les mesures d'utilisation sont généralement conservées pendant une période plus courte, sauf si ces données sont utilisées pour renforcer la sécurité ou améliorer la fonctionnalité de notre service, ou si nous sommes légalement tenus de conserver ces données pendant des périodes plus longues.

### Transfert de vos données personnelles

Vos informations, y compris les Données personnelles, sont traitées dans Nos bureaux d'exploitation et dans tout autre lieu où se trouvent les parties impliquées dans le traitement. Cela signifie que ces informations peuvent être transférées vers - et conservées sur - des ordinateurs situés en dehors de Votre état, province, pays ou autre juridiction gouvernementale où les lois de protection des données peuvent différer de celles de Votre juridiction.

Votre consentement à la présente politique de confidentialité, suivi de votre soumission de ces informations, représente votre accord à ce transfert.

Nous prendrons toutes les mesures raisonnablement nécessaires pour garantir que Vos données sont traitées en toute sécurité et conformément à la présente Politique de confidentialité et aucun transfert de Vos données personnelles n'aura lieu vers une organisation ou un pays à moins qu'il n'existe des contrôles adéquats en place, y compris la sécurité de Vos données et autres informations personnelles.

### Divulgation de vos données personnelles

#### Application de la loi

Dans certaines circonstances, le Nous pouvons être tenus de divulguer Vos Données Personnelles si la loi l'exige ou en réponse à des demandes valables des autorités publiques (par exemple, un tribunal ou une agence gouvernementale).

#### Autres exigences légales

Nous pouvons divulguer Vos Données personnelles en croyant de bonne foi qu'une telle action est nécessaire pour :

- se conformer à une obligation légale
- protéger et défendre les droits ou la propriété de la Commission européenne
- prévenir ou enquêter sur d'éventuels actes répréhensibles en rapport avec le Service
- protéger la sécurité personnelle des utilisateurs du Service ou du public
- se protéger contre toute responsabilité juridique.

### Sécurité de vos données personnelles

La sécurité de Vos Données Personnelles est importante pour Nous, mais n'oubliez pas qu'aucune méthode de transmission sur Internet, ou méthode de stockage électronique n'est sûre à 100%. Bien que Nous nous efforcions d'utiliser des moyens commercialement acceptables pour protéger Vos Données Personnelles, Nous ne pouvons pas garantir leur sécurité absolue.

## GDPR Privacy

### Fondement juridique du traitement des données personnelles en vertu du GDPR

Nous pouvons traiter les Données personnelles dans les conditions suivantes :

- **Consentement:** Vous avez donné Votre consentement au traitement des Données personnelles pour une ou plusieurs finalités spécifiques.

- **Exécution d'un contrat:** La fourniture de Données personnelles est nécessaire à l'exécution d'un contrat avec Vous et/ou à toute obligation précontractuelle de celui-ci.

- **Obligations légales:** Le traitement des Données personnelles est nécessaire au respect d'une obligation légale à laquelle la Commission européenne est soumise.

- Intérêts vitaux:** Le traitement des Données Personnelles est nécessaire à la protection de Vos intérêts vitaux ou de ceux d'une autre personne physique.

- Intérêts publics : ** Le traitement des données personnelles est lié à une tâche effectuée dans l'intérêt public ou dans l'exercice de l'autorité officielle dont est investie la Commission européenne.

- **Intérêts légitimes:** Le traitement des Données à caractère personnel est nécessaire aux fins des intérêts légitimes poursuivis par la Commission européenne.

Dans tous les cas, Nous aiderons volontiers à clarifier la base juridique spécifique qui s'applique au traitement, et en particulier si la fourniture de Données personnelles est une exigence légale ou contractuelle, ou une exigence nécessaire pour conclure un contrat.

### Vos droits en vertu du GDPR

La Commission européenne s'engage à respecter la confidentialité de Vos Données personnelles et à Vous garantir l'exercice de Vos droits.

Vous avez le droit, en vertu de la présente politique de confidentialité et de la législation de l'UE, de :

- **Demander l'accès à Vos Données Personnelles.** Le droit d'accéder, de mettre à jour ou de supprimer les informations que Nous possédons sur Vous. Dans la mesure du possible, Vous pouvez accéder, mettre à jour ou demander la suppression de Vos Données Personnelles directement dans la section des paramètres de Votre compte. Si vous n'êtes pas en mesure d'effectuer ces actions vous-même, veuillez nous contacter pour vous aider. Cela vous permet également de recevoir une copie des données personnelles que nous détenons à votre sujet.

- Vous avez le droit de faire corriger les informations incomplètes ou inexactes que nous détenons à votre sujet.

- Ce droit existe lorsque nous nous fondons sur un intérêt légitime comme base juridique de notre traitement et qu'un élément de votre situation particulière vous incite à vous opposer au traitement de vos données personnelles pour ce motif. Vous avez également le droit de vous opposer lorsque Nous traitons Vos Données personnelles à des fins de marketing direct.

- Vous avez le droit de nous demander d'effacer ou de supprimer des données personnelles lorsque nous n'avons aucune raison valable de continuer à les traiter.

- Nous vous fournirons, ou à un tiers que vous aurez choisi, vos données personnelles dans un format structuré, couramment utilisé et lisible par machine. Veuillez noter que ce droit ne s'applique qu'aux informations automatisées que vous avez initialement consenties à ce que nous utilisions ou lorsque nous avons utilisé ces informations pour exécuter un contrat avec vous.

- Vous avez le droit de retirer votre consentement à l'utilisation de vos données personnelles. Si Vous retirez Votre consentement, Nous pourrions ne pas être en mesure de Vous fournir l'accès à certaines fonctionnalités spécifiques du Service.

### Exercice de vos droits de protection des données au titre du GDPR

Vous pouvez exercer Vos droits d'accès, de rectification, d'annulation et d'opposition en Nous contactant. Veuillez noter que nous pouvons Vous demander de vérifier votre identité avant de répondre à ces demandes. Si Vous faites une demande, Nous ferons de notre mieux pour Vous répondre dans les meilleurs délais.

Vous avez le droit de déposer une plainte auprès d'une autorité de protection des données concernant notre collecte et notre utilisation de vos données personnelles. Pour plus d'informations, veuillez contacter votre autorité locale de protection des données dans l'Espace économique européen.

## Liens vers d'autres sites Web et services

Notre Service peut contenir des liens vers d'autres sites Web ou services qui ne sont pas exploités par Nous. Si vous cliquez sur un lien tiers, vous serez dirigé vers le site de ce tiers. Nous vous conseillons vivement d'examiner la politique de confidentialité de chaque site que vous visitez.

Nous n'avons aucun contrôle et n'assumons aucune responsabilité quant au contenu, aux politiques de confidentialité ou aux pratiques de tout site ou service tiers.

## Modifications de la présente politique de confidentialité

Nous pouvons mettre à jour notre politique de confidentialité de temps à autre. Nous vous informerons de toute modification en publiant la nouvelle politique de confidentialité sur cette page.

Nous vous informerons par e-mail et/ou par un avis bien visible sur notre service, avant que la modification n'entre en vigueur et que la date de " dernière mise à jour " ne soit mise à jour en haut de la présente politique de confidentialité.

Nous vous conseillons de consulter régulièrement la présente politique de confidentialité pour prendre connaissance des modifications éventuelles. Les modifications apportées à la présente politique de confidentialité prennent effet lorsqu'elles sont publiées sur cette page.

## Nous contacter

Si vous avez des questions sur cette politique de confidentialité, vous pouvez nous contacter :

- Par courriel : <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- En visitant cette page sur notre site web : <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
