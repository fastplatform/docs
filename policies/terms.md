# Terms and Conditions

_Last updated on January 17, 2022_

Please read these terms and conditions carefully before using Our Service.

## Interpretation and Definitions

### Interpretation

The words of which the initial letter is capitalized have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or in plural.

### Definitions

For the purposes of these Terms and Conditions:

- **Application** means the software program provided by the European Commission downloaded by You on any electronic device, named FaST

- **Application Store** means the digital distribution service operated and developed by Apple Inc. (Apple App Store) or Google Inc. (Google Play Store) in which the Application has been downloaded.

- **Affiliate** means an entity that controls, is controlled by or is under common control with a party, where "control" means ownership of 50% or more of the shares, equity interest or other securities entitled to vote for election of directors or other managing authority.

- **Account** means a unique account created for You to access our Service or parts of our Service.

- **CAP Paying Agency** refers to:
    - For Andalucia users: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla  s/n, 41071-Sevilla, España
    - For Castilla y Leon users: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - For Piemonte users: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - For Estonia users: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - For Wallonia users: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgique
    - For Greece users: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - For Bulgaria users: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - For Slovakia users: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - For Romania users: Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, Bucureşti, România

- **European Commission** (referred to as either "the European Commission", "We", "Us" or "Our" in this Agreement) refers to the European European Commission, DG DEFIS, Unit I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgium

- **Content** refers to content such as text, images, or other information that can be posted, uploaded, linked to or otherwise made available by You, regardless of the form of that content.

- **Device** means any device that can access the Service such as a computer, a cellphone or a digital tablet.

- **Feedback** means feedback, innovations or suggestions sent by You regarding the attributes, performance or features of our Service.

- **Service** refers to the Application.

- **Terms and Conditions** (also referred as "Terms") mean these Terms and Conditions that form the entire agreement between You and the European Commission regarding the use of the Service.

- **Third-party Authentication Service** means any user authentication service provided by a third-party (such as a National Identity Provider from a European Union Member State) to whom the European Commission delegates user authentication to access the Application

- **Third-party Opt-In Service** means any services or content (including data, information, products or services) provided by a third-party that may be displayed, included or made available by the Service.

- **You** means the individual accessing or using the Service, or the company, or other legal entity on behalf of which such individual is accessing or using the Service, as applicable.

## Acknowledgment

These are the Terms and Conditions governing the use of this Service and the agreement that operates between You and Us. These Terms and Conditions set out the rights and obligations of all users regarding the use of the Service.

Your access to and use of the Service is conditioned on Your acceptance of and compliance with these Terms and Conditions. These Terms and Conditions apply to all visitors, users and others who access or use the Service.

By accessing or using the Service You agree to be bound by these Terms and Conditions. If You disagree with any part of these Terms and Conditions then You may not access the Service.

You represent that You are over the age of 18. The European Commission does not permit those under 18 to use the Service.

Your access to and use of the Service is also conditioned on Your acceptance of and compliance with the Privacy Policy of the European Commission. Our Privacy Policy describes Our policies and procedures on the collection, use and disclosure of Your personal information when You use the Application or the Website and tells You about Your privacy rights and how the law protects You. Please read Our Privacy Policy carefully before using Our Service.

## User Accounts

When You create an account with Us, You must provide Us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of Your account on Our Service.

You are responsible for safeguarding the password that You use to access the Service and for any activities or actions under Your password, whether Your password is with Our Service or a Third-Party Authentication Service.
You agree not to disclose Your password to any third party. You must notify Us immediately upon becoming aware of any breach of security or unauthorized use of Your account.

You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than You without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.

## Content

### Your Right to Upload Content

Our Service allows You to upload Content (e.g. geotagged photos). You are responsible for the Content that You post to the Service, including its legality, reliability, and appropriateness.

You represent and warrant that: (i) the Content is Yours (You own it) or You have the right to use it and grant Us the rights and license as provided in these Terms, and (ii) the posting of Your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person.

### Content Restrictions

The European Commission is not responsible for the content of the Service's users. You expressly understand and agree that You are solely responsible for the Content and for all activity that occurs under your account, whether done so by You or any third person using Your account.

You may not transmit any Content that is unlawful, offensive, upsetting, intended to disgust, threatening, libelous, defamatory, obscene or otherwise objectionable. Examples of such objectionable Content include, but are not limited to, the following:

- Unlawful or promoting unlawful activity.
- Defamatory, discriminatory, or mean-spirited content, including references or commentary about religion, race, sexual orientation, gender, national/ethnic origin, or other targeted groups.
- Spam, machine – or randomly – generated, constituting unauthorized or unsolicited advertising, chain letters, any other form of unauthorized solicitation, or any form of lottery or gambling.
- Containing or installing any viruses, worms, malware, trojan horses, or other content that is designed or intended to disrupt, damage, or limit the functioning of any software, hardware or telecommunications equipment or to damage or obtain unauthorized access to any data or other information of a third person.
- Infringing on any proprietary rights of any party, including patent, trademark, trade secret, copyright, right of publicity or other rights.
- Impersonating any person or entity including the European Commission and its employees or representatives.
- Violating the privacy of any third person.
- False information and features.

The European Commission reserves the right, but not the obligation, to, in its sole discretion, determine whether or not any Content is appropriate and complies with this Terms, refuse or remove this Content. The European Commission further reserves the right to make formatting and edits and change the manner any Content. The European Commission can also limit or revoke the use of the Service if You post such objectionable Content.

As the European Commission cannot control all content posted by users and/or third parties on the Service, You agree to use the Service at your own risk. You understand that by using the Service You may be exposed to content that You may find offensive, indecent, incorrect or objectionable, and You agree that under no circumstances will the European Commission be liable in any way for any content, including any errors or omissions in any content, or any loss or damage of any kind incurred as a result of your use of any content.

### Content Backups

Although regular backups of Content are performed, the European Commission do not guarantee there will be no loss or corruption of data.

Corrupt or invalid backup points may be caused by, without limitation, Content that is corrupted prior to being backed up or that changes during the time a backup is performed.

The European Commission will provide support and attempt to troubleshoot any known or discovered issues that may affect the backups of Content. But You acknowledge that the European Commission has no liability related to the integrity of Content or the failure to successfully restore Content to a usable state.

You agree to maintain a complete and accurate copy of any Content in a location independent of the Service.

## Copyright Policy

### Intellectual Property Infringement

We respect the intellectual property rights of others. It is Our policy to respond to any claim that Content posted on the Service infringes a copyright or other intellectual property infringement of any person.

If You are a copyright owner, or authorized on behalf of one, and You believe that the copyrighted work has been copied in a way that constitutes copyright infringement that is taking place through the Service, You must submit Your notice in writing to the attention of our copyright agent via email at contact@fastplatform.eu and include in Your notice a detailed description of the alleged infringement.

You may be held accountable for damages (including costs and attorneys' fees) for misrepresenting that any Content is infringing Your copyright.

## Your Feedback to Us

You assign all rights, title and interest in any Feedback You provide the European Commission. If for any reason such assignment is ineffective, You agree to grant the European Commission a non-exclusive, perpetual, irrevocable, royalty free, worldwide right and license to use, reproduce, disclose, sub-license, distribute, modify and exploit such Feedback without restriction.

## Links to Other Websites

Our Service may contain links to third-party web sites or services that are not owned or controlled by the European Commission.

The European Commission has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third-party web sites or services. You further acknowledge and agree that the European Commission shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such web sites or services.

We strongly advise You to read the terms and conditions and privacy policies of any third-party web sites or services that You visit.

## Termination

We may terminate or suspend Your Account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if You breach these Terms and Conditions.

Upon termination, Your right to use the Service will cease immediately. If You wish to terminate Your Account, You may simply discontinue using the Service.

## Limitation of Liability

Notwithstanding any damages that You might incur, the entire liability of the European Commission and any of its suppliers under any provision of this Terms and Your exclusive remedy for all of the foregoing shall be limited to the amount actually paid by You through the Service or 100 USD if You haven't purchased anything through the Service.

To the maximum extent permitted by applicable law, in no event shall the European Commission or its suppliers be liable for any special, incidental, indirect, or consequential damages whatsoever (including, but not limited to, damages for loss of profits, loss of data or other information, for business interruption, for personal injury, loss of privacy arising out of or in any way related to the use of or inability to use the Service, third-party software and/or third-party hardware used with the Service, or otherwise in connection with any provision of this Terms), even if the European Commission or any supplier has been advised of the possibility of such damages and even if the remedy fails of its essential purpose.

Some states do not allow the exclusion of implied warranties or limitation of liability for incidental or consequential damages, which means that some of the above limitations may not apply. In these states, each party's liability will be limited to the greatest extent permitted by law.

## "AS IS" and "AS AVAILABLE" Disclaimer

The Service is provided to You "AS IS" and "AS AVAILABLE" and with all faults and defects without warranty of any kind. To the maximum extent permitted under applicable law, the European Commission, on its own behalf and on behalf of its Affiliates and its and their respective licensors and service providers, expressly disclaims all warranties, whether express, implied, statutory or otherwise, with respect to the Service, including all implied warranties of merchantability, fitness for a particular purpose, title and non-infringement, and warranties that may arise out of course of dealing, course of performance, usage or trade practice. Without limitation to the foregoing, the European Commission provides no warranty or undertaking, and makes no representation of any kind that the Service will meet Your requirements, achieve any intended results, be compatible or work with any other software, applications, systems or services, operate without interruption, meet any performance or reliability standards or be error free or that any errors or defects can or will be corrected.

Without limiting the foregoing, neither the European Commission nor any of the European Commission's provider makes any representation or warranty of any kind, express or implied: (i) as to the operation or availability of the Service, or the information, content, and materials or products included thereon; (ii) that the Service will be uninterrupted or error-free; (iii) as to the accuracy, reliability, or currency of any information or content provided through the Service; or (iv) that the Service, its servers, the content, or e-mails sent from or on behalf of the European Commission are free of viruses, scripts, trojan horses, worms, malware, timebombs or other harmful components.

Some jurisdictions do not allow the exclusion of certain types of warranties or limitations on applicable statutory rights of a consumer, so some or all of the above exclusions and limitations may not apply to You. But in such a case the exclusions and limitations set forth in this section shall be applied to the greatest extent enforceable under applicable law.

## Governing Law

The laws of the European Union, supplemented if necessary by the law of Belgium, shall govern this Terms and Your use of the Service. Your use of the Application may also be subject to other local, state, national, or international laws.

## Disputes Resolution

If You have any concern or dispute about the Service, You agree to first try to resolve the dispute informally by contacting the European Commission.

## For European Union (EU) Users

If You are a European Union consumer, You will benefit from any mandatory provisions of the law of the country in which You are resident in.

## Severability and Waiver

### Severability

If any provision of these Terms is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions will continue in full force and effect.

### Waiver

Except as provided herein, the failure to exercise a right or to require performance of an obligation under this Terms shall not effect a party's ability to exercise such right or require such performance at any time thereafter nor shall be the waiver of a breach constitute a waiver of any subsequent breach.

## Translation Interpretation

These Terms and Conditions may have been translated if We have made them available to You on our Service.

You agree that the original English text shall prevail in the case of a dispute.

## Changes to These Terms and Conditions

We reserve the right, at Our sole discretion, to modify or replace these Terms at any time. If a revision is material We will make reasonable efforts to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at Our sole discretion.

By continuing to access or use Our Service after those revisions become effective, You agree to be bound by the revised terms. If You do not agree to the new terms, in whole or in part, please stop using the website and the Service.

## Contact Us

If you have any questions about these Terms and Conditions, You can contact us:

- By email: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- By visiting this page on our website: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
