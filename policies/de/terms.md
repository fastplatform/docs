# Bedingungen und Konditionen

_Zuletzt aktualisiert am 17. Januar 2022._

Bitte lesen Sie diese Bedingungen und Konditionen sorgfältig durch, bevor Sie unseren Service nutzen.

## Auslegung und Definitionen

### Auslegung

Die Wörter, deren Anfangsbuchstaben groß geschrieben sind, haben die unter den folgenden Bedingungen definierte Bedeutung. Die folgenden Definitionen haben dieselbe Bedeutung, unabhängig davon, ob sie im Singular oder im Plural stehen.

### Definitionen

Für die Zwecke dieser Bedingungen:

- **Anwendung** bezeichnet das von der Europäischen Kommission bereitgestellte Softwareprogramm, das Sie auf ein beliebiges elektronisches Gerät mit der Bezeichnung FaST herunterladen.

- Application Store** bezeichnet den von Apple Inc. betriebenen und entwickelten digitalen Vertriebsdienst. (Apple App Store) oder Google Inc. (Google Play Store) betriebenen und entwickelten digitalen Vertriebsdienst, über den die Anwendung heruntergeladen wurde.

- Verbundenes Unternehmen** bezeichnet ein Unternehmen, das eine Partei beherrscht, von ihr beherrscht wird oder mit ihr unter gemeinsamer Beherrschung steht, wobei "Beherrschung" den Besitz von 50 % oder mehr der Aktien, Kapitalanteile oder anderer Wertpapiere bedeutet, die zur Wahl von Direktoren oder anderen leitenden Angestellten berechtigt sind.

- **Konto** bezeichnet ein einzigartiges Konto, das für Sie erstellt wurde, um auf unseren Service oder Teile unseres Service zuzugreifen.

- **CAP-Zahlstelle** bezieht sich auf:
    - Für Andalusien-Nutzer: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Für Nutzer aus Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Für piemontesische Nutzer: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Für estnische Nutzer: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Für wallonische Nutzer: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgien
    - Für griechische Nutzer: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Für bulgarische Nutzer: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Für Benutzer aus der Slowakei: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Für rumänische Nutzer: Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, Bucureşti, România

- **Europäische Kommission** (in dieser Vereinbarung entweder als "die Europäische Kommission", "wir", "uns" oder "unser" bezeichnet) bezieht sich auf die Europäische Kommission, GD DEFIS, Referat I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgien

- **Inhalt** bezieht sich auf Inhalte wie Text, Bilder oder andere Informationen, die von Ihnen veröffentlicht, hochgeladen, verlinkt oder anderweitig verfügbar gemacht werden können, unabhängig von der Form dieses Inhalts.

- Gerät** bezeichnet jedes Gerät, das auf den Dienst zugreifen kann, wie z.B. ein Computer, ein Mobiltelefon oder ein digitales Tablet.

- **Feedback** bezeichnet Feedback, Innovationen oder Vorschläge, die von Ihnen in Bezug auf die Eigenschaften, Leistung oder Funktionen unseres Dienstes gesendet werden.

- **Dienst** bezieht sich auf die Anwendung.

- **Nutzungsbedingungen** (auch als "Bedingungen" bezeichnet) bezeichnet diese Bedingungen, die die gesamte Vereinbarung zwischen Ihnen und der Europäischen Kommission bezüglich der Nutzung des Dienstes bilden.

- Drittanbieter-Authentifizierungsdienst** bezeichnet jeden Benutzerauthentifizierungsdienst, der von einem Drittanbieter (z. B. einem nationalen Identitätsanbieter aus einem Mitgliedstaat der Europäischen Union) bereitgestellt wird, an den die Europäische Kommission die Benutzerauthentifizierung für den Zugriff auf die Anwendung delegiert.

- Opt-In-Dienst eines Dritten** bezeichnet alle Dienste oder Inhalte (einschließlich Daten, Informationen, Produkte oder Dienstleistungen), die von einem Dritten bereitgestellt werden und durch den Dienst angezeigt, einbezogen oder verfügbar gemacht werden können.

- **Sie** bezeichnet die Person, die auf den Dienst zugreift oder ihn nutzt, oder das Unternehmen oder eine andere juristische Person, in deren Namen eine solche Person auf den Dienst zugreift oder ihn nutzt.

*# Anerkennung

Dies sind die Allgemeinen Geschäftsbedingungen, die die Nutzung dieses Dienstes regeln, und die Vereinbarung, die zwischen Ihnen und uns besteht. Diese Allgemeinen Geschäftsbedingungen legen die Rechte und Pflichten aller Nutzer in Bezug auf die Nutzung des Dienstes fest.

Ihr Zugang zu und Ihre Nutzung des Dienstes setzt voraus, dass Sie diese Bedingungen akzeptieren und einhalten. Diese Allgemeinen Geschäftsbedingungen gelten für alle Besucher, Benutzer und andere Personen, die auf den Dienst zugreifen oder ihn nutzen.

Indem Sie auf den Dienst zugreifen oder ihn nutzen, erklären Sie sich mit den vorliegenden Bedingungen einverstanden. Wenn Sie mit einem Teil dieser Bedingungen nicht einverstanden sind, dürfen Sie den Dienst nicht nutzen.

Sie versichern, dass Sie über 18 Jahre alt sind. Die Europäische Kommission gestattet Personen unter 18 Jahren nicht die Nutzung des Dienstes.

Ihr Zugang zu und Ihre Nutzung des Dienstes setzt außerdem voraus, dass Sie die Datenschutzbestimmungen der Europäischen Kommission akzeptieren und einhalten. Unsere Datenschutzrichtlinie beschreibt unsere Richtlinien und Verfahren für die Erfassung, Verwendung und Offenlegung Ihrer persönlichen Daten, wenn Sie die Anwendung oder die Website nutzen, und informiert Sie über Ihre Datenschutzrechte und darüber, wie das Gesetz Sie schützt. Bitte lesen Sie unsere Datenschutzrichtlinie sorgfältig durch, bevor Sie unseren Service nutzen.

## Benutzerkonten

Wenn Sie ein Konto bei uns erstellen, müssen Sie uns Informationen zur Verfügung stellen, die jederzeit korrekt, vollständig und aktuell sind. Wenn Sie dies nicht tun, stellt dies einen Verstoß gegen die Bedingungen dar, der zur sofortigen Kündigung Ihres Kontos bei Unserem Dienst führen kann.

Sie sind verantwortlich für den Schutz des Passworts, das Sie für den Zugriff auf den Dienst verwenden, sowie für alle Aktivitäten oder Handlungen unter Ihrem Passwort, unabhängig davon, ob Ihr Passwort bei unserem Dienst oder einem Authentifizierungsdienst eines Drittanbieters verwendet wird.
Sie verpflichten sich, Ihr Passwort nicht an Dritte weiterzugeben. Sie müssen uns sofort benachrichtigen, wenn Sie von einer Verletzung der Sicherheit oder einer unbefugten Nutzung Ihres Kontos erfahren.

Es ist Ihnen nicht gestattet, als Benutzernamen den Namen einer anderen natürlichen oder juristischen Person oder einen Namen oder eine Marke zu verwenden, der/die ohne entsprechende Genehmigung den Rechten einer anderen natürlichen oder juristischen Person als Ihnen unterliegt, oder einen Namen, der anderweitig anstößig, vulgär oder obszön ist.

## Inhalt

### Ihr Recht zum Hochladen von Inhalten

Unser Service erlaubt es Ihnen, Inhalte hochzuladen (z.B. Fotos mit Geotags). Sie sind für die Inhalte verantwortlich, die Sie in den Dienst einstellen, einschließlich ihrer Rechtmäßigkeit, Zuverlässigkeit und Angemessenheit.

Sie erklären und garantieren, dass: (i) der Inhalt Ihr Eigentum ist oder Sie das Recht haben, ihn zu nutzen und uns die in diesen Bedingungen vorgesehenen Rechte und Lizenzen zu gewähren, und (ii) die Veröffentlichung Ihres Inhalts im oder über den Dienst nicht gegen die Datenschutzrechte, Veröffentlichungsrechte, Urheberrechte, Vertragsrechte oder andere Rechte einer Person verstößt.

### Inhaltsbeschränkungen

Die Europäische Kommission ist nicht für die Inhalte der Nutzer des Dienstes verantwortlich. Sie erklären sich ausdrücklich damit einverstanden, dass Sie allein für die Inhalte und alle Aktivitäten verantwortlich sind, die unter Ihrem Konto stattfinden, unabhängig davon, ob diese von Ihnen oder einer dritten Person, die Ihr Konto nutzt, durchgeführt werden.

Sie dürfen keine Inhalte übermitteln, die ungesetzlich, beleidigend, verärgernd, abstoßend, bedrohend, verleumderisch, diffamierend, obszön oder anderweitig anstößig sind. Beispiele für solche anstößigen Inhalte sind unter anderem die folgenden:

- Rechtswidrig oder die Förderung rechtswidriger Aktivitäten.
- Diffamierende, diskriminierende oder böswillige Inhalte, einschließlich Verweise oder Kommentare über Religion, Rasse, sexuelle Orientierung, Geschlecht, nationale/ethnische Herkunft oder andere Zielgruppen.
- Spam, maschinell oder zufällig generiert, unerlaubte oder unaufgeforderte Werbung, Kettenbriefe, jede andere Form von unerlaubter Aufforderung, oder jede Form von Lotterie oder Glücksspiel.
- Enthalten oder Installieren von Viren, Würmern, Malware, trojanischen Pferden oder anderen Inhalten, die dazu bestimmt sind, das Funktionieren von Software, Hardware oder Telekommunikationsgeräten zu stören, zu beschädigen oder einzuschränken oder Daten oder andere Informationen einer dritten Person zu beschädigen oder sich unbefugt Zugang zu ihnen zu verschaffen.
- Verletzung von Eigentumsrechten Dritter, einschließlich Patent-, Marken-, Geschäftsgeheimnis-, Urheber-, Veröffentlichungsrechten oder anderen Rechten.
- Nachahmung einer Person oder Einrichtung, einschließlich der Europäischen Kommission und ihrer Mitarbeiter oder Vertreter.
- Verletzung der Privatsphäre einer dritten Person.
- Falsche Informationen und Merkmale.

Die Europäische Kommission behält sich das Recht, aber nicht die Pflicht vor, nach eigenem Ermessen zu entscheiden, ob ein Inhalt angemessen ist und mit diesen Bedingungen übereinstimmt, diesen Inhalt abzulehnen oder zu entfernen. Die Europäische Kommission behält sich ferner das Recht vor, Formatierungen und Bearbeitungen vorzunehmen und die Art und Weise von Inhalten zu ändern. Die Europäische Kommission kann auch die Nutzung des Dienstes einschränken oder widerrufen, wenn Sie solche beanstandeten Inhalte einstellen.

Da die Europäische Kommission nicht alle von Nutzern und/oder Dritten in den Dienst eingestellten Inhalte kontrollieren kann, erklären Sie sich damit einverstanden, den Dienst auf eigenes Risiko zu nutzen. Sie sind sich darüber im Klaren, dass Sie durch die Nutzung des Dienstes mit Inhalten konfrontiert werden können, die Sie als beleidigend, unanständig, falsch oder anstößig empfinden, und Sie erklären sich damit einverstanden, dass die Europäische Kommission unter keinen Umständen für Inhalte, einschließlich etwaiger Fehler oder Auslassungen in Inhalten, oder für Verluste oder Schäden jeglicher Art, die durch die Nutzung von Inhalten entstehen, haftbar gemacht werden kann.

### Backups von Inhalten

Obwohl regelmäßige Backups der Inhalte durchgeführt werden, kann die Europäische Kommission nicht garantieren, dass keine Daten verloren gehen oder beschädigt werden.

Beschädigte oder ungültige Sicherungspunkte können unter anderem durch Inhalte verursacht werden, die vor der Sicherung beschädigt wurden oder die sich während der Durchführung einer Sicherung ändern.

Die Europäische Kommission wird Unterstützung leisten und versuchen, alle bekannten oder entdeckten Probleme zu beheben, die sich auf die Sicherungskopien der Inhalte auswirken können. Sie erkennen jedoch an, dass die Europäische Kommission keine Haftung für die Unversehrtheit der Inhalte oder für das Scheitern der erfolgreichen Wiederherstellung eines brauchbaren Zustands der Inhalte übernimmt.

Sie verpflichten sich, eine vollständige und genaue Kopie aller Inhalte an einem vom Dienst unabhängigen Ort aufzubewahren.

## Urheberrechtspolitik

### Verletzung von geistigem Eigentum

Wir respektieren die geistigen Eigentumsrechte anderer. Es gehört zu unseren Grundsätzen, auf jede Behauptung zu reagieren, dass im Dienst veröffentlichte Inhalte das Urheberrecht oder andere geistige Eigentumsrechte einer Person verletzen.

Wenn Sie ein Urheberrechtsinhaber sind oder im Namen eines solchen bevollmächtigt sind und glauben, dass Ihr urheberrechtlich geschütztes Werk in einer Weise kopiert wurde, die eine Urheberrechtsverletzung darstellt, die durch den Dienst erfolgt, müssen Sie Ihre Mitteilung schriftlich zu Händen unseres Urheberrechtsvertreters per E-Mail an contact@fastplatform.eu einreichen und in Ihrer Mitteilung eine detaillierte Beschreibung der angeblichen Verletzung enthalten.

Sie können für Schadenersatz (einschließlich Kosten und Anwaltsgebühren) haftbar gemacht werden, wenn Sie fälschlicherweise behaupten, dass ein Inhalt Ihr Urheberrecht verletzt.

## Ihr Feedback an uns

Sie treten alle Rechte, Titel und Interessen an allen Rückmeldungen ab, die Sie der Europäischen Kommission zur Verfügung stellen. Für den Fall, dass eine solche Abtretung aus irgendeinem Grund unwirksam sein sollte, erklären Sie sich damit einverstanden, der Europäischen Kommission ein nicht ausschließliches, unbefristetes, unwiderrufliches, gebührenfreies, weltweites Recht und eine Lizenz zur uneingeschränkten Nutzung, Vervielfältigung, Offenlegung, Unterlizenzierung, Verbreitung, Änderung und Verwertung solcher Rückmeldungen zu gewähren.

## Links zu anderen Websites

Unser Dienst kann Links zu Websites oder Diensten Dritter enthalten, die sich nicht im Besitz oder unter der Kontrolle der Europäischen Kommission befinden.

Die Europäische Kommission hat keine Kontrolle über den Inhalt, die Datenschutzrichtlinien oder die Praktiken von Websites oder Diensten Dritter und übernimmt keine Verantwortung für diese. Sie erkennen ferner an und erklären sich damit einverstanden, dass die Europäische Kommission weder direkt noch indirekt für Schäden oder Verluste verantwortlich oder haftbar ist, die durch oder in Verbindung mit der Nutzung von oder dem Vertrauen auf solche Inhalte, Waren oder Dienstleistungen, die auf oder über solche Websites oder Dienstleistungen verfügbar sind, verursacht wurden oder angeblich verursacht wurden.

Wir empfehlen Ihnen dringend, die Geschäftsbedingungen und Datenschutzrichtlinien der von Ihnen besuchten Websites oder Dienste Dritter zu lesen.

## Beendigung

Wir können Ihr Konto sofort und ohne vorherige Ankündigung oder Haftung kündigen oder aussetzen, aus welchem Grund auch immer, einschließlich und ohne Einschränkung, wenn Sie gegen diese Bedingungen und Konditionen verstoßen.

Bei einer Kündigung erlischt Ihr Recht, den Dienst zu nutzen, sofort. Wenn Sie Ihr Konto kündigen möchten, können Sie einfach die Nutzung des Dienstes einstellen.

## Haftungsbeschränkung

Ungeachtet etwaiger Schäden, die Ihnen entstehen könnten, ist die gesamte Haftung der Europäischen Kommission und ihrer Zulieferer gemäß den Bestimmungen dieser Bedingungen und Ihr ausschließlicher Rechtsbehelf in allen vorgenannten Fällen auf den Betrag beschränkt, den Sie tatsächlich über den Dienst bezahlt haben, oder auf 100 USD, wenn Sie nichts über den Dienst gekauft haben.

Soweit dies nach geltendem Recht zulässig ist, haften die Europäische Kommission oder ihre Zulieferer in keinem Fall für besondere, zufällige, indirekte oder Folgeschäden (einschließlich, aber nicht beschränkt auf Schäden aus entgangenem Gewinn, Verlust von Daten oder anderen Informationen, Geschäftsunterbrechung, Personenschäden, Verlust der Privatsphäre, die sich aus der Nutzung oder der Unmöglichkeit der Nutzung des Dienstes, der mit dem Dienst verwendeten Software und/oder Hardware Dritter oder in sonstiger Weise in Verbindung mit einer Bestimmung dieser Bedingungen ergeben), selbst wenn die Europäische Kommission oder ein Lieferant auf die Möglichkeit solcher Schäden hingewiesen wurde und selbst wenn die Abhilfe ihren wesentlichen Zweck verfehlt.

In einigen Staaten ist der Ausschluss stillschweigender Garantien oder die Beschränkung der Haftung für beiläufig entstandene Schäden oder Folgeschäden nicht zulässig, was bedeutet, dass einige der oben genannten Beschränkungen möglicherweise nicht gelten. In diesen Staaten ist die Haftung jeder Partei auf das gesetzlich zulässige Maß beschränkt.

## "AS IS" und "AS AVAILABLE" Haftungsausschluss

Der Dienst wird Ihnen "wie besehen" und "wie verfügbar" und mit allen Fehlern und Mängeln ohne jegliche Garantie zur Verfügung gestellt. Soweit dies nach geltendem Recht zulässig ist, lehnt die Europäische Kommission in ihrem eigenen Namen und im Namen ihrer verbundenen Unternehmen und ihrer jeweiligen Lizenzgeber und Dienstleistungsanbieter ausdrücklich alle ausdrücklichen, stillschweigenden, gesetzlichen oder sonstigen Gewährleistungen in Bezug auf den Dienst ab, einschließlich aller stillschweigenden Gewährleistungen der Marktgängigkeit, der Eignung für einen bestimmten Zweck, des Eigentumsrechts und der Nichtverletzung von Rechten Dritter sowie Gewährleistungen, die sich aus dem Geschäftsverlauf, der Leistung, den Gepflogenheiten oder der Handelspraxis ergeben können. Ohne Einschränkung des Vorstehenden übernimmt die Europäische Kommission keine Garantie oder Verpflichtung und gibt keinerlei Zusicherungen ab, dass der Dienst Ihren Anforderungen entspricht, die beabsichtigten Ergebnisse erzielt, mit anderer Software, Anwendungen, Systemen oder Diensten kompatibel ist oder zusammenarbeitet, ohne Unterbrechung funktioniert, Leistungs- oder Zuverlässigkeitsstandards erfüllt oder fehlerfrei ist oder dass Fehler oder Mängel behoben werden können oder werden.

Ohne das Vorstehende einzuschränken, gibt weder die Europäische Kommission noch einer ihrer Anbieter eine ausdrückliche oder stillschweigende Zusicherung oder Gewährleistung irgendeiner Art: (i) in Bezug auf den Betrieb oder die Verfügbarkeit des Dienstes oder der darin enthaltenen Informationen, Inhalte und Materialien oder Produkte; (ii) dass der Dienst ununterbrochen oder fehlerfrei funktioniert; (iii) in Bezug auf die Genauigkeit, Zuverlässigkeit oder Aktualität von Informationen oder Inhalten, die über den Dienst bereitgestellt werden; oder (iv) dass der Dienst, seine Server, die Inhalte oder E-Mails, die von der Europäischen Kommission oder in ihrem Namen versandt werden, frei von Viren, Skripten, trojanischen Pferden, Würmern, Malware, Zeitbomben oder anderen schädlichen Komponenten sind.

In einigen Rechtsordnungen ist der Ausschluss bestimmter Arten von Garantien oder die Einschränkung geltender gesetzlicher Rechte eines Verbrauchers nicht zulässig, so dass einige oder alle der oben genannten Ausschlüsse und Einschränkungen möglicherweise nicht für Sie gelten. In einem solchen Fall werden die in diesem Abschnitt dargelegten Ausschlüsse und Beschränkungen jedoch im größtmöglichen Umfang angewandt, der nach geltendem Recht durchsetzbar ist.

## Geltendes Recht

Für diese Bedingungen und Ihre Nutzung des Dienstes gilt das Recht der Europäischen Union, gegebenenfalls ergänzt durch das Recht Belgiens. Ihre Nutzung der Anwendung kann auch anderen lokalen, staatlichen, nationalen oder internationalen Gesetzen unterliegen.

## Beilegung von Streitigkeiten

Sollten Sie Bedenken oder Streitigkeiten bezüglich des Dienstes haben, erklären Sie sich damit einverstanden, zunächst zu versuchen, die Streitigkeit informell zu lösen, indem Sie sich an die Europäische Kommission wenden.

## Für Nutzer aus der Europäischen Union (EU)

Wenn Sie ein Verbraucher aus der Europäischen Union sind, gelten für Sie die zwingenden Bestimmungen des Rechts des Landes, in dem Sie Ihren Wohnsitz haben.

## Salvatorische Klausel und Verzicht

### Trennbarkeit

Sollte sich eine Bestimmung dieser Bedingungen als nicht durchsetzbar oder ungültig erweisen, wird diese Bestimmung so geändert und ausgelegt, dass die Ziele dieser Bestimmung so weit wie möglich unter dem anwendbaren Recht erreicht werden, und die übrigen Bestimmungen bleiben in vollem Umfang in Kraft und wirksam.

### Verzicht

Sofern hierin nichts anderes bestimmt ist, hat das Versäumnis, ein Recht auszuüben oder die Erfüllung einer Verpflichtung im Rahmen dieser Bedingungen zu verlangen, keine Auswirkung auf die Fähigkeit einer Partei, ein solches Recht auszuüben oder eine solche Erfüllung zu einem späteren Zeitpunkt zu verlangen, noch stellt der Verzicht auf einen Verstoß einen Verzicht auf einen späteren Verstoß dar.

## Übersetzung Interpretation

Diese Allgemeinen Geschäftsbedingungen können übersetzt worden sein, wenn wir sie Ihnen in unserem Service zur Verfügung gestellt haben.

Sie erklären sich damit einverstanden, dass im Falle eines Rechtsstreits der englische Originaltext maßgebend ist.

## Änderungen dieser Bedingungen und Konditionen

Wir behalten uns das Recht vor, diese Bedingungen jederzeit nach unserem alleinigen Ermessen zu ändern oder zu ersetzen. Wenn eine Änderung wesentlich ist, werden wir uns in angemessener Weise bemühen, Sie mindestens 30 Tage vor Inkrafttreten der neuen Bedingungen zu informieren. Was eine wesentliche Änderung darstellt, wird nach unserem alleinigen Ermessen festgelegt.

Indem Sie nach Inkrafttreten dieser Änderungen weiterhin auf unseren Dienst zugreifen oder ihn nutzen, erklären Sie sich mit den überarbeiteten Bedingungen einverstanden. Wenn Sie mit den neuen Bedingungen ganz oder teilweise nicht einverstanden sind, beenden Sie bitte die Nutzung der Website und des Dienstes.

## Kontakt

Wenn Sie Fragen zu diesen Geschäftsbedingungen haben, können Sie uns kontaktieren:

- Per E-Mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Durch den Besuch dieser Seite auf unserer Website: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
