# Datenschutzrichtlinie

_Zuletzt aktualisiert am 17. Januar 2022._

Diese Datenschutzrichtlinie beschreibt unsere Richtlinien und Verfahren für die Erfassung, Verwendung und Offenlegung Ihrer Daten bei der Nutzung des Dienstes und informiert Sie über Ihre Datenschutzrechte und wie das Gesetz Sie schützt.

Wir verwenden Ihre persönlichen Daten zur Bereitstellung und Verbesserung des Dienstes. Durch die Nutzung des Dienstes erklären Sie sich mit der Erfassung und Nutzung von Informationen in Übereinstimmung mit dieser Datenschutzrichtlinie einverstanden.

## Auslegung und Definitionen

### Auslegung

Die Wörter, deren Anfangsbuchstaben großgeschrieben sind, haben die unter den folgenden Bedingungen definierten Bedeutungen. Die folgenden Definitionen haben dieselbe Bedeutung, unabhängig davon, ob sie im Singular oder im Plural stehen.

### Definitionen

Für die Zwecke dieser Datenschutzrichtlinie:

- **Konto** bezeichnet ein einzigartiges Konto, das für Sie erstellt wurde, um auf unseren Dienst oder Teile unseres Dienstes zuzugreifen.

- Verbundenes Unternehmen** bedeutet eine Einheit, die eine Partei kontrolliert, von ihr kontrolliert wird oder unter gemeinsamer Kontrolle mit ihr steht, wobei "Kontrolle" den Besitz von 50 % oder mehr der Aktien, Anteile oder anderer Wertpapiere bedeutet, die zur Wahl von Direktoren oder anderen leitenden Angestellten berechtigt sind.

- Anwendung** bezeichnet das von der Europäischen Kommission zur Verfügung gestellte Softwareprogramm, das Sie auf ein beliebiges elektronisches Gerät mit dem Namen FaST herunterladen.

- **Europäische Kommission** (in dieser Vereinbarung entweder als "die Europäische Kommission", "wir", "uns" oder "unser" bezeichnet) bezieht sich auf die Europäische Kommission, GD DEFIS, Referat I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Brüssel, Belgien

- **GDPR** ist die Allgemeine Datenschutzverordnung

- **CAP-Zahlstelle** bezieht sich auf:
    - Für Nutzer aus Andalusien: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - Für Nutzer aus Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Für piemontesische Nutzer: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Für estnische Nutzer: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Für wallonische Nutzer: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgien
    - Für griechische Nutzer: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Für bulgarische Nutzer: Държавен фонд "Земеделие", София 1618, "Цар Борис III" 136, България
    - Für Benutzer aus der Slowakei: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - Für rumänische Nutzer: Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, Bucureşti, România

- **Datenverantwortlicher** (in dieser Vereinbarung entweder als "Wir", "Uns" oder "Unser" bezeichnet) bezieht sich für die Zwecke der DSGVO auf die Europäische Kommission als die juristische Person, die die Zwecke und Mittel der Verarbeitung personenbezogener Daten bestimmt.

- **Gerät** bezeichnet jedes Gerät, das auf den Dienst zugreifen kann, wie z. B. einen Computer, ein Mobiltelefon oder ein digitales Tablet.

- **Personenbezogene Daten** sind alle Informationen, die sich auf eine identifizierte oder identifizierbare Person beziehen. Für die Zwecke der DSGVO sind personenbezogene Daten alle Informationen, die sich auf Sie beziehen, wie z. B. ein Name, eine Identifikationsnummer, Standortdaten, Online-Kennungen oder ein oder mehrere Faktoren, die für die physische, physiologische, genetische, mentale, wirtschaftliche, kulturelle oder soziale Identität spezifisch sind.

- **Dienst** bezieht sich auf die Anwendung.

- Diensteanbieter** bezeichnet jede natürliche oder juristische Person, die die Daten im Auftrag der Europäischen Kommission verarbeitet. Er bezieht sich auf Drittunternehmen oder Einzelpersonen, die von der Europäischen Kommission oder der Zahlstelle beschäftigt werden, um den Dienst zu erleichtern, um den Dienst im Namen der Europäischen Kommission bereitzustellen, um Dienstleistungen im Zusammenhang mit dem Dienst zu erbringen oder um die Europäische Kommission und die Zahlstelle bei der Analyse der Nutzung des Dienstes zu unterstützen. Für die Zwecke der Datenschutz-Grundverordnung gelten Dienstanbieter als Datenverarbeiter.

- **Authentifizierungsdienst eines Dritten** bezieht sich auf jeden Dienst, über den sich ein Nutzer anmelden oder ein Konto erstellen kann, um den Dienst zu nutzen.

- Opt-In-Dienst eines Dritten** bezieht sich auf jeden externen Dienst, der im Dienst registriert wurde und für den Sie sich anmelden können.

- **Nutzungsmetriken** bezieht sich auf automatisch erfasste Daten, die entweder durch die Nutzung des Dienstes oder durch die Infrastruktur des Dienstes selbst erzeugt werden (z. B. die Dauer eines Seitenbesuchs).

- **Sie** bezeichnet die Person, die auf den Dienst zugreift oder ihn nutzt, bzw. das Unternehmen oder eine andere juristische Person, in deren Namen eine solche Person auf den Dienst zugreift oder ihn nutzt, sofern zutreffend. Gemäß der GDPR (General Data Protection Regulation) können Sie als Datensubjekt oder als Nutzer bezeichnet werden, da Sie die Person sind, die den Dienst nutzt.

## Erhebung und Verwendung Ihrer persönlichen Daten

### Arten der gesammelten Daten

#### Persönliche Daten

Während der Nutzung unseres Dienstes können wir Sie bitten, uns bestimmte personenbezogene Daten mitzuteilen, die dazu verwendet werden können, Sie zu kontaktieren oder zu identifizieren. Persönlich identifizierbare Informationen können unter anderem sein

- E-Mail Adresse
- Vorname und Nachname
- Telefonnummer
- Adresse, Bundesland, Provinz, Postleitzahl, Stadt
- Nachrichten und Anhänge, die Sie mit anderen Nutzern austauschen (auch mit Ihrer CAP-Zahlstelle)
- Mit Geotags versehene Fotos, die Sie auf der Plattform austauschen (auch mit Ihrer GAP-Zahlstelle)
- Beschreibung Ihres Betriebs (z. B. Parzellen, Kulturen), landwirtschaftliche Praktiken (z. B. Düngeempfehlungen, die Sie erhalten), Bodendaten, die Sie in den Dienst eingeben
- Nutzungsmetriken (siehe unten)

#### Nutzungsmetriken

Nutzungsmetriken werden bei der Nutzung des Dienstes automatisch erfasst.

Nutzungsmetriken können Informationen wie die Internetprotokolladresse Ihres Geräts (z. B. IP-Adresse), den Browsertyp, die Browserversion, die von Ihnen besuchten Seiten unseres Dienstes, die Uhrzeit und das Datum Ihres Besuchs, die auf diesen Seiten verbrachte Zeit, eindeutige Gerätekennungen und andere Diagnosedaten umfassen.

Wenn Sie mit oder über ein mobiles Gerät auf den Dienst zugreifen, können wir bestimmte Informationen automatisch erfassen, einschließlich, aber nicht beschränkt auf die Art des von Ihnen verwendeten mobilen Geräts, die eindeutige ID Ihres mobilen Geräts, die IP-Adresse Ihres mobilen Geräts, Ihr mobiles Betriebssystem, die Art des von Ihnen verwendeten mobilen Internetbrowsers, eindeutige Gerätekennungen und andere Diagnosedaten.

Wir können auch Informationen erfassen, die Ihr Browser sendet, wenn Sie unseren Dienst besuchen oder wenn Sie über ein mobiles Gerät auf den Dienst zugreifen.

#### Informationen, die während der Nutzung der Anwendung gesammelt werden

Während der Nutzung unserer Anwendung können wir mit Ihrer vorherigen Zustimmung Folgendes erfassen

- Informationen über Ihren Standort
- Bilder und andere Informationen von der Kamera und der Fotobibliothek Ihres Geräts

Wir verwenden diese Informationen, um die Geotagging-Fotofunktion Unseres Dienstes bereitzustellen und um Sie auf unseren Satellitenkarten zu lokalisieren. Diese Informationen können auf unsere Server und/oder auf den Server eines Dienstanbieters hochgeladen oder einfach auf Ihrem Gerät gespeichert werden.

Sie können den Zugriff auf diese Informationen jederzeit über die Einstellungen Ihres Geräts aktivieren oder deaktivieren.

### Verwendung Ihrer persönlichen Daten

Unser Dienst befindet sich derzeit in der Beta-Phase und daher werden Ihre persönlichen Daten nur zur **Entwicklung, Verbesserung und Prüfung unseres Dienstes** verwendet.

Während Ihrer Nutzung des Dienstes können wir Sie per E-Mail, Telefonanruf, SMS oder andere gleichwertige Formen der elektronischen Kommunikation, wie z. B. Push-Benachrichtigungen einer mobilen Anwendung, bezüglich Updates oder informativer Mitteilungen über Funktionalitäten, Verbesserungen oder Neuigkeiten zu unseren Diensten, einschließlich der Sicherheitsupdates, kontaktieren, wenn dies für deren Implementierung notwendig oder sinnvoll ist. Wir werden Ihre persönlichen Daten auch speichern, um Ihre Anfragen an uns zu bearbeiten und zu verwalten.

### Aufbewahrung Ihrer persönlichen Daten

Wir werden Ihre persönlichen Daten nur so lange aufbewahren, wie es für die in dieser Datenschutzrichtlinie genannten Zwecke erforderlich ist. Wir werden Ihre personenbezogenen Daten in dem Umfang aufbewahren und verwenden, der erforderlich ist, um unseren rechtlichen Verpflichtungen nachzukommen (z. B. wenn wir Ihre Daten aufbewahren müssen, um geltende Gesetze einzuhalten), Streitigkeiten beizulegen und unsere rechtlichen Vereinbarungen und Richtlinien durchzusetzen.

Wir werden auch Nutzungsmetriken für interne Analysezwecke aufbewahren. Nutzungsmetriken werden im Allgemeinen für einen kürzeren Zeitraum aufbewahrt, es sei denn, diese Daten werden verwendet, um die Sicherheit oder die Funktionalität unseres Dienstes zu verbessern, oder wir sind gesetzlich verpflichtet, diese Daten für längere Zeiträume aufzubewahren.

### Übertragung Ihrer persönlichen Daten

Ihre Informationen, einschließlich personenbezogener Daten, werden in unseren Betriebsbüros und an anderen Orten, an denen sich die an der Verarbeitung beteiligten Parteien befinden, verarbeitet. Das bedeutet, dass diese Daten an Computer außerhalb Ihres Staates, Ihrer Provinz, Ihres Landes oder einer anderen staatlichen Gerichtsbarkeit, in der andere Datenschutzgesetze als in Ihrer Gerichtsbarkeit gelten, übertragen und dort gespeichert werden können.

Mit Ihrer Zustimmung zu dieser Datenschutzrichtlinie und der anschließenden Übermittlung dieser Daten erklären Sie sich mit dieser Übertragung einverstanden.

Wir werden alle angemessenen Maßnahmen ergreifen, um sicherzustellen, dass Ihre Daten sicher und in Übereinstimmung mit dieser Datenschutzrichtlinie behandelt werden, und es wird keine Übermittlung Ihrer persönlichen Daten an eine Organisation oder ein Land stattfinden, wenn nicht angemessene Kontrollen vorhanden sind, die die Sicherheit Ihrer Daten und anderer persönlicher Informationen einschließen.

### Offenlegung Ihrer persönlichen Daten

#### Strafverfolgung

Unter bestimmten Umständen können wir verpflichtet sein, Ihre persönlichen Daten offenzulegen, wenn dies gesetzlich vorgeschrieben ist oder als Antwort auf berechtigte Anfragen von Behörden (z. B. einem Gericht oder einer Regierungsbehörde).

#### Andere gesetzliche Anforderungen

Wir können Ihre persönlichen Daten offenlegen, wenn wir in gutem Glauben davon ausgehen, dass eine solche Maßnahme notwendig ist, um

- einer gesetzlichen Verpflichtung nachzukommen
- die Rechte oder das Eigentum der Europäischen Kommission zu schützen und zu verteidigen
- Um mögliches Fehlverhalten im Zusammenhang mit dem Dienst zu verhindern oder zu untersuchen
- die persönliche Sicherheit der Nutzer des Dienstes oder der Öffentlichkeit zu schützen
- Schutz vor rechtlicher Haftung

### Sicherheit Ihrer persönlichen Daten

Die Sicherheit Ihrer persönlichen Daten ist uns wichtig, aber denken Sie daran, dass keine Methode der Übertragung über das Internet oder der elektronischen Speicherung 100% sicher ist. Obwohl wir uns bemühen, Ihre persönlichen Daten mit kommerziell akzeptablen Mitteln zu schützen, können wir keine absolute Sicherheit garantieren.

## GDPR Datenschutz

### Rechtsgrundlage für die Verarbeitung personenbezogener Daten gemäß GDPR

Wir können personenbezogene Daten unter den folgenden Bedingungen verarbeiten:

- **Zustimmung:** Sie haben Ihre Zustimmung zur Verarbeitung personenbezogener Daten für einen oder mehrere bestimmte Zwecke gegeben.

- **Vertragserfüllung:** Die Bereitstellung personenbezogener Daten ist für die Erfüllung eines Vertrags mit Ihnen und/oder für vorvertragliche Verpflichtungen erforderlich.

- **Rechtliche Verpflichtungen:** Die Verarbeitung personenbezogener Daten ist für die Erfüllung einer rechtlichen Verpflichtung erforderlich, der die Europäische Kommission unterliegt.

- **Wesentliche Interessen:** Die Verarbeitung personenbezogener Daten ist erforderlich, um Ihre lebenswichtigen Interessen oder die einer anderen natürlichen Person zu schützen.

- **Öffentliche Interessen:** Die Verarbeitung personenbezogener Daten ist mit einer Aufgabe verbunden, die im öffentlichen Interesse oder in Ausübung öffentlicher Gewalt erfolgt, die der Europäischen Kommission übertragen wurde.

- **Legitime Interessen:** Die Verarbeitung personenbezogener Daten ist für die Zwecke der von der Europäischen Kommission verfolgten legitimen Interessen erforderlich.

In jedem Fall sind wir gerne bereit, Ihnen bei der Klärung der für die Verarbeitung geltenden Rechtsgrundlage behilflich zu sein, insbesondere bei der Klärung der Frage, ob die Bereitstellung der personenbezogenen Daten gesetzlich oder vertraglich vorgeschrieben oder für den Abschluss eines Vertrags erforderlich ist.

### Ihre Rechte nach der GDPR

Die Europäische Kommission verpflichtet sich, die Vertraulichkeit Ihrer personenbezogenen Daten zu wahren und zu gewährleisten, dass Sie Ihre Rechte wahrnehmen können.

Sie haben im Rahmen dieser Datenschutzrichtlinie und des EU-Rechts das Recht auf:

- **Zugang zu Ihren personenbezogenen Daten zu verlangen.** Das Recht, auf die Informationen, die wir über Sie haben, zuzugreifen, sie zu aktualisieren oder zu löschen. Wann immer es möglich ist, können Sie direkt in den Einstellungen Ihres Kontos auf Ihre persönlichen Daten zugreifen, sie aktualisieren oder ihre Löschung beantragen. Wenn Sie nicht in der Lage sind, diese Aktionen selbst durchzuführen, wenden Sie sich bitte an uns, um Ihnen zu helfen. Auf diese Weise haben Sie auch die Möglichkeit, eine Kopie der personenbezogenen Daten zu erhalten, die wir über Sie gespeichert haben.

- Sie haben das Recht, unvollständige oder ungenaue Informationen, die wir über Sie gespeichert haben, korrigieren zu lassen.

- **Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten.** Dieses Recht besteht, wenn wir uns auf ein berechtigtes Interesse als Rechtsgrundlage für unsere Verarbeitung berufen und es etwas an Ihrer besonderen Situation gibt, das Sie dazu veranlasst, gegen unsere Verarbeitung Ihrer personenbezogenen Daten aus diesem Grund Widerspruch einzulegen. Sie haben auch das Recht, Widerspruch einzulegen, wenn wir Ihre personenbezogenen Daten für Direktmarketingzwecke verarbeiten.

- Sie haben das Recht, von uns die Löschung oder Entfernung Ihrer personenbezogenen Daten zu verlangen, wenn es für uns keinen triftigen Grund gibt, sie weiter zu verarbeiten.

- **Verlangen Sie die Übermittlung Ihrer personenbezogenen Daten.** Wir werden Ihnen oder einem von Ihnen gewählten Dritten Ihre personenbezogenen Daten in einem strukturierten, allgemein verwendeten und maschinenlesbaren Format zur Verfügung stellen. Bitte beachten Sie, dass dieses Recht nur für automatisierte Informationen gilt, deren Verwendung Sie ursprünglich zugestimmt haben, oder wenn wir die Informationen zur Erfüllung eines Vertrags mit Ihnen verwendet haben.

- Sie haben das Recht, Ihre Zustimmung zur Verwendung Ihrer personenbezogenen Daten zu widerrufen. Wenn Sie Ihre Zustimmung zurückziehen, können wir Ihnen möglicherweise keinen Zugang zu bestimmten Funktionen des Dienstes gewähren.

### Ausübung Ihrer GDPR-Datenschutzrechte

Sie können Ihre Rechte auf Auskunft, Berichtigung, Löschung und Widerspruch ausüben, indem Sie sich mit uns in Verbindung setzen. Bitte beachten Sie, dass wir Sie möglicherweise auffordern, Ihre Identität zu überprüfen, bevor wir auf solche Anfragen antworten. Wenn Sie eine Anfrage stellen, werden wir unser Bestes tun, um Ihnen so schnell wie möglich zu antworten.

Sie haben das Recht, sich bei einer Datenschutzbehörde über die Erfassung und Verwendung Ihrer persönlichen Daten durch uns zu beschweren. Für weitere Informationen wenden Sie sich bitte an Ihre lokale Datenschutzbehörde im Europäischen Wirtschaftsraum.

## Links zu anderen Websites und Diensten

Unser Service kann Links zu anderen Websites oder Diensten enthalten, die nicht von uns betrieben werden. Wenn Sie auf einen Link einer dritten Partei klicken, werden Sie auf die Seite dieser dritten Partei weitergeleitet. Wir empfehlen Ihnen dringend, die Datenschutzrichtlinien jeder von Ihnen besuchten Website zu lesen.

Wir haben keine Kontrolle über und übernehmen keine Verantwortung für den Inhalt, die Datenschutzrichtlinien oder Praktiken von Websites oder Diensten Dritter.

## Änderungen an dieser Datenschutzrichtlinie

Wir können unsere Datenschutzrichtlinie von Zeit zu Zeit aktualisieren. Wir werden Sie über alle Änderungen informieren, indem wir die neue Datenschutzrichtlinie auf dieser Seite veröffentlichen.

Wir informieren Sie per E-Mail und/oder durch einen auffälligen Hinweis in unserem Service, bevor die Änderung in Kraft tritt, und aktualisieren das Datum der letzten Aktualisierung" oben in dieser Datenschutzrichtlinie.

Wir empfehlen Ihnen, diese Datenschutzrichtlinie regelmäßig auf Änderungen zu überprüfen. Änderungen an dieser Datenschutzrichtlinie treten in Kraft, wenn sie auf dieser Seite veröffentlicht werden.

## Kontakt

Wenn Sie Fragen zu dieser Datenschutzrichtlinie haben, können Sie uns kontaktieren:

- Per E-Mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Durch den Besuch dieser Seite auf unserer Website: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
