# Andmekaitsetingimused

_Viimati uuendatud 17. jaanuaril 2022_

Nendes andmekaitsetingimustes on kirjeldatud meie põhimõtteid ja korda seoses Teie isikuandmete kogumise, kasutamise ja avaldamisega, kui teenust kasutate, ning on kirjeldatud teie privaatsusega seotud õigusi ja seda, kuidas seadus Teid kaitseb.

Kasutame Teie isikuandmeid teenuse pakkumiseks ja parandamiseks. Teenust kasutades nõustute teabe kogumise ja kasutamisega nende andmekaitsetingimuste kohaselt.

## Tõlgendamine ja mõisted

### Tõlgendamine

Järgmiste sõnade tähendus on määratletud nendes tingimustes. Järgmistel mõistetel on sama tähendus, olenemata sellest, kas need on esitatud ainsuses või mitmuses.

### Mõisted

Nendes andmekaitsetingimustes on järgmistel sõnadel järgmine tähendus:

- **Konto** on ainulaadne konto, mis luuakse Teile, et saaksite meie teenuseid või osa neist kasutada.

- **Sidusettevõtja** on äriühing, mis kontrollib või mida kontrollitakse või mis on mõne partneriga ühise kontrolli all, kusjuures kontrollima tähendab  vähemalt 50% aktsiate, osakute või muude juhtkonna või juhtorgani valimiseks õiguse andvate osade omamist.

- **Rakendus** on Euroopa Komisjoni pakutav FaST nimeline tarkvaraprogramm, mille Te elektroonikaseadmesse alla laadite.

- **Euroopa Komisjon** (edaspidi kas Euroopa Komisjon või meie) tähendab Euroopa Komisjoni, DG DEFIS, Unit I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Brüssel, Belgia.

- **GDPR** on isikuandmete kaitse üldmäärus.

- **ÜPP makseasutus** viitab järgmisele:
    -  Andaluusia kasutajatele: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla  s/n, 41071-Sevilla, España
    - Castilla y Leoni kasutajatele: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Piemonte kasutajatele: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Eesti kasutajatele: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Valloonia kasutajad: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgia.
    - Kreeka kasutajatele: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Bulgaaria kasutajatele: Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България.
    - Slovakkia kasutajad: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko.
    - Rumeenia kasutajad: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- **Vastutav töötleja** (edaspidi: meie) on GDPR-i kohaldamisel Euroopa Komisjon kui juriidiline isik, kes määratleb isikuandmete töötlemise eesmärgi ja vahendid.

- **Seade** on mis tahes seade, millega pääseb teenusele ligi, nt arvuti, telefon või tahvelarvuti.

- **Isikuandmed** on andmed, mis on seotud tuvastatud või tuvastatava isikuga. GDPR-i kohaldamisel on isikuandmed Teiega seotud andmed, nagu nimi, isikukood, asukoha andmed, võrguidentifikaator või füüsiline, psühholoogiline, geneetiline, vaimne, majanduslik, kultuuriline või ühiskondlik tunnus.

- **Teenus** tähendab rakendust.

- **Teenusepakkuja** on füüsiline või juriidiline isik, kelle kasutuses on andmed Euroopa Komisjoni nimel. See viitab kolmandatest isikutest äriühingutele või Euroopa Komisjoni palgatud isikutele või teenuse lihtsustamiseks kasutatavale makseasutusele, et pakkuda Euroopa Komisjoni nimel teenust, osutada teenusega seotud teenuseid või aidata Euroopa Komisjonil või makseasutusel analüüsida, kuidas teenust kasutatakse. GDPR-i kohaldamisel käsitletakse teenusepakkujaid vastutavate töötlejatena.

- **Kolmanda isiku autentimisteenus** on mis tahes teenus, mille kaudu kasutaja saab sisse logida või luua konto teenuse kasutamiseks.

- **Kolmanda isiku Opt-In teenus** on mis tahes väline teenus, mis on teenuses registreeritud ja millele saate anda nõusoleku.

- **Kasutusnäitajad** on automaatselt kogutud andmed, mis on loodud teenuse kasutamisel või mille on loonud teenuse taristu ise (nt lehekülje külastamise ajal).

- **Teie** tähendab isikut, kes külastab või kasutab teenust, või ettevõtet või muud juriidilist isikut, kelle nimel selline isik teenust külastab või kasutab, kui see on kohaldatav. GDPR-i (isikuandmete kaitse üldmääruse) kohaldamisel võidakse Teile viidata kui andmesubjektile või kasutajale, sest te olete isik, kes kasutab teenust.

## Teie isikuandmete kogumine ja kasutamine

### Kogutavate andmete liigid

#### Isikuandmed

Võime paluda Teil meie teenuse kasutamisel esitada meile teatud isikuandmeid, mida on vaja Teiega ühendust võtmiseks või Teie tuvastamiseks. Isikuandmed võivad muu hulgas hõlmata järgmisi andmeid:

- e-posti aadress;
- ees- ja perekonnanimi;
- telefoniumbe;
- aadress, riik, maakond, indeks, linn;
- teated ja manused, mida jagate teiste kasutajatega (sealhulgas Teie ÜPP makseasutusega);
- asukohamärgisega fotod, mida platvormil jagate (sealhulgas Teie ÜPP makseasutusega);
- Teie talumajapidamise kirjeldus (nt maatükid, põllukultuurid), põllumajandustavad (nt Teile saadetud väetamissoovitused), mullaandmed, mida teenusesse sisestate;
- kasutusnäitajad (vt altpoolt).

#### Kasutusnäitajad

Kasutusnäitajaid kogutakse automaatselt teenuse kasutamisel.

Kasutusnäitajad võivad sisaldada teavet, nagu Teie seadme internetiprotokolli aadress (nt IP-aadress), brauseri tüüp, brauseri versioon, meie teenuse leheküljed, mida külastate, külastuse kellaaeg ja kuupäev, nendel lehekülgedel veedetud aeg, ainulaadsed seadme identifikaatorid ja muud diagnostilised andmed.

Kui külastate teenust mobiiliseadme kaudu, võime koguda teatud teavet, mis võib olla muu hulgas Teie kasutatava mobiiliseadme tüüp, Teie mobiiliseadme ainulaadne tunnus, IP-aadress, operatsioonisüsteem, kasutatav brauseri liik, seadme ainulaadsed identifikaatorid ja muud diagnostilised andmed.

Võime koguda teavet, mida Teie brauser saadab, kui Te meie teenust külastate või kui Te kasutate teenust mobiiliseadmes.

#### Rakenduse kasutamisel kogutav teave

Kui kasutate meie rakendust, võime Teie loal koguda järgmist teavet:

- Teie asukohaga seotud teave;
- pildid ja muu teave Teie seadme kaamerast ja fototeegist.

Kasutame seda teavet asukohamärgisega fotode funktsiooni pakkumiseks meie teenuses ning Teie asukoha kindlakstegemiseks meie satelliitkaartidel. Seda teavet võidakse üles laadida meie serveritesse ja/või teenusepakkuja serverisse või säilitada Teie seadmes.

Saate sellele teabele juurdepääsu igal ajal võimaldada või keelata oma seadme seadete kaudu.

### Teie isikuandmete kasutus

Meie teenus on praegu beeta etapis ja seega kasutatakse Teie isikuandmeid ainult **meie teenuse arendamiseks, täiendamiseks ja testimiseks**.

Kui kasutate meie teenust, võime Teiega ühendust võtta e-posti, telefoni, lühisõnumi teel või muu sarnase elektroonilise suhtlusviisi, nagu mobiilirakenduse tõuketeadete kaudu seoses teenust käsitlevate funktsioonide, täiustuste või uudistega, sealhulgas turvauuendustega, kui see on nende rakendamiseks vajalik või mõistlik. Lisaks säilitame Teie isikuandmeid, et täita ja hallata Teie tehtud päringuid.

### Teie isikuandmete säilitamine

Säilitame Teie isikuandmeid nii kaua kui see on vajalik nendes andmekaitsetingimustes sätestatud eesmärgil. Säilitame ja kasutame Teie isikuandmeid vajalikul määral, et järgida meie seaduslike kohustusi (nt kui peame säilitama Teie isikuandmeid, et täita kohaldatavaid õigusakte), lahendada vaidlusi ja jõustada meie seaduslikke kokkuleppeid ja põhimõtteid.

Lisaks säilitame kasutusnäitajad ettevõttesiseseks analüüsiks. Kasutusnäitajaid säilitatakse tavaliselt lühema aja vältel, välja arvatud kui neid andmeid kasutatakse turvalisuse tugevdamiseks või meie teenuste funktsioonide täiustamiseks või kui oleme seaduse alusel kohustatud neid andmeid kauem säilitama.

### Teie isikuandmete edastamine

Teie andmeid, sealhulgas isikuandmeid, töödeldakse meie kontorites ja teistes kohtades, kus töötlemisega tegelevad isikud asuvad. See tähendab, et seda teavet võidakse edastada (ja säilitada) arvutites, mis asuvad väljaspool Teie riiki, maakonda või muud valitsuse jurisdiktsiooni, kus andmekaitseseadused võivad erineda teie jurisdiktsioonis kehtivatest seadustest.

Kui nõustute nende andmekaitsetingimustega ja esitate oma andmed, tähendab et nõustute nende edastamisega.

Võtame kõiki vajalikke meetmeid, tagamaks, et Teie andmeid töödeldakse turvaliselt ja kooskõlas nende andmekaitsetingimustega ning Teie isikuandmeid ei edastata organisatsioonidele või riikidele, kui puuduvad nõuetekohased kontrolltegevused, sealhulgas teie andmete ja isikuandmete turvalisus.

### Teie isikuandmete avaldamine

#### Õiguskaitse

Teatud juhtudel peame Teie isikuandmeid avaldama õigusaktide alusel või riigiasutuste (nt kohtu või valitsusasutuse) asjakohasel nõudel.

#### Muud seaduslikud nõuded

Võime avaldada Teie isikuandmeid heas usus, kui selline tegevus on vajalik:

- seaduslike nõuete täitmiseks;
- Euroopa Komisjoni õiguste või vara kaitseks;
- teenusega seotud võimaliku rikkumise ennetamiseks või uurimiseks;
- teenuse kasutajate isikliku turvalisuse või avalikkuse kaitseks;
- kaitseks õigusliku vastutuse eest.

### Teie isikuandmete turvalisus

Teie isikuandmete turvalisus on meie jaoks oluline, aga pidage meeles, et ükski elektrooniline teabe edastamise või säilitamise viis ei ole sada protsenti kindel.  Meie eesmärk on kasutada Teie isikuandmete kaitseks kaubanduslikult vastuvõetavaid meetmeid, ent me ei saa tagada Teie isikuandmete absoluutset turvalisust.

## Privaatsus isikuandmete kaitse üldmääruse alusel

### Isikuandmete töötlemise õiguslik alus isikuandmete kaitse üldmääruse kohaselt

Võime töödelda Teie isikuandmeid järgmistel tingimustel.

- **Nõusolek:** Annate oma nõusoleku isikuandmete töötlemiseks ühel või mitmel konkreetsel eesmärgil.

- **Lepingu täitmine:** Isikuandmete esitamine on vajalik Teiega sõlmitud lepingu täitmiseks ja/või asjakohaste lepingueelsete kohustuste täitmiseks.

- **Õiguslikud kohustused:** Isikuandmete töötlemine on vajalik Euroopa Komisjoni juriidilise kohustuse täitmiseks.

- **Olulised huvid:** Isikuandmete töötlemine on vajalik Teie või mõne muu füüsilise isiku oluliste huvide kaitsmiseks.

- **Avalikud huvid:** Isikuandmete töötlemine on seotud avalikes huvides oleva ülesandega või Euroopa Komisjoni avaliku võimu teostamiseks.

- **Õigustatud huvid:** Isikuandmete töötlemine on vajalik Euroopa Komisjoni õigustatud huvides.

Selgitame Teile meeleldi, mis on Teie isikuandmete töötlemise õiguslik alus ning jagame teavet selle kohta, kas isikuandmete esitamine on õigusaktist või lepingust tulenev kohustus või lepingu sõlmimiseks vajalik nõue.

### Teie õigused isikuandmete kaitse üldmääruse kohaselt

Euroopa Komisjon kohustub austama Teie isikuandmete konfidentsiaalsust ja tagama teile võimaluse kasutada oma õiguseid.

Teil on nende andmekaitsetingimuste ja ELi seaduste alusel:

- **õigus isikuandmetega tutvuda.** Õigus tutvuda teabega, mis meil Teie kohta on ning seda ajakohastada või kustutada. Võimaluse korral saate oma isikuandmetega tutvuda ja neid ajakohastada või taotleda nende kustutamist otse oma kontoseadete alt. Kui Te ei saa neid toiminguid ise teha, siis pöörduge abi saamiseks meie poole. Nii saate ka koopia isikuandmetest, mida Teie kohta säilitame.

- **õigus taotleda isikuandmete parandamist, mida Teie kohta säilitame.** Teil on õigus taotleda, et parandataks kõik ebaõiged või mittetäielikud andmed, mida Teie kohta säilitame.

- **õigus esitada vastuväiteid oma isikuandmete töötlemise suhtes.** See õigus kehtib juhul, kui tugineme andmete töötlemise õigusliku alusena õigustatud huvile ja miski Teie olukorras tekitab Teis soovi esitada vastuväiteid oma isikuandmete töötlemisele sellel alusel. Ühtlasi on Teil õigus esitada vastuväiteid, kui töötleme Teie isikuandmeid otseturunduse eesmärgil.

- **õigus taotleda oma isikuandmete kustutamist.** Teil on õigus taotleda oma isikuandmete kustutamist või eemaldamist, kui meil ei ole mõjuvat põhjust jätkata nende töötlemist.

- **õigus taotleda isikuandmete edastamist.**Anname teile või Teie valitud kolmandale isikule Teie isikuandmed struktureeritud, üldkasutatavas vormingus ja masinloetaval kujul. Pange tähele, et see õigus kehtib üksnes automatiseeritud teabe kohta, mille kasutamiseks te meile nõusoleku andsite, või mida me kasutasime Teiega sõlmitud lepingu täitmiseks.

- **õigus oma nõusolek tagasi võtta.** Teil on õigus nõusolek oma isikuandmete kasutamiseks igal ajal tagasi võtta. Kui võtate oma nõusoleku tagasi, ei pruugi me tagada juurdepääsu teatavatele teenuse funktsioonidele.

### Andmekaitseõiguste kasutamine isikuandmete kaitse üldmääruse kohaselt

Võite kasutada oma õigust andmetega tutvumiseks, nende parandamiseks, nende töötlemise lõpetamiseks ja nende töötlemisele vastuväidete esitamiseks, võttes meiega ühendust. Juhime tähelepanu, et võime paluda Teilt oma isiku tuvastamist, enne kui taotlusele vastame. Taotluse esitamise korral anname endast kõik, et vastata Teile esimesel võimalusel.

Teil on õigus esitada andmekaitseasutusele Teie isikuandmete kogumise ja kasutamise kohta kaebus. Kui asute Euroopa Majanduspiirkonnas (EMP), siis võtke lisateabe saamiseks ühendust oma kohaliku andmekaitseasutusega.

## Lingid teistele veebilehtedele ja teenustele

Meie teenus võib sisaldada linke teistele veebilehtedele või teenustele, mida meie ei halda. Kui klõpsate kolmanda isiku lingil, suunatakse Teid sellele kolmanda isiku saidile. Soovitame Teil tungivalt tutvuda iga kasutatava saidi andmekaitsetingimustega.

Meil puudub kolmandate isikute veebisaitide või teenuste sisu, andmekaitsetingimuste või tavade üle kontroll ja me ei võta selle eest mingit vastutust.

## Andmekaitsetingimuste muutmine

Võime aeg-ajalt andmekaitsetingimusi uuendada. Teavitame Teid muudatustest, postitades uued andmekaitsetingimused sellele lehele.

Anname Teile muudatuste jõustumisest e-posti teel ja/või teenuses väljapaistva teatega ette teada ning uuendame andmekaitsetingimuste alguses olevat viimase uuendamise kuupäeva.

Soovitame teil andmekaitsetingimused korrapäraselt üle vaadata, et muudatustest teada saada. Andmekaitsetingimustes tehtud muudatused jõustuvad alates nende postitamisest sellele lehele.

## Võtke meiega ühendust

Kui teil on andmekaitsetingimuste kohta küsimusi, siis võtke meiega ühendust:

- e-posti teel: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- külastades meie veebisaidil lehte: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
