# Tingimused

_Viimati uuendatud 17. jaanuaril 2022_

Palun lugege neid tingimusi hoolikalt, enne kui meie teenuseid kasutate.

## Tõlgendamine ja mõisted

### Tõlgendamine

Järgmiste sõnade tähendus on määratletud nendes tingimustes. Järgmistel mõistetel on sama tähendus, olenemata sellest, kas need on esitatud ainsuses või mitmuses.

### Mõisted

Nendes tingimustes on järgmistel sõnadel järgmine tähendus:

- **Rakendus** on Euroopa Komisjoni pakutav FaST nimeline tarkvaraprogramm, mille Te elektroonikaseadmesse alla laadite.

- **Rakenduste pood** on digitaalne jagamisteenus, mida valdab ja arendab Apple Inc. (Apple App Store) või Google Inc. (Google Play Store) ja millest rakendus on alla laetud.

- **Sidusettevõtja** on äriühing, mis kontrollib või mida kontrollitakse või mis on mõne partneriga ühise kontrolli all, kusjuures kontrollima tähendab vähemalt 50% aktsiate, osakute või muude juhtkonna või juhtorgani valimiseks õiguse andvate osade omamist.

- **Konto** on ainulaadne konto, mis luuakse Teile, et saaksite meie teenuseid või osa neist kasutada.

- **ÜPP makseasutus** viitab järgmisele:
    -  Andaluusia kasutajatele: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla  s/n, 41071-Sevilla, España
    - Castilla y Leoni kasutajatele: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - Piemonte kasutajatele: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - Eesti kasutajatele: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - Valloonia kasutajad: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgia.
    - Kreeka kasutajatele: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - Bulgaaria kasutajatele: Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България.
    - Slovakkia kasutajad: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko.
    - Rumeenia kasutajad: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România.

- **Euroopa Komisjon** (edaspidi kas Euroopa Komisjon või meie) tähendab Euroopa Komisjoni, DG DEFIS, Unit I3 Space Data for Societal Challenges and Growth, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Brüssel, Belgia

- **Sisu** on selline sisu, nagu tekst, pildid või muu teave, mida saab postitada, üles laadida, linkida või muul viisil Teile kättesaadavaks teha, olenemata selle vormingust.

- **Seade** on mis tahes seade, millega pääseb teenusele ligi, nt arvuti, telefon või tahvelarvuti.

- **Tagasiside** on tagasiside, uuendused või soovitused, mis on teile saadetud seoses meie teenuste atribuutide, toimivuse või funktsioonidega.

- **Teenus** tähendab rakendust.

- **Tingimused** on need tingimused, mis moodustavad Teie ja Euroopa Komisjoni vahelise lepingu seoses teenuse kasutamisega.

- **Kolmanda isiku autentimisteenus** on mis tahes autentimisteenus, mida pakub kolmas isik (nt mõne Euroopa Liidu liikmesriigi riiklik identimisteenuse pakkuja), kellele Euroopa Komisjon delegeerib kasutaja autentimise, et rakendusele juurde pääseda.

- **Kolmanda isiku Opt-In teenus** on teenus või sisu (sealhulgas andmed, teave, tooted või teenused), mida pakub kolmas isik ja mida teenus võib kuvada, sisaldada või kättesaadavaks teha.

- **Teie** tähendab isikut, kes külastab või kasutab teenust, või ettevõtet või muud juriidilist isikut, kelle nimel selline isik teenust külastab või kasutab, kui see on kohaldatav.

## Kinnitus

Need on tingimused, mis reguleerivad selle teenuse pakkumist ning Teie ja meie vahel sõlmitud lepingu kasutamist. Tingimustes on sätestatud kõigi kasutajate õigused ja kohustused seoses teenuse kasutamisega.

Teie saate teenusele juurdepääsu ja teenuseid kasutada, kui nõustute nende tingimustega ning järgite neid. Tingimused kehtivad kõigile külastajatele, kasutajatele ja teistele isikutele, kellel on teenusele juurdepääs.

Kui külastate või kasutate teenust, nõustute nende tingimustega. Kui te ei ole osade tingimustega nõus, siis te ei tohi teenust kasutada.

Kinnitate, et olete vanem kui 18. Euroopa Komisjon ei luba alla 18aastastel isikutel teenust kasutada.

Kui soovite teenust külastada ja kasutada, peate nõustuma ka Euroopa Komisjoni privaatsuspõhimõtetega ja neid järgima. Meie privaatsuspõhimõtted kirjeldavad meie põhimõtteid ja korda seoses sellega, kuidas teie isikuandmeid kogume, kasutame ja avaldame, kui kasutate rakendust või veebilehte, ning kirjeldab teie privaatsusega seotud õiguseid ning seda, kuidas seadus teid kaitseb. Palun lugege enne teenuse kasutamist hoolega meie privaatsuspõhimõtteid.

## Kasutajakontod

Kui loote meie juures konto, peate alati esitama meile täpsed, täielikud ja kehtivad andmed. Kui te seda tingimust ei järgi, rikute lepingutingimusi, mille tagajärjeks võib olla meie teenuse kasutamiseks vajaliku konto viivitamatu sulgemine.

Vastutate teenuse kasutamiseks vajaliku parooli kaitsmise eest ning parooliga seotud tegevuste eest, olenemata sellest, kas teie parool on seotud meie teenuste või kolmanda isiku autentimisteenuse pakkujaga.
Nõustute, et ei avalda oma parooli kolmandatele isikutele. Peate meid viivitamata teavitama, kui saate teada oma konto turvalisuse rikkumisest või volitamata kasutamisest.

Te ei tohi kasutada kasutajanimena teise isiku või äriühingu nime ega nime, mida pole seaduse alusel lubatud kasutada, kaubamärgi nime, mis kuulub teisele isikule või äriüksusele ilma vastava loata, või nime, mis on muul viisil solvav, vulgaarne või sündsusetu.

## Sisu

### Teie õigus sisu üles laadida

Meie teenus võimaldab Teil sisu üles laadida (nt lisada asukohamärgisega fotosid). Vastutate sisu eest, mida teenusesse postitate, sealhulgas selle seaduslikkuse, usaldusväärsuse ja sobivuse eest.

Te kinnitate, et: i) sisu kuulub Teile (olete selle omanik) või Teil on õigus seda kasutada ning anda meile nende tingimuste kohaselt õigus ja luba selle kasutamiseks, ja ii) kui postitate oma sisu teenusesse või teenuse kaudu, siis te ei riku eraelu puutumatusega seotud õigusi, avalikustamise õigusi, autoriõigusi, lepinguõigusi ega mõne teise isiku õigusi.

### Sisu piirangud

Euroopa Komisjon ei vastuta teenuse kasutajate sisu eest. Mõistate ja nõustute sõnaselgelt, et vastutate ainuisikuliselt sisu ja kõigi Teie kontol toimuvate tegevuste eest, olenemata sellest, kas neid teete Teie või Teie kontot kasutav kolmas isik.

Te ei tohi edastada ebaseaduslikku, solvavat, ärritavat, vastikust tekitavat, ähvardavat, laimavat, alandavat või muul viisil taunitavat sisu. Sellise taunitava sisu näited on muu hulgas järgmised:

- ebaseaduslik tegevus või selle õhutamine;
- laimav, diskrimineeriv või halvustav sisu, sealhulgas viited või märkused usu, rassi, seksuaalse sättumuse, soo, rahvusliku/etnilise päritolu või muu sihtrühma kohta;
- rämpspost, masin- või suvaliselt loodud sisu, mis hõlmab volitamata või soovimatut reklaami, ketikirju või muud volitamata üleskutset, või mis tahes kujul loteriid või hasartmängud;
- viirused, ussviirused, pahavara, trooja hobused või muu sisu, mille eesmärk on häirida, kahjustada või piirata tarkvara, riistvara või telekommunikatsiooniseadmeid või kahjustada kolmandate isikute andmeteid või muud teavet või saada nendele volitamata juurdepääs;
- kellegi omandiõiguste, sealhulgas patendi, kaubamärgi, ärisaladuse, autoriõiguse, avaldamisõiguse või muude õiguste rikkumine;
- teise isiku või äriühingu, sealhulgas Euroopa Komisjoni ja selle töötajate või esindajate jäljendamine;
- kolmandate isikute privaatsuse rikkumine;
- valed andmed ja funktsioonid.

Euroopa Komisjon jätab endale õiguse, aga ei võta kohustust, teha enda äranägemisel kindlaks, kas sisu on sobiv ja vastab nendele tingimustele, ning keelduda sisust või see eemaldada. Euroopa Komisjon saab ka piirata teenuse kasutamist või selle tühistada, kui Te postitate sellist taunitavat sisu.

Et Euroopa Komisjon ei saa kontrollida kogu sisu, mille kasutajad ja/või kolmandad isikud on teenusesse postitatud, nõustute kasutama teenust omal riskil. Mõistate, et teenuse kasutamisel võite kokku puutuda sisuga, mis on teie jaoks solvav, ebasünnis, sobimatu või taunitav, ning nõustute, et Euroopa Komisjon ei vastuta mingil juhul sellise sisu eest, sealhulgas sisus esinevate vigade või puudujääkide eest, või kahjude või kadude eest, mis tulenevad sellest, et olete sisu kasutanud.

### Sisu varundamine

Kuigi sisu varundatakse korrapäraselt, ei taga Euroopa Komisjon, et andmed ei või kaotsi minna või neid ei rikuta.

Vigased või kehtetud varunduspunktid võivad olla põhjustatud sisust, mis on enne varundamist rikutud või mida muudeti varundamise ajal.

Euroopa Komisjon pakub tuge ja proovib leida avastatud vead, mis võivad sisu varundamist mõjutada. Siiski mõistate ja kinnitate, et Euroopa Komisjon ei vastuta sisu tervilikkuse ega sisu sobivasse olekusse taastamise võimaliku ebaõnnstumise eest.

Nõustute säilitama sisust täieliku ja täpse koopia teenusest sõltumatus asukohas.

## Autoriõigusega seotud põhimõtted

### Intellektuaalomandiõiguste rikkumine

Austame teiste isikute intellektuaalomandiõigusi. Meie eesmärk on vastata kõigile kaebustele, et teenusesse postitatud sisu rikub autoriõigusi või teiste isikute intellektuaalomandiõiguseid.

Kui olete autoriõiguse omanik või teile on antud tema nimel vastavad õigused ja usute, et autoriõigusega kaitstud töö on kopeeritud viisil, mis rikub teenuse kasutamisel autoriõigusi, peate esitama kirjaliku avalduse meie autoriõigustega tegelevale esindajale aadressil contact@fastplatform.eu ning lisama oma avaldusse väidetava rikkumise üksikasjaliku kirjelduse.

Teid võidakse pidada kahjude (sealhulgas kulude ja advokaaditasude) eest vastutavaks, kui esitate eksitavat teavet, et mingi sisu rikub teie autoriõigusi.

## Teie tagasiside meile

Loovutate kõik õigused, omandiõigused ja huvid seoses tagasisidega, mida Euroopa Komisjonile annate. Kui selline loovutamine on mis tahes põhjusel jõustamatu, nõustute andma Euroopa Komisjonile tagasivõetava, üleilmse, kestva, kasutustasuta ja mittevälistava loa kasutada, taasesitada, avaldada, all-litsentsida, jagada, muuta ja kasutada seda tagasisidet ilma piiranguteta.

## Lingid teistele veebilehtedele

Meie teenused võivad sisaldada linke kolmandate isikute veebilehtedele või teenustele, mida Euroopa Komisjon ei oma ega kontrolli.

Euroopa Komisjon ei kontrolli kolmandate isikute veebilehtede või teenuste sisu, privaatsuspõhimõtteid ega tavasid ning ei võta nende eest vastutust. Kinnitate ja nõustute, et Euroopa Komisjon ei vastuta otseselt ega kaudselt tegelike või väidetavate kahjude või kadude eest, mis tulenevad selliste veebilehtede või teenuste kaudu kättesaadava sisu, kauba või teenuste kasutamisest või sellele tuginemisest.

Soovitame Teil tungivalt lugeda kõigi külastatavate kolmandate isikute veebilehtede või teenuste privaatsuspõhimõtteid.

## Konto sulgemine

Kui rikute neid tingimusi, võime Teie konto viivitamatult ja ette teatamata mis tahes põhjusel sulgeda või peatada, sealhulgas piiranguteta.

Konto sulgemisel lõpevad viivitamata teie õigused teenuseid kasutada. Kui soovite oma konto sulgeda, võite lihtsalt teenuse kasutamist lõpetada.

## Vastutuse piiramine

Teie võimalikest kahjudest hoolimata on Euroopa Komisjoni ja selle tarnijate vastutus nende tingimuste ja Teie ainsa õiguskaitsevahendi kohaselt piiratud summaga, mille olete tegelikult teenuse kaudu maksnud, või 100 USA dollarit, kui te pole teenuse kaudu midagi ostnud.

Kohaldatavate õigusaktidega lubatud suurimas võimalikus ulatuses ei vastuta Euroopa Komisjon ega selle tarnijad ühelgi juhul eri-, juhuslike, kaudsete või järgnevate kahjude eest (sealhulgas saamata jäänud tulu, andmete või muu teabe kadumise, äritegevuse peatumise, isiklike vigastuste, privaatsuse hävinemise, mis tuleneb teenuse või teenuse raames kasutatava kolmandate isikute tarkvara ja/või riistvara kasutamisest või kasutamise piirangutest, või on muul viisi seotud nende tingimuste sätetega), isegi kui Euroopa Komisjon või selle tarnijaid on nende kahjude võimalikkusest teavitatud ning õiguskaitsevahend ei täida oma peamist eesmärki.

Mõnes riigis ei ole kaudsed garantiid või piirangud juhuslike või järgnevate kahjude eest lubatud, mis tähendab, et mõned eelnimetatud piirangutest ei pruugi kehtida. Sellistes riikides on iga isiku vastutus piiratud õigusaktidega lubatud suurimas võimalikus ulatuses.

## Lahtiütlus seoses hetkeseisu ja -saadavusega

Teenust pakutakse hetkeseisu ja -saadavuse põhmõttel ning võimalike vigade ja puuduste suhtes ei anta mingeid garantiisid. Kohaldatavate õigusaktidega lubatud suurimas võimalikus ulatuses ütlevad Euroopa Komisjon enda ja oma sidusrühmade ning viimaste litsentsiandjate ja teenusepakkujate nimel sõnaselgelt lahti kõigist teenusega seotud otsestest, kaudsetest või muudest garantiidest, sealhulgas kaudsest turustatavuse garantiist, teatud otstarbele sobivuse garantiist, omandiõigusese ja rikkumiste puudumise garantiist, ning garantiidest, mis võivad tekkida tehingutest, käitusest, tehingutavadest või muust. Ilma et see piiraks eelsätestatu kohaldamist, ei paku Euroopa Komisjon mingit garantiid ega kindlustust ega kinnita mingil viisil, et teenus vastab teie nõuetele, saavutab soovitud tulemusi, sobib kasutamiseks muu tarkvara, rakenduste, süsteemide või teenustega, toimib katkestusteta, vastab tulemuslikkuse või usaldusväärsuse standarditele või on vigadeta, või et neid vigu saab parandada ja need ka parandatakse.

Ilma et see piiraks eelsätestatu kohaldamist, ei anna Euroopa Komisjon ega ükski Euroopa Komisjoni teenusepakkuja otsest ega kaudset kinnitust ega garantiid, et:i) teenus või selles sisalduvad andmed, sisu ja materjalid või tooted toimivad või on kättesaadavad; ii) teenus toimib katkestusteta või on vigadeta; iii) teenuse kaudu saadav teave ja sisu on täpne, usaldusväärne või ajakohane; või et iv) teenus, selle pakkujad, sisu või Euroopa Komisjoni poolt või nimel saadetud e-kirjad ei sisalda viiruseid, skripte, trooja hobuseid, viirususse, pahavara, ajapomme ega teisi kahjulikke komponente.

Mõnes kohtualluvuses ei ole lubatud teatud liiki garantiide välistamine või kohaldatavate tarbijaõiguste piirangud, seega mõned või kõik eelnimetatud eranditest ei pruugi teile kehtida. Ent sellisel juhul kohaldatakse selles punktis sätestatud erandeid ja piiranguid õigusaktidega lubatud suurimas võimalikus ulatuses.

## Kohaldatav õigus

Tingimustele ja teenuse kasutamisel kohaldatakse Euroopa Liidu ja vajadusel Belgia õigust. Teiepoolsele teenuste kasutamisele võivad kohalduda ka teised kohalikud, osariiklikud, riiklikud või rahvusvahelised seadused.

## Vaidluste lahendamine

Kui Teil on teenusega seoses muresid või vaidlusküsimusi, peate proovima neid kõigepealt lahendada mitteametlikult, võttes Euroopa Komisjoniga ühendust.

## Euroopa Liidu (EL) kasutajatele

Kui olete Euroopa Liidu tarbija, saate kasu teie elukohariigis kehtivates kohustuslikest õigusnormidest.

## Loobumine ja kehtivus

### Kehtivus

Kui leitakse, et nende tingimuste säte on kehtetu või seda pole võimalik täitmisele pöörata, muudetakse ja tõlgendatakse seda sätet nii, et saavutada selle sätte eesmärk kohaldatavate õigusaktidega lubatud suurimas võimalikus ulatuses, ning ülejäänud sätted jäävad täies ulatuses kehtima.

### Loobumine

Õiguse kasutamata jätmine või nende tingimuste alusel kohustuse täitmise nõudmine ei mõjuta isiku võimet seda õigust hiljem kasutada või täitmist nõuda ning rikkumise aktsepteerimine ei tähenda järgmiste rikkumiste aktsepteerimist, välja arvatud käesolevaga sätestatud erandid

## Tõlke tõlgendamine

Need tingimused võivad olla tõlgitud, kui oleme need Teile oma teenuse kaudu kättesaadavaks teinud.

Nõustute, et vaidluste korral on ülimuslik ingliskeelne originaaltekst.

## Tingimuste muutmine

Jätame endale õiguse muuta või asendada igal ajal tingimusi omal äranägemisel. Kui muudatus on oluline, anname endast parima, et teavitada sellest enne tingimuste jõustumist vähemalt 30 päeva ette.  Otsustame omal äranägemisel, milline muutus on oluline.

Kui jätkate meie teenuse kasutamist pärast muudatuste jõustumist, siis nõustute muudetud tingimusi täitma. Kui te ei nõustu muudetud tingimustega osaliselt või täielikult, siis lõpetage palun veebilehe ja teenuse kasutamine.

## Võtke meiega ühendust

Kui teil on tingimuste kohta küsimusi, saate meiega ühendust võtta:

- e-posti teel: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- külastades meie veebisaidil järgmist lehte: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
