# Informativa sulla privacy

_Ultimo aggiornamento il 17 gennaio 2022_

La presente informativa sulla privacy descrive le nostre politiche e procedure sulla raccolta, l'uso e la divulgazione delle sue informazioni quando usa il Servizio e si informa sui Suoi diritti alla privacy e su come la legge la protegge.

Utilizziamo i Suoi Dati Personali per fornire e migliorare il Servizio. Utilizzando il Servizio, Lei accetta la raccolta e l'utilizzo delle informazioni in conformità con la presente Informativa sulla Privacy.

## Interpretazione e definizioni

### Interpretazione

Le parole di cui la lettera iniziale è maiuscola hanno significati definiti nelle seguenti condizioni. Le seguenti definizioni hanno lo stesso significato indipendentemente dal fatto che appaiano al singolare o al plurale.

### Definizioni

Ai fini della presente Informativa sulla Privacy:

- **Account** indica un account unico creato per Lei per accedere al nostro Servizio o a parti del nostro Servizio.

- **Affiliato** significa un'entità che controlla, è controllata da o è sotto controllo comune con una parte, dove per "controllo" si intende la proprietà del 50% o più delle azioni, interessi azionari o altri titoli con diritto di voto per l'elezione degli amministratori o altra autorità di gestione.

- **Applicazione** indica il programma software fornito dalla Commissione Europea scaricato da Lei su qualsiasi dispositivo elettronico, denominato FaST

- **Commissione Europea** (indicata come "Commissione Europea", "Noi", "Ci" o "Nostro" nel presente Accordo) si riferisce alla Commissione Europea, DG DEFIS, Unità I3 Dati Spaziali per le sfide e la crescita della società, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgio

- **GDPR** è il Regolamento Generale sulla Protezione dei Dati

- **Organismo pagante della PAC** si riferisce a:

  - Per gli utenti dell'Andalusia: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
  - Per gli utenti di Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
  - Per gli utenti del Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
  - Per gli utenti dell'Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
  - Per gli utenti della Vallonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgio
  - Per gli utenti della Grecia: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
  - Per gli utenti della Bulgaria: Държавен фонд "Земеделие", Софффия 1618, "Цар Борис III" 136, България
  - Per gli utenti della Slovacchia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
  - Per gli utenti della Romania: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România

- **Controllore dei dati** (indicato come "Noi", "Ci" o "Nostro" nel presente accordo), ai fini del GDPR, si riferisce alla Commissione Europea, in quanto soggetto giuridico che determina le finalità e i mezzi del trattamento dei Dati Personali.

- **Dispositivo** indica qualsiasi dispositivo in grado di accedere al Servizio, come un computer, un telefono cellulare o una tavoletta digitale.

- **Dati Personali** è qualsiasi informazione che si riferisce ad un individuo identificato o identificabile. Ai fini del GDPR, per Dati Personali si intende qualsiasi informazione relativa all'Utente come un nome, un numero di identificazione, dati di localizzazione, un identificatore online o uno o più fattori specifici dell'identità fisica, fisiologica, genetica, mentale, economica, culturale o sociale.

- **Servizio** si riferisce all'Applicazione.

- Per **Fornitore di servizi** si intende qualsiasi persona fisica o giuridica che elabora i dati per conto della Commissione Europea. Si riferisce a società o persone terze impiegate dalla Commissione Europea o dall'Organismo pagante per facilitare il Servizio, per fornire il Servizio per conto della Commissione Europea, per eseguire servizi relativi al Servizio o per assistere la Commissione Europea e l'Organismo pagante nell'analisi delle modalità di utilizzo del Servizio. Ai fini del GDPR, i fornitori di servizi sono considerati responsabili del trattamento dei dati.

- **Servizio di autenticazione di terze parti** si riferisce a qualsiasi servizio attraverso il quale un Utente può accedere o creare un account per utilizzare il Servizio.

- **Servizio di opt-in di terze parti** si riferisce a qualsiasi servizio esterno che è stato registrato sul Servizio e al quale l'Utente può effettuare l'opt-in.

- **Metriche di utilizzo** si riferisce ai dati raccolti automaticamente, generati dall'uso del Servizio o dall'infrastruttura del Servizio stesso (per esempio, la durata della visita di una pagina).

- **Lei** indica l'individuo che accede o utilizza il Servizio, o la società o altra persona giuridica per conto della quale tale individuo accede o utilizza il Servizio, a seconda dei casi. Ai sensi del GDPR (Regolamento generale sulla protezione dei dati), Lei può essere indicato come il Soggetto dei dati o come l'Utente in quanto è la persona fisica che utilizza il Servizio.

## Raccolta e utilizzo dei Suoi Dati Personali

### Tipi di dati raccolti

#### Dati Personali

Durante l'utilizzo del nostro servizio, potremmo chiederle di fornirci alcune informazioni di identificazione personale che possono essere utilizzate per contattarla o identificarla. Le informazioni di identificazione personale possono includere, ma non sono limitate a

- Indirizzo e-mail
- Nome e cognome
- numero di telefono
- Indirizzo, Stato, Provincia, CAP, Città
- Messaggi e allegati che scambi con altri utenti (anche con il vostro Organismo Pagatore della CAP)
- Foto georeferenziate che scambiate sulla piattaforma (anche con il vostro Organismo Pagatore della CAP)
- Descrizione della Sua azienda agricola (ad esempio, appezzamenti, colture), pratiche agricole (ad esempio, raccomandazioni di concimazione che Lei riceve), dati del suolo che Lei inserisce nel Servizio
- Metriche di utilizzo (vedi sotto)

#### Metriche di utilizzo

Metriche di utilizzo sono raccolte automaticamente quando si utilizza il Servizio.

Metriche di utilizzo possono includere informazioni quali l'indirizzo di protocollo Internet del Suo dispositivo (ad esempio l'indirizzo IP), il tipo di browser, la versione del browser, le pagine del nostro Servizio che Lei visita, l'ora e la data della Sua visita, il tempo trascorso su quelle pagine, gli identificatori unici del dispositivo e altri dati diagnostici.

Quando accedi al Servizio da o attraverso un dispositivo mobile, possiamo raccogliere automaticamente alcune informazioni, tra cui, ma non solo, il tipo di dispositivo mobile che utilizzi, l'ID unico del Suo dispositivo mobile, l'indirizzo IP del Suo dispositivo mobile, il Suo sistema operativo mobile, il tipo di browser Internet mobile che utilizza, gli identificatori unici del dispositivo e altri dati diagnostici.

Possiamo anche raccogliere informazioni che il Suo browser invia ogni volta che visita il nostro Servizio o quando accede al Servizio da o attraverso un dispositivo mobile.

#### Informazioni raccolte durante l'utilizzo dell'Applicazione

Durante l'utilizzo della nostra Applicazione, potremmo raccogliere, con il Suo permesso, le seguenti informazioni

- Informazioni relative alla sua posizione
- Immagini e altre informazioni dalla fotocamera e dalla libreria fotografica del Suo dispositivo

Utilizziamo queste informazioni per fornire la funzione di foto georeferenziate del Nostro Servizio e per localizzarti sulle nostre mappe satellitari. Queste informazioni possono essere caricate sui nostri server e/o sul server di un fornitore di servizi o essere semplicemente memorizzate sul Suo dispositivo.

Puoi abilitare o disabilitare l'accesso a queste informazioni in qualsiasi momento, attraverso le impostazioni del Suo dispositivo.

### Utilizzo dei Suoi Dati Personali

Il nostro servizio è attualmente in fase beta e quindi i Suoi Dati Personali saranno utilizzati solo per **sviluppare, migliorare e testare il nostro servizio**.

Durante il Suo utilizzo del Servizio, potremmo contattarla via email, telefonate, SMS o altre forme equivalenti di comunicazione elettronica, come le notifiche push di un'applicazione mobile, per quanto riguarda gli aggiornamenti o le comunicazioni informative relative alle funzionalità, ai miglioramenti o alle novità dei nostri Servizi, compresi gli aggiornamenti di sicurezza, quando necessario o ragionevole per la loro attuazione. Conserveremo inoltre i Suoi Dati Personali per assistere e gestire le richieste che Lei ci farà.

### Conservazione dei Suoi Dati Personali

Conserveremo i Suoi Dati Personali solo per il tempo necessario per gli scopi indicati nella presente Informativa sulla privacy. Conserveremo e utilizzeremo i Dati Personali dell'Utente nella misura necessaria per adempiere ai nostri obblighi legali (ad esempio, se ci viene richiesto di conservare i dati dell'Utente per rispettare le leggi applicabili), risolvere le controversie e applicare i nostri accordi e politiche legali.

Conserveremo anche i dati di utilizzo per scopi di analisi interna. I parametri di utilizzo sono generalmente conservati per un periodo di tempo più breve, tranne quando questi dati sono utilizzati per rafforzare la sicurezza o per migliorare la funzionalità del nostro servizio, o siamo legalmente obbligati a conservare questi dati per periodi di tempo più lunghi.

### Trasferimento dei Suoi Dati Personali

Le Suoi informazioni, compresi i Dati Personali, sono trattate presso le Nostre sedi operative e in qualsiasi altro luogo in cui si trovano le parti coinvolte nel trattamento. Ciò significa che queste informazioni possono essere trasferite a - e mantenute su - computer situati al di fuori del Suo stato, provincia, paese o altra giurisdizione governativa dove le leggi sulla protezione dei dati possono essere diverse da quelle della Sua giurisdizione.

Il Suo consenso a questa Informativa sulla privacy seguito dall'invio di tali informazioni rappresenta il Suo consenso a tale trasferimento.

Prenderemo tutte le misure ragionevolmente necessarie per garantire che i Suoi dati siano trattati in modo sicuro e in conformità con la presente Informativa sulla privacy e nessun trasferimento dei Suoi Dati Personali avrà luogo verso un'organizzazione o un paese a meno che non ci siano controlli adeguati in atto compresa la sicurezza dei Suoi dati e altre informazioni personali.

### Divulgazione dei Suoi Dati Personali

#### Applicazione della legge

In determinate circostanze, potremmo essere tenuti a divulgare i Suoi Dati Personali se richiesto dalla legge o in risposta a richieste valide da parte di autorità pubbliche (ad esempio un tribunale o un'agenzia governativa).

#### Altri requisiti legali

Possiamo divulgare i Suoi Dati Personali nella convinzione in buona fede che tale azione sia necessaria per:

- Rispettare un obbligo legale
- Proteggere e difendere i diritti o la proprietà della Commissione Europea
- Prevenire o indagare su possibili atti illeciti in relazione al Servizio
- Proteggere la sicurezza personale degli utenti del Servizio o del pubblico
- Proteggere da responsabilità legali

### Sicurezza dei Suoi Dati Personali

La sicurezza dei Suoi Dati Personali è importante per noi, ma ricorda che nessun metodo di trasmissione su Internet, o metodo di archiviazione elettronica è sicuro al 100%. Sebbene ci sforziamo di utilizzare mezzi commercialmente accettabili per proteggere i Suoi Dati Personali, non possiamo garantirne la sicurezza assoluta.

## GDPR Privacy

### Base giuridica per il trattamento dei Dati Personali ai sensi del GDPR

Possiamo trattare i Dati Personali alle seguenti condizioni:

- **Consenso:** Ha dato il Suo consenso al trattamento dei Dati Personali per uno o più scopi specifici.

- **Esecuzione di un contratto:** La fornitura di Dati Personali è necessaria per l'esecuzione di un accordo con Lei e/o per eventuali obblighi precontrattuali dello stesso.

- **Obblighi legali:** Il trattamento dei Dati Personali è necessario per l'adempimento di un obbligo legale al quale la Commissione Europea è soggetta.

- Interessi vitali:\*\* Il trattamento dei Dati Personali è necessario per proteggere i Suoi interessi vitali o di un'altra persona fisica.

- **Interessi pubblici:** Il trattamento dei Dati Personali è legato ad un compito che viene svolto nell'interesse pubblico o nell'esercizio dei poteri ufficiali conferiti alla Commissione Europea.

- **Interessi legittimi:** Il trattamento dei Dati Personali è necessario ai fini dei legittimi interessi perseguiti dalla Commissione Europea.

In ogni caso, saremo lieti di aiutare a chiarire la base giuridica specifica che si applica al trattamento, e in particolare se la fornitura di Dati Personali è un requisito legale o contrattuale, o un requisito necessario per stipulare un contratto.

### I Suoi diritti secondo il GDPR

La Commissione Europea si impegna a rispettare la riservatezza dei Suoi Dati Personali e a garantire che Lei possa esercitare i Suoi diritti.

Lei ha il diritto, ai sensi della presente Informativa sulla privacy e della legge dell'UE, di:

- **Richiedere l'accesso ai Suoi Dati Personali.** Il diritto di accedere, aggiornare o cancellare le informazioni che abbiamo su di Lei. Quando è possibile, puoi accedere, aggiornare o richiedere la cancellazione dei Suoi Dati Personali direttamente nella sezione delle impostazioni del Suo account. Se non è in grado di eseguire queste azioni da solo, La preghiamo di contattarci per assisterLa. Questo Le permette anche di ricevere una copia dei Dati Personali in nostro possesso che La riguardano.

- **Richiedere la correzione dei Dati Personali in nostro possesso che la riguardano.** Lei ha il diritto di far correggere qualsiasi informazione incompleta o imprecisa in nostro possesso che la riguarda.

- **Opporsi al trattamento dei Suoi Dati Personali.** Questo diritto esiste quando ci basiamo su un interesse legittimo come base legale per il nostro trattamento e c'è qualcosa nella tua situazione particolare che ti spinge ad opporti al nostro trattamento dei Suoi Dati Personali per questo motivo. Lei ha anche il diritto di opporsi se stiamo elaborando i Suoi Dati Personali per scopi di marketing diretto.

- **Richiedere la cancellazione dei Suoi Dati Personali.** Lei ha il diritto di chiederci di cancellare o rimuovere i Suoi Dati Personali quando non c'è una buona ragione per continuare a trattarli.

- **Richiedere il trasferimento dei Suoi Dati Personali.** Chiedere il trasferimento dei Suoi Dati Personali.\*\* Ti forniremo, o a una terza parte da Lei scelta, i Suoi Dati Personali in un formato strutturato, comunemente usato e leggibile da una macchina. Si prega di notare che questo diritto si applica solo alle informazioni automatizzate di cui Lei ci ha inizialmente fornito il consenso all'utilizzo o dove abbiamo utilizzato le informazioni per eseguire un contratto con Lei.

- **Ritirare il Suo consenso.** Lei ha il diritto di ritirare il Suo consenso all'utilizzo dei Suoi Dati Personali. Se ritiri il Suo consenso, potremmo non essere in grado di fornirti l'accesso ad alcune funzionalità specifiche del servizio.

### Esercizio dei Suoi diritti di protezione dei dati GDPR

Può esercitare i Suoi diritti di accesso, rettifica, cancellazione e opposizione contattandoci. La preghiamo di notare che potremmo chiederLe di verificare la Sua identità prima di rispondere a tali richieste. Se Lei fa una richiesta, faremo del nostro meglio per risponderLe il più presto possibile.

Lei ha il diritto di presentare un reclamo a un'autorità per la protezione dei dati in merito alla nostra raccolta e all'uso dei Suoi Dati Personali. Per ulteriori informazioni, si prega di contattare l'autorità locale per la protezione dei dati nello Spazio economico europeo.

## Link ad altri siti web e servizi

Il nostro Servizio può contenere link ad altri siti web o servizi che non sono gestiti da noi. Se Lei clicca su un link di terzi, sarà indirizzato al sito di quei terzi. Le consigliamo vivamente di esaminare l'informativa sulla privacy di ogni sito che visita.

Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per il contenuto, le politiche sulla privacy o le pratiche di qualsiasi sito o servizio di terzi.

## Modifiche alla presente Informativa sulla privacy

Possiamo aggiornare la nostra Informativa sulla Privacy di tanto in tanto. La informeremo di qualsiasi modifica pubblicando la nuova Informativa sulla Privacy su questa pagina.

La informeremo via email e/o con un avviso ben visibile sul nostro servizio, prima che la modifica diventi effettiva e aggiorneremo la data "Ultimo aggiornamento" all'inizio di questa Informativa sulla Privacy.

Si consiglia di rivedere periodicamente la presente Informativa sulla privacy per eventuali modifiche. Le modifiche alla presente informativa sulla privacy sono effettive quando vengono pubblicate su questa pagina.

## Contattateci

Se ha domande su questa Privacy Policy, può contattarci:

- Per e-mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Visitando questa pagina del nostro sito web: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
