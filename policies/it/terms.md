# Termini e Condizioni

_Ultimo aggiornamento il 17 gennaio 2022_

Si prega di leggere attentamente questi Termini e Condizioni prima di utilizzare il nostro servizio.

## Interpretazione e definizioni

### Interpretazione

Le parole di cui la lettera iniziale è maiuscola hanno significati definiti nelle seguenti condizioni. Le seguenti definizioni hanno lo stesso significato indipendentemente dal fatto che appaiano al singolare o al plurale.

### Definizioni

Ai fini di questi Termini e Condizioni:

- **Applicazione** indica il programma software fornito dalla Commissione Europea scaricato da Lei su qualsiasi dispositivo elettronico, denominato FaST

- **Application Store** indica il servizio di distribuzione digitale gestito e sviluppato da Apple Inc. (Apple App Store) o da Google Inc. (Google Play Store) in cui l'Applicazione è stata scaricata.

- **Affiliato** indica un'entità che controlla, è controllata da o è sotto controllo comune con una parte, dove per "controllo" si intende la proprietà del 50% o più delle azioni, degli interessi azionari o di altri titoli con diritto di voto per l'elezione degli amministratori o altra autorità di gestione.

- **Account** indica un conto unico creato per Lei per accedere al nostro Servizio o a parti del nostro Servizio.

- **Organismo pagante della PAC** si riferisce a:

  - Per gli utenti dell'Andalusia: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
  - Per gli utenti di Castilla y Leon: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
  - Per gli utenti del Piemonte: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
  - Per gli utenti dell'Estonia: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
  - Per gli utenti della Vallonia: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Belgio
  - Per gli utenti della Grecia: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
  - Per gli utenti della Bulgaria: Държавен фонд "Земеделие", Софффия 1618, "Цар Борис III" 136, България
  - Per gli utenti della Slovacchia: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
  - Per gli utenti della Romania: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România

- **Commissione Europea** (indicata come "Commissione Europea", "Noi", "Ci" o "Nostro" nel presente accordo) si riferisce alla Commissione Europea, DG DEFIS, Unità I3 Dati spaziali per le sfide sociali e la crescita, BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Belgio

- **Contenuto** si riferisce a contenuti quali testi, immagini o altre informazioni che possono essere pubblicati, caricati, collegati o altrimenti resi disponibili da Lei, indipendentemente dalla forma di tale contenuto.

- **Dispositivo** indica qualsiasi dispositivo in grado di accedere al Servizio, come un computer, un telefono cellulare o una tavoletta digitale.

- **Feedback** indica feedback, innovazioni o suggerimenti inviati da Lei in merito agli attributi, alle prestazioni o alle caratteristiche del nostro Servizio.

- **Servizio** si riferisce all'Applicazione.

- **Termini e Condizioni** (chiamati anche "Termini") indica i presenti Termini e Condizioni che costituiscono l'intero accordo tra Lei e la Commissione Europea in merito all'utilizzo del Servizio.

- **Servizio di autenticazione di terzi**: qualsiasi servizio di autenticazione dell'Utente fornito da un terzo (come un fornitore di identità nazionale di uno Stato membro dell'Unione europea) al quale la Commissione Europea delega l'autenticazione dell'Utente per accedere all'Applicazione

- **Servizio Opt-In di terzi**: qualsiasi servizio o contenuto (inclusi dati, informazioni, prodotti o servizi) fornito da terzi che può essere visualizzato, incluso o reso disponibile dal Servizio.

- **Lei** indica l'individuo che accede o utilizza il Servizio, o la società o altra entità legale per conto della quale tale individuo accede o utilizza il Servizio, a seconda dei casi.

## Riconoscimento

Questi sono i Termini e le Condizioni che regolano l'uso di questo Servizio e l'accordo che opera tra Lei e Noi. I presenti Termini e Condizioni stabiliscono i diritti e gli obblighi di tutti gli utenti per quanto riguarda l'uso del Servizio.

L'accesso e l'utilizzo del Servizio sono subordinati all'accettazione e al rispetto di questi Termini e Condizioni da parte dell'Utente. I presenti Termini e Condizioni si applicano a tutti i visitatori, utenti e altri che accedono o utilizzano il Servizio.

Accedendo o utilizzando il Servizio Lei accetta di essere vincolato da questi Termini e Condizioni. Se non sei d'accordo con qualsiasi parte di questi Termini e Condizioni, non puoi accedere al Servizio.

Lei dichiara di avere più di 18 anni. La Commissione Europea non permette ai minori di 18 anni di utilizzare il Servizio.

L'accesso e l'utilizzo del Servizio sono inoltre subordinati all'accettazione e al rispetto da parte dell'Utente dell'Informativa sulla privacy della Commissione Europea. La nostra Informativa sulla Privacy descrive le nostre politiche e procedure sulla raccolta, l'uso e la divulgazione dei Suoi dati personali quando utilizza l'Applicazione o il Sito web e La informa sui Suoi diritti alla privacy e su come la legge La protegge. La preghiamo di leggere attentamente la nostra Informativa sulla privacy prima di utilizzare il nostro Servizio.

## Account Utente

Quando l'Utente crea un account con noi, deve fornirci informazioni accurate, complete e aggiornate in ogni momento. Il mancato rispetto di tali informazioni costituisce una violazione dei Termini, che può comportare l'immediata chiusura del Suo account sul nostro Servizio.

L'Utente è responsabile della salvaguardia della password che utilizza per accedere al Servizio e di qualsiasi attività o azione svolta con la sua password, sia che si tratti del nostro Servizio o di un Servizio di autenticazione di terze parti.
L'Utente accetta di non divulgare la propria password a terzi. L'Utente deve informarci immediatamente quando viene a conoscenza di qualsiasi violazione della sicurezza o uso non autorizzato del Suo account.

L'Utente non può utilizzare come nome utente il nome di un'altra persona o entità o che non è legalmente disponibile per l'uso, un nome o un marchio che è soggetto a diritti di un'altra persona o entità diversa dall'Utente senza un'adeguata autorizzazione, o un nome che sia altrimenti offensivo, volgare o osceno.

## Contenuto

### Il tuo diritto di caricare contenuti

Il nostro Servizio le permette di caricare un Contenuto (ad esempio, foto georeferenziate). Lei è responsabile del Contenuto che pubblica sul Servizio, inclusa la sua legalità, affidabilità e appropriatezza.

Lei dichiara e garantisce che: (i) il Contenuto è Suo (ne è il proprietario) o ha il diritto di usarlo e di concederci i diritti e la licenza come previsto in questi Termini, e (ii) la pubblicazione del Suo Contenuto su o attraverso il Servizio non viola i diritti di privacy, i diritti pubblicitari, i diritti d'autore, i diritti contrattuali o qualsiasi altro diritto di qualsiasi persona.

### Restrizioni sul Contenuto

La Commissione Europea non è responsabile del Contenuto degli utenti del Servizio. L'Utente comprende e accetta espressamente di essere l'unico responsabile del Contenuto e di tutte le attività che si verificano sotto il Suo account, sia da parte sua che di terzi che utilizzano il Suo account.

Lei non può trasmettere alcun Contenuto che sia illegale, offensivo, sconvolgente, destinato a disgustare, minaccioso, calunnioso, diffamatorio, osceno o altrimenti discutibile. Esempi di tale Contenuto discutibile includono, ma non sono limitati a, quanto segue:

- Contenuti illegali o che promuovono attività illegali.
- Contenuti diffamatori, discriminatori o di cattivo gusto, inclusi riferimenti o commenti su religione, razza, orientamento sessuale, sesso, origine nazionale/etnica o altri gruppi specifici.
- Spam, generato automaticamente o casualmente, che costituisce pubblicità non autorizzata o non richiesta, lettere a catena, qualsiasi altra forma di sollecitazione non autorizzata, o qualsiasi forma di lotteria o gioco d'azzardo.
- Contenere o installare virus, worm, malware, cavalli di Troia o altri contenuti progettati o destinati a interrompere, danneggiare o limitare il funzionamento di qualsiasi software, hardware o attrezzatura di telecomunicazione o a danneggiare o ottenere l'accesso non autorizzato a qualsiasi dato o altra informazione di una terza persona.
- Violare i diritti di proprietà di qualsiasi parte, compresi brevetti, marchi, segreti commerciali, copyright, diritti di pubblicità o altri diritti.
- Impersonare qualsiasi persona o entità, compresa la Commissione Europea e i suoi dipendenti o rappresentanti.
- Violare la privacy di qualsiasi terza persona.
- Informazioni e caratteristiche false.

La Commissione Europea si riserva il diritto, ma non l'obbligo, di determinare, a sua esclusiva discrezione, se un Contenuto è appropriato e conforme alle presenti Condizioni, di rifiutare o rimuovere tale Contenuto. La Commissione Europea si riserva inoltre il diritto di effettuare formattazioni e modifiche e di cambiare le modalità di qualsiasi Contenuto. La Commissione Europea può anche limitare o revocare l'uso del Servizio se l'Utente pubblica tale Contenuto discutibile.

Poiché la Commissione Europea non può controllare tutti i contenuti pubblicati dagli utenti e/o da terzi sul Servizio, l'Utente accetta di utilizzare il Servizio a proprio rischio. L'Utente è consapevole che utilizzando il Servizio può essere esposto a contenuti che può trovare offensivi, indecenti, scorretti o discutibili, e accetta che in nessuna circostanza la Commissione Europea sarà responsabile in alcun modo per qualsiasi Contenuto, compresi eventuali errori o omissioni in qualsiasi Contenuto, o qualsiasi perdita o danno di qualsiasi tipo sostenuto come risultato dell'utilizzo di qualsiasi Contenuto.

### Backup del Contenuto

Anche se vengono eseguiti backup regolari del Contenuto, la Commissione Europea non garantisce che non ci sarà alcuna perdita o corruzione dei dati.

Punti di backup corrotti o non validi possono essere causati, senza limitazione, da un Contenuto che è corrotto prima di essere sottoposto a backup o che cambia durante il tempo in cui viene eseguito un backup.

La Commissione Europea fornirà supporto e cercherà di risolvere qualsiasi problema noto o scoperto che possa influenzare i backup del Contenuto. Tuttavia, l'Utente riconosce che la Commissione Europea non ha alcuna responsabilità relativa all'integrità del Contenuto o al mancato ripristino del Contenuto in uno stato utilizzabile.

Lei accetta di mantenere una copia completa e accurata di qualsiasi Contenuto in un luogo indipendente dal Servizio.

## Copyright Policy

### Violazione della proprietà intellettuale

Noi rispettiamo i diritti di proprietà intellettuale degli altri. La nostra politica è quella di rispondere a qualsiasi reclamo che il Contenuto pubblicato sul Servizio violi un copyright o altre violazioni della proprietà intellettuale di qualsiasi persona.

Se Lei è proprietario di copyright, o autorizzato per conto di copyright, e ritiene che il lavoro protetto da copyright sia stato copiato in un modo che costituisce una violazione del copyright che sta avvenendo attraverso il Servizio, è necessario inviare la Sua notifica per iscritto all'attenzione del nostro agente di copyright via e-mail a contact@fastplatform.eu e includere nella notifica una descrizione dettagliata della presunta violazione.

L'Utente può essere ritenuto responsabile dei danni (compresi i costi e le spese legali) per aver dichiarato erroneamente che un qualsiasi Contenuto sta violando il Suo copyright.

## Il Suo feedback a noi

Cede tutti i diritti, i titoli e gli interessi in qualsiasi Feedback che fornisci alla Commissione Europea. Se per qualsiasi motivo tale assegnazione è inefficace, l'Utente accetta di concedere alla Commissione Europea un diritto e una licenza non esclusivi, perpetui, irrevocabili, senza royalty, in tutto il mondo per utilizzare, riprodurre, divulgare, sub-licenziare, distribuire, modificare e sfruttare tale Feedback senza restrizioni.

## Link ad altri siti web

Il nostro Servizio può contenere link a siti web o servizi di terzi che non sono di proprietà o controllati dalla Commissione Europea.

La Commissione Europea non ha alcun controllo e non si assume alcuna responsabilità per il Contenuto, le politiche sulla privacy o le pratiche di qualsiasi sito web o Servizio di terzi. L'Utente riconosce e accetta inoltre che la Commissione Europea non è responsabile, direttamente o indirettamente, per qualsiasi danno o perdita causata o presunta causata da o in connessione con l'uso di o l'affidamento su tali contenuti, beni o servizi disponibili su o attraverso tali siti web o servizi.

Le consigliamo vivamente di leggere i termini e le condizioni e le politiche sulla privacy di qualsiasi sito web o Servizio di terzi che Lei visita.

## Cessazione

Possiamo terminare o sospendere il Suo account immediatamente, senza preavviso o responsabilità, per qualsiasi motivo, incluso, senza limitazione, se Lei viola i presenti Termini e Condizioni.

In caso di rescissione, il Suo diritto di utilizzare il Servizio cesserà immediatamente. Se desidera terminare il tuo account, puoi semplicemente interrompere l'utilizzo del Servizio.

## Limitazione di responsabilità

A prescindere da eventuali danni che l'Utente potrebbe subire, l'intera responsabilità della Commissione Europea e dei suoi fornitori ai sensi di qualsiasi disposizione delle presenti Condizioni e il rimedio esclusivo dell'Utente per tutto quanto sopra è limitata all'importo effettivamente pagato dall'Utente attraverso il Servizio o 100 USD se l'Utente non ha acquistato nulla attraverso il Servizio.

Nella misura massima consentita dalla legge applicabile, in nessun caso la Commissione Europea o i suoi fornitori saranno responsabili per danni speciali, incidentali, indiretti o consequenziali di qualsiasi tipo (inclusi, ma non limitatamente a, danni per perdita di profitti, perdita di dati o altre informazioni, per interruzione di attività, per lesioni personali, perdita della privacy derivanti da o in qualsiasi modo connessi all'utilizzo o all'impossibilità di utilizzare il Servizio, il software di terzi e/o l'hardware di terzi utilizzato con il Servizio, o altrimenti in relazione a qualsiasi disposizione delle presenti Condizioni), anche se la Commissione Europea o qualsiasi fornitore è stato avvisato della possibilità di tali danni e anche se il rimedio non raggiunge il suo scopo essenziale.

Alcuni stati non consentono l'esclusione di garanzie implicite o la limitazione di responsabilità per danni incidentali o consequenziali, il che significa che alcune delle limitazioni di cui sopra potrebbero non essere applicabili. In questi stati, la responsabilità di ciascuna parte sarà limitata nella misura massima consentita dalla legge.

## Disclaimer "COSÌ COM'È" e "COME DISPONIBILE"

Il Servizio viene fornito all'Utente "COSÌ COM'È" e "COME DISPONIBILE" e con tutti gli errori e i difetti senza garanzia di alcun tipo. Nella misura massima consentita dalla legge applicabile, la Commissione Europea, per proprio conto e per conto delle sue Affiliate e dei suoi e loro rispettivi licenzianti e fornitori di servizi, declina espressamente tutte le garanzie, siano esse esplicite, implicite, legali o di altro tipo, in relazione al Servizio, comprese tutte le garanzie implicite di commerciabilità, idoneità per uno scopo particolare, titolo e non violazione, e le garanzie che possono derivare da una trattativa, da un'esecuzione, dall'uso o dalla pratica commerciale. Senza limitazione di quanto sopra, la Commissione Europea non fornisce alcuna garanzia o impegno e non rilascia alcuna dichiarazione di alcun tipo che il Servizio soddisferà le esigenze dell'Utente, raggiungerà i risultati previsti, sarà compatibile o funzionerà con qualsiasi altro software, applicazione, sistema o Servizio, funzionerà senza interruzioni, soddisferà qualsiasi standard di prestazione o affidabilità o sarà privo di errori o che qualsiasi errore o difetto potrà o sarà corretto.

Senza limitare quanto sopra, né la Commissione Europea né alcun fornitore della Commissione Europea rilascia alcuna dichiarazione o garanzia di alcun tipo, espressa o implicita (i) per quanto riguarda il funzionamento o la disponibilità del Servizio, o le informazioni, il Contenuto e i materiali o prodotti inclusi in esso; (ii) che il Servizio sarà ininterrotto o privo di errori; (iii) per quanto riguarda l'accuratezza, l'affidabilità o l'aggiornamento di qualsiasi informazione o Contenuto fornito attraverso il Servizio; o (iv) che il Servizio, i suoi server, il Contenuto o le e-mail inviate da o per conto della Commissione Europea siano privi di virus, script, trojan horse, worms, malware, timebombs o altri componenti dannosi.

Alcune giurisdizioni non consentono l'esclusione di alcuni tipi di garanzie o limitazioni sui diritti legali applicabili di un consumatore, quindi alcune o tutte le esclusioni e limitazioni di cui sopra potrebbero non essere applicabili a Lei. Ma in tal caso le esclusioni e le limitazioni di cui alla presente sezione saranno applicate nella misura massima applicabile ai sensi della legge applicabile.

## Legge applicabile

Le leggi dell'Unione Europea, integrate se necessario dalla legge del Belgio, regoleranno le presenti Condizioni e il Suo utilizzo del Servizio. Il Suo uso dell'Applicazione può anche essere soggetto ad altre leggi locali, statali, nazionali o internazionali.

## Risoluzione delle controversie

In caso di dubbi o controversie sul Servizio, Lei accetta di tentare innanzitutto di risolvere la controversia in modo informale contattando la Commissione Europea.

## Per gli utenti dell'Unione Europea (UE)

Se Lei è un consumatore dell'Unione Europea, Lei beneficerà di tutte le disposizioni obbligatorie della legge del paese in cui risiede.

## Clausola e rinuncia

### Scindibilità

Se una qualsiasi disposizione di questi Termini è ritenuta inapplicabile o non valida, tale disposizione sarà modificata e interpretata per realizzare gli obiettivi di tale disposizione nella misura più ampia possibile secondo la legge applicabile e le restanti disposizioni continueranno ad avere pieno vigore ed effetto.

### Rinuncia

Ad eccezione di quanto previsto nel presente documento, il mancato esercizio di un diritto o la mancata richiesta di adempimento di un obbligo ai sensi delle presenti Condizioni non influisce sulla capacità di una parte di esercitare tale diritto o richiedere tale adempimento in qualsiasi momento successivo, né la rinuncia a una violazione costituisce una rinuncia a qualsiasi violazione successiva.

## Interpretazione della traduzione

I presenti Termini e Condizioni possono essere stati tradotti se li abbiamo messi a Sua disposizione sul nostro Servizio.

Lei accetta che il testo originale inglese prevarrà in caso di controversia.

## Modifiche ai presenti Termini e Condizioni

Ci riserviamo il diritto, a Nostra esclusiva discrezione, di modificare o sostituire le presenti Condizioni in qualsiasi momento. Se una revisione è materiale, faremo il possibile per fornire un preavviso di almeno 30 giorni prima che i nuovi termini entrino in vigore. Ciò che costituisce un cambiamento materiale sarà determinato a Nostra esclusiva discrezione.

Continuando ad accedere o a utilizzare il nostro Servizio dopo che tali revisioni sono entrate in vigore, l'Utente accetta di essere vincolato dai termini rivisti. Se Lei non accetta i nuovi termini, in tutto o in parte, La preghiamo di smettere di utilizzare il sito web e il Servizio.

## Contatti

Se ha delle domande su questi Termini e Condizioni, può contattarci:

- Per e-mail: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Visitando questa pagina del nostro sito web: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
