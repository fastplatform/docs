# Правила и условия

_Последна актуализация на януари 17, 2022_

Моля, прочетете внимателно тези условия, преди да използвате нашата услуга.

## Тълкуване и определения

### Тълкуване

Думите, чиято начална буква е изписана с главна буква, имат значения, определени при следните условия. Следните определения имат едно и също значение независимо от това дали се появяват в единствено или в множествено число.

### Определения

За целите на настоящите Общи условия:

- **Приложение** означава софтуерната програма, предоставена от Европейската комисия, изтеглена от Вас на каквото и да е електронно устройство, наречена FaST

- **Aplication Store** означава услугата за цифрово разпространение, управлявана и разработена от Apple Inc. (Apple App Store) или Google Inc. (Google Play Store), в която е изтеглено Приложението.

- **Свързано лице** означава субект, който контролира, е контролиран от или е под общ контрол с дадена страна, където "контрол" означава притежание на 50% или повече от акциите, дяловото участие или други ценни книжа, които имат право на глас за избор на директори или други ръководни органи.

- **Акаунт** означава уникален акаунт, създаден за Вас за достъп до нашата Услуга или части от нея.

- **Разплащателна агенция на САР** означава:
    - За потребители от Андалусия: Organismo Pagador de Andalucia, Consejería De Agricultura, Ganadería, Pesca Y Desarrollo Sostenible, Junta De Andalucía, C/ Tabladilla s/n, 41071-Sevilla, España
    - За потребители от Кастилия и Леон: Organismo Pagador de Castilla y Leon, C/ Rigoberto Cortejoso, 14, 7ª planta, 47014 Valladolid, España
    - За потребителите от Пиемонте: Agenzia Regionale Piemontese per le Erogazioni in Agricoltura (ARPEA), Via Bogino 23, 10123 Torino, Italia
    - За потребители от Естония: Põllumajanduse Registrite ja Informatsiooni Amet (PRIA), 51010, Tähe 4, 50103 Tartu, Eesti
    - За потребителите от Валония: Organisme Payeur de Wallonie (OPW), Chaussée de Louvain, 14, 5000 Namur, Белгия
    - За потребители от Гърция: ΟΠΕΚΕΠΕ, Δομοκού 5, Αθήνα,Αττική 10445, Ελλάδα
    - За потребителите в България: Държавен фонд "Земеделие", Соффия 1618, "Цар Борис III" 136, България
    - За потребители от Словакия: Pôdohospodárska platobná agentúra (PPA), Hraničná ul. č. 12, 815 26 Bratislava, Slovensko
    - За потребители от Румъния: Agencia de Plăți și Intervenție pentru Agricultură (APIA), Bulevardul Carol I nr. 17, sector 2, București, România

- **Европейската комисия** (наричана в настоящото споразумение "Европейската комисия", "Ние", "Нас" или "Нашата") се отнася до Европейската комисия, ГД DEFIS, отдел I3 "Космически данни за обществени предизвикателства и растеж", BREY 9/260, Oudergemselaan / Avenue d'Auderghem, 1049 Bruxelles, Белгия

- **Съдържанието** се отнася до съдържание като текст, изображения или друга информация, която може да бъде публикувана, качена, свързана или предоставена по друг начин от Вас, независимо от формата на това съдържание.

- **Устройство** означава всяко устройство, което може да получи достъп до Услугата, като например компютър, мобилен телефон или цифров таблет.

- **Отзиви** означава обратна връзка, нововъведения или предложения, изпратени от Вас по отношение на характеристиките, работата или функциите на нашата Услуга.

- **Услуга** се отнася до Приложението.

- **Условия** (наричани също "Условия") означава настоящите Общи условия, които представляват цялостното споразумение между Вас и Европейската комисия относно използването на Услугата.

- **Услуга за удостоверяване на самоличността на трета страна** означава всяка услуга за удостоверяване на самоличността на потребителя, предоставяна от трета страна (като например национален доставчик на услуги за установяване на самоличност от държава - членка на Европейския съюз), на която Европейската комисия делегира удостоверяване на самоличността на потребителя за достъп до Заявлението

- **Услуга за избор на трета страна** означава всякакви услуги или съдържание (включително данни, информация, продукти или услуги), предоставени от трета страна, които могат да бъдат показвани, включвани или предоставяни от Услугата.

- **Вие** означава физическото лице, което осъществява достъп до или използва Услугата, или дружеството, или друго юридическо лице, от името на което това лице осъществява достъп до или използва Услугата, според случая.

## Потвърждение

Това са Общите условия, които регулират използването на тази Услуга, и споразумението, което действа между Вас и Нас. Тези Общи условия определят правата и задълженията на всички потребители по отношение на използването на Услугата.

Вашият достъп до Услугата и нейното използване се обуславя от приемането и спазването от Ваша страна на настоящите Общи условия. Настоящите Общи условия се прилагат за всички посетители, потребители и други лица, които имат достъп до Услугата или я използват.

С достъпа до или използването на Услугата Вие се съгласявате да бъдете обвързани с настоящите Общи условия. Ако не сте съгласни с която и да е част от настоящите Общи условия, нямате право на достъп до Услугата.

Вие декларирате, че сте навършили 18 години. Европейската комисия не разрешава на лица под 18 години да използват Услугата.

Вашият достъп до Услугата и нейното използване също така зависят от приемането и спазването на Политиката за поверителност на Европейската комисия. Нашата Политика за поверителност описва нашите политики и процедури за събирането, използването и разкриването на Вашата лична информация, когато използвате Приложението или Уебсайта, и Ви разказва за Вашите права на поверителност и как Ви защитава законът. Моля, прочетете внимателно Нашата Политика за поверителност, преди да използвате Нашата услуга.

## Потребителски акаунти

Когато създавате акаунт при Нас, Вие трябва да ни предоставите информация, която е точна, пълна и актуална по всяко време. Ако не го направите, това представлява нарушение на Условията, което може да доведе до незабавно прекратяване на Вашия акаунт в Нашата услуга.

Вие носите отговорност за опазването на паролата, която използвате за достъп до Услугата, и за всички дейности или действия под Вашата парола, независимо дали паролата Ви е в Нашата услуга или в услуга за удостоверяване на трета страна.
Вие се съгласявате да не разкривате Вашата парола на трета страна. Трябва да ни уведомите незабавно, след като разберете за всяко нарушение на сигурността или неоторизирано използване на Вашия акаунт.

Нямате право да използвате като потребителско име име на друго физическо или юридическо лице или което не е законно достъпно за използване, име или търговска марка, които са обект на права на друго физическо или юридическо лице, различно от Вас, без съответното разрешение, или име, което е по друг начин обидно, вулгарно или неприлично.

## Съдържание

### Вашето право да качвате съдържание

Нашата Услуга Ви позволява да качвате Съдържание (напр. снимки с географски обозначения). Вие носите отговорност за Съдържанието, което публикувате в Услугата, включително за неговата законност, надеждност и уместност.

Вие заявявате и гарантирате, че: (i) Съдържанието е Ваше (притежавате го) или имате право да го използвате и да ни предоставите правата и лиценза, както е предвидено в настоящите Условия, и (ii) публикуването на Вашето Съдържание във или чрез Услугата не нарушава правата на личен живот, правата на публичност, авторските права, договорните права или други права на което и да е лице.

### Ограничения на съдържанието

Европейската комисия не носи отговорност за съдържанието на потребителите на Услугата. Вие изрично разбирате и се съгласявате, че носите пълната отговорност за Съдържанието и за всички дейности, които се извършват под Вашия акаунт, независимо дали са извършени от Вас или от трето лице, което използва Вашия акаунт.

Нямате право да предавате каквото и да е Съдържание, което е незаконно, обидно, разстройващо, с цел отвращение, заплаха, клевета, обида, непристойно или по друг начин неприемливо. Примерите за такова неприемливо Съдържание включват, но не се ограничават до следното:

- Незаконна или насърчаваща незаконна дейност.
- Клеветническо, дискриминационно или злонамерено съдържание, включително препратки или коментари относно религия, раса, сексуална ориентация, пол, национален/етнически произход или други целеви групи.
- Спам, генериран машинно или на случаен принцип, представляващ неразрешена или непоискана реклама, верижни писма, всяка друга форма на неразрешено привличане на клиенти или всяка форма на лотария или хазарт.
- Съдържане или инсталиране на вируси, червеи, зловреден софтуер, троянски коне или друго съдържание, което е проектирано или има за цел да наруши, повреди или ограничи функционирането на софтуер, хардуер или телекомуникационно оборудване или да повреди или получи неоторизиран достъп до данни или друга информация на трето лице.
- Нарушаване на права на собственост на която и да е страна, включително патент, търговска марка, търговска тайна, авторско право, право на публичност или други права.
- Да се представяте за лице или организация, включително Европейската комисия и нейните служители или представители.
- Нарушаване на неприкосновеността на личния живот на трето лице.
- Невярна информация и функции.

Европейската комисия си запазва правото, но не и задължението, по свое усмотрение да определи дали дадено Съдържание е подходящо и съответства на настоящите Условия, да откаже или премахне това Съдържание. Освен това Европейската комисия си запазва правото да извършва форматиране и редакции и да променя начина на работа на всяко Съдържание. Европейската комисия може също така да ограничи или да отмени използването на Услугата, ако публикувате такова неприемливо Съдържание.

Тъй като Европейската комисия не може да контролира цялото съдържание, публикувано от потребители и/или трети страни в Услугата, Вие се съгласявате да използвате Услугата на свой собствен риск. Вие разбирате, че използвайки Услугата, може да бъдете изложени на съдържание, което може да сметнете за обидно, неприлично, некоректно или неприемливо, и се съгласявате, че при никакви обстоятелства Европейската комисия няма да носи отговорност по какъвто и да е начин за каквото и да е съдържание, включително за грешки или пропуски в съдържанието, както и за загуби или щети от какъвто и да е вид, възникнали в резултат на използването от Ваша страна на каквото и да е съдържание.

### Резервни копия на съдържанието

Въпреки че се извършват редовни резервни копия на съдържанието, Европейската комисия не гарантира, че няма да има загуба или повреда на данни.

Повредени или невалидни точки на архивиране могат да бъдат причинени, без ограничение, от Съдържание, което е повредено преди да бъде архивирано или което се променя по време на извършването на архивирането.

Европейската комисия ще осигури поддръжка и ще се опита да отстрани всички известни или открити проблеми, които могат да повлияят на резервните копия на Съдържанието. Но Вие потвърждавате, че Европейската комисия не носи никаква отговорност, свързана с целостта на Съдържанието или с невъзможността за успешно възстановяване на Съдържанието в използваемо състояние.

Вие се съгласявате да поддържате пълно и точно копие на всяко Съдържание на място, независимо от Услугата.

## Политика за авторско право

### Нарушение на интелектуалната собственост

Ние уважаваме правата на интелектуална собственост на другите. Нашата политика е да реагираме на всяко твърдение, че Съдържание, публикувано в Услугата, нарушава авторско право или друго нарушение на интелектуалната собственост на което и да е лице.

Ако сте собственик на авторски права или сте упълномощени от негово име и смятате, че произведението, защитено с авторски права, е копирано по начин, който представлява нарушение на авторските права, което се извършва чрез Услугата, трябва да изпратите Вашето уведомление в писмен вид на вниманието на нашия представител по авторските права по електронна поща на адрес contact@fastplatform.eu и да включите в уведомлението си подробно описание на предполагаемото нарушение.

Можете да бъдете подведени под отговорност за щети (включително разходи и адвокатски хонорари) за невярна информация, че дадено Съдържание нарушава Вашето авторско право.

## Вашата обратна връзка към нас

Вие прехвърляте всички права, собственост и интереси върху всяка Обратна връзка, която предоставяте на Европейската комисия. Ако по някаква причина това прехвърляне е неефективно, Вие се съгласявате да предоставите на Европейската комисия неизключително, постоянно, неотменимо, безвъзмездно, световно право и лиценз за използване, възпроизвеждане, разкриване, сублицензиране, разпространение, промяна и експлоатация на тази Обратна връзка без ограничения.

## Връзки към други уебсайтове

Нашата Услуга може да съдържа връзки към уебсайтове или услуги на трети страни, които не са собственост или не се контролират от Европейската комисия.

Европейската комисия не контролира и не поема отговорност за съдържанието, политиките за поверителност или практиките на уебсайтовете или услугите на трети страни. Освен това Вие потвърждавате и се съгласявате, че Европейската комисия не носи отговорност, пряко или косвено, за каквито и да било щети или загуби, причинени или за които се твърди, че са причинени от или във връзка с използването или разчитането на такова съдържание, стоки или услуги, налични на или чрез такива уебсайтове или услуги.

Настоятелно Ви съветваме да се запознаете с общите условия и политиките за поверителност на всички уебсайтове или услуги на трети страни, които посещавате.

## Прекратяване

Можем да прекратим или спрем Вашия акаунт незабавно, без предварително уведомление или отговорност, по каквато и да е причина, включително, но не само, ако нарушите настоящите Общи условия.

При прекратяване Вашето право да използвате Услугата се прекратява незабавно. Ако желаете да прекратите Акаунта си, можете просто да преустановите използването на Услугата.

## Ограничение на отговорността

Независимо от евентуалните щети, които бихте могли да понесете, цялата отговорност на Европейската комисия и на всеки от нейните доставчици съгласно която и да е разпоредба на настоящите Условия и Вашето изключително средство за защита за всички горепосочени случаи се ограничава до сумата, действително платена от Вас чрез Услугата, или до 100 USD, ако не сте закупили нищо чрез Услугата.

В максималната степен, позволена от приложимото законодателство, в никакъв случай Европейската комисия или нейните доставчици не носят отговорност за каквито и да било специални, случайни, косвени или последващи щети (включително, но не само, щети за пропуснати ползи, загуба на данни или друга информация, за прекъсване на дейността, за телесни повреди, загуба на неприкосновеност на личния живот, произтичащи от или по какъвто и да е начин свързани с използването или невъзможността за използване на Услугата, софтуера на трети страни и/или хардуера на трети страни, използван с Услугата, или по друг начин във връзка с която и да е разпоредба на настоящите Условия), дори ако Европейската комисия или някой от доставчиците са били уведомени за възможността за такива вреди и дори ако средството за защита не изпълнява основната си цел.

Някои държави не разрешават изключването на подразбиращи се гаранции или ограничаването на отговорността за случайни или последващи щети, което означава, че някои от горните ограничения може да не се прилагат. В тези държави отговорността на всяка от страните ще бъде ограничена до най-голямата степен, позволена от закона.

## Отказ от отговорност "КАКТО Е" и "КАКТО Е ДОСТЪПНО"

Услугата Ви се предоставя "ТАКАВА, КАКВАТО Е" и "КАКВАТО Е ДОСТЪПНА" и с всички недостатъци и дефекти, без каквато и да е гаранция. В максималната степен, позволена от приложимото законодателство, Европейската комисия, от свое име и от името на своите Свързани лица и своите и техните съответни лицензодатели и доставчици на услуги, изрично се отказва от всички гаранции, независимо дали са изрични, подразбиращи се, законови или други, по отношение на Услугата, включително всички подразбиращи се гаранции за продаваемост, годност за определена цел, право на собственост и ненарушаване на права, както и гаранции, които могат да произтичат от обичайната практика, начина на изпълнение, използването или търговската практика. Без да се ограничава до горепосоченото, Европейската комисия не предоставя никакви гаранции или ангажименти и не прави никакви изявления от какъвто и да е вид, че Услугата ще отговаря на Вашите изисквания, ще постигне планираните резултати, ще бъде съвместима или ще работи с друг софтуер, приложения, системи или услуги, ще работи без прекъсване, ще отговаря на стандарти за производителност или надеждност, ще бъде без грешки или че всякакви грешки или дефекти могат или ще бъдат отстранени.

Без да се ограничава горното, нито Европейската комисия, нито който и да е от доставчиците на Европейската комисия прави каквито и да е изявления или гаранции, изрични или подразбиращи се: (i) по отношение на функционирането или наличността на Услугата или на информацията, съдържанието и материалите или продуктите, включени в нея; (ii) че Услугата ще бъде непрекъсната или без грешки; (iii) по отношение на точността, надеждността или актуалността на всяка информация или съдържание, предоставени чрез Услугата; или (iv) че Услугата, нейните сървъри, съдържанието или електронните съобщения, изпратени от или от името на Европейската комисия, не съдържат вируси, скриптове, троянски коне, червеи, зловреден софтуер, времеви бомби или други вредни компоненти.

Някои юрисдикции не позволяват изключването на определени видове гаранции или ограничения на приложимите законови права на потребителя, така че някои или всички от горепосочените изключения и ограничения може да не се прилагат за Вас. Но в такъв случай изключенията и ограниченията, посочени в този раздел, се прилагат в най-голямата степен, приложима съгласно приложимото право.

## Приложимо право

Законите на Европейския съюз, допълнени, ако е необходимо, от законите на Белгия, уреждат настоящите Условия и използването на Услугата от Ваша страна. Използването на Приложението от Ваша страна може да бъде предмет и на други местни, щатски, национални или международни закони.

## Разрешаване на спорове

Ако имате някакви притеснения или спорове относно Услугата, Вие се съгласявате първо да се опитате да разрешите спора неофициално, като се свържете с Европейската комисия.

## За потребители от Европейския съюз (ЕС)

Ако сте потребител от Европейския съюз, ще се възползвате от всички задължителни разпоредби на законодателството на страната, в която пребивавате.

## Разделимост и отказ от права

### Разделимост

Ако някоя от разпоредбите на настоящите Условия бъде счетена за неприложима или невалидна, тази разпоредба ще бъде променена и тълкувана така, че да постигне целите на тази разпоредба във възможно най-голяма степен съгласно приложимото право, а останалите разпоредби ще продължат да бъдат в пълна сила и действие.

### Отказ от права

С изключение на предвиденото в настоящия документ, неупражняването на право или неизпълнението на задължение по настоящите Условия не засяга възможността на страната да упражни такова право или да изиска такова изпълнение по всяко време след това, нито отказът от нарушение представлява отказ от всяко следващо нарушение.

## Превод Устен превод

Възможно е настоящите Общи условия да са преведени, ако сме ги направили достъпни за Вас в нашата Услуга.

Вие се съгласявате, че оригиналният текст на английски език ще има предимство в случай на спор.

## Промени в настоящите Общи условия

Ние си запазваме правото, по наше усмотрение, да променяме или заменяме тези Условия по всяко време. Ако промяната е съществена, Ние ще положим разумни усилия да предоставим поне 30-дневно предизвестие преди влизането в сила на новите условия. Какво представлява съществена промяна, ще бъде определено по Наше усмотрение.

Продължавайки да осъществявате достъп до или да използвате Нашата услуга след влизането в сила на тези промени, Вие се съгласявате да бъдете обвързани с ревизираните условия. Ако не сте съгласни с новите условия, изцяло или частично, моля, спрете да използвате уебсайта и Услугата.

## Свържете се с нас

Ако имате някакви въпроси относно настоящите Общи условия, можете да се свържете с нас:

- По електронна поща: <a href="mailto:contact@fastplatform.eu">contact@fastplatform.eu</a>
- Като посетите тази страница на нашия уебсайт: <a href="https://fastplatform.eu/#contact" rel="external nofollow noopener" target="_blank">https://fastplatform.eu/#contact</a>
