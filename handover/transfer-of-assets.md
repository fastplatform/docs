# Transfer of assets

This document summarizes the transfer of assets between the contractor (PwC) and the European Commission (EC) and participating Member States.

The execution of the FaST pilot project generated several types of assets, in the form of source code, live platforms and accounts with external suppliers.

## Assets that will be transferred to the European Commission

### [fastplatform.eu]() domain name

The [fastplatform.eu]() domain name is used for:
- DNS zone management and routing
- Email redirects for the [tech.xx@fastplatform.eu]() accounts

The domain is currently registered (and already paid for) until Feb 4, 2023, on the [Gandi](https://gandi.net) registrar.

##### How will we transfer it?

The [fastplatform.eu]() domain name can either be kept under the Gandi registrar, or transferred to a different registrar.
- if the domain name is kept in Gandi, the EC will create and provide a Gandi account that will be added as the sole owner of the domain name. The WHOIS information will also be updated to reflect the new owner (the EC).
- if the domain name is to be transferred to a different registrar, the domain transfer will be initiated from within Gandi to the registrar designated by the EC. **Important note: the email aliases (currently configured in Gandi) will need to be recreated in the new registrar. These email aliases are extremely important as they are used as login / username on numerous services, see below.**

> The [fastplatform.eu]() domain name *is needed* to be maintained for the proper running of the national platforms. The DNS zones declared on the domain are used to route traffic to the national platforms. The email aliases (see below) are used as accounts usernames for other services, such as DIAS or the Google Play Store.

### [gitlab.com/fastplatform](https://gitlab.com/fastplatform) group

This [GitLab](https://gitlab.com) group contains all the repositories of all the source code created during the course of the FaST Pilot. It is used by each Member State platform as a repository of the common services.

##### How will we transfer it?

A GitLab user provided by the EC will be set as *owner* of the group (and therefore granted access to all the repositories and resources within the group).

### [netlify/fastplatform](https://app.netlify.com/teams/fastplatform) account

Netlify is used to host the project website [https://fastplatform.eu](https://fastplatform.eu). Our usage of Netlify falls within their free tier.

##### How will we transfer it?

A Netlify user provided by the EC will be set as *owner* of the [fastplatform team](https://app.netlify.com/teams/fastplatform).

### [freshdesk/fastplatformeu](https://fastplatformeu.freshdesk.com/) account

Freshdesk is a ticketing management system, and just powers the contact form on the [https://fastplatform.eu](https://fastplatform.eu) website. Our usage of Freshdesk falls within their free tier.

##### How will we transfer it?

A Freshdesk user provided by the EC will be set as *owner* of the [fastplatform team](https://fastplatformeu.freshdesk.com/).


## Assets that will be transferred to each participating Member State

### [Orange Business Services](https://www.orange-business.com/) contracts

These accounts correspond to the *live platforms* that are currently hosting the FaST service for each participating Member State. Each account contains all the cloud resources that make up the FaST service:
- Cluster of virtual machines
- Storage and databases (including current data)
- Encryptions keys and certificates

There currently are 6 accounts:
- Contract `OCB0005394` / `fastplatform-be-wal` (Wallonia)
- Contract `OCB0005396` / `fastplatform-bg` (Bulgaria)
- Contract `OCB0005398` / `fastplatform-gr` (Greece)
- Contract `OCB0005420` / `fastplatform-it-21` (Piemonte)
- Contract `OCB0005397` / `fastplatform-ro` (Romania)
- Contract `OCB0005399` / `fastplatform-sk` (Slovakia)

##### How will we transfer them?

The contractual elements with Orange Business Services as well as the procedure to follow have been sent to each participating Member State. Once the transfer is completed, all the cloud resources under the account will be the property and the responsibility of each Member State.

### [tech.xx@fastplatform.eu]() email aliases

These email aliases are used as usernames for all the services that make up each national deployment of FaST, for example:
- Technical ownership of the Orange Business Services contracts
- Ownership of the GitLab groups
- User accounts for the DIAS (see below)

All these alias are currently configured to redirect to the contractor's email addresses (@pwc.com). The addresses are:
- [tech.be-wal@fastplatform.eu]() (Wallonia)
- [tech.bg@fastplatform.eu]() (Bulgaria)
- [tech.gr@fastplatform.eu]() (Greece)
- [tech.it-21@fastplatform.eu]() (Piemonte)
- [tech.ro@fastplaform.eu]() (Romania)
- [tech.sk@fastplatform.eu]() (Slovakia)

##### How will we transfer them?

Each alias will be configured to redirect to the addresses designated by each Member State.

### [gitlab.com/fastplatform-xx]() groups

These groups are the private repositories of the source code used by each deployed platform. 

They contain:
- a copy of the source code of the *core services* (the FaST common services)
- a copy of the source code of the *add-ons* used by each Member State
- the source code of *custom services* that were developed for each Member State
- a copy of the source code of the mobile application

The encryptions keys used in each private group (to encrypt sensitive data) will be provided at the same time.

The groups are:
- [gitlab.com/fastplatform-be-wal](https://gitlab.com/fastplatform-be-wal) (Wallonia)
- [gitlab.com/fastplatform-bg](https://gitlab.com/fastplatform-bg) (Bulgaria)
- [gitlab.com/fastplatform-gr](https://gitlab.com/fastplatform-gr) (Greece)
- [gitlab.com/fastplatform-it-21](https://gitlab.com/fastplatform-it-21) (Piemonte)
- [gitlab.com/fastplatform-ro](https://gitlab.com/fastplatform-ro) (Romania)
- [gitlab.com/fastplatform-sk](https://gitlab.com/fastplatform-sk) (Slovakia)

##### How will we transfer them?

As each [tech.xx@fastplatform.eu]() email alias will be configured to point to a Member State provided address, we will only need to provide the current password of each account to 
each Member State. It is advisable for each Member State to immediately change each password they receive.

### DIAS accounts

These accounts are used to query the catalogues and retrieve satellite imagery, for each Member State. There currently are 4 accounts with: [Sobloo](https://sobloo.eu/), [ONDA](https://www.onda-dias.eu/), [CREODIAS](https://creodias.eu/) and [SciHub](https://scihub.copernicus.eu/).

The following accounts will be transferred:
- Sobloo account `tech.be-wal@fastplatform.eu` (Wallonia)
- Sobloo account `tech.bg@fastplatform.eu` (Bulgaria)
- Sobloo account `tech.gr@fastplatform.eu` (Greece)
- Sobloo account `tech.it-21@fastplatform.eu` (Piemonte)
- Sobloo account `tech.ro@fastplatform.eu` (Romania)
- Sobloo account `tech.sk@fastplatform.eu` (Slovakia)
- ONDA account `tech.be-wal@fastplatform.eu` (Wallonia)
- ONDA account `tech.bg@fastplatform.eu` (Bulgaria)
- ONDA account `tech.gr@fastplatform.eu` (Greece)
- ONDA account `tech.it-21@fastplatform.eu` (Piemonte)
- ONDA account `tech.ro@fastplatform.eu` (Romania)
- ONDA account `tech.sk@fastplatform.eu` (Slovakia)
- CREODIAS account `tech.be-wal@fastplatform.eu` (Wallonia)
- CREODIAS account  `tech.bg@fastplatform.eu` (Bulgaria)
- CREODIAS account  `tech.gr@fastplatform.eu` (Greece)
- CREODIAS account  `tech.it-21@fastplatform.eu` (Piemonte)
- CREODIAS account  `tech.ro@fastplatform.eu` (Romania)
- CREODIAS account  `tech.sk@fastplatform.eu` (Slovakia)
- SciHub account `fastplatform-be-wal` (Wallonia)
- SciHub account `fastplatform-bg` (Bulgaria)
- SciHub account `fastplatform-gr` (Greece)
- SciHub account `fastplatform-21` (Piemonte)
- SciHub account `fastplatform-ro` (Romania)
- SciHub account `fastplatform-sk` (Slovakia)

##### How will we transfer them?

As each [tech.xx@fastplatform.eu]() email alias will be configured to point to a Member State provided address, we will only need to provide the current password of each account to 
each Member State. It is advisable for each Member State to immediately change each password they receive.

### [tech.xx@fastplatform.eu]() Google Play Store accounts

> WIP

## Assets that will be decommissioned at the end of the project

- The Apple Developer account ([appleid@fastplatform.eu]()), used during phases 1 & 2 to publish the FaST mobile application for iOS, will be deleted at the end of the project. It is expected that each Member State will create/use their own Apple Developer account to republish the FaST mobile application for iOS, and hence the account used to publish the "EC version" in beta will not be needed anymore.

- Google Play Store account ([googleid@fastplatform.eu]()), used to publish the FaST mobile application for Android, will be deleted at the end of the project. It is expected that each Member State will create/user a separate Google Play Store account to deploy their own version of the FaST mobile application for Android, and hence the account used to publish the "EC version" in beta will not be needed anymore.

- Orange Business Services contract `OCB0004040` (`fastplatform`), used to host the "common platform" during the first phase of the project, will be closed.
