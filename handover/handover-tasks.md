# FaST Handover tasks and timeline

This document aims at presenting the tasks and timeline of the process of handing over the customized FaST platforms to the participating Member States, as well as the source code, material and technical documentation.

We have identified the following tasks to be performed:
- [Task 1: Transfer of the cloud subscription currently hosting the live platform](#task-1-transfer-of-the-cloud-subscription)
- [Task 2: Republishing of the mobile applications on Google and Apple stores](#task-2-republishing-of-the-mobile-applications)
- [Task 3: FaST administration portal training (1 workshop)](#task-3-fast-administration-portal-training)
- [Task 4: Technical handover (2 workshops)](#task-4-technical-handover)
- [Task 5: Transfer of the source code repositories and secrets](#task-5-transfer-of-the-source-code-repositories-and-secrets)
- [Task 6: Rewrite of the policies (Privacy Policy, Terms & Conditions)](#task-6-rewrite-of-the-policies-privacy-policy-terms-conditions)
- [Other tasks to be considered by the Member States](#other-tasks-to-be-considered-by-the-member-states)

#### Proposed timeline

The proposed timeline for each task is represented below. We propose to have a final deadline for the transfer on May 15th, i.e. 2 weeks before the end of the PwC contract.

```plantuml
@startuml
printscale weekly

Project starts 2022-02-14

[T1: Transfer of the cloud subscription] starts 2022-03-01 and ends 2022-05-15
[T2: Republishing mobile apps] starts 2022-03-20 and ends 2022-05-15
[T3: FaST admin training] starts 2022-03-20 and ends 2022-04-30
[T4: Technical workshops] starts 2022-03-20 and ends 2022-05-15
[T5: Transfer source code] starts 2022-05-01 and ends 2022-05-15
[T6: Rewrite policies] starts 2022-04-01 and ends 2022-05-15
[Transfer to MS is finalized] happens on 2022-05-15
[End of PwC contract with the EC] happens on 2022-06-01

@enduml
```

> Note that there is a **hard** deadline on May 31st, when the PwC FaST contract with the EC expires. Each platform _has to be fully transferred to each Member State before that date_.

## Task 1: Transfer of the cloud subscription

Each current Member State platform is hosted on the [Sobloo DIAS](https://sobloo.eu/) and, more specifically, under an independent [Orange Business Services](https://www.orange-business.com/en) subscription. Each of these subscriptions is currently contracted by PwC, and must be transferred to the designated Member State legal entity before the end of May 2022.

```plantuml
rectangle "Wallonia" as be_wal
rectangle "Bulgaria" as bg
rectangle "Greece" as gr
rectangle "Romania" as ro
rectangle "Slovakia" as sk

rectangle "Orange Business Services\nsubscriptions" as obs {
    rectangle "OCB0005394\nfastplatform-be-wal" as obs_be_wal
    rectangle "OCB0005396\nfastplatform-bg" as obs_bg
    rectangle "OCB0005398\nfastplatform-gr" as obs_gr
    rectangle "OCB0005397\nfastplatform-ro" as obs_ro
    rectangle "OCB0005399\nfastplatform-sk" as obs_sk
}

be_wal --> obs_be_wal
bg --> obs_bg
gr --> obs_gr
ro --> obs_ro
sk --> obs_sk

```

This task implies that:
- **Task 1.1**: Each Member State formally identifies the legal entity that will be the new owner of the subscription
- **Task 1.2**: The designated Member State legal entity enters into a contract with Orange Business Services.
- **Task 1.3**: The subscription (and all associated live resources) is then transferred by Orange Business Services from the PwC contract to the Member State contract, on an agreed date, no later than mid-May 2022.

This task can happen in parallel of all the other technical tasks.

##### What are the terms and conditions of the Orange Business Services contract?

The terms and conditions of the contract will be communicated to each Member State legal entity by Orange Business Services before the end of February 2022. The proposed conditions are the standard conditions, and are identical to the conditions contracted by PwC for the past 2 years.

##### What is the estimated cost of hosting the FaST platform on the Orange Business Services cloud subscription?

Each FaST platform is dynamic and scales up and down according to the current load of the system (i.e. the number of users currently using the platform). The cloud resources used are dynamically allocated according to the current load of the platform, therefore optimizing the cost of hosting the platform by releasing unused resources as soon as possible.

With the above in mind, our estimates are that hosting the FaST platform on the Orange Business Services subscription will cost **around €2,000 per month** per Member State.

##### What happens if the subscription is not yet transferred on May 31st?

If the subscription is not transferred when the PwC contract with the European Commission expires on May 31st 2022, then it will be destroyed and the platform will be unavailable.

It will then be up to each Member State to re-deploy the platform on a live environment, reload all the data and start the platform again.

## Task 2: Republishing of the mobile applications

The Android and iOS mobile application for FaST are currently published on the Google Play and Apple App Store using a single PwC-owned account. This account will be decommissioned on the end of May 2022.

Therefore, the mobile applications need to be re-published using accounts owned by each participating Member State.

Each Member State must:
- **Task 2.1**: Identify if Apple / Google accounts exist within the Paying Agency, to be used to publish the mobile application of FaST
- **Task 2.2**: If such accounts do not exist, the Paying Agency creates its accounts them on the Apple App Store and Google Play Store. This creation of accounts bears a fee (€99 for Apple and $25 for Google, as of writing this document).
- **Task 2.3**: The Paying Agency re-publishes the mobile application on the stores using its accounts

The signing and republishing of the mobile application is a technical task that needs to be performed by a skilled technical expert. Documentation will be provided on the process currently used by PwC to deploy the application on the current test accounts.

##### Why can't PwC create accounts for each Member State, publish the apps from there and transfer the account at the end of May?

PwC cannot create those accounts and transfer them, for different reasons:

- Apple App Store: each account created on behalf of an organization (company or institution) must be managed by the organization itself. PwC cannot create an account on behalf of a Paying Agency, for example ; the account has to be created and managed by the Paying Agency itself.

- Google Play Store: PwC cannot create multiple accounts at once, as these would be considered as "linked accounts", which are forbidden by the store's policies.

##### If we do not republish the mobile applications, will FaST become inaccessible to farmers?

No, the FaST application will still be available for farmers on laptop and mobile browsers.

## Task 3: FaST administration portal training

PwC will organize one online workshop per participating Member State on the FaST administration portal, to be held on the end in April 2022. This workshop will:

- cover the basics of FaST, in terms of features and data model
- cover the main features of the portal:
    - interface
    - data access
    - user and campaign management
    - data import/export
- be designed for a non-IT audience
- be held in English
- be expected to last approx 3 hours, with 1 hour reserved for Q&A

## Task 4: Technical handover

PwC will organize two technical workshops per Member State, aimed at presenting the technical infrastructure of the FaST platform and its day-to-day technical management.

Both workshops will be designed for a technical (IT) audience and will be held in English.

The **first workshop** will cover:
- The structure of the source code repositories on GitLab: infrastructure code, common services source code, custom source code, add-ons.
    - Technologies used: [GitLab.com](https://gitlab.com/)
- The underlying cloud infrastructure: machines, storage, networks, security, monitoring
    - Technologies used: [Orange Business Services](https://www.orange-business.com/en) resources, [Terraform](https://www.terraform.io/)/[Terragrunt](https://terragrunt.gruntwork.io/), [Flux CD](https://fluxcd.io/)
- The workload orchestrator: microservice architecture, service mesh, 
    - Technologies used: [Kubernetes](https://kubernetes.io/), [Istio](https://istio.io/), [Knative](https://knative.dev/)
- The continuous integration and deployment (CI/CD) pipelines for the infrastructure and the services.
    - Technologies used: [Bazel](https://bazel.build/), [GitLab CI](https://docs.gitlab.com/ee/ci/)

The **second workshop** will cover:
- The structure of the FaST platform: services, databases, API gateways
    - Technologies used: [Hasura](https://hasura.io/)/[GraphQL](https://graphql.org/), [Python](https://www.python.org/), [Django](https://www.djangoproject.com/), [fastapi](https://fastapi.tiangolo.com/), [Postgres](https://www.postgresql.org/)/[PostGIS](https://postgis.net/), [MapProxy](https://mapproxy.org/), [GDAL](https://gdal.org/), [pg_tileserv](https://github.com/CrunchyData/pg_tileserv)
- The FaST data model: users, farms, campaigns, etc
- The FaST farmer application
    - Technologies used: [Vue.js](https://vuejs.org/), [Capacitor](https://capacitorjs.com/), [Quasar.dev](https://quasar.dev/), [Leaflet](https://leafletjs.com/), [Apollo GraphQL Client](https://www.apollographql.com/), iOS/XCode, Android Studio

##### Is it mandatory that the technical audience is proficient in the technologies used in the workshops?

Yes. The purpose of the technical workshops is to handover specific FaST information, not to upskill the audience in the technologies used. Also, the workshops will be very dense, given the amount of information to convey and the limited amount of time.

It is expected that the technical experts present have at least 2 years of experience in the above technologies.

## Task 5: Transfer of the source code repositories and secrets

We will transfers the ownership of the source code repositories of each Member State to the designated technical entity. The source code repositories contain:
- the source code for the infrastructure and the services
- the CI/CD pipelines
- the secret keys (certificates, encryption keys, etc)

This transfer will happen at the same time as the subscription transfer (Task 1).

The source code for each Member State is currently hosted on GitLab in separate _groups_:
- Wallonia: https://gitlab.com/fastplatform-be-wal
- Bulgaria: https://gitlab.com/fastplatform-bg
- Greece: https://gitlab.com/fastplatform-gr
- Romania: https://gitlab.com/fastplatform-ro
- Slovakia: https://gitlab.com/fastplatform-sk

The ownership of each group will be transferred to the designated technical entity of each Member State.

## Task 6: Rewrite of the policies (Privacy Policy, Terms & Conditions)

The current policies for the platform are designed for the development/test period and make mention of the European Commission / PwC being responsible for each platform (notably in the GDPR role of data controller). Under the [current policies](../policies/), usage of the data is strictly limited to "development of the platform and improvement of the services".

As PwC will transfer the ownership of the platform to the designated Member State, the policies need to be rewritten to reflect the new owner, and the transfer of responsibility. The allowed usage of the data needs to be adapted to each Member State's own objectives, in compliance with GDPR.

Therefore, the following policies need to be rewritten by each Member State and updated in the Administration Portal:
- Privacy Policy
- Terms & Conditions

## Other tasks to be considered by the Member States

In this section, we list the other tasks that are not part of the PwC assignment, but should however be considered by each Member State performing the roll-out of the service.

- **Communication**: a communication plan should be designed and implemented by each Member State to let the farming community know about the platform, its features and how to access it.
- **User support**: a user support plan (email, phone, etc) should be designed and implemented by each Member State to help the farmers to get in touch with human support for the platform.
- **Governance of the platform**: the governance of the platform, at the Member State level, should be defined, to ensure roles and responsibilities are well understood, plan and manage evolutions and liaise with the European Commission.
