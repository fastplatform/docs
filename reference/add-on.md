# Add-On


The add-ons modules contain services that are optional and can be deployed in one or more regions.

```plantuml
caption Add-on can interact with FaST APIs

rectangle add_on [
    Add-on    
    ---
    Custom
]

component api_gateway [
    API Gateways
    ---
    <i>Hasura GraphQL
]

add_on --> api_gateway : HTTPS

```

Add-Ons are external services that are able to interact with FaST by consuming APIs. There is two kinds of add-ons:
- auto-subscribed add-on
- manual-subscribed add-on

What happened when an add-on is added to the FaST platform? What does it mean "add an add-on to FaST"? As an administrator, from the FaST administration panel, you can register an add-on by providing:
- a name
- a provider
- a quick description
- a logo
- a website
Then some technical settings:
- whether this add-on should be active (visible) in the app 
- a callback url (The callback URL that FaST will call using POST when the user accesses the service from the app)
- whether the user ID should be passed when FaST calls the callback URL

You can also specify whether all holdings are automatically subscribed to this add-on and cannot unsubscribe or not. This parameter is very important because it means that each auto-subscribed add-ons will be able to access all the information about each holdings.
By contrast, manual add-on will be able to access only information relative to holdings that have previously subscribed to the add-on.


## API keys

Add-ons are authenticated through API keys.

From the administration panel, API keys can be generated for each add-on. An API key can be use to run authenticated queries against FaST APIs. 

## Add-on to access FaST APIs

```plantuml
caption API key to authenticate queries

actor "Scientists" as act

component api_gateway [
    API Gateways
    ---
    <i>Hasura GraphQL
]

act --> api_gateway : HTTP request with add-on API-key in headers

```

Example:
1. Register new add-on in the administration panel
2. Generate an API key for this add-on
3. (Optional) Subscribe holding to this add-on
4. Using curl (or similar tool) execute query with a specific header "X-Hasura-AddOn-Id" to pass API key (in order to authenticate the query)

