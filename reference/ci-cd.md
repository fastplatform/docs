# CI/CD (Continuous Integration & Continuous Delivery)

CI/CD (Continuous Integration / Continuous Delivery) is an iterative practice that reduces the feedback loop through frequent code merges and deployments. Build and deployment pipelines have been implemented to release and deploy a new version of the FaST platform to users every week.

## Bazel

The code base of the modules of the FaST platform are versionned in Git monorepositories. [Bazel](https://bazel.build/) is used to build and test the services of all the FaST modules.

[Bazel](https://bazel.build/) is an open-source build and test tool that scalably supports multi-language and multi-platform projects. It enables fast and incremental builds with advanced local and distributed caching, optimized dependency analysis and parallel execution.


## Workflow

The steps from coding to deployment are organized in the following workflow:

1. An issue is created to request a new feature or a bug fix
2. The issue is assigned
3. The assigned developper creates a new Git branch and commits his work
4. A [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) is opened:<br/>
  Build and Test pipelines are executed<br/>
  A code review is performed<br/>
5. The Merge Request is closed and the development branch is sqashed and merged into the master branch
6. The merge commit is deployed to the staging environment
7. A tag is created and deployed to the beta environment

## Environments

Each participating Member State has three types of environments:

1. **Ephemeral Review**, a temporary non-persistent environment to deploy the code associated with a Merge Request (for custom modules only)
2. **Staging**, a test environment that automatically hibernates after 6 hours
3. **Beta**, an environment for the real FaST users

## GitLab CI

Build and deployment jobs are orchestrated and executed by GitLab CI. Each participating Member State has its own GitLab runner to run these jobs.
