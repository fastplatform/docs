# Useful tools and command

## Table of Contents
- [Useful tools and command](#useful-tools-and-command)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [GPG](#gpg)
  - [Git Crypt](#git-crypt)
  - [SOPS](#sops)
  - [Kustomize](#kustomize)
  - [Terragrunt](#terragrunt)
  - [Flux](#flux)
  - [Git vendor](#git-vendor)
  - [Renew certificate for atlantis](#renew-certificate-for-atlantis)

## Introduction

In this part you will find all the specific commands for use the technical stack on you local environment.

## GPG

Cheat sheet for [GnuPG](https://github.com/gpg/gnupg).

With PGP you had two keys, one is public for encrypt and the private key is used for decrypt.

```bash
# Create PGP public and private keys
gpg --full-generate-key
# List PGP keys
gpg --list-keys
# Export PGP key ID as env vars. (Find it with --list-keys)
export KEY_FP=my_pgp_key_id 
# Export PGP public and private keys
gpg --export --armor "${KEY_FP}" > ~/path/to/my_public_key.pgp
gpg --export-secret-keys --armor "${KEY_FP}" > ~/path/to/my_private_key.pgp
# Import PGP public and private keys
gpg --import /path/to/my_public_key.pgp
gpg --import /path/to/my_private_key.pgp
# Edit specific PGP key
gpg --edit-key ${KEY_FP}
# Delete PGP public and private keys from local dev envs
gpg --delete-key ${KEY_FP}
gpg --delete-secret-keys "${KEY_FP}"
```

## Git Crypt

Cheat sheet for [Git-Crypt](https://github.com/AGWA/git-crypt).

:warning: Be sure you have an `.gitattributes` file inside your folder for repository with custom rules.

```bash
# Initialize git-crypt in folder
git-crypt init
# Export git-crypt key after initialize
git-crypt export-key ~/path/to/my_git_crypt_key
# Add user for encrypt / decrypt with GPG
git-crypt add-gpg-user ${KEY_FP}
# Decrypt files in folder
git-crypt unlock
# View files status
git-crypt status
# Force encryption of files
git-crypt status -f
```

## SOPS

Cheat sheet for [SOPS](https://github.com/mozilla/sops).

:warning: Be sure you have a `.sops.yaml` file with a specific rules and PGP key used for encryption.

```yaml
creation_rules:
  - path_regex: .*.yaml
    encrypted_regex: ^(data|stringData)$
    pgp: my_pgp_key_id
```

```bash
# Encrypt yaml files
sops --encrypt --config .sops.yaml -i path/to/my_file.yaml
# Decrypt yaml files
sops --decrypt --config .sops.yaml -i path/to/my_file.yaml
```

## Kustomize

Cheat sheet for [Kustomize](https://github.com/kubernetes-sigs/kustomize).

```bash
# Dry-run kustomize build
kustomize build .
# Apply or delete all ressources 
kustomize build . | kubectl apply -f
kustomize build . | kubectl delete -f
# Apply or delete ressources with label option
kustomize build . | kubectl apply -f - -l "fastplatform.eu/system-component in (cert-manager)"
kustomize build . | kubectl delete -f - -l "fastplatform.eu/system-component in (cert-manager)"
```

## Terragrunt

All terragrunt command are the approximatively same as Terraform command but with terragrunt binary

```bash
### Terragrunt 

# Refresh Init
terragrunt init -reconfigure
# Plan on all sub folder
terragrunt run-all plan -compact-warnings -no-color --terragrunt-non-interactive --terragrunt-ignore-dependency-errors
# Apply on all sub folder
terragrunt run-all apply -compact-warnings
# Destroy on all sub folder
terragrunt run-all destroy -compact-warnings
# Get state
terragrunt state pull > state.json
# Upload State
terragrunt state push -force state.json
# Get outputs
terragrunt output
# Lint code
terragrunt hclfmt
# Clear local terragrunt cache (Add in .bashrc)
clear_t_cache () {
find . -type f -name ".terraform.lock.hcl" -prune -exec rm -rf {} \;
find . -type d -name ".terragrunt-cache" -prune -exec rm -rf {} \;
find . -type d -name ".terraform" -prune -exec rm -rf {} \;
}

### Terraform

# Scan code and generate docs
terraform-docs markdown . > README.md
# Lint code
terraform fmt --recursive
```

## Flux 

```bash
# Get all ressources
flux get all -n fastplatform-system-flux
# Re-sync git source manually
flux reconcile source git my_git_source_name -n fastplatform-system-flux
# Get flux sources controler
flux get sources -n fastplatform-system-flux
# Get flux kustomize controler
flux get kustomization -n fastplatform-system-flux
# Export kustomize controler as yaml
flux export kustomization -n fastplatform-system-flux my_kustomize_controler_name > my_file.yaml
# Export source controler as yaml
flux export git -n fastplatform-system-flux my_source_controler_name > my_file.yaml
```

## Git vendor 

```bash
# Reset vendoring infrastructure bootstrap
rm -rf stack && git add -A && git commit -m "[Changes] vendoring" && git vendor add --prefix stack stack git@gitlab.com:fastplatform/ops/terraform/infrastructure-bootstrap.git master && git-crypt status -f && git add -A && git commit -m "[Changes] vendoring" && git push origin master
# Reset vendoring infrastructure
rm -rf stack && git add -A && git commit -m "[Changes] vendoring" && git vendor add --prefix stack stack git@gitlab.com:fastplatform/ops/terraform/infrastructure.git master && git-crypt status -f && git add -A && git commit -m "[Changes] vendoring" && git push origin master
```

## Renew certificate for atlantis

```bash
sudo su
cd /root/
docker rm -f atlantis
DOMAIN="atlantis.<custom>.fastplatform.eu"
EMAIL="tech.<custom>.fastplatform.eu"

sudo docker run -it --rm --name certbot -v "$PWD/letsencrypt:/etc/letsencrypt" -p 80:80 -p 443:443 certbot/certbot certonly -d $DOMAIN -m $EMAIL --standalone -n --agree-tos
cp letsencrypt/live/atlantis.<custom>.fastplatform.eu/fullchain.pem letsencrypt/live/atlantis.<custom>.fastplatform.eu/privkey.pem atlantis-config/

docker run -d \
    --restart=unless-stopped \
    --name atlantis \
    -p 443:4141 \
    -v /root/atlantis-config/data/atlantis:/home/atlantis/.atlantis/ \
    -v /root/atlantis-config/fullchain.pem:/etc/ssl/certs/cert.pem \
    -v /root/atlantis-config/privkey.pem:/etc/ssl/private/key.pem \
    -v /root/atlantis-config/server.yaml:/home/atlantis/server.yaml \
    -v /root/atlantis-config/repos.yaml:/home/atlantis/repos.yaml \
    atlantis-fastplatform server \
    --config="/home/atlantis/server.yaml" --log-level=DEBUG
```