# Modules and services

The FaST platform features are organized around _services_ that are deployed on the orchestration layer. The security, redundancy, backup, roll-out, etc of the services is fully delegated to the orchestration layer.

In this section, we will describe what services are running on the platform, how they communicate and how they store their data. We will not cover orchestration matters (such as observability, redundancy, etc) or infrastructure.

But first, let's see how the services are organized in the codebase.

## Modularity: core, custom and add-on services

For each FaST region, a certain number of services are deployed from the FaST catalogue:

- the core module is deployed
- one custom module is deployed
- some add-on modules are deployed

This philosophy of modularity is replicated exactly the same way in the codebase.

### Core module

The [core module](https://gitlab.com/fastplatform/core) comprises services that are deployed in each FaST namespace / region. These services are _generic_ and therefore the codebase is shared amongst the FaST regions.

The detailed list and documentation of services of the core module is accessible [here](https://gitlab.com/fastplatform/core).

### Custom modules

The [custom modules](https://gitlab.com/fastplatform/custom) comprise services that are custom built for each region. These services are deployed in a single region and are not shared between namespaces. The features implemented in those services are usually similar (like data import pipelines) but do take into account the specificities of the region they are being deployed in.

The current custom modules of the FaST platform are:

- Phase 1:
    - [Piemonte (Italy) custom module](https://gitlab.com/fastplatform-it-21/it-21)
    - [Estonia custom module - deprecated](https://gitlab.com/fastplatform/custom/ee)
    - [Castilla y Leon (Spain) custom module - deprecated](https://gitlab.com/fastplatform/custom/es-cl)
    - [Andalucia (Spain) custom module - deprecated](https://gitlab.com/fastplatform/custom/es-an)
- Phase 2
    - [Wallonia (Belgium) custom module](https://gitlab.com/fastplatform-be-wal/be-wal)
    - [Bulgaria custom module](https://gitlab.com/fastplatform-bg/bg)
    - [Greece custom module](https://gitlab.com/fastplatform-gr/gr)
    - [Romania custom module](https://gitlab.com/fastplatform-ro/ro)
    - [Slovakia custom module](https://gitlab.com/fastplatform-sk/sk)

The list of implemented services is detailed in the documentation of each module

### Add-on modules

The add-on modules contain services that are optional and can be deployed in one or more regions.

###### Sallite imagery
- [Sentinel RGB/NDVI tiling and catalogue add-on](https://gitlab.com/fastplatform/addons/sobloo)

###### Weather
- [Weather add-on for Piemonte (Italy) - deprecated in favor of Met.no](https://gitlab.com/fastplatform/addons/bolletino-meteorologico)
- [Weather add-on for Spain](https://gitlab.com/fastplatform/addons/siar-aemet)
- [Weather add-on for Estonia](https://gitlab.com/fastplatform/addons/ilmateenistus)
- [Weather add-on for Greece, Slovakia, Romania and Piemonte](https://gitlab.com/fastplatform/addons/metno)
- [Weather add-on for Wallonia](https://gitlab.com/fastplatform/addons/agromet)
- [Weather add-on for Bulgaria](https://gitlab.com/fastplatform/addons/nimh)

###### Fertiization
- [Fertilization add-on for Piemonte (Italy)](https://gitlab.com/fastplatform/addons/visione)
- [Fertilization add-on for Spain](https://gitlab.com/fastplatform/addons/fertilicalc)
- [Fertilization add-on for Spain / greenhouses](https://gitlab.com/fastplatform/addons/vegsyst)
- [Fertilization add-on for Estonia](https://gitlab.com/fastplatform/addons/arc)
- [Fertilization add-on for Wallonia](https://gitlab.com/fastplatform/addons/requaferti)
- [Fertilization add-on for Slovakia](https://gitlab.com/fastplatform/addons/kalkulacky-hnojenie)
- [Fertilization add-on for Bulgaria and Greece](https://gitlab.com/fastplatform/addons/navigator-f3)
- [Fertilization add-on for Romania](https://gitlab.com/fastplatform/addons/icpa-pmn)

## High-level description of services and technologies

The number of services running on the platform and the complexity of interactions are difficult to comprehend by reading the detailed documentation of each service, or even the code. In this section, we will attempt to explain the big picture and then dive into more specific aspects of how each feature is implemented across multiple services.

```plantuml
caption High-level components of the FaST platform

actor "Paying Agency user" as staff
actor "Farmer / advisor" as farmer
actor "Paying Agency\ndata consumer" as api_consumer

rectangle mobile_app [
    Mobile / desktop app
    ---
    SPA Vuejs
    Capacitor
]

rectangle admin_portal [
    Admin Portal
    ---
    Django
]

component web_backend [
    Web server
    ""core/web/backend""
    ---
    <i>Django
]

component vector_tiles [
    Vector tile server
    ""core/web/vector_tiles""
    ---
    <i>pg_tileserv
]

component mapproxy [
    RGB/NDVI
    tile server
    ""core/mapproxy""
    ---
    <i>MapProxy
]

component media [
    Media server
    ""core/web/media""
    ----
    <i>Nginx
]

storage s3 [
    Object Storage (S3)
]

component api_gateway [
    API Gateway
    ""core/api_gateway""
    ---
    <i>Hasura GraphQL
]

database postgres [
    Databases
    ----
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

rectangle "Services" as services {
    component service_1 [
        Service
    ]
    component service_2 [
        Service
    ]
}

rectangle "Event handlers" as event_handlers {
    component handler_1 [
        Handler
    ]
    component handler_2 [
        Handler
    ]
}

cloud identity_providers [
    Identity providers
    (federated)
]

cloud external_apis [
    External APIs
    (IACS, weather,
    push notification, etc)
]

staff --> admin_portal
farmer --> mobile_app
api_consumer --> api_gateway

mobile_app --> api_gateway
admin_portal --> web_backend
mobile_app --> vector_tiles
admin_portal --> vector_tiles
mobile_app --> mapproxy
admin_portal --> mapproxy
mobile_app --> media

web_backend --> postgres
api_gateway --> postgres
vector_tiles --> postgres

web_backend --> identity_providers
api_gateway --> services
api_gateway --> event_handlers
service_1 ..> external_apis
service_2 ..> external_apis
handler_1 ..> external_apis
handler_2 ..> external_apis

mapproxy --> s3
media --> s3
web_backend --> s3
```

Starting from the user, the platform can be accessed by 3 main entrypoints:

- the Administration Portal for Paying Agencies (on the desktop, but responsive)
- the mobile application for farmers (and its desktop counterpart)
- an API for querying/ingesting data

### Web server

The web server is based on the [Django framework](https://www.djangoproject.com/) and runs under Python 3.8. The web server:

- manages the data model of the main Postgres databases. The models are defined as code and updates are pushed to the databases using [Django migrations commands](https://docs.djangoproject.com/en/3.0/topics/migrations/).
- serves the pages of the Admin Portal, based on the [Django admin framework](https://docs.djangoproject.com/en/3.0/ref/contrib/admin/)
- provides authentication services either directly (for users manually defined in the database) or by delegating to federated identity providers such as IACS login systems or national identity providers
- issues and validates access tokens through the [Open ID Connect (OIDC)](https://openid.net/connect/) protocol

```plantuml


caption The database schemas are managed by Django

component web_backend [
    Web server
    ---
    <i>Django
]

circle "Database\nrouter" as database_router

database postgres_fastplatform [
    Main database
    <b>fastplatform
    ---
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

database postgres_external [
    Main database
    <b>external
    ---
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

web_backend --> database_router : global schema\nas code\n(Django ORM)
database_router --> postgres_fastplatform : fastplatform\nschema
database_router --> postgres_external : external\nschema
```

### Databases

The data on the platform is mainly stored on 2 separate [PostgreSQL 12](https://www.postgresql.org/) servers.

- the first server (named `fastplatform`) is the main datastore of the platform and stores the users, farms definitions, agricultural activities, etc
- the second server (named `external`) stores the GIS open data imported for the region (like hydrography, nitrate vulnerables zones, public soil databases, natura2000 areas, etc)

Each Postgres server is:

- running version 12 of Postgres, with the [PostGIS 2.5](https://postgis.net/) geospatial extension
- replicated in real-time (for high availability and performance) to multiple read replicas.
- served behind multiple [pgBouncer](https://www.pgbouncer.org/) instances for connection pooling and routing of traffic to the master (write) or the replicas (read)

```plantuml

agent clients [
    Clients
    (web server or API gateway)
]

component pg_bouncer [
    Connection pooler
    ---
    <i>pgBouncer
]

rectangle "Database cluster" as postgres_cluster {
    database postgres_master [
        Master
        ---
        <i>PostgreSQL
    ]
    rectangle "Set of replicas" as postgres_replica_set {
        database postgres_replica_1 [
            Replica 1
            ---
            <i>PostgreSQL
        ]
        database postgres_replica_2 [
            Replica...
            ---
            <i>PostgreSQL
        ]
        database postgres_replica_n [
            Replica N
            ---
            <i>PostgreSQL
        ]
    }
}

clients --> pg_bouncer : multiple\nconnections
pg_bouncer --> postgres_master : write
pg_bouncer --> postgres_replica_1 : read
pg_bouncer --> postgres_replica_2 : read
pg_bouncer --> postgres_replica_n : read
postgres_master --> postgres_replica_set : replication
```

Note that some services or add-ons that require specific storage have _their own databases / storage solutions_. For example the core [push notification service](https://gitlab.com/fastplatform/core/-/tree/master/services/mobile/push_notification) and the [weather add-on for Spain](https://gitlab.com/fastplatform/addons/siar-aemet) both have a custom storage solution based on [Redis](https://redis.io/).

### API Gateway

The API Gateway is based on the [Hasura GraphQL Engine](https://hasura.io/opensource/). Its main purpose is to:

- expose the PostgreSQL _database schema_ as an automatic _[GraphQL schema](https://hasura.io/docs/1.0/graphql/core/schema/index.html)_. Note that some system tables from the schema are not exposed through the API.
- manage [permissions](https://hasura.io/docs/1.0/graphql/core/auth/index.html) to access objects, for each request, based on the identity of the requesting user. Authentication of requests is delegated to the web server through an authentication webhook.
- [trigger event handlers](https://hasura.io/docs/1.0/graphql/core/event-triggers/index.html) based on CRUD actions performed on the database
- [merge distant APIs](https://hasura.io/docs/1.0/graphql/core/remote-schemas/index.html) in a single unified GraphQL schema

The API Gateway also serves GraphQL [subscriptions](https://hasura.io/docs/1.0/graphql/core/subscriptions/index.html) based on websockets, which is used in FaST for real-time messaging.

```plantuml

component api_gateway [
    API Gateway
    ""core/api_gateway""
    ---
    <i>Hasura GraphQL Engine
]

database postgres_fastplatform [
    Main database
    <b>fastplatform
    ---
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

database postgres_external [
    Main database
    <b>external
    ---
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

api_gateway --> postgres_fastplatform
api_gateway --> postgres_external
```

### Vector tiles servers

The maps in the FaST mobile app and Admin Portal display layers of information that is based ojn public data sources and on the content of the Postgres databases.

The tiles displayed in the maps are generated on the fly using vector tiling servers ([pg_tileserv](https://github.com/CrunchyData/pg_tileserv)) and custom SQL functions. The tile server requests are authenticated using the same mechanism as the API gateways.

```plantuml
agent clients [
    Clients
    (Admin Portal / mobile app / desktop app)
]

component nginx_fastplatform [
    Reverse proxy
    ---
    <i>Nginx
]

storage "Cache" as cache_fastplatform

component pg_tileserv_fastplatform [
    Vector tiling server
    ---
    <i>pg_tileserv
]

database postgres_fastplatform [
    Main database
    <b>fastplatform
    ---
    <i>PostgreSQL 12
    <i>PostGIS 2.5
]

clients --> nginx_fastplatform : Request for a tile\nhttps://xxx.fastplatform.eu/z/x/y.pbf?token=ttt
nginx_fastplatform <--> cache_fastplatform
nginx_fastplatform --> pg_tileserv_fastplatform
pg_tileserv_fastplatform --> postgres_fastplatform : SQL SELECT\nbased on HTTP\nquery parameters
```

The tiles are currently cached for 1 hour for all open source public data (e.g. Natura2000 areas, Nitrate Vulnerable Zones, etc), and not cached for private data (farm plots/geometries).

The vector tiling servers are replicated multiple times for performance and use the pgBouncer pooler described above to evenly spread database queries.

More details about this service can be found in its [documentation](https://gitlab.com/fastplatform/core/-/tree/master/services/web/vector_tiles).

### Media files

The media files (especially the geotagged photos are stored in an S3 Object Storage and served behind an authentication proxy.

All media requests are authenticated using the user token (if the request comes from the app) or the user session (if the request comes from the Admin Portal).


```plantuml
together {
    agent admin_portal [
        Admin Portal
    ]
    agent app [
        Mobile app / desktop app
    ]
}

component web_backend [
    Web server
    ---
    <i>Django
]

component api_gateway [
    API Gateway
    ---
    <i>Hasura GraphQL
]

component nginx [
    Authentication proxy
    ---
    <i>Nginx
]

storage s3 [
    Object Storage (S3)
    ---
    <i>Orange Object Storage
]

app --> nginx : Read media
nginx <-> web_backend : Authenticates &\nauthorizes request
nginx --> s3 : Get media

app --> api_gateway : Write media
api_gateway --> web_backend
web_backend --> s3 : Get/set media

admin_portal --> web_backend : Read/write media



```

### RGB/NDVI tiles

The RGB/NDVI tiles are computed by a specific service that periodically connects to the  [sobloo](https://sobloo.eu/) DIAS catalogue to retrieve [L2A Sentinel imagery](https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/product-types/level-2a) for a given Area Of Interest.

The cloud mask (part of the L2A artefacts) is applied to the images, the NDVI layer is computed and the RGB and NDVI layers are tiled in [OGC TMS format](https://wiki.osgeo.org/wiki/Tile_Map_Service_Specification).

The tiles are stored on a volume which is exposed to the web using an Nginx proxy.

More details about this service can be found in its [documentation](https://gitlab.com/fastplatform/addons/sobloo).