# Infrastructure

The FaST platform is modular and can be tailored to the specifics of the participating Member States. This modularity is based on a microservice architecture that consists of core and custom services that are encapsulated in [Docker](https://www.docker.com) containers and orchestrated on a [Kubernetes](https://kubernetes.io) cluster.

For each participating Member State, a Kubernetes cluster is deployed and managed on [Flexible Engine](https://www.orange-business.com/en/products/flexible-engine) as part of a dedicated subscription. Flexible Engine is the Cloud solution of [Orange Business Services](https://www.orange-business.com/en) that is a member of the [Sobloo](https://sobloo.eu/) consortium. Only two managed services are used:
- Kubernetes
- Object storage (S3)

All cloud resources deployed on Flexible Engine are managed and provisioned using an "Infrastructure as Code" approach.

## Compute

The Kubernetes managed service on Flexible Engine is called [CCE](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/cloud-container-engine/) for Cloud Container Engine. It transparently manages Master nodes (Control Plane) and Worker nodes (Data plane) and ensures high availability.

Adding or removing worker nodes is done automatically, within a certain limit, depending on the current load. This feature allows the scalability of the FaST platform.

## Network

The resources on Flexible Engine are placed in a Virtual Private Cloud (VPC). Subnets, Routing tables, Network Access Control Lists (NACLs), Gateways, and Security Groups are used to route and secure the traffic. Services orchestrated on the Kubernetes cluster are accessible from the Internet via a Load Balancer and a public IP address.

The best practices of configurations have been followed, such as naming convention, least privilege, segment traffic and separation of roles.

## Storage

Elastic Volume Storage (EVS) and Object Storage (S3) volumes are used for data persistence. Persistent volumes can be provisioned directly by Kubernetes (Persistent Volume Claim) and mounted on the file system of statefull containers. 
