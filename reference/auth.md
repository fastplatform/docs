# Authentication & authorization in FaST


The authentication and authorization layers are separate in FaST: the authentication is provided by one or several *Identity Providers* and the authorizations are provided by different systems depending on where the user connects from and what actions the user is intending to perform.

## Authentication

Authentication is the process in which a system is able to provide an unambiguous identity for a given person : ie the purpose of authentication is to unequivocally map a *human being* to a *user identifier*. To perform this task, FaST can either authenticate users itself or delegate this task to another external system.
- The FaST identify provider is based on logins and password, and hence it is the responsibility of the administrators of the system (Paying Agencies) to make sure the logins and password are dispatched to their rightful owners. Usage of this identity provider should be limited to administration or demo accounts: it is hardly feasible to initiate and maintain such a login/password base for a very high number of users (like farmers).
- Federated identity providers are connected to FaST and are expected authenticate users and return a consistent user identifiers for a set of users.

Currently in FaST, the following identity providers have been federated:

| Member state | Identity provider | Returned information |
|---------|----------------------|-------------|
| Estonia | [TARA](https://www.ria.ee/en/state-information-system/eid/partners.html)| Estonian Personal identification Code |
| Italy - Piemonte | [SPID](https://www.spid.gov.it/) | Italian Fiscal Code |
| Spain - Castilla y Leon | [Organismo Pagador de Castilla y Leon](https://agriculturaganaderia.jcyl.es/web/es/organismo-pagador-castilla-leon.html#:~:text=De%20conformidad%20con%20la%20referida,FEAGA%20y%20por%20el%20FEADER.) | National identity document number (DNI) |
| Spain - Andalucia | [Documento de Acompañamiento al Transporte (DAT)](https://www.juntadeandalucia.es/organismos/agriculturaganaderiapescaydesarrollosostenible/areas/agricultura/produccion-agricola/paginas/dat.html) | National identity document number (DNI) |
| Belgium - Wallonia | [Federal Authentication Service (CSAM)](https://dt.bosa.be/fr/identification_et_securisation/federal_authentication_service) | National identity document number (DNI) |
| Greece | [Ο.Π.Ε.Κ.Ε.Π.Ε. (OPEKEPE)](https://www.opekepe.gr/) | Tax number |
| Bulgaria | [Държавен фонд Земеделие (DFZ)](https://www.dfz.bg/) | DFZ identifier |
| Slovakia | [Slovenská platobná agentúra (APA)](https://www.apa.sk/) | APA identifier |
| Romania | [Agenţia de Plăţi şi Intervenţie pentru Agricultură (APIA)](https://apia.org.ro/) | APIA identifier |

<br/>

```plantuml
agent "User" as user

component federated_idp [
    FaST federated login
    ---
    Identity Provider 1
    Identity Provider 2
    Identity Provider ...
    Identity Provider N
    FaST Identity Provider
]

note right of federated_idp: example: user selects\nIdentity Provider 2

component idp2 [
    Identity provider 2
    (e.g. TARA for Estonia)
]

note right of idp2: example: user logs in\non Identity Provider 2\n(outside FaST)

component federated_idp_back [
    FaST federated login
]

usecase "User is logged into FaST" as logged_in

user --> federated_idp : wishes to login to FaST
federated_idp --> idp2 : FaST delegates\nauthentication to IdP 2
idp2 --> federated_idp_back : IdP 2 calls back FaST with user identity
federated_idp_back --> logged_in : FaST creates user if first connection\nand generates session/token for the user

caption High-level authentication flow
```

#### User Identifiers must be unique in FaST

In FaST, it is expected that a given user can log in using multiple identity providers and always get back to his/her unique account. Therefore, user identifiers are unique in FaST and *do not depend* on the identity provider used to log in.

For example, an administrator can create a user account (in the Admin Portal) with `username="EE12345678"`. If the person holding this `EE12345678` id then connects through another identity provider (like a national one), the identity provider will return `EE12345678` as well and this will log the user into the account initially created manually.

## Authorization

Authorizations in FaST are managed separately depending on where the user connects from: from the mobile application or from the Administration Portal.
- In the mobile application, authorizations are farm-based  and therefore depend on whether the user is "member" of a farm. Users can only "see" and "act on" farms they are members of. Authorizations are also set at the API Gateway level for all the app users to restrict general access to the API.
- In the Administration Portal, authorizations depend on the activity being performed: for example, permissions can be set to allow an Admin Portal user to communicate with farmers and/or to only view (but not edit) geotagged photos. These permissions sets can be arranged in groups (or "roles"), to be reused for multiple users. In the Admin Portal, there is no farm-based restriction, in the current version, only activity-based permissions.

Notes:
- CRUD: Create Read Update Delete
- for a user to have access to the Admin Portal at all, he/she first needs to have the `staff` status

> It is entirely possible that a FaST user is a member of a farm (= is a farmer or an advisor) AND is staff / Paying Agency employee (= can access the Admin Portal). This is expected and does happen in reality.

### Mobile app authorizations

At the heart of the permissions system for the mobile application is the concept "being member of a farm". In the FaST data model, this is captured by the notion of Related Party, similar to the [INSPIRE Related Party](https://inspire-regadmin.jrc.ec.europa.eu/dataspecification/ScopeObjectDetail.action?objectDetailId=9612)). 

> Once a user is *related party* to a farm, then he/she can view and edit *all the data of this farm*.

These authorizations are enforced at 3 locations:
- in the API gateways to access data and services
- in the media proxy to access media files, such as geotagged photos
- in the vector tiling servers to access farm-specific tiles, such as the tiles of the plots of the farm

In the FaST data model, this information is captured by the UserRelatedParty model, which holds the relationships/memberships of users within farms:

```plantuml
class Holding
class HoldingCampaign

package "Farm membership" as membership <<Rectangle>> {
    class UserRelatedParty {
        role
    }
    class User
}

package "Campaign-based elements" as holding_campaign <<Rectangle>> {
  class Site
  class Plot
  HoldingCampaign "1" *-- "∞" Site
  Site "1" *-- "∞" Plot
}

package "Farm-based elements" as other <<Rectangle>> {
  class SoilSite
  class Ticket
  class TicketMessage
  Holding "1" *-- "∞" SoilSite
  Holding "1" *-- "∞" Ticket
  Ticket "1" *-- "∞" TicketMessage
}

Holding "1" *-- "∞" HoldingCampaign
Holding "1" *-- "∞" UserRelatedParty
User "1" *-- "∞" UserRelatedParty

legend
    When a User is RelatedParty to a Holding, then this User can
    perform all actions on this Holding and the Campaigns of this Holding.
endlegend

caption Extract of the FaST data model for farm-based authorizations
```

#### Initial memberships

The farm memberships of users are initially derived from the regional IACS system. When a user first logs in, he/she is not member of any farm: the memberships are then pulled from the regional IACS APIs and mapped to FaST *related parties*.

#### Administering memberships

The farm memberships can be edited from the Admin Portal (either on the page of a farm or on the page of a user). 

### Admin Portal authorizations

As mentioned above, the authorization system of the Administration Portal is *completely different* from the one of the mobile application. In the adminstration portal, authorizations do not depend on the user's membership to farms, but instead depends on an activity to be performed.

> The activity-based permissions of the Admin Portal are built according to the following semantics. A permission is the combination of:
> - an action from ADD, VIEW, CHANGE, DELETE
> - a FaST object

For example, a user could have the following permissions:
- ADD Ticket
- ADD TicketMessage
- VIEW Ticket
- VIEW TicketMessage
- CHANGE Ticket

With these permissions, the user can view all tickets and their conversation, create a new ticket or a new message in a conversation, or change the ticket's metadata. The user cannot delete any ticket or item of conversation or change an item of a conversation.

Note:
- the ADD, VIEW, CHANGE, DELETE actions in FaST map one-to-one to the usual database acronym CRUD (CREATE, READ, UPDATE, DELETE)
- the permissions system in the Admin Portal is *additive*, ie users initially have zero permissions by default, and permissions are then *added* as necessary

```plantuml
actor "User" as user

component admin_portal [
    Admin Portal
]

note right of admin_portal: Only users with ""staff"" status\ncan connect to the Admin Portal

user --> admin_portal : connects to

component admin_portal_permissions [
    Admin Portal permission system
    ...
    //based on CRUD permissions for//
    //all FaST objects//
]

admin_portal --> admin_portal_permissions

usecase use [
    User can use the Admin Portal\nwithin his/her CRUD permissions
]

admin_portal_permissions --> use
```

#### Grouping permissions

Even though a user can be attached individual permissions if necessary, it is better to define *roles* and attach permissions to these roles. In FaST, the concept of *role* is captured by the Group object. A Group is an object that has a certain number of permissions and a certain number of members. By being member of a group, a user inherits the permissions of this group.

If a user is a member of several groups, then he/she will get as permissions the *union* of the permissions of all the groups the user is member of.

```plantuml
actor User

usecase view_farm_description [
    view_farm_description
    ---
    //can VIEW Holding//
    //can VIEW Campaign//
    //can VIEW HoldingCampaign//
    //can VIEW Site//
    //can VIEW Plot//
    //can VIEW PlotPlantVariety//
]

User --> view_farm_description : is member of

usecase view_fertilization_plans [
    view_fertilization_plans
    ---
    //can VIEW FertilizationPlan//
    //can VIEW Plot//
]

User --> view_fertilization_plans : is also member of

rectangle final [
    Final permissions
    ---
    //can VIEW Holding//
    //can VIEW Campaign//
    //can VIEW HoldingCampaign//
    //can VIEW Site//
    //can VIEW Plot//
    //can VIEW PlotPlantVariety//
    //can VIEW FertilizationPlan//
]

view_farm_description --> final
view_fertilization_plans --> final

```

#### Superusers

Some FaST Paying Agency users can be set as `superusers` (the option is available in the user page). When a user is *superuser* then **he/she gets all the permissions** without needing to assign them.

Because when a user is superuser he/she can see and change everything in the portal, this is a very dangerous situation. Some system objects of FaST will be exposed to the user and changing those objects can lead to breaking situations.

> Superuser accounts should be reserved to one or two technical users, and for all other users, permission-based accounts should be used.

### Special permissions

Some extra ad-hoc permissions are defined in FaST, that do not respect the CRUD (ADD, VIEW, CHANGE, DELETE) semantics explained above. This is for example the case for the `farm | Can insert/update demo holding for a given user` permission, which allows the user to generate demo farms for other users.

### Special case of the ticketing system

The FaST ticketing system has the concept of `Queue` which is a way to categorize incoming tickets by categories. A queue is attached to a certain number of groups (configurable), and therefore a user can only see the tickets assigned to queues he/she is member of. This behaviour supersedes the VIEW permissions on the Ticket and TicketMessage objects.