# Data Model

## Backend Data Model

The FaST backend data is stored in 2 separate Postgres databases, `fastplatform` and `external`, exposed through Hasura in the same GraphQL API endpoint.

- `fastplatform` contains the **private data of the farmer**, centered mostly around the concept of farms (`holding`), and agricultural plots (`plot`);
- `external` contains the **public GIS data**, such as protected sites, hydrography data, public soil samples etc…

## Holding and plots

The FaST data model is derived from the [INSPIRE](https://inspire.ec.europa.eu/) directive. 

The data model specificities of the `holding` and `plot` objects are already extensively covered in the User documentation `User farms and fertilization plans` accessible from the backend: [backend_URI]/services/web/backend/docs/en/farm.md, but we present here an overview of the underlying data model.

### Data model overview

2 main points of interest:

1. The relation between a `farm` and a `plot` is not direct. 2 intermediary objects are created, namely:
   1. `holding_campaign`: Holds all the data of a Farm for a specific growing season (= "campaign");
   2. `site`:  This intermediary object is created so as to maintain conformity with the [INSPIRE Annex III - Agricultural and Aquaculture Facilities](https://inspire.ec.europa.eu/Themes/137/2892) ontology. In the current implementation of the platform, there is only one site ever created for each campaign of a farm (= 1 per `holding_campaign`).
   
2. The `plot` holds most of the private data of the farm, as it is directly referenced ("foreign-key") by the following 5 objects:
   1. `fertilization_plan`: A fertilization plan, as computed by an algorithm for a plot, and based on a set of input parameters
   2. `plot_plant_variety`: A plant variety cultivated on a plot. A Plot object is local to a Site, which is local to a HoldingCampaign. A PlotPlantVariety is also local to a HoldingCampaign
   3. `geo_tagged_photo`: Photo with geolocation information. As per 6.2.5 of https://publications.jrc.ec.europa.eu/repository/bitstream/JRC108816/cts-2017pubsy-online.pdf
   4. `soil_derived_object`: A synthetic collection of soil observations computed from other soil sites and attached to a specific plot
   5. `constraints`: The Model for any constraint: NVZ, Natura 2000, hydrology, etc…

> NB: A thorough presentation of the data model can be found in the `data_model.md` files, accessible at the root of each Django Apps in the backend, as well as [here](full-data-model.md).

```plantuml
@startuml
class "add_ons.AddOn" as add_ons.AddOn #e8f4d6 {
    add-on
    --
    --
}

class "add_ons.AddOnSubscription" as add_ons.AddOnSubscription #e8f4d6 {
    add-on subscription
    --
    --
}
add_ons.AddOnSubscription *-- add_ons.AddOn
add_ons.AddOnSubscription *-- farm.Holding

class "farm.Holding" as farm.Holding #dcd6f4 {
    farm
    --
    --
}

class "farm.HoldingCampaign" as farm.HoldingCampaign #dcd6f4 {
    farm campaign
    --
    --
}
farm.HoldingCampaign *-- farm.Holding
farm.HoldingCampaign *-- farm.Campaign

class "farm.UserRelatedParty" as farm.UserRelatedParty #dcd6f4 {
    farm member
    --
    --
}
farm.UserRelatedParty *-- farm.Holding

class "farm.Campaign" as farm.Campaign #dcd6f4 {
    campaign
    --
    --
}

class "farm.Site" as farm.Site #dcd6f4 {
    site
    --
    --
}
farm.Site *-- farm.HoldingCampaign

class "farm.Plot" as farm.Plot #dcd6f4 {
    plot
    --
    --
}
farm.Plot *-- farm.Site

class "farm.PlotPlantVariety" as farm.PlotPlantVariety #dcd6f4 {
    plot plant variety
    --
    --
}
farm.PlotPlantVariety *-- farm.Plot
farm.PlotPlantVariety *-- fieldbook.PlantVariety

class "farm.Constraint" as farm.Constraint #dcd6f4 {
    constraint
    --
    --
}
farm.Constraint *-- farm.Plot

class "fieldbook.CustomFertilizer" as fieldbook.CustomFertilizer #e7f4d6 {
    custom fertilizer
    --
    --
}
fieldbook.CustomFertilizer *-- farm.Holding

class "fieldbook.FertilizationPlan" as fieldbook.FertilizationPlan #e7f4d6 {
    fertilization plan
    --
    --
}
fieldbook.FertilizationPlan *-- farm.Plot
fieldbook.FertilizationPlan *-- add_ons.AddOn

class "fieldbook.PlantVariety" as fieldbook.PlantVariety #e7f4d6 {
    plant variety
    --
    --
}

class "soil.SoilDerivedObject" as soil.SoilDerivedObject #f4d6dc {
    estimated soil properties
    --
    --
}
soil.SoilDerivedObject *-- farm.Plot


class "soil.DerivedObservation" as soil.DerivedObservation #f4d6dc {
    estimated soil properties observation
    --
    --
}
soil.DerivedObservation *-- soil.SoilDerivedObject
soil.DerivedObservation *-- soil.ObservableProperty

class "soil.ObservableProperty" as soil.ObservableProperty #f4d6dc {
    observable property
    --
    --
}
@enduml
```

### Farm membership

The Farm (`holding`) and the agricultural plot (`plot`) objects hold the private data of the farmer.

This data is associated to the farmer through the `user_related_party` relational table:

```plantuml
skinparam defaultFontSize 12

entity "Holding" as holding
entity "User" as user

package "Farm membership" as membership <<Rectangle>> #cyan {
    entity UserRelatedParty {
        role
    }
}

holding ||--o{ UserRelatedParty
user ||--|| UserRelatedParty

caption "Diagram of FaST objects\nfor holding User membership"
```

> The farmer can only access data of a farm that they are a member of, as per `UserRelatedParty`.

Under specific conditions, the private data of the farm can be accessed by the AddOn. This is only possible if the Farm (`holding`) has **specifically subscribed to the AddOn**. This relationship (=subscription) is materialized in the `add_ons.AddOnSubscription` table:


```plantuml
skinparam defaultFontSize 12

entity "Holding" as holding
entity "Addon" as addon

package "Farm membership" as membership <<Rectangle>> #cyan {
    entity AddOnSubscription {
    }
}

holding ||--o{ AddOnSubscription
addon ||--|| AddOnSubscription

caption "Diagram of FaST objects\nfor holding Addon membership"
```

> As soon as the Farm has unsubscribed from the AddOn, the AddOn will no longer be able to access the Farm's private data.

## Observations

Finally, we present here the data model for the soil samples (`soil.SoilDerivedObject`) and their underlying observations (`soil.DerivedObservation`):

```plantuml
package "fastplatform database" as fastplatform_db <<Rectangle>> {
    class holding {
        id
    }

    folder "farmer sample" #lightblue {
      class soil_site {
          id
          holding_id
          geometry
          valid_from
          valid_to
          source
      }

      class observation {
          id
          result
          result_time
          soil_site_id
          observed_property_id
      }
    }

    class observable_property {
        id
        name
        validation_rule
    }

    soil_site::holding_id -up-> holding::id
    observation::soil_site_id -up-> soil_site::id
    observation::observed_property_id -up-> observable_property::id

    class plot << A plot that belongs to a farm >> {
        id
    }

    folder "Computed sample" #yellow {
      class soil_derived_object << The sample computed for the plot >> {
          id
          plot_id
      }

      class derived_observation << The computed measure or determination >> {
          id
          soil_derived_object_id
          observed_property_id
      }
    }

    soil_derived_object::plot_id -up-> plot::id
    derived_observation::soil_derived_object_id -up-> soil_derived_object::id
    derived_observation::observed_property_id -up-> observable_property::id
}

package "external database" as external_db <<Rectangle>> {
    folder "public sample" #lightblue {
      class external__soil_site {
          id
          geometry
          version_id
      }
          
      class external__observation {
          id
          soil_site_id
          observed_property_id
      }
    }

    class external__soil_site_version {
        id
        is_active
    }

    class external__observable_property {
        id
        name
        validation_rule
    }
    external__soil_site::version_id -up-> external__soil_site_version::id
    external__observation::soil_site_id -up-> external__soil_site::id
    external__observation::observed_property_id -up-> external__observable_property::id
}
```

> NB: this schema can also be found in the README.md file at the root of the `core/event/farm` module:

## Other references

More info on the data model can be found here:

- [Backend Data Model](backend-data-model.md): 
  - How the `data_model.md` files are generated in the backend, 
  - simplified data model schema for 3 main Django apps of the backend: 
    - `Farm`, 
    - `External`, 
    - `Addons`.
- [Full Data Model](reference/full-data-model.md): An Exhaustive data model schema for all the Django Apps of the backend.