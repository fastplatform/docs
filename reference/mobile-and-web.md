> This is a general overview of the mobile and web application: the *[technical documentation](https://gitlab.com/fastplatform/mobile/farmer-mobile-app/-/blob/master/services/apps/farmer/README.md)* is in the project repository.

# Mobile and web application

The FaST farmer application is available as a *web* version and a *native mobile app* version for iOS and Android.

The web and mobile versions of the app are very similar. The features are the same, to the exception of the device notifications which are only available in the iOS and Android versions.

>The web version of the application is fully usable on mobile devices using a web browser such as Safari on iOS and Chrome or Firefox on Android: all of the app features can be used on a smartphone without installing the app from the iOS Appstore or the Android Playstore.


### Overview

The application is intended for farmers and their agricultural advisors. 

Users can import and manage their farm data, visualize satellite and geographical data, manage their parcels, create fertilization plans, take geotagged photos and communicate with their Paying Agency.

> See the *[Quickstart document](../quickstart/app-quickstart-en.pdf)* for a quick explanation of the main features.


### One single app codebase, multiple regions

The codebase of the app is currently the same for every region as by design there are no specific features in the app for any region (however some features can be disabled in the configuration).

> The exception to this rule is the fertilization algorithm which is generally unique to a region: it that case all of the fertilization screens are embedded in the same codebase, but are only accessible when the app is configured to use this algorithm.


### Layouts

The app adjusts automatically its layout according to the size of the user's screen:
- the *mobile layout* on small devices such as smartphone (the native mobile app only uses the mobile layout)
- the *desktop layout* on larger devices such as laptops

>The two layouts have the exact same features but are displayed slightly differently to offer a better experience on each device using responsive design techniques.

  <img src="images/mobile-layout.png" height="400">
  <img src="images/desktop-layout.png" height="400" style="margin-left:20px">


### Configuration

In order to work with the backend from a specific region, the app must be configured to load the region configuration file.

The configuration file is specific to each region and deployed along the region backend services.
It contains the URLs of all the services exposed by FaST for this region, plus some extra configuration variables and feature flags which allow to show or hide some features according to the requirements of the region.

```plantuml

agent "Mobile app" as app

rectangle "FaST backend" as backend {    
    component "Configuration endpoint" as backend_config
    component "Web server &\nTile servers\n..." as backend_web
    component "API Gateways" as backend_api
    database "Databases" as backend_db
    backend_api --> backend_db
}

app -> backend_config : Configuration
app --> backend_api : Services

```

### Internationalization

The app is fully internationalized into the languages of the participating regions plus English, the development language. The internationalization files are included within the app and are loaded when the users selects his/her region.

>Some of the text displayed on the app is fully configurable on the backend side and thus only available in the language of the region. The is the case, for example, with *plant species*.

## Frameworks and deployments

### Main UI frameworks

The app is coded using web technologies (HTML/CSS/JS). Most importantly, it is a single-page application, i.e. user actions in the app do not require server roundtrips that cause the page to reload.

The main frameworks used to build the interface are:
- [Vue.JS](https://vuejs.org/): an open-source model–view–viewmodel front end JavaScript framework for building user interfaces and single-page applications [[Wikipedia]](https://en.wikipedia.org/wiki/Vue.js)
- [Quasar](https://quasar.dev/): an open-source Vue.JS based framework for building apps, with a single codebase, and deploy it on the Web as a SPA, PWA, SSR, to a Mobile App, using Cordova for iOS & Android, and to a Desktop App, using Electron for Mac, Windows, and Linux [[Wikipedia]](https://en.wikipedia.org/wiki/Quasar_framework)
- [Capacitor](https://capacitorjs.com/): is an open source native runtime for building Web Native apps.

<img src="images/vue-js-1.svg" height="50">
<img src="images/quasar-logo-full-inline.svg" height="50">
<img src="images/capacitor-logo-color.svg" height="50">

In the mobile deployments of the app (Android and iOS), some native capabilities are surfaced to the web layer by Capacitor. For example accessing the camera while processing geolocation information, for the geotagged photo feature.
On the web version, these native capabilities are replaced by the web browser standard features.

### API communications

The app mainly communicates with the FaST backed through the API Gateways. These gateways are running the [Hasura GraphQL Engine](https://hasura.io/opensource/) and hence the backend capabilities are exposed using GraphQL semantics.

On the app side, the GraphQL communication is handled by the [Apollo GraphQL client](https://www.apollographql.com/docs/react/), with a [Vue.JS wrapper](https://apollo.vuejs.org/). Amongst other things, the Apollo client provides comprehensive state management that enables the app to manage both local and remote data with GraphQL. The app uses it to fetch, cache, and modify application data, all while automatically updating the UI (through the Vue.JS binding).

<img src="images/apollo-graphql-1.svg" height="50">

## Packaging and deployment

As the app is fundamentally a single-page web application, it is packaged into different forms to suit different platforms.
- for iOS and Android deployments, the app is packaged using the [Capacitor framework](https://capacitorjs.com/), and then compiled and signed using regular Apple (XCode) and Google (Android Studio) tools. The packaged apps can be deployed to the iOS Appstore and Android Playstore.
- for web deployments, the app is packaged into a single-page application/website that is served as static assets from an [NGINX](https://www.nginx.com/) service.

```plantuml
rectangle "Mobile app\nsingle codebase" as code

control "Capacitor" as cap
control "Docker + NGINX" as nginx

artifact "iOS app\n(Apple Store)" as ios
artifact "Android app\n(Google Play Store)" as android
artifact "Web app" as web {
    rectangle "Desktop layout" as desktop
    rectangle "Mobile layout" as mobile
}

code --> cap
code --> nginx

cap --> ios
cap --> android

nginx --> web

desktop <-> mobile : responsive

caption One single codebase, multiple deployments
```

