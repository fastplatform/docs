# Deploy the FaST Platform infrastructure on Flexible Engine

This document explains how to deploy the infrastructure of the FaST Platform on Flexible Engine.

To begin, make sure the following requirements have been installed on your local system: 

- [git-crypt](https://github.com/AGWA/git-crypt) (0.6.0) is used to enable transparent encryption and decryption of files in a Git repository
- [gpg](https://github.com/gpg/gnupg) (2.3.2) is used to manage GPG keys
- [terraform](https://github.com/hashicorp/terraform) (1.0.8) is used to deploy infrastructure resources on Cloud providers
- [terragrunt](https://github.com/gruntwork-io/terragrunt) (0.31.10) is used to manage multiple Terraform modules at once with a single remote state

Make sure the repositories below are cloned. They can be located in the GitLab ```ops``` subgroup of each participating Member State and contain all the materials needed to deploy the infrastructure.

- ```atlantis```: contains the installation script of Atlantis
- ```infrastructure-bootstrap```: contains the Terraform modules to bootstrap the infrastructure
- ```infrastructure```: contains the Terraform modules to deploy the infrastructure
- ```secrets```: contains all the secrets

> Please note that the ```secret``` repository itself is encrypted. Contact your local administrator to get the key and the decryption procedure.

## Local environment setup

### PGP keys

Install ```GnuPG``` and import the PGP keys in your local system.

```bash
  gpg --import pgp/<iso_code>_private_key.pgp
  gpg --import pgp/<iso_code>_public_key.pgp
```

These PGP keys will be used for the decryption of encrypted repositories.

### SSH keys

Copy the SSH key of the ```Jumpbox``` in your local system.

```bash
mkdir -p ~/.ssh/fastplatform/<env>
cp obs-fe/ecs/jumbox/* ~/.ssh/fastplatform/<env>/
```

These SSH keys will be used to connect to the ```Jumpbox``` virtual machine.

### Flexible Engine Access Key

In the Flexible Engine console, in the ```Credentials``` section of a nominative user or service account (e.g. sysadmin.```<iso-code>```) create a new Accesss Key.

<img src="images/obs-fe/fe-console/my_credentials.png" width="800"/>

This Access Key will be used to access the Flexible Engine APIs.


## Bootstrap the infrastructure

The repository ```infrastructure-bootstrap``` contains the Terraform code to bootstrap prerequisite resources on Flexible Engine:
- IAM / Agency
- IAM / Users
- IAM / Groups
- IAM / Custom Policy
- KMS / Keys
- Network / VPC
- Network / Elatic IP

Clone and unlock the ```infrastructure-bootstrap``` repository:
```bash
git clone https://gitlab.com/fastplatform-<iso-code>/ops/infrastructure-bootstrap.git
cd infrastructure-bootstrap
git crypt unlock
```

Set the following environment variables with the Flexible Engine Access Key:
```bash
export AWS_ACCESS_KEY_ID="" # Flexible Engine Access Key ID
export AWS_SECRET_ACCESS_KEY="" # Flexible Engine Access Key
```

Bootstrap the infrastructure:
```bash
terragrunt run-all init
terragrunt run-all apply -compact-warnings
```

Print the output variables:
```bash
terragrunt run-all output
```

Register new records for the domain fastplatform.eu in the DNS provider with the IP address printed above:
- ```<iso-code>.fastplatform.eu```, record of type A with the IP address associated with the Load Balancer
- ```*.<iso-code>.fastplatform.eu```, wildcard record of type CNAME with the alias ```<iso-code>.fastplatform.eu```
- ```atlantis.<iso-code>.fastplatform.eu```, record of type A with the IP address associated with the Jumpbox

## Deploy the infrastructure

The repository ```infrastructure``` contains the Terraform code to deploy the infrastructure on Flexible Engine:
- Network / Subnets, Gateway, Elastic Load Balancer, Security Groups, Firewall rules, etc ...
- OSS / Buckets: ```private```, ```public```, ```backup```
- ECS / Jumpbox virtual machine
- CCE / Kubernetes cluster + Settings 
- FluxCD / Controller deployment + Settings

Clone and unlock the ```infrastructure``` repository:
```bash
git clone https://gitlab.com/fastplatform-<iso-code>/ops/infrastructure.git
cd infrastructure
git crypt unlock
```

Set the following environment variables with the Flexible Engine Access Key:
```bash
export AWS_ACCESS_KEY_ID="" # Flexible Engine Access Key ID
export AWS_SECRET_ACCESS_KEY="" # Flexible Engine Access Key
```

Clone, unlock the ```secrets``` repository and source environment settings: 
```bash
  git clone https://gitlab.com/fastplatform-<iso-code>/ops/secrets.git
  source secrets/obs-fe/openstack-setup.sh
```


Deploy the infrastructure:
```bash
terragrunt run-all apply -compact-warnings
```

## Atlantis

Once the infrastructure is deployed, the ```Jumpbox``` virtual machine can be setup with Atlantis.

Create a GitLab service account and add it to the GitLab group of the participating Member State, and upload the SSH key of the ```Jumpbox```.
- **Name**: ```atlantis-<iso-code>-bot```
- **Email**: ```tech.<iso-code>@fastplatform.eu```
- **Role**: ```Developer```

Configure a Webhook on the ```ìnfrastructure``` repository to trigger Atlantis endpoints on specific events as described in the documentation [online](https://www.runatlantis.io/docs/configuring-webhooks.html):
- Push events
- Comments
- Merge Request events

Get the ```install_atlantis.sh``` script from the ```ops/atlantis``` repository of the participating Member State, and copy it to the ```Jumpbox``` virtual machine. Open an SSH connection and run the script from the root directory to install Atlantis.
