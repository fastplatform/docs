# Monitoring

For each participating Member State, monitoring tools are deployed in the FaST platform to provide metrics and observability.

## Metrics (Grafana + Prometheus)

Each component of the FaST platform exposes metrics that are collected by [Prometheus](https://prometheus.io/) and displayed in [Grafana](https://grafana.com/) dashboards.

A synthetic dashboard provides an overview of the health of the FaST platform.  Other specific dashboards are available and are divided into five groups:
- Kubernetes
- Istio 
- Knative
- PostgreSQL
- Alerting
 
For security reasons, the Grafana and Prometheus endpoints are not accessible from the public Internet, but only via port forwarding.

## Distributed tracing (OpenTelemetry + Jaeger)

Observability of network traffic between microservices is essential to perform performance and root cause analysis. The complete trace of calls can be obtained by instrumenting the stack using the [OpenTelemetry](https://opentelemetry.io/)  approach. 

[Jaeger](https://www.jaegertracing.io/) is an Open Source implementation of OpenTelemetry and is used in the FaST platform to provide maximum observability.

For security reasons, the Jaeger endpoint is not accessible from the public Internet, but only via port forwarding.
