# Runbook 

This document provides a basic set of information for managing the FaST platform.

## How can I apply new changes to the infrastructure ?

First, update the Terraform files according to the changes to be made to the infrastructure. This can be a variable in the ```terraform.tfvars``` files or the implementation of a module itself.

Then the changes can be applied. Atlantis is deployed with the FaST platform and is configured on the directories hosting the Terraform code. It allows to remotely apply a command via the comments of a Merge Request:
1. Push the changes on a new development branch
2. Create a Merge Request for this new branch
3. Atlantis will automatically run a ```terragrunt run-all plan``` command and publish the result as a comment on the Merge Request
4. Comment the merge request with a ```terragrunt run-all apply``` command to apply the changes to the infrastructure

> Please note that the ```terragrunt run-all plan``` and ```terragrunt run-all apply``` commands can also be applied locally. 

## What can I do from the Cloud Customer Space of Orange ?

The Cloud subscriptions are managed from the [Cloud Customer Space]((https://selfcare.cloud.orange-business.com/)). From this space a user can:
* Manage the contract with Orange
* Check the monthly invoices
* Open a ticket to the Support service
* Manage users and their permissions
* Access the Flexible Engine console

The documentation of the Flexible Engine console is available [here](https://docs.prod-cloud-ocb.orange-business.com/).

## How do I get access to a CCE Kubernetes cluster (Flexible Engine) ?

A certificate as well as a private key are needed to access a Kubernetes cluster. From the CCE interface, the corrresponding ```kubeconfig.json``` file can be downloaded. It contains credentials that are nominative and subject to the permissions assigned.

The ```kubeconfig.json``` file is to be used with the ```kubectl``` CLI. The binary can be downloaded from the official [website](https://kubernetes.io/docs/tasks/tools/install-kubectl). The documentation of Kubernetes is avilable [online](https://kubernetes.io).

## How do I access to the S3 buckets (Flexible Engine) ?

S3 buckets are used to store public and private data. To access this data you need to use the service accounts associated with each of the buckets:
- ```tech.<iso-code>``` service account for ```fastplatform-<iso-code>-public``` and ```fastplatform-<iso-code>-private``` buckets
- ```backup.<iso-code>``` service account for ```fastplatform-<iso-code>-bucket``` bucket

On the Flexible Engine console, you can access S3 buckets from the "Object Storage Service" section if you have the permissions.

## How can I deploy to FaST platform environments (beta, staging, ephemeral)

Deployments are done using GitLab jobs which are available according to the context of each commit:
- ```deploy:beta``` job is only available on tags
- ```deploy:staging``` job is only available for commits of the ```master``` branch
- ```deploy:review``` job is only available on a Merge Request

## Troubleshooting

### Is it normal for Terraform to consider converging the number of nodes in a CCE cluster to its initial value ?

Depending on the workload, the number of nodes in a CCE cluster may differ from its initial number. In some cases, certain changes may involve resizing a CCE cluster to its original number of nodes. This is an issue with Flexible Engine and the corresponding Terraform provider implementation.
