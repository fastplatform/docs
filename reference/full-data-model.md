# Full Data model

You will find here the full data model of the platform, for each Django app.

- [Full Data model](#full-data-model)
  - [Addons](#addons)
  - [Authentication](#authentication)
  - [Common](#common)
  - [Configuration](#configuration)
  - [External](#external)
  - [Farm](#farm)
  - [Fieldbook](#fieldbook)
  - [Messaging](#messaging)
  - [Photos](#photos)
  - [Soil](#soil)


## Addons

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "add_ons.Provider <Add-ons>" as add_ons.Provider #e8f4d6 {
    provider
    ..
    A provider of add-ons
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - The name of the provider, as displayed in the application
and in the admin portal
    + description (TextField) - A description of the provider, that will be
displayed in the app
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the provider
    + is_active (BooleanField) - Whether this provider and all its add-ons should
be active (visible) in the app. Disabling a provider also disables all the add-
ons of this provider.
    # members (ManyToManyField) - The users that are allowed to manage this
provider
    --
}


class "add_ons.ProviderMember <Add-ons>" as add_ons.ProviderMember #e8f4d6 {
    provider member
    ..
    ProviderMember(id, provider, user)
    --
    + id (BigAutoField) - 
    ~ provider (ForeignKey) - 
    ~ user (ForeignKey) - 
    --
}
add_ons.ProviderMember *-- add_ons.Provider


class "add_ons.AddOn <Add-ons>" as add_ons.AddOn #e8f4d6 {
    add-on
    ..
    AddOn(i18n, id, is_api_access, provider, name, description, is_active,
is_visible, logo, website, callback, callback_secret_key, callback_with_user_id,
callback_with_holding_id, callback_with_campaign_id, auto_subscribe)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + is_api_access (BooleanField) - 
    ~ provider (ForeignKey) - The provider that manages this add-on
    + name (CharField) - The name of the add-on, as displayed in the app. Try to
keep it short.
    + description (TextField) - A description of the add-on, that will be displayed
in the app
    + is_active (BooleanField) - Whether this add-on should be active. Inactive
add-ons cannot query the FaST API and are not visible in the app.
    + is_visible (BooleanField) - Whether this add-on should be visible
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the add-on
    + callback (URLField) - The callback URL that FaST will call using GET when the
user accesses the service from the app.
    + callback_secret_key (CharField) - The callback secret key is used to create
the jwt token. If a callback URL has been set and this field is left empty, a
secret key will be randomly generated on save.
    + callback_with_user_id (BooleanField) - Whether the username should be passed
when FaST calls the callback URL
    + callback_with_holding_id (BooleanField) - Whether the holding identifier
should be passed when FaST calls the callback URL
    + callback_with_campaign_id (BooleanField) - Whether the campaign identifier
should be passed when FaST calls the callback URL
    + auto_subscribe (BooleanField) - Whether this add-on can access the data of
all holdings (without users first needing to subscribe)
    # groups (ManyToManyField) - The groups this add-on belongs to. An add-on will
get all permissions granted to each of their groups.
    # permissions (ManyToManyField) - Specific permissions for this add-on
    # holdings (ManyToManyField) - 
    --
}
add_ons.AddOn *-- add_ons.Provider


class "add_ons.APIAccess <Add-ons>" as add_ons.APIAccess #e8f4d6 {
    API access
    ..
    APIAccess(i18n, id, is_api_access, provider, name, description, is_active,
is_visible, logo, website, callback, callback_secret_key, callback_with_user_id,
callback_with_holding_id, callback_with_campaign_id, auto_subscribe)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + is_api_access (BooleanField) - 
    ~ provider (ForeignKey) - The provider that manages this add-on
    + name (CharField) - The name of the add-on, as displayed in the app. Try to
keep it short.
    + description (TextField) - A description of the add-on, that will be displayed
in the app
    + is_active (BooleanField) - Whether this add-on should be active. Inactive
add-ons cannot query the FaST API and are not visible in the app.
    + is_visible (BooleanField) - Whether this add-on should be visible
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the add-on
    + callback (URLField) - The callback URL that FaST will call using GET when the
user accesses the service from the app.
    + callback_secret_key (CharField) - The callback secret key is used to create
the jwt token. If a callback URL has been set and this field is left empty, a
secret key will be randomly generated on save.
    + callback_with_user_id (BooleanField) - Whether the username should be passed
when FaST calls the callback URL
    + callback_with_holding_id (BooleanField) - Whether the holding identifier
should be passed when FaST calls the callback URL
    + callback_with_campaign_id (BooleanField) - Whether the campaign identifier
should be passed when FaST calls the callback URL
    + auto_subscribe (BooleanField) - Whether this add-on can access the data of
all holdings (without users first needing to subscribe)
    # groups (ManyToManyField) - The groups this add-on belongs to. An add-on will
get all permissions granted to each of their groups.
    # permissions (ManyToManyField) - Specific permissions for this add-on
    # holdings (ManyToManyField) - 
    --
}
add_ons.APIAccess *-- add_ons.Provider


class "add_ons.AddOnAPIKey <Add-ons>" as add_ons.AddOnAPIKey #e8f4d6 {
    add-on API key
    ..
    AddOnAPIKey(id, add_on, key, generated_by, generated_at)
    --
    - id (AutoField) - 
    ~ add_on (ForeignKey) - The add-on this key belongs to
    + key (CharField) - The randomly generated key
    ~ generated_by (ForeignKey) - The user that generated this key
    + generated_at (DateTimeField) - The date/time this key was generated
    --
}
add_ons.AddOnAPIKey *-- add_ons.AddOn


class "add_ons.AddOnSubscription <Add-ons>" as add_ons.AddOnSubscription #e8f4d6 {
    add-on subscription
    ..
    AddOnSubscription(id, add_on, holding, subscribed_at, subscribed_by)
    --
    + id (BigAutoField) - 
    ~ add_on (ForeignKey) - 
    ~ holding (ForeignKey) - 
    + subscribed_at (DateTimeField) - 
    ~ subscribed_by (ForeignKey) - 
    --
}
add_ons.AddOnSubscription *-- add_ons.AddOn


@enduml
```

## Authentication

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "authentication.FederatedProvider <Authentication & Authorization>" as authentication.FederatedProvider #ded6f4 {
    federated identity provider
    ..
    A identity provider that FaST can delegate to for guaranteeing
    user identity and logging into FaST
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - The name of the federated identity provider, as displayed
on the login screen. Try to keep it short.
    + description (TextField) - A description of the identity provider. Keep it
short as well.
    + logo (ImageField) - 
    + url (URLField) - The URL of the login page of the identity provider, that
FaST will redirect the user to
    + background_color (ColorField) - The background color of the button on the
login screen
    + text_color (ColorField) - The text color of the button on the login screen
    + is_active (BooleanField) - If this box is not checked, the identity provider
will not be proposed to the users.
    --
}


class "authentication.FederatedProviderLogEntry <Authentication & Authorization>" as authentication.FederatedProviderLogEntry #ded6f4 {
    federated provider log entry
    ..
    Audit trail entries for the federated identity providers
    --
    - id (AutoField) - 
    ~ federated_provider (ForeignKey) - 
    + entry_time (DateTimeField) - 
    + entry_type (CharField) - 
    + username (CharField) - 
    + ip_address (GenericIPAddressField) - 
    + user_agent (CharField) - 
    + path (CharField) - 
    + data (JSONField) - 
    --
}
authentication.FederatedProviderLogEntry *-- authentication.FederatedProvider


class "authentication.Country <Authentication & Authorization>" as authentication.Country #ded6f4 {
    country
    ..
    Country(iso_code, name, default_language)
    --
    + iso_code (CharField) - The ISO code of the country as per ISO 3166-1 alpha-2
    + name (TextField) - The name of the country in its local language
    ~ default_language (ForeignKey) - The default/official language of the country
    # languages (ManyToManyField) - The acceptable languages for this country
    --
}
authentication.Country *-- authentication.Language
authentication.Country *--* authentication.Language


class "authentication.Region <Authentication & Authorization>" as authentication.Region #ded6f4 {
    region
    ..
    Region(id, iso_code, name, country, default_language)
    --
    + id (CharField) - The region identifier, as XX-YYY, where XX is the ISO 3166-1
alpha 2 code of the country and YY is the ISO 3166-2 alpha 2 code of the region.
    + iso_code (CharField) - The ISO 3166-2 alpha 2 code of the region
    + name (CharField) - The region name in its local language
    ~ country (ForeignKey) - The country this region belongs in
    ~ default_language (ForeignKey) - The default/official language of the region
    # languages (ManyToManyField) - The acceptable languages of the region
    --
}
authentication.Region *-- authentication.Country
authentication.Region *-- authentication.Language
authentication.Region *--* authentication.Language


class "authentication.Language <Authentication & Authorization>" as authentication.Language #ded6f4 {
    language
    ..
    A language available in the system
    --
    + iso_code (CharField) - The ISO 639-1 code of the language
    + name (CharField) - The name of the language
    --
}


class "authentication.User <Authentication & Authorization>" as authentication.User #ded6f4 {
    User
    ..
    A user of the FaST platform
    Can be a farmer, advisor or PA staff, or all at once
    --
    + password (CharField) - 
    + last_login (DateTimeField) - 
    + is_superuser (BooleanField) - Designates that this user has all permissions
without explicitly assigning them.
    + is_staff (BooleanField) - Designates whether the user can log into this admin
site.
    + is_active (BooleanField) - Designates whether this user should be treated as
active. Unselect this instead of deleting accounts.
    + date_joined (DateTimeField) - 
    + id (CharField) - 
    + username (CharField) - Required. 255 characters or fewer. Letters, digits and
@/./+/-/_ only. For users authenticated through a third party service, this
username is determined by the the third party service. The username MUST be
unique accross the FaST environment.
    + first_name (CharField) - The first name(s) of the user
    + last_name (CharField) - The last name(s) of the user
    + name (CharField) - The full name of the user. This field is not computed
based on first name and last name, and must be filled manually.
    ~ region (ForeignKey) - The region this user belongs to
    + picture (ImageField) - 
    ~ preferred_language (ForeignKey) - 
    + additional_data (JSONField) - Additional data stored for the user, as a key-
value JSON, usually to store region-specific authentication data.
    + email (EmailField) - Email address of the user, if provided by the third-
party authentication system or by the user
    # user_permissions (ManyToManyField) - Specific permissions for this user.
    # groups (ManyToManyField) - 
    --
}
authentication.User *-- authentication.Region
authentication.User *-- authentication.Language


class "authentication.Group <Authentication & Authorization>" as authentication.Group #ded6f4 {
    group
    ..
    Group(id, name)
    --
    - id (AutoField) - 
    + name (CharField) - 
    # permissions (ManyToManyField) - 
    --
}


class "authentication.TermsAndConditionsConsentRequest <Authentication & Authorization>" as authentication.TermsAndConditionsConsentRequest #ded6f4 {
    terms & conditions consent request
    ..
    TermsAndConditionsConsentRequest(i18n, id, activate_at, deactivate_at, title,
short_description, url, html_content, in_farmer_app, in_administration_portal)
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + activate_at (DateTimeField) - Consent will not be requested from users before
that date (can be left blank).
    + deactivate_at (DateTimeField) - Consent will not be requested from users
after that date (can be left blank).
    + title (CharField) - 
    + short_description (TextField) - 
    + url (URLField) - 
    + html_content (TextField) - 
    + in_farmer_app (BooleanField) - Consent will be requested in the farmer mobile
and web app
    + in_administration_portal (BooleanField) - Consent will be requested in the
Administration Portal
    --
}


class "authentication.UserTermsAndConditionsConsent <Authentication & Authorization>" as authentication.UserTermsAndConditionsConsent #ded6f4 {
    user terms & conditions consent
    ..
    UserTermsAndConditionsConsent(id, user, consent_request, consent_received_at,
metadata)
    --
    - id (AutoField) - 
    ~ user (ForeignKey) - 
    ~ consent_request (ForeignKey) - 
    + consent_received_at (DateTimeField) - 
    + metadata (JSONField) - 
    --
}
authentication.UserTermsAndConditionsConsent *-- authentication.User
authentication.UserTermsAndConditionsConsent *-- authentication.TermsAndConditionsConsentRequest


class "authentication.UserIACSConsent <Authentication & Authorization>" as authentication.UserIACSConsent #ded6f4 {
    user IACS consent
    ..
    UserIACSConsent(id, user, consent_received_at, metadata)
    --
    - id (AutoField) - 
    ~ user (ForeignKey) - 
    + consent_received_at (DateTimeField) - 
    + metadata (JSONField) - 
    --
}
authentication.UserIACSConsent *-- authentication.User


class "authentication.OIDCClient <Authentication & Authorization>" as authentication.OIDCClient #ded6f4 {
    OIDC Client
    ..
    OIDCClient(id, name, owner, client_type, client_id, client_secret, jwt_alg,
date_created, website_url, terms_url, contact_email, logo, reuse_consent,
require_consent, require_pkce, _redirect_uris, _post_logout_redirect_uris,
_scope)
    --
    + id (BigAutoField) - 
    + name (CharField) - 
    ~ owner (ForeignKey) - 
    + client_type (CharField) - <b>Confidential</b> clients are capable of
maintaining the confidentiality of their credentials. <b>Public</b> clients are
incapable.
    + client_id (CharField) - 
    + client_secret (CharField) - 
    + jwt_alg (CharField) - Algorithm used to encode ID Tokens.
    + date_created (DateField) - 
    + website_url (CharField) - 
    + terms_url (CharField) - External reference to the privacy policy of the
client.
    + contact_email (CharField) - 
    + logo (FileField) - 
    + reuse_consent (BooleanField) - If enabled, server will save the user consent
given to a specific client, so that user won't be prompted for the same
authorization multiple times.
    + require_consent (BooleanField) - If disabled, the Server will NEVER ask the
user for consent.
    + require_pkce (BooleanField) - Require PKCE (RFC7636) for public clients. You
should only disable this if you know that the client does not support it. Has no
effect on confidential clients.
    + _redirect_uris (TextField) - Enter each URI on a new line. For native
applications using looback addresses the port will be ignored to support
applications binding to ephemeral ports.
    + _post_logout_redirect_uris (TextField) - Enter each URI on a new line.
    + _scope (TextField) - Specifies the authorized scope values for the client
app.
    # response_types (ManyToManyField) - 
    --
}
authentication.OIDCClient *-- authentication.User


class "authentication.OIDCCode <Authentication & Authorization>" as authentication.OIDCCode #ded6f4 {
    OIDC Authorization code
    ..
    OIDCCode(id, client, _scope, user, code, nonce, is_authentication,
code_challenge, code_challenge_method, expires_at)
    --
    + id (BigAutoField) - 
    ~ client (ForeignKey) - 
    + _scope (TextField) - 
    ~ user (ForeignKey) - 
    + code (CharField) - 
    + nonce (CharField) - 
    + is_authentication (BooleanField) - 
    + code_challenge (CharField) - 
    + code_challenge_method (CharField) - 
    + expires_at (DateTimeField) - 
    --
}
authentication.OIDCCode *-- authentication.User


class "authentication.OIDCToken <Authentication & Authorization>" as authentication.OIDCToken #ded6f4 {
    OIDC Token
    ..
    OIDCToken(id, client, _scope, issued_at, user, access_token, access_expires_at,
refresh_token, _id_token)
    --
    + id (BigAutoField) - 
    ~ client (ForeignKey) - 
    + _scope (TextField) - 
    + issued_at (DateTimeField) - 
    ~ user (ForeignKey) - 
    + access_token (CharField) - 
    + access_expires_at (DateTimeField) - 
    + refresh_token (CharField) - 
    + _id_token (TextField) - 
    --
}
authentication.OIDCToken *-- authentication.User


class "authentication.LoginAccessAttempt <Authentication & Authorization>" as authentication.LoginAccessAttempt #ded6f4 {
    Login attempt
    ..
    LoginAccessAttempt(id, user_agent, ip_address, username, http_accept, path_info,
attempt_time, get_data, post_data, failures_since_start)
    --
    - id (AutoField) - 
    + user_agent (CharField) - 
    + ip_address (GenericIPAddressField) - 
    + username (CharField) - 
    + http_accept (CharField) - 
    + path_info (CharField) - 
    + attempt_time (DateTimeField) - 
    + get_data (TextField) - 
    + post_data (TextField) - 
    + failures_since_start (PositiveIntegerField) - 
    --
}



@enduml
```

## Common

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "common.UnitOfMeasure <Reference datasets>" as common.UnitOfMeasure #d6d7f4 {
    unit of measure
    ..
    UnitOfMeasure(i18n, id, name, symbol)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + symbol (CharField) - 
    --
}


@enduml
```

## Configuration

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "configuration.Configuration <Configuration>" as configuration.Configuration #f4d6d7 {
    configuration
    ..
    Configuration(id, backend_leaflet_default_center_latitude,
backend_leaflet_default_center_longitude, backend_leaflet_default_zoom,
backend_leaflet_min_zoom, backend_leaflet_max_zoom, backend_leaflet_map_width,
backend_leaflet_map_height, backend_leaflet_display_raw,
backend_leaflet_map_srid, protected_site_overlay_style,
hydrography_overlay_style, soil_overlay_style,
management_restriction_or_regulation_zone_overlay_style,
agri_plot_overlay_style, plots_overlay_style, rgb_ndvi_overlay_style,
display_surface_unit_name, display_surface_unit_short_name,
display_surface_unit_ratio_to_hectare)
    --
    + id (CharField) - 
    + backend_leaflet_default_center_latitude (FloatField) - Default center
latitude for maps in the admin backend
    + backend_leaflet_default_center_longitude (FloatField) - Default center
longitude for maps in the admin backend
    + backend_leaflet_default_zoom (IntegerField) - Default zoom level of maps in
the admin backend
    + backend_leaflet_min_zoom (IntegerField) - Minimum zoom level of maps in the
admin backend
    + backend_leaflet_max_zoom (IntegerField) - Maximum zoom level of maps in the
admin backend
    + backend_leaflet_map_width (CharField) - Default width of maps in the admin
backend, in CSS units
    + backend_leaflet_map_height (CharField) - Default height of maps in the admin
backend, in CSS units
    + backend_leaflet_display_raw (BooleanField) - Whether or not to display the
current geometry as WKT in the admin backend
    + backend_leaflet_map_srid (IntegerField) - SRID of the maps in the admin
backend
    + protected_site_overlay_style (JSONField) - 
    + hydrography_overlay_style (JSONField) - 
    + soil_overlay_style (JSONField) - 
    + management_restriction_or_regulation_zone_overlay_style (JSONField) - 
    + agri_plot_overlay_style (JSONField) - 
    + plots_overlay_style (JSONField) - 
    + rgb_ndvi_overlay_style (JSONField) - 
    + display_surface_unit_name (CharField) - Will default to 'hectare' if not set
    + display_surface_unit_short_name (CharField) - Will default to 'ha' if not set
    + display_surface_unit_ratio_to_hectare (FloatField) - Will default to 1.0 if
not set
    --
}


class "configuration.MapBaseLayer <Configuration>" as configuration.MapBaseLayer #f4d6d7 {
    map base layer
    ..
    MapBaseLayer(i18n, id, title, min_zoom, max_zoom, is_active_in_farmer_app,
is_active_in_admin_portal, layer_type, url, max_native_zoom, attribution,
layers, proj_format, proj_name, proj_wkt, wms_version, styles, is_transparent,
display)
    --
    + i18n (JSONField) - 
    + id (CharField) - Can contain only lowercase letters (a-z), digits (0-9), dash
(-) and underscore (_).
    + title (CharField) - 
    + min_zoom (IntegerField) - Below this zoom level, the layer will not be
displayed.
    + max_zoom (IntegerField) - Above this zoom level, the layer will not be
displayed.
    + is_active_in_farmer_app (BooleanField) - Whether or not to display this layer
in the layer selection menu in the Farmer (mobile) App
    + is_active_in_admin_portal (BooleanField) - Whether or not to display this
layer in the layer selection menu in the Administration Portal
    + layer_type (CharField) - 
    + url (CharField) - 
    + max_native_zoom (IntegerField) - Maximum zoom level provided by the source
    + attribution (CharField) - 
    + layers (CharField) - Comma-separated list of source WMS layer(s) to display
    + proj_format (CharField) - 
    + proj_name (CharField) - Authority name of the source projection, in EPSG:XXXX
format
    + proj_wkt (TextField) - 
    + wms_version (CharField) - 
    + styles (CharField) - Comma-separated list of styles, leave blank to use
default style
    + is_transparent (BooleanField) - Whether the selected layer(s) contains any
alpha transparency (alpha channel)
    + display (BooleanField) - Whether or not this is the default base layer. Only
one base layer can be default at a time.
    --
}


class "configuration.MapOverlay <Configuration>" as configuration.MapOverlay #f4d6d7 {
    map overlay
    ..
    MapOverlay(i18n, id, title, min_zoom, max_zoom, is_active_in_farmer_app,
is_active_in_admin_portal, layer_type, url, max_native_zoom, attribution,
layers, proj_format, proj_name, proj_wkt, wms_version, styles, is_transparent,
layer_order, display)
    --
    + i18n (JSONField) - 
    + id (CharField) - Can contain only lowercase letters (a-z), digits (0-9), dash
(-) and underscore (_).
    + title (CharField) - 
    + min_zoom (IntegerField) - Below this zoom level, the layer will not be
displayed.
    + max_zoom (IntegerField) - Above this zoom level, the layer will not be
displayed.
    + is_active_in_farmer_app (BooleanField) - Whether or not to display this layer
in the layer selection menu in the Farmer (mobile) App
    + is_active_in_admin_portal (BooleanField) - Whether or not to display this
layer in the layer selection menu in the Administration Portal
    + layer_type (CharField) - 
    + url (CharField) - 
    + max_native_zoom (IntegerField) - Maximum zoom level provided by the source
    + attribution (CharField) - 
    + layers (CharField) - Comma-separated list of source WMS layer(s) to display
    + proj_format (CharField) - 
    + proj_name (CharField) - Authority name of the source projection, in EPSG:XXXX
format
    + proj_wkt (TextField) - 
    + wms_version (CharField) - 
    + styles (CharField) - Comma-separated list of styles, leave blank to use
default style
    + is_transparent (BooleanField) - Whether the selected layer(s) contains any
alpha transparency (alpha channel)
    + layer_order (IntegerField) - Stacking order of the layer
    + display (BooleanField) - Whether this overlay is displayed by default.
    --
}


@enduml
```

## External

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "external.UnitOfMeasure <Imported GIS data sources>" as external.UnitOfMeasure #d6edf4 {
    unit of measure
    ..
    UnitOfMeasure(i18n, id, name, symbol)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + symbol (CharField) - 
    --
}


class "external.ManagementRestrictionOrRegulationZoneVersion <Imported GIS data sources>" as external.ManagementRestrictionOrRegulationZoneVersion #d6edf4 {
    management restriction or regulation zone version
    ..
    A version of the regulated zones data (NVZ)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.ManagementRestrictionOrRegulationZone <Imported GIS data sources>" as external.ManagementRestrictionOrRegulationZone #d6edf4 {
    management restriction or regulation zone
    ..
    A zone where a specific regulation or restriction applies (eg NVZ)
    Inspire - Annex III - Area Management Restriction, Regulation Zones and
Reporting units
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (MultiPolygonField) - 
    + thematic_id (CharField) - 
    + name (CharField) - 
    ~ specialized_zone_type (ForeignKey) - 
    + designation_period (DateRangeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - The source data from which this object was derived.
    # zone_type (ManyToManyField) - 
    # environmental_domain (ManyToManyField) - 
    --
}
external.ManagementRestrictionOrRegulationZone *-- external.ManagementRestrictionOrRegulationZoneVersion
external.ManagementRestrictionOrRegulationZone *-- external.SpecialisedZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.ZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.EnvironmentalDomain


class "external.ZoneTypeCode <Imported GIS data sources>" as external.ZoneTypeCode #d6edf4 {
    zone type code
    ..
    A type of regulated zone
    http://inspire.ec.europa.eu/codelist/ZoneTypeCode
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.ZoneTypeCode *-- external.ZoneTypeCode


class "external.SpecialisedZoneTypeCode <Imported GIS data sources>" as external.SpecialisedZoneTypeCode #d6edf4 {
    specialized zone type code
    ..
    Additional classification value that defines the specialised type of zone
    https://inspire.ec.europa.eu/codelist/SpecialisedZoneTypeCode
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.SpecialisedZoneTypeCode *-- external.SpecialisedZoneTypeCode


class "external.EnvironmentalDomain <Imported GIS data sources>" as external.EnvironmentalDomain #d6edf4 {
    environmental domain
    ..
    Environmental domain, for which environmental objectives can be defined
    http://inspire.ec.europa.eu/codelist/EnvironmentalDomain
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.EnvironmentalDomain *-- external.EnvironmentalDomain


class "external.SurfaceWaterVersion <Imported GIS data sources>" as external.SurfaceWaterVersion #d6edf4 {
    surface water version
    ..
    A version of the surface water data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.SurfaceWater <Imported GIS data sources>" as external.SurfaceWater #d6edf4 {
    surface water
    ..
    A surface water body (ie still water)
    Inspire - Annex I - Hydrography - Physical Waters
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    + hydro_id (CharField) - 
    + geometry (GeometryField) - 
    + geographical_name (CharField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + buffer (IntegerField) - 
    + local_type (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    ~ version (ForeignKey) - 
    --
}
external.SurfaceWater *-- external.SurfaceWaterVersion


class "external.WaterCourseVersion <Imported GIS data sources>" as external.WaterCourseVersion #d6edf4 {
    water course version
    ..
    A version of the water course data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.WaterCourse <Imported GIS data sources>" as external.WaterCourse #d6edf4 {
    water course
    ..
    A water course (ie running water)
    Inspire - Annex I - Hydrography - Physical Waters
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    + hydro_id (CharField) - 
    + geometry (GeometryField) - 
    + geographical_name (CharField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + buffer (IntegerField) - 
    + local_type (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    ~ version (ForeignKey) - 
    --
}
external.WaterCourse *-- external.WaterCourseVersion


class "external.ProtectedSiteVersion <Imported GIS data sources>" as external.ProtectedSiteVersion #d6edf4 {
    protected site version
    ..
    A version of the protected sites data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.ProtectedSite <Imported GIS data sources>" as external.ProtectedSite #d6edf4 {
    protected site
    ..
    A protected site (eg Natura 2000)
    Inspire - Annex I - Protected Site
    An area designated or managed within a framework of international,
    Community and Member States' legislation to achieve specific conservation
    objectives.
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (GeometryField) - 
    + legal_foundation_date (DateTimeField) - The date that the protected site was
legally created. This is the date that the real world object was created, not
the date that its representation in an information system was created.
    + legal_foundation_document (CharField) - A URL or text citation referencing
the legal act that created the Protected Site.
    + site_name (CharField) - 
    + site_protection_classification (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    # site_designation (ManyToManyField) - 
    --
}
external.ProtectedSite *-- external.ProtectedSiteVersion
external.ProtectedSite *--* external.DesignationType


class "external.DesignationType <Imported GIS data sources>" as external.DesignationType #d6edf4 {
    designation type
    ..
    A data type designed to contain a designation for the Protected Site,
    including the designation scheme used and the value within that scheme.
    --
    - id (AutoField) - 
    ~ designation_scheme (ForeignKey) - The scheme from which the designation code
comes.
    ~ designation (ForeignKey) - 
    + percentage_under_designation (FloatField) - The percentage of the site that
falls under the designation. This is used in particular for the IUCN
categorisation. If a value is not provided for this attribute, it is assumed to
be 100%.
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.DesignationType *-- external.DesignationScheme
external.DesignationType *-- external.Designation


class "external.DesignationScheme <Imported GIS data sources>" as external.DesignationScheme #d6edf4 {
    designation scheme
    ..
    A scheme used to assign a designation to the Protected Sites.
    Inspire - Annex I - Protected Sites
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.DesignationScheme *-- external.DesignationScheme


class "external.Designation <Imported GIS data sources>" as external.Designation #d6edf4 {
    designation
    ..
    An abstract base type for code lists containing the
    classification and desigation types under different schemes.
    Inspire - Annex I - Protected Sites
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.Designation *-- external.Designation


class "external.SoilSiteVersion <Imported GIS data sources>" as external.SoilSiteVersion #d6edf4 {
    soil site version
    ..
    SoilSiteVersion(id, is_active, status, import_started_at, import_completed_at,
import_log)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.SoilSite <Imported GIS data sources>" as external.SoilSite #d6edf4 {
    soil site
    ..
    Soil sites
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (GeometryField) - 
    ~ soil_investigation_purpose (ForeignKey) - 
    + valid_from (DateTimeField) - 
    + valid_to (DateTimeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.SoilSite *-- external.SoilSiteVersion
external.SoilSite *-- external.SoilInvestigationPurpose


class "external.Observation <Imported GIS data sources>" as external.Observation #d6edf4 {
    observation
    ..
    Observation(id, soil_site, observed_property, result, phenomenon_time,
result_time, valid_time, source)
    --
    - id (AutoField) - 
    ~ soil_site (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - Must be an JSON object with a "value" property, e.g.
{"value": 123}
    + phenomenon_time (DateTimeField) - 
    + result_time (DateTimeField) - 
    + valid_time (DateTimeRangeField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.Observation *-- external.SoilSite
external.Observation *-- external.ObservableProperty


class "external.ObservableProperty <Imported GIS data sources>" as external.ObservableProperty #d6edf4 {
    observable property
    ..
    Observable property
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    ~ base_phenomenon (ForeignKey) - 
    ~ uom (ForeignKey) - 
    + validation_rules (JSONField) - 
    --
}
external.ObservableProperty *-- external.PhenomenonType
external.ObservableProperty *-- external.UnitOfMeasure


class "external.PhenomenonType <Imported GIS data sources>" as external.PhenomenonType #d6edf4 {
    phenomenon type
    ..
    Phenomenon type
    Origin: INSPIRE Annex III - Soil
    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.PhenomenonType *-- external.PhenomenonType


class "external.SoilInvestigationPurpose <Imported GIS data sources>" as external.SoilInvestigationPurpose #d6edf4 {
    soil investigation purpose
    ..
    Soil investigation purpose
    Origin: INSPIRE Annex III - Soil
    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.SoilInvestigationPurpose *-- external.SoilInvestigationPurpose


class "external.AgriPlotVersion <Imported GIS data sources>" as external.AgriPlotVersion #d6edf4 {
    agricultural plot version
    ..
    AgriPlotVersion(id, is_active, status, import_started_at, import_completed_at,
import_log)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.AgriPlot <Imported GIS data sources>" as external.AgriPlot #d6edf4 {
    public agricultural plot
    ..
    Agricultural plots
    These are not the plots of the farms of each farmer, but the general flat
    layer of plots that the farmer can pick from to add to his farm/campaign
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (MultiPolygonField) - 
    + plant_species_id (CharField) - 
    + plant_variety_id (CharField) - 
    + area (FloatField) - In square meters (m2)
    + valid_from (DateField) - 
    + valid_to (DateField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.AgriPlot *-- external.AgriPlotVersion


@enduml
```

## Farm

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "farm.Holding <Farm>" as farm.Holding #dcd6f4 {
    farm
    ..
    A holding
    The whole area and all infrastructures included on it, covering the same or
    different "sites", under the control of an operator to perform
    agricultural or aquaculture activities. The holding includes one
    specialisation of ActivityComplex, ie. Activity. the values of
    ActivityType are expressed in conformity with the classification of the
    economic activity of the holding, according to the NACE rev. 2.0 coding
    Holding is a thematic extension of the generic Class “Activity Complex”
    shared with other thematic areas describing entities related with
    Economical Activities (Legal Entity Class – Business).
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    + id (CharField) - This identifier must be unique in FaST
    + name (CharField) - 
    + description (TextField) - 
    --
}


class "farm.HoldingCampaign <Farm>" as farm.HoldingCampaign #dcd6f4 {
    farm campaign
    ..
    HoldingCampaign(id, holding, campaign)
    --
    - id (AutoField) - 
    ~ holding (ForeignKey) - 
    ~ campaign (ForeignKey) - 
    --
}
farm.HoldingCampaign *-- farm.Holding
farm.HoldingCampaign *-- farm.Campaign


class "farm.UserRelatedParty <Farm>" as farm.UserRelatedParty #dcd6f4 {
    farm member
    ..
    A party (person) linked to an activity complex within a certain role
    --
    - id (AutoField) - 
    ~ holding (ForeignKey) - 
    + role (CharField) - 
    ~ user (ForeignKey) - 
    --
}
farm.UserRelatedParty *-- farm.Holding


class "farm.Campaign <Farm>" as farm.Campaign #dcd6f4 {
    campaign
    ..
    A growing season
    The campaign label/name should be correspond to the harvest year (eg for the
2018-2019 season,
    the season name should be 2019). It also corresponds to the year the CAP
subsidies are applied for.
    --
    + i18n (JSONField) - 
    + id (IntegerField) - 
    + name (CharField) - The display name of the campaign
    + is_current (BooleanField) - Whether this campaign is the current one (only
one campaign can be the current one at a time)
    + start_at (DateTimeField) - 
    + end_at (DateTimeField) - 
    --
}


class "farm.Site <Farm>" as farm.Site #dcd6f4 {
    site
    ..
    Site
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ holding_campaign (ForeignKey) - 
    + name (CharField) - 
    --
}
farm.Site *-- farm.HoldingCampaign


class "farm.Plot <Farm>" as farm.Plot #dcd6f4 {
    plot
    ..
    Plot(id, authority_id, site, name, geometry, valid_from, valid_to)
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ site (ForeignKey) - 
    + name (CharField) - 
    + geometry (MultiPolygonField) - 
    + valid_from (DateField) - 
    + valid_to (DateField) - 
    --
}
farm.Plot *-- farm.Site


class "farm.PlotPlantVariety <Farm>" as farm.PlotPlantVariety #dcd6f4 {
    plot plant variety
    ..
    A plant variety cultivated on a plot
    Note:
    - as a Plot object is local to a Site, which is local to a HoldingCampaign,
    a PlotPlantVariety is also local to a HoldingCampaign
    - we allow multiple plant varieties on the same plot during the
    same campaign (ie a many-to-many relationship)
    --
    - id (AutoField) - 
    ~ plot (ForeignKey) - 
    ~ plant_variety (ForeignKey) - 
    + percentage (FloatField) - Percentage of the plot dedicated to this plant
variety
    ~ irrigation_method (ForeignKey) - 
    + crop_yield (FloatField) - 
    --
}
farm.PlotPlantVariety *-- farm.Plot


class "farm.Constraint <Farm>" as farm.Constraint #dcd6f4 {
    constraint
    ..
    The Model for any constraint: NVZ, Natura 2000, hydrology, etc.
    --
    - id (AutoField) - 
    + name (CharField) - 
    + description (JSONField) - 
    ~ plot (ForeignKey) - 
    --
}
farm.Constraint *-- farm.Plot


@enduml
```

## Fieldbook

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "fieldbook.IrrigationMethod <Fieldbook & Agriculture>" as fieldbook.IrrigationMethod #e7f4d6 {
    irrigation method
    ..
    A status in the conversion to organic
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.IrrigationMethod *-- fieldbook.IrrigationMethod


class "fieldbook.DeterminationMethod <Fieldbook & Agriculture>" as fieldbook.DeterminationMethod #e7f4d6 {
    determination method
    ..
    A method for determining the composition of a product
    Origin: Estonian eFieldbook 2019 K-4
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.DeterminationMethod *-- fieldbook.DeterminationMethod


class "fieldbook.ChemicalElement <Fieldbook & Agriculture>" as fieldbook.ChemicalElement #e7f4d6 {
    chemical element
    ..
    An agronomically relevant chemical element
    Estonian eFieldbook 2019 K-6
    For example N, P, K, Ca, etc
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.ChemicalElement *-- fieldbook.ChemicalElement


class "fieldbook.FertilizerType <Fieldbook & Agriculture>" as fieldbook.FertilizerType #e7f4d6 {
    fertilizer type
    ..
    FertilizerType(i18n, id, label, definition, description, parent)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.FertilizerType *-- fieldbook.FertilizerType


class "fieldbook.RegisteredFertilizer <Fieldbook & Agriculture>" as fieldbook.RegisteredFertilizer #e7f4d6 {
    registered fertilizer
    ..
    A fertilization product from a national database
    Derived from Estonian eFieldbook 2019 G-8.1 and K-14
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + name (CharField) - 
    + additional_data (JSONField) - 
    + organic (BooleanField) - 
    ~ fertilizer_type (ForeignKey) - 
    + recommended_quantity (FloatField) - 
    ~ recommended_quantity_unit_of_measure (ForeignKey) - 
    + reference (CharField) - 
    --
}
fieldbook.RegisteredFertilizer *-- fieldbook.FertilizerType


class "fieldbook.RegisteredFertilizerElement <Fieldbook & Agriculture>" as fieldbook.RegisteredFertilizerElement #e7f4d6 {
    fertilizer element
    ..
    RegisteredFertilizerElement(id, chemical_element, percentage, efficiency,
determination_method, fertilizer)
    --
    - id (AutoField) - 
    ~ chemical_element (ForeignKey) - 
    + percentage (FloatField) - Percentage of mass, between 0 and 100.
    + efficiency (FloatField) - Percentage of efficiency, between 0 and 100.
Defaults to 100.
    ~ determination_method (ForeignKey) - 
    ~ fertilizer (ForeignKey) - 
    --
}
fieldbook.RegisteredFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.RegisteredFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.RegisteredFertilizerElement *-- fieldbook.RegisteredFertilizer


class "fieldbook.CustomFertilizer <Fieldbook & Agriculture>" as fieldbook.CustomFertilizer #e7f4d6 {
    custom fertilizer
    ..
    A fertilization product that is custom-defined by the farmer for his farm
    Origin: derived from class G-8.1 from the Estonian eFieldbook, to take into
account the
    fact that not all fertilizers might be defined in the national database and
the farmer
    should be allowed to customize his own products for his farm
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + name (CharField) - 
    + additional_data (JSONField) - 
    + organic (BooleanField) - 
    ~ fertilizer_type (ForeignKey) - 
    + recommended_quantity (FloatField) - 
    ~ recommended_quantity_unit_of_measure (ForeignKey) - 
    ~ holding (ForeignKey) - 
    ~ created_by (ForeignKey) - 
    + created_at (DateTimeField) - 
    --
}
fieldbook.CustomFertilizer *-- fieldbook.FertilizerType


class "fieldbook.CustomFertilizerElement <Fieldbook & Agriculture>" as fieldbook.CustomFertilizerElement #e7f4d6 {
    fertilizer element
    ..
    CustomFertilizerElement(id, chemical_element, percentage, efficiency,
determination_method, fertilizer)
    --
    - id (AutoField) - 
    ~ chemical_element (ForeignKey) - 
    + percentage (FloatField) - Percentage of mass, between 0 and 100.
    + efficiency (FloatField) - Percentage of efficiency, between 0 and 100.
Defaults to 100.
    ~ determination_method (ForeignKey) - 
    ~ fertilizer (ForeignKey) - 
    --
}
fieldbook.CustomFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.CustomFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.CustomFertilizerElement *-- fieldbook.CustomFertilizer


class "fieldbook.FertilizationPlan <Fieldbook & Agriculture>" as fieldbook.FertilizationPlan #e7f4d6 {
    fertilization plan
    ..
    FertilizationPlan(id, plot, name, algorithm, parameters, results, updated_at,
updated_by)
    --
    - id (AutoField) - 
    ~ plot (ForeignKey) - 
    + name (CharField) - 
    ~ algorithm (ForeignKey) - 
    + parameters (JSONField) - 
    + results (JSONField) - 
    + updated_at (DateTimeField) - 
    ~ updated_by (ForeignKey) - 
    --
}


class "fieldbook.PlantSpecies <Fieldbook & Agriculture>" as fieldbook.PlantSpecies #e7f4d6 {
    plant species
    ..
    A plant species
    The content of the list is derived from the European plant variety
    database.
    Origin: semantics from code list K-12 of the Estonian eFieldbook
    See also:
    - https://www.unece.org/fileadmin/DAM/cefact/brs/BRS_eCROP_v1.pdf
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_ca
talogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + genus (CharField) - Standard latin name of the plant species
    ~ group (ForeignKey) - 
    + notes (TextField) - 
    --
}
fieldbook.PlantSpecies *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantSpeciesGroup <Fieldbook & Agriculture>" as fieldbook.PlantSpeciesGroup #e7f4d6 {
    plant species group
    ..
    A group of plant species
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    ~ parent (ForeignKey) - The parent group of this group, in case of a
hierarchical structure
    --
}
fieldbook.PlantSpeciesGroup *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantVariety <Fieldbook & Agriculture>" as fieldbook.PlantVariety #e7f4d6 {
    plant variety
    ..
    A plant variety from a given species
    The content of the list is derived from the European plant variety
    database.
    Origin: code list K-13 from the Estonian eFieldbook
    See also:
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_ca
talogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    ~ plant_species (ForeignKey) - 
    + name (CharField) - The name of the variety, can be left empty to designate an
undetermined variety within a species
    + notes (TextField) - 
    --
}
fieldbook.PlantVariety *-- fieldbook.PlantSpecies


@enduml
```

## Messaging

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "messaging.Queue <Messaging>" as messaging.Queue #d6f4eb {
    Queue
    ..
    A collection of tickets
    --
    - id (AutoField) - 
    + title (CharField) - 
    # groups (ManyToManyField) - 
    --
}


class "messaging.Ticket <Messaging>" as messaging.Ticket #d6f4eb {
    ticket
    ..
    A ticket is a two-way communication between the Paying Agency (users with
"staff" status) and
    a **holding** (not a specific user, but all the users that are related
parties to this holding)
    --
    - id (AutoField) - 
    ~ queue (ForeignKey) - Queues you are a member of
    + created_at (DateTimeField) - 
    ~ created_by (ForeignKey) - 
    ~ holding (ForeignKey) - Only members of this holding will be able to view and
reply.
    ~ assigned_to (ForeignKey) - 
    + title (CharField) - 
    + body (TextField) - The text content of the ticket. Will appear at the top of
the conversation in the app.
    + status (CharField) - 
    + resolution (TextField) - (Optional) The resolution provided to the ticket.
Will appear at the top of the conversation in the app.
    + priority (IntegerField) - 1 = Highest Priority, 5 = Low Priority
    --
}
messaging.Ticket *-- messaging.Queue


class "messaging.TicketMessage <Messaging>" as messaging.TicketMessage #d6f4eb {
    ticket message
    ..
    A message of a conversation/ticket
    --
    - id (AutoField) - 
    ~ ticket (ForeignKey) - 
    + created_at (DateTimeField) - 
    ~ created_by (ForeignKey) - 
    + body (TextField) - The body is overridden with '<<< FaST GeoTaggedPhoto
id=XXX >>>' when a geo-tagged photo is attached to the message
    + file (FileField) - 
    + file_name (CharField) - 
    + file_type (CharField) - 
    + file_size (IntegerField) - 
    ~ geo_tagged_photo (ForeignKey) - (Optional) The photo attached to the message
(mutually exclusive with the body)
    --
}
messaging.TicketMessage *-- messaging.Ticket


class "messaging.Broadcast <Messaging>" as messaging.Broadcast #d6f4eb {
    broadcast
    ..
    A one-way notification sent by a a Paying Agency staff user to all holdings
    --
    - id (AutoField) - 
    + title (CharField) - Limited to 48 characters.
    + body (TextField) - Limited to 178 characters to fit in a push notification.
    + icon (ImageField) - Try to have an image as square as possible. If not
provided, FaST default icon will be used.
    + status (CharField) - 
    + is_sending_triggered (BooleanField) - 
    + sent_at (DateTimeField) - 
    ~ sent_by (ForeignKey) - 
    --
}


class "messaging.BroadcastUserReadStatus <Messaging>" as messaging.BroadcastUserReadStatus #d6f4eb {
    Broadcast user read status
    ..
    A joined model to set read status between a User and a Broadcast
    --
    - id (AutoField) - 
    ~ broadcast (ForeignKey) - 
    ~ user (ForeignKey) - 
    + read_at (DateTimeField) - The date/time (UTC) at which the broadcast was
read.
    --
}
messaging.BroadcastUserReadStatus *-- messaging.Broadcast


class "messaging.TicketUserReadStatus <Messaging>" as messaging.TicketUserReadStatus #d6f4eb {
    Ticket user read status
    ..
    A joined model to set read status between a User and a Ticket
    --
    - id (AutoField) - 
    ~ ticket (ForeignKey) - 
    ~ user (ForeignKey) - 
    + read_at (DateTimeField) - The date/time (UTC) at which the ticket was read.
    ~ last_read_ticket_message (ForeignKey) - 
    --
}
messaging.TicketUserReadStatus *-- messaging.Ticket
messaging.TicketUserReadStatus *-- messaging.TicketMessage


@enduml
```

## Photos

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "photos.GeoTaggedPhoto <Photos>" as photos.GeoTaggedPhoto #eef4d6 {
    geo-tagged photo
    ..
    Photo with geolocation information
    As per 6.2.5 of
    https://publications.jrc.ec.europa.eu/repository/bitstream/JRC108816/cts-201
7pubsy-online.pdf
    Geo-tagged photos
    =================
    Geotagging is the process of adding and embedding geographical information,
    and possibly additional temporal and/or textual information into the
metadata
    file of a photograph. Usually the location and time of acquisition will be
obtained
    through the GNSS antenna of the photo camera or smartphone.
    Member State Administrations should strongly consider the utility of this
lowcost,
    readily available, and easy to use technology. They could integrate, in
their
    IACS, geo-tagged photos provided by farmers as evidence of compliance to
    specific conditions or rules. For instance, a farmer could send a geo-tagged
    photo of his Durum wheat field(s) being part of a Voluntary Couple Support.
    Also, a Geo-tagged photo could be sent of a grassland just mowed to show
    compliance with a mowing date imposed by the legislation. The provision of
such
    geo-tagged information will avoid the field visit that otherwise would be
    necessary.
    The minimum information to be embedded or stored, for each image taken,
    should consist of:
    - Its geographical location (longitude, latitude);
    - Its date and time of shooting;
    - The direction the camera was pointing. This could be obtain, among
    other possibilities, directly from a compass system embedded in the device,
or
    by pointing the camera toward a feature/ a landscape element that could be
    easily depicted afterwards on imagery maps (see example below);
    Example of photo containing elements allowing determining the direction the
    camera was pointing when shooting the photo.
    - It is also recommended to register the elevation and the Dilution of
    precision (DOP). This last parameter gives a qualitative indication on the
    positional precision.
    - At last, a procedure should exist to identify the farmer providing the
    photo (e.g. image uploaded in the IACS after an authentication through
personal
    login and password).
    Administrations should develop a standard operating procedure to ensure the
    information integrity and security of received geo-tagged photos; in other
    words, ensure that the whole information content has, in no manner, been
    falsified.
    Some administration have already developed such dedicated smartphone/tablet
    Applets to be used by farmers (or by inspectors).
    Geo-tagged images should be stored in their original file formats. Critical
    image information may be lost and artefacts introduced as a result of a
lossy
    compression process. Files should also be as ‘read-only’ to avoid
modifications
    or deletions.
    --
    - id (AutoField) - 
    + shot_at (DateTimeField) - 
    + description (TextField) - 
    + uploaded_at (DateTimeField) - 
    ~ uploaded_by (ForeignKey) - 
    ~ holding_campaign (ForeignKey) - 
    ~ plot (ForeignKey) - 
    + location (PointField) - 
    + heading (FloatField) - 
    + elevation (FloatField) - 
    + horizontal_dilution_of_precision (FloatField) - 
    + vertical_dilution_of_precision (FloatField) - 
    + photo (ImageField) - 
    + photo_width (IntegerField) - pixels
    + photo_height (IntegerField) - pixels
    + thumbnail (ImageField) - 
    + thumbnail_width (IntegerField) - 
    + thumbnail_height (IntegerField) - 
    --
}


@enduml
```

## Soil

```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "soil.SoilDerivedObject <Soil>" as soil.SoilDerivedObject #f4d6dc {
    estimated soil properties
    ..
    A synthetic collection of soil observations computed from other soil sites
    and attached to a specific plot
    --
    - id (AutoField) - 
    + is_derived_from (JSONField) - 
    + soil_sample_origin (CharField) - 
    + soil_sites_count (IntegerField) - Number of soil samples used to estimate the
soil properties
    ~ plot (ForeignKey) - 
    --
}


class "soil.DerivedObservation <Soil>" as soil.DerivedObservation #f4d6dc {
    estimated soil properties observation
    ..
    DerivedObservation(id, soil_derived_object, observed_property, result)
    --
    - id (AutoField) - 
    ~ soil_derived_object (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - 
    --
}
soil.DerivedObservation *-- soil.SoilDerivedObject
soil.DerivedObservation *-- soil.ObservableProperty


class "soil.SoilSite <Soil>" as soil.SoilSite #f4d6dc {
    soil sample
    ..
    A private soil site of the holding
    (public soil sites are stored in the external app/database)
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ holding (ForeignKey) - 
    + geometry (GeometryField) - 
    ~ soil_investigation_purpose (ForeignKey) - 
    + valid_from (DateTimeField) - 
    + valid_to (DateTimeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - 
    + label (CharField) - 
    --
}
soil.SoilSite *-- soil.SoilInvestigationPurpose


class "soil.Observation <Soil>" as soil.Observation #f4d6dc {
    soil sample observation
    ..
    A soil observation of a private soil site
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    ~ soil_site (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - 
    + phenomenon_time (DateTimeField) - 
    + result_time (DateTimeField) - 
    + valid_time (DateTimeRangeField) - 
    + source (JSONField) - 
    --
}
soil.Observation *-- soil.SoilSite
soil.Observation *-- soil.ObservableProperty


class "soil.ObservableProperty <Soil>" as soil.ObservableProperty #f4d6dc {
    observable property
    ..
    A property that can be observed in private soil sites and in derived soil
objects
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    ~ base_phenomenon (ForeignKey) - 
    ~ uom (ForeignKey) - 
    + validation_rules (JSONField) - 
    --
}
soil.ObservableProperty *-- soil.PhenomenonType


class "soil.PhenomenonType <Soil>" as soil.PhenomenonType #f4d6dc {
    soil phenomenon type
    ..
    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
soil.PhenomenonType *-- soil.PhenomenonType


class "soil.SoilInvestigationPurpose <Soil>" as soil.SoilInvestigationPurpose #f4d6dc {
    soil investigation purpose
    ..
    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
soil.SoilInvestigationPurpose *-- soil.SoilInvestigationPurpose


@enduml
```

