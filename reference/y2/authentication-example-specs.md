# Authentication of users

This document presents a proposed mechanism for an authentication delegation similar to the one developed for/by Castilla y Leon (Spain) during the first phase of the FaST project.

## Sequence

The delegated authentication sequence should be implemented as follow:
1. in the FaST app, the user will click on the "Login with my Paying Agency" button
2. the app opens a browser window, and loads the following url: [https://PA_LOGIN_URL?key=SECRET_KEY&state=STATE&next=CALLBACK_URL]()
    - `PA_LOGIN_URL` is the URL of the login page (served by the Paying Agency) where users will enter their credentials
    - `SECRET_KEY` is a secret key provided by Paying Agency to FaST and used to authenticate the FaST backend
    - `STATE` is a session-specific random string that needs to be returned when FaST is called back after login
    - `CALLBACK_URL` is the FaST URL that needs to be called back once the user is properly authenticated
3. Once the user clicks on `Ok` and is properly authenticated by the Paying Agency, he/she should be redirected to the FaST callback URL [https://CALLBACK_URL?state=STATE&token=TOKEN]() with a `HTTP 200` status code
    - `STATE` is the same random string as in step 2
    - `TOKEN` is a signed JWT containing the user identity
4. If the user clicks on `Cancel`, then he/she should be redirected to the FaST callback URL [https://CALLBACK_URL?state=STATE&status=cancelled]() (no token), with a parameter that clearly states that the action was cancelled by the user (either in the URL or in the payload)
5. If an unrecoverable error happens during authentication of the user, then he/she should be redirected to the FaST callback URL [https://CALLBACK_URL?state=STATE&status=error]() (no token), with a parameter that clearly states that an error happened (either in the URL or in the payload)
6. FaST will validate the `STATE` parameter (from the FaST session) and the `TOKEN` received:
    - the `TOKEN` signature will be checked against the public key corresponding to the private key used to sign it. This public key should be provided by the Paying Agency, ideally in the form of a JWKS endpoint where the FaST backend can retrieve it.
7. After all the validations have passed, the user will be logged into FaST.

Remarks:
- `SECRET_KEY`: the authentication of the FaST backend through a secret key passed in the URL can be replaced by other methods such as Basic-Auth.
- `CALLBACK_URL`: the callback URL can alternatively be provided to (and stored by) the Paying Agency in advance. However, this implies that the FaST multiple environments (development, staging, production) must be taken into account and - for example -  attached to different secret keys.
- the Paying Agency authentication service MUST be idempotent: it MUST return always the same user identifier for the the same physical person

```plantuml
top to bottom direction

(*) -down-> "
FaST identity provider selection
----

{{
salt
{+
    scale 1.4
    [ Login with FaST ]
    [ Login with my Paying Agency ]
}
}}
" as idp

idp -down->[Open browser and\nload PA login form] "
Paying Agency login page
----
__https://<pa-login-url>?key=<secret-key>&state=<state>&next=<fastplatform-callback-url>__

{{
salt
{+
    scale 1.4
    <&person> Login    | "john.doe "
    <&key> Password | "****     "
    ----------
    [Cancel] | [  OK   ]
}
}}
" as login

login -right->[On fail, retry] login

login -down->[Ok] "
FaST callback
----
__https://<fast-callback-url>?state=<state>&token=<token>__
" as callback

login -down->[Cancel] "
FaST callback
----
__https://<fast-callback-url>?state=<state>&status=cancelled__
" as cancel

cancel --> "Redirect to the FaST\nidentity provider selection" as redirect_idp

callback -down-> "FaST backend checks token\nsignature and state" as checks

checks -right-->[Wrong signature or state] redirect_idp

checks -down->[Log in to FaST] (*)

```

## Token payload

The token payload must contain, at a minimum, the following information:
- user's **first and last name**, ideally in 2 separate fields
- user's **identifier**: this identifier **MUST be the same as the one that will be used to query the IACS/GSAA by FaST**. It must obviously be unique and unambiguously identify the user.

Example:
```json
{
    "id": "1234567890ABCDEF",
    "first_name": "John",
    "last_name": "Doe"
}
```

## Token signature

As described above, we favor an asymmetric signature mechanism, where the public key is publicly available on the internet (i.e. served by the Paying Agency). The main benefit being that the signature keys can be transparently rotated by the Paying Agency when necessary, with no intervention necessary on the FaST side.

Alternative methods include symmetric signing with shared secret (the FaST backend knows the signing secret) or symmetric signing with validation endpoint (FaST backend does not know the signing secret, but each token is validated by FaST through an additional Paying Agency endpoint).
