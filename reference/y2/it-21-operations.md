# it-21 - Operations

This document complements the [documentation](/reference/services.md) of the architecture of the modules and services of the FaST platform with a focused approach on the operational side. The purpose is to provide an exhaustive description of all the services deployed to date (v1.1.1), including details on internal and external communications.

All the information in this document refers to the setup currently deployed on the ```beta``` environment with the dedicated ```it-21.beta.fastplatform.eu``` sub-domain.

## API Gateway ontology

This section details the [API Gateways](https://gitlab.com/fastplatform/docs/-/blob/master/reference/services.md#api-gateways) part of the documentation mentioned above for the ```it-21``` region.

### api-gateway-fastplatform

```plantuml
caption api-gateway-fastplatform ontology

component api_gateway [
    api-gateway-fastplatform
    ---
    <i>Hasura GraphQL
]

database postgres [
    fastplatform
    ----
    <i>PostgreSQL 12.7
    <i>PostGIS 2.5
]

cloud external_api_iacs [
    IACS API
    https://secure.sistemapiemonte.it
]

cloud external_api_weather [
    Weather API
    www.arpa.piemonte.it
]

cloud external_api_apple_push_notification [
    Apple Push Notification API
]

cloud external_api_google_push_notification [
    Google Push Notification API
]

rectangle "Services" as services {
    component service_fertilization_visione_api_gateway [
        Service fertilization-visione-api-gateway
        ----
        <i>Knative
    ]
    component service_iacs_arpea [
        Service iacs-arpea
        ----
        <i>Knative
    ]
    component service_meteorology_weather [
        Service meteorology-weather
        ----
        <i>Knative
    ]
    component service_mobile_push_notification [
        Service mobile-push-notification
        ----
        <i>Knative
    ]
}

rectangle "Event handlers" as event_handlers {
    component handler_event_farm [
        Handler event-farm
        ----
        <i>Knative
    ]
    component handler_event_messaging [
        Handler event-messaging
        ----
        <i>Knative
    ]
    component handler_mapproxy_api [
        Handler mapproxy-api
        ----
        <i>Knative
    ]
    component handler_web_backend [
        Handler web-backend
        ----
        <i>Knative
    ]
}

api_gateway --> postgres

api_gateway --> services
api_gateway --> event_handlers

service_iacs_arpea --> external_api_iacs
service_meteorology_weather --> external_api_weather
service_mobile_push_notification --> external_api_apple_push_notification
service_mobile_push_notification --> external_api_google_push_notification
```

### api-gateway-external

```plantuml
caption api-gateway-external ontology

component api_gateway [
    api-gateway-external
    ---
    <i>Hasura GraphQL
]

database postgres [
    external
    ----
    <i>PostgreSQL 12.7
    <i>PostGIS 2.5
]

rectangle "Services" as services {
    component service_tile_map_catlogue [
        Service tile-map-catalogue
        ----
        <i>Knative
    ]
}

api_gateway --> postgres

api_gateway --> services
```

## Deployed services

Below, you will find the exhaustive list of all deployed services. For each service, the deployment configuration and the associated source code (built as a Docker image via Bazel) are specified. Communications to external services accessible on the public internet are also indicated.

- **Name**: api-gateway-fastplatform<br/>
  **Public**: Yes <br/>
  **Public endpoint**: api-gateway-fastplatform.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: api-gateway-fastplatform.it-21.svc.cluster.local<br/>
  **Source**: [core/services/api_gateway/fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform)<br/>
  **Deployment conf**: [core/manifests/api_gateway/fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/api_gateway/fastplatform) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L133-147)<br/>

- **Name**: api-gateway-fastplatform-out<br/>
  **Public**: Yes <br/>
  **Public endpoint**: api-gateway-fastplatform-out.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: api-gateway-fastplatform-out.it-21.svc.cluster.local<br/>
  **Source**: [core/services/api_gateway/fastplatform/out](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/api_gateway/fastplatform/out)<br/>
  **Deployment conf**: [core/manifests/api_gateway/fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/api_gateway/fastplatform) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L149-162)<br/>
  **Note**: Dedicated instance for API accesses

- **Name**: event-farm<br/>
  **Public**: No <br/>
  **Private endpoint**: event-farm.it-21.svc.cluster.local<br/>
  **Source**: [core/services/event/farm](https://gitlab.com/fastplatform/core/-/tree/master/services/event/farm)<br/>
  **Deployment conf**: [core/manifests/event/farm](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/event/farm) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L164-181)<br/>

- **Name**: event-messaging<br/>
  **Public**: No <br/>
  **Private endpoint**: event-messaging.it-21.svc.cluster.local<br/>
  **Source**: [core/services/event/messaging](https://gitlab.com/fastplatform/core/-/tree/master/services/event/messaging)<br/>
  **Deployment conf**: [core/manifests/event/messaging](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/event/messaging) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L183-207)<br/>

- **Name**: fertilization-visione<br/>
  **Public**: Yes <br/>
  **Public endpoint**: fertilization-visione.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: fertilization-visione.it-21.svc.cluster.local<br/>
  **Source**: [addons/visione/services/fertilization/visione](https://gitlab.com/fastplatform/addons/visione/-/tree/master/services/fertilization/visione)<br/>
  **Deployment conf**: [addons/visione/manifests/fertilization/visione](https://gitlab.com/fastplatform/addons/visione/-/tree/master/manifests/resources/services/fertilization/visione) -> [visione.conf](https://gitlab.com/fastplatform/addons/visione/-/blob/db9a66d76ad8f5fe72c31928f0e86602da1db78b/manifests/visione.conf#L11-72)<br/>

- **Name**: fertilization-visione-api-gateway<br/>
  **Public**: No <br/>
  **Private endpoint**: fertilization-visione-api-gatewa.it-21.svc.cluster.local<br/>
  **Source**: [addons/visione/services/fertilization/visione_api_gateway](https://gitlab.com/fastplatform/addons/visione/-/tree/master/services/fertilization/visione_api_gateway)<br/>
  **Deployment conf**: [addons/visione/manifests/fertilization/visione_api_gateway](https://gitlab.com/fastplatform/addons/visione/-/tree/master/manifests/resources/services/fertilization/visione_api_gateway) -> [visione.conf](https://gitlab.com/fastplatform/addons/visione/-/blob/db9a66d76ad8f5fe72c31928f0e86602da1db78b/manifests/visione.conf#L74-87)<br/>

- **Name**: iacs-arpea<br/>
  **Public**: No <br/>
  **Private endpoint**: iacs-arpea.it-21.svc.cluster.local<br/>
  **Source**: [core/services/iacs/arpea](https://gitlab.com/fastplatform/core/-/tree/master/services/iacs/arpea)<br/>
  **Deployment conf**: [core/manifests/iacs/arpea](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/iacs/arpea) -> [it-21.conf](https://gitlab.com/fastplatform/custom/it-21/-/blob/v1.1.1/manifests/it-21.conf#L7-27)<br/>
  **Outgoing external communications**<br>- https://secure.sistemapiemonte.it

- **Name**: mapproxy-api<br/>
  **Public**: No <br/>
  **Private endpoint**: mapproxy-api.it-21.svc.cluster.local<br/>
  **Source**: [core/services/mapproxy/api](https://gitlab.com/fastplatform/core/-/tree/master/services/mapproxy/api)<br/>
  **Deployment conf**: [core/manifests/mapproxy/api](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/mapproxy/api) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L209-233)<br/>

- **Name**: mapproxy-server<br/>
  **Public**: Yes <br/>
  **Public endpoint**: map.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: mapproxy-server.it-21.svc.cluster.local<br/>
  **Source**: [core/services/mapproxy/server](https://gitlab.com/fastplatform/core/-/tree/master/services/mapproxy/server)<br/>
  **Deployment conf**: [core/manifests/mapproxy/server](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/mapproxy/server) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L235-246)<br/>
  **Outgoing external communications**<br/>- oss.eu-west-0.prod-cloud-ocb.orange-business.com (S3)<br/>- openstreetmap.org<br/>- wms.cartografia.agenziaentrate.gov.it<br/>- sentinel.territorio.csi.it

- **Name**: meteorology-weather<br/>
  **Public**: No <br/>
  **Private endpoint**: meteorology-weather.it-21.svc.cluster.local<br/>
  **Source**: [addons/bolletino-meteorologico/services/meteorology/weather](https://gitlab.com/fastplatform/addons/bolletino-meteorologico/-/tree/master/services/meteorology/weather)<br/>
  **Deployment conf**: [addons/bolletino-meteorologico/manifests/meteorology/weather](https://gitlab.com/fastplatform/addons/bolletino-meteorologico/-/tree/master/manifests/resources/services/meteorology/weather) -> [bolletino-meteorologico.conf](https://gitlab.com/fastplatform/addons/bolletino-meteorologico/-/blob/fe9eb0240232a551128924397f746928a2495453/manifests/bolletino-meteorologico.conf#L10-31)<br/>
  **Outgoing external communications**<br/>- www.arpa.piemonte.it

- **Name**: mobile-push-notification<br/>
  **Public**: No <br/>
  **Private endpoint**: mobile-push-notification.it-21.svc.cluster.local<br/>
  **Source**: [core/services/mobile/push-notification](https://gitlab.com/fastplatform/core/-/tree/master/services/mobile/push-notification)<br/>
  **Deployment conf**: [core/manifests/mobile/push-notification](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/mapproxy/server) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L248-265)<br/>
  **Outgoing external communications**<br>- TODO

- **Name**: tile-map-api<br/>
  **Public**: No <br/>
  **Private endpoint**: tile-map-api.it-21.svc.cluster.local<br/>
  **Source**: [addons/sobloo/services/tile_map/api](https://gitlab.com/fastplatform/addons/sobloo/-/tree/v0.2.0/services/tile_map/api)<br/>
  **Deployment conf**: [addons/sobloo/manifests/tile_map/api](https://gitlab.com/fastplatform/addons/sobloo/-/tree/v0.2.0/manifests/resources/services/tile_map/api) -> [sobloo.conf](https://gitlab.com/fastplatform/addons/sobloo/-/blob/master/manifests/sobloo.conf#L10-17)<br/>

- **Name**: tile-map-catalogue<br/>
  **Public**: No <br/>
  **Private endpoint**: tile-map-catalogue.it-21.svc.cluster.local<br/>
  **Source**: [addons/sobloo/services/tile_map/catalogue](https://gitlab.com/fastplatform/addons/sobloo/-/tree/v0.2.0/services/tile_map/catalogue)<br/>
  **Deployment conf**: [addons/sobloo/manifests/tile_map/catalogue](https://gitlab.com/fastplatform/addons/sobloo/-/tree/v0.2.0/manifests/resources/services/tile_map/catalogue) -> [sobloo.conf](https://gitlab.com/fastplatform/addons/sobloo/-/blob/master/manifests/sobloo.conf#L19-55)<br/>

- **Name**: web-authentication<br/>
  **Public**: No <br/>
  **Private endpoint**: web-authentication.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/backend](https://gitlab.com/fastplatform/core/-/tree/master/services/web/backend)<br/>
  **Deployment conf**: [core/manifests/web/authentication](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/authentication) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L267-290)<br/>

- **Name**: web-backend<br/>
  **Public**: Yes <br/>
  **Public endpoint**: portal.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: web-backend.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/backend](https://gitlab.com/fastplatform/core/-/tree/master/services/web/backend)<br/>
  **Deployment conf**: [core/manifests/web/backend](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/backend) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L292-382)<br/>

- **Name**: web-media<br/>
  **Public**: Yes <br/>
  **Public endpoint**: media.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: web-media.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/media](https://gitlab.com/fastplatform/core/-/tree/master/services/web/media)<br/>
  **Deployment conf**: [core/manifests/web/media](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/media) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L384-393)<br/>
  **Outgoing external communications**<br>- oss.eu-west-0.prod-cloud-ocb.orange-business.com (S3)

- **Name**: web-pdf-generator<br/>
  **Public**: No <br/>
  **Private endpoint**: web-pdf-generator.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/pdf_generator](https://gitlab.com/fastplatform/core/-/tree/master/services/web/pdf_generator)<br/>
  **Deployment conf**: [core/manifests/web/pdf_generator](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/pdf_generator) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L395-416)<br/>

- **Name**: web-vector-tiles-external<br/>
  **Public**: No <br/>
  **Private endpoint**: web-vector-tiles-external.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/services/web/vector_tiles)<br/>
  **Deployment conf**: [core/manifests/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/vector_tiles) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L419-435)<br/>

- **Name**: web-vector-tiles-external-cache<br/>
  **Public**: Yes <br/>
  **Public endpoint**: web-vector-tiles-external.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: web-vector-tiles-external-cache.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/services/web/vector_tiles)<br/>
  **Deployment conf**: [core/manifests/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/vector_tiles) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L437-444)<br/>

- **Name**: web-vector-tiles-fastplatform<br/>
  **Public**: Yes <br/>
  **Public endpoint**: web-vector-tiles-fastplatform.it-21.beta.fastplatform.eu<br/>
  **Private endpoint**: web-vector-tiles-fastplatform.it-21.svc.cluster.local<br/>
  **Source**: [core/services/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/services/web/vector_tiles)<br/>
  **Deployment conf**: [core/manifests/web/vector_tiles](https://gitlab.com/fastplatform/core/-/tree/master/manifests/resources/services/web/vector_tiles) -> [core.conf](https://gitlab.com/fastplatform/core/-/blob/v1.1.1/manifests/core.conf#L446-462)<br/>

Note that it is the Knative controller that manages all network configurations to ensure access to internal and external services if necessary.

## Deployed databases

All deployed database instances are listed below:

### Postgres

- **Name**: external<br/>
  **Version**: 12.7<br/>
  **Modules**:<br/>- PostGis (2.5)<br>
  **Private endpoint**: external.it-21.svc.cluster.local

- **Name**: fastplatform<br/>
  **Version**: 12.7<br/>
  **Modules**:<br/>- PostGis (2.5)<br/>
  **Private endpoint**: fastplatform.it-21.svc.cluster.local

- **Name**: fertilization-visione-postgres<br/>
  **Version**: 12.7<br/>
  **Private endpoint**: fertilization-visione-postgres.it-21.svc.cluster.local

- **Name**: tile-map-catalogue-postgres<br/>
  **Version**: 12.7<br/>
  **Modules**:<br/>- PostGis (2.5)<br/>
  **Private endpoint**: tile-map-catalogue-postgres.it-21.svc.cluster.local

### Redis

- **Name**: mobile-push-notification-redis<br/>
  **Version**: 5.0.8<br/>
  **Private endpoint**: mobile-push-notification-redis-headless.it-21.svc.cluster.local

### Memcached

- **Name**: web-authentication-memcached<br/>
  **Version**: 1.6.7<br/>
  **Private endpoint**: web-authentication-memcached.it-21.svc.cluster.local

- **Name**: web-backend-memcached<br/>
  **Version**: 1.6.7<br/>
  **Private endpoint**: web-backend-memcached.it-21.svc.cluster.local
