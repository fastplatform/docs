# IACS/GSAA APIs

This document present Examples of implementation of the IACS/GSAA APIs that FaST connects to, to retrieve the user's farm data and display it in the FaST interfaces.

Conceptually, FaST expects 2 very simple APIs to be built and made available:
- a first one to get the "access rights" of a user, i.e. "what farms a user is allowed to see in IACS"
- second one to get the "farm data", i.e. "what are the parcels and crops of this specific farm"

The returned data should be the most valid and up to date. Usually, the data corresponds to the latest Geospatial Aid Application (GSAA) of the farmer, from the previous month of May.

## "Access rights" API

**Input:** requesting access rights for user with ID `123456789`

```json
POST
{
    "user_id": "123456789"
}
```
> *This `user_id` is the same as the one returned by the Authentication System.*

**Output:** this user is the titular of holding/farm `ABCDEF`, but is also advising on holdings/farms `GHIJKL` and `MNOPQR`.

```json
HTTP 200
{
    "holdings": [
        {
            "id": "ABCDEF",
            "name": "John's farm",
            "role": "farmer"
        },
        {
            "id": "GHIJKL",
            "name": "Paul's farm",
            "role": "advisor",
        },
        {
            "id": "MNOPQR",
            "name": "Ringo's farm",
            "role": "advisor",
        },
        ...
    ]
}
```

where:
- `"id"` is a unique identifier of the holding/farm in the IACS system.
- `"name"` (optional) is a readable name of the holding/farm. If not provided, the `"id"` field will be displayed to the user.
- `"role"` (optional) is a descriptor that precises in what capacity the user is allowed to see the data of this holding/farm. This is only for information, is stored by FaST but currently is not used. The current roles that FaST supports are *farmer*, *advisor*, *farmer_association*, *public_administration*

Note: if each user has a single holding in the IACS system (ie if the system does not make use of multiple access rights and the user <-> holding relationship is one-to-one) then the list can/should contain a single element.

**Error cases:**

You should make use of standard HTTP codes for error cases, such as:
- `HTTP 404` if the user does not exist (or only `HTTP 400` if you are reluctant to expose this detail)
- `HTTP 400` if the request is malformed
- etc

## "Farm data" API

Input: requesting the description of holding/farm `"ABCDEF"` for user `"123456789"`
```json
{
    "user_id": "123456789",
    "holding_id": "ABCDEF"
}
```

Output:
```json
HTTP 200
{
    "plots": [
        {
            "id": "28649264904",
            "geometry": "POLYGON (...)",
            "plant_species_id": 1234,
            "name": "My farm parcel"
        },
        {
            "id": "108373047923",
            "geometry": "MULTIPOLYGON (...)",
            "plant_species_id": 1234,
            "name": "My farm parcel"
        },
        ...
    ]
}
```

where:
- `"id"` is a unique identifier of the plot/parcel. Ideally, this should be the same identifier as the one the GSAA.
- `"geometry"` is the geometry (coordinates) of the parcel. In the example above, the geometry is assumeed to be received as WKT, but GeoJSON is ok as well. The SRID needs to be communicated either in the returned payload or through a spec.
- `"plant_species_id"` is the *IACS code* of the crop declared by the farmer in his GSAA for this plot/parcel.
- `"name"` (optional) is a user friendly name that the farmer may have entered for his/her parcel in the IACS/GSAA

**Error cases:**

If the requested `user_id` is not allowed to see the detail of the requested `holding_id`, the API should return an error, such as `HTTP 403` for example.

Other errors should follow standard HTTP codes as much as possible.

## Other formats

The examples above are only simple examples of what can be proposed by the Paying Agency. Other protocols/formats have been dealt with during the first phase of the project, such as SOAP/XML, WFS, etc. In case of sepcific need, do not hesitate to contact the development team and we will guide you.

## Volumes

The expected volumes of queries are quite low, in most cases. The most common use can is the following:
- the farmer makes his/her CAP submission in the IACS/GSAA system (not in FaST) usually in May every year. This submission is made _once a year_, hence the data is not expected top change during each "CAP year" (May to May).
- he/she will then, before the next growing season starts, use the FaST application to retrieve the farm parcels and compute the fertilization recommendation. This should happen mostly during the few months after the CAP submission.
- once this is done, the farm data is **copied** in FaST and therefore no further call is necessary until a new version of the farm data is available in the IACS/GSAA database.

What that means is that the order of magnitude of the expected volume of calls is 1 to 20 calls per farmer per year (max), mostly concentrated during the few months after the CAP submission.

Note: as explained above, the call to both APIs are initiated by a *manual user action*. Hence, the calls are expected to be randomly spread in time within the working hours of the users.
The data is *not* loaded by some automated process that would, for example, send hundreds of requests at midnight.

## SLA

FaST itself does not impose any SLA, as the system is expected to be handed over to each Paying Agecy upon completion ; hence, each Paying Agency should assess the restrictiveness of its own SLA depending on its own budget / high availabilty requirements.

## Security arrangements

The necessary security arrangements (protection) of these APIs are left to the Paying Agency to determine, depending on their own requirements. A custom connector will be built on the FaST side, that can adapt top most security requirements in terms of authentication and encrytpion.

For example, the following security arrangements have been proposed by the Paying Agency of the first phase of the project.

- IP whitelisting/filtering
- HTTP Basic Auth
- Shared secret (API key)
- OAuth tokens
- Custom network (XRoad/Estonia) through a secure access point server

## Privacy

FaST will *not* download the user's data from the APIs before having received the *explicit* consent from the user. This means that:
- the user is asked for his/her consent (in the mobile app) every time he/she requests data from the IACS/GSAA system through FaST
- FaST does not load or store data for users that have not yet given their explicit consent in the app
- FaST stores all the consents that the farmers have given, with a timestamp
