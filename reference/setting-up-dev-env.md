# Setting up your development environment

> This documentation is geared towards MacOS users as the development team was using this OS. However, it should work on any OS (except for the iOS application which requires a MacOS environment).

## Prepare

### Folder structure

Several git repositories make up the full FaST platform development environment.

Depending on your own development requirements, you may not have access to all the existing repositories listed here, nor would you necessarily need to clone all repositories available to you.

Example: Only developers working for Slovakia have any interest in cloning the `kalkulacky-hnojenie` repository.

We **strongly** recommend to clone the repositories using the following folder structure, as many scripts use relative paths to access the files from other repositories.

```bash
.
├── addons
│   ├── agromet: `git clone https://gitlab.com/fastplatform-be-wal/agromet.git`
│   ├── icpa-pmn: `git clone https://gitlab.com/fastplatform-ro/icpa-pmn.git`
│   ├── kalkulacky-hnojenie: `git clone https://gitlab.com/fastplatform-sk/kalkulacky-hnojenie.git`
│   ├── metno: `git clone https://gitlab.com/fastplatform-[REGION]/metno.git`
│   ├── navigator-f3: `git clone https://gitlab.com/fastplatform-[REGION]/navigator-f3.git`
│   ├── nimh: `git clone https://gitlab.com/fastplatform-bg/nimh.git`
│   ├── requaferti: `git clone https://gitlab.com/fastplatform-be-wal/requaferti.git`
│   ├── sobloo: `git clone https://gitlab.com/fastplatform-[REGION]/sobloo.git`
│   └── visione: `git clone https://gitlab.com/fastplatform-it-21/visione.git`
├── core: `git clone https://gitlab.com/fastplatform-[REGION]/core.git`
├── custom
│   ├── be-wal: `git clone https://gitlab.com/fastplatform-be-wal/be-wal.git`
│   ├── bg: `git clone https://gitlab.com/fastplatform-bg/bg.git`
│   ├── gr: `git clone https://gitlab.com/fastplatform-gr/gr.git`
│   ├── it-21: `git clone https://gitlab.com/fastplatform-it-21/it-21.git`
│   ├── ro: `git clone https://gitlab.com/fastplatform-ro/ro.git`
│   └── sk: `git clone https://gitlab.com/fastplatform-sk/sk.git`
├── docs: `git clone https://gitlab.com/fastplatform-[REGION]/docs.git`
├── farmer-mobile-app: `git clone https://gitlab.com/fastplatform-[REGION]/farmer-mobile-app.git`
```

### IDE: VSCode

We recommend using `VSCode` as a development IDE:
https://code.visualstudio.com/

### Packages

The following MacOS packages are recommended to run the development environment (can be installed with `brew install` in MacOS):

- bazel
- black
- buildifier
- coreutils
- gdal
- git
- git-crypt
- hasura-cli
- llvm
- node
- nvm
- openconnect
- plantuml
- postgresql
- proj@7
- pyenv
- python@3.7
- python@3.8
- translate-toolkit

### Docker

Docker is used to run the development environment.

We recommend using Docker Desktop, as it provides a GUI interface:

https://www.docker.com/products/docker-desktop/

### Misc

Finally, the following programs come in handy when developing on the platform:
- `Altair` (GraphQL Client): https://altair.sirmuel.design/
- `Lens` (Kubernetes IDE to monitor pods): https://docs.k8slens.dev/main/
- `pgAdmin` (Postgres administration platform): https://www.pgadmin.org/
- `Postman` (Try out various API endpoints): https://www.postman.com/downloads/
- `QGIS` (Free and Open Source Geographic Information System): https://qgis.org/


### Secrets

Many services require to access environment variables that are quite sensitive. For that reason, they are either crypted (using `git-crypt`), or simply never committed to the repository with their actual values filled in.

For example, `secrets.env` for the backend looks like this:

```
# Authentication
AUTHENTICATION_API_GATEWAY_SERVICE_KEY=

# Cache
DJANGO_MEMCACHED_PASSWORD=

# Databases
POSTGRES_PASSWORD=
POSTGRES_EXTERNAL_PASSWORD=

# GraphQL
API_GATEWAY_SERVICE_KEY=

# Media
S3_ACCESS_KEY=
S3_SECRET_KEY=

# Security
DJANGO_SECRET_KEY=
DOMAIN_NAME=fastplatform.eu
SESSION_COOKIE_DOMAIN=fastplatform.eu
DJANGO_ALLOWED_HOSTS=localhost
SITE_URL=https://localhost.fastplatform.eu:48000
CSRF_COOKIE_SECURE=TRUE
SESSION_COOKIE_SECURE=TRUE

SESSION_COOKIE_AGE=86400

# Static
STATIC_AWS_ACCESS_KEY_ID=
STATIC_AWS_SECRET_ACCESS_KEY=

# Email
SMTP_LOGIN=do-not-reply@fastplatform.eu
SMTP_PASSWORD=

# For dev only, docs translations
DEEPL_API_KEY=
```

In order for the backend to work locally, you need to fill in with the appropriate values. It is nevertheless essential not to commit any of the modified `secrets.env` files to any repository, for obvious security reasons:

```bash
git update-index --skip-worktree **/secrets.env
```

Finally, for the `custom` environments, we use `git-crypt` to encrypt/decrypt the `secrets.env` files. More info here:
- `be-wal`: https://gitlab.com/fastplatform-be-wal/be-wal#encryption-of-sensitive-information
- `bg`: https://gitlab.com/fastplatform-bg/bg#encryption-of-sensitive-information
- `it-21`: https://gitlab.com/fastplatform-it-21/it-21#encryption-of-sensitive-information
- `gr`: https://gitlab.com/fastplatform-gr/gr#encryption-of-sensitive-information
- `ro`: https://gitlab.com/fastplatform-ro/ro#encryption-of-sensitive-information
- `sk`: https://gitlab.com/fastplatform-sk/sk#encryption-of-sensitive-information


#### Certificates

Most services require HTTPS to run.
To facilitate the local development, we created certificates for a `localhost.fastplatform.eu` domain with a DNS redirection to `192.168.1.33`.
In this way we can use the same certificates for all developpers on the backend and the frontend apps.

To make an alias the `localhost.fastplatform.eu` to the `localhost` IP address, we use the following command:
```bash
sudo ifconfig lo0 alias 192.168.1.33
```

When you access your service at `https://localhost.fastplatform.eu:[PORT]/[SERVICE_URI]` you will be redirected to the `198.168.1.33` which is aliased to the `localhost` IP address, the default host for services in development.

In all projects requiring HTTPS, you will need to create a `certificates` folder at the root of the project (set to `.gitignore` by default) containing a `localhost.fastplatform.eu` folder with the following files:
- `privkey1.pem`: the private key for the `localhost.fastplatform.eu` domain
- `cert1.pem`: the certificate for the `localhost.fastplatform.eu` domain
- `fullchain1.pem`: the CA chain for the `localhost.fastplatform.eu` domain


## Running

### Python projects

All python projects can be setup by creating a Python `virtualenv`, activating it then installing the dependencies:

```bash
# Make sure pip is installed and up-to-date:
pip install --upgrade pip

# Make sure you have `virtualenv` installed:
pip install virtualenv

# Create your virtual environment inside a folder called ".venv"
virtualenv .venv

# Activate it, so that all installations will be done in the virtual environment:
source .venv/bin/activate

# Install the service dependencies (mandatory):
pip install -r requirements.txt

# Install the development dependencies (optional, but recommended):
pip install -r requirements-dev.txt
```

More info in setting up your environment is available in the `README.md` file of each service. For more, read [here](#readmemd-files).

### Makefile

Each service comes with a Makefile, that contains various commands that come in handy when running and developing the service, like:
- `make help`: shows the list of available commands
- `make start`, `make stop`, etc…: stop and start the service
- `make test`: run the tests
- `make scan-secrets` and `make audit-secrets`: scan and audit your code for any sensitive string that should not be committed.
- `make start-tracing` and `make stop-tracing`: start and stop the tracing service, when available.
- etc…

Most (if not all) bash commands needed to run or develop the service are already wrapped inside a command available in the `Makefile`. Taking a first good look at what is already available might save you time.


### Typical launch for the backend

Let's cover the typical launch for the backend:

- Make sure `Docker` is up and running.

- Execute

```bash
# Set in variable the uri to your main fastplatform folder (top folder with all cloned repos)
fastplatform_main_folder= …

# ifconfig
sudo ifconfig lo0 alias 192.168.1.33 # Or any other local IP address for localhost

# Launch Hasura
echo "Starting Hasura…"
cd ${fastplatform_main_folder}/core/services/api_gateway/fastplatform
make stop
make starts
echo "Hasura started"

# Launch Django backend
echo "Starting backend postgres…"
cd ${fastplatform_main_folder}/core/services/web/backend
source .venv/bin/activate
make restart-postgres
echo "Backend postgres started"

# Open Hasura Admin Console and Django Admin in chrome before `make starts`:
open -a "Google Chrome" "https://localhost.fastplatform.eu:48087/console"
open -a "Google Chrome" "https://localhost.fastplatform.eu:48000/en/admin/"

# Following command suspends terminal, so launch last:
echo "Launching backend…"
make starts
```

Keep in mind those services need a little time to be up and running (up to several minutes, depending on your machine).

Hasura is now accessible at https://localhost.fastplatform.eu:48087/console
Django backend is now accessible at https://localhost.fastplatform.eu:48000/admin/

#### Troubleshooting

The 4 following Docker containers should be up and running in order for you to access the Django backend and Hasura Console inside your browser:
- `hasura-fastplatform`
- `nginx-hasura-fastplatform`
- `postgres-fastplatform`
- `postgres-external`

If Hasura / Django don't seem to start, restart any of those 4 that is not started already.

## Service Documentation

### README.md files

All services comes with a documentation that covers a wide range of topics, including:
- Architecture
- Authentication
- Administration Portal
- Development setup
  - Prerequisites
  - Environment variables
  - Setup
  - Troubleshooting
- Data Model
- Tests
- etc…

Again, it is strongly advised to take a good look at this documentation before starting to develop on the service.

List of most important README.md files across the platform:

- addons
  - [agromet](https://gitlab.com/fastplatform-be-wal/agromet/-/blob/master/README.md)
  - [icpa-pmn](https://gitlab.com/fastplatform-ro/icpa-pmn/-/blob/master/README.md)
  - [kalkulacky-hnojenie](https://gitlab.com/fastplatform-sk/kalkulacky-hnojenie/-/blob/master/README.md)
  - metno: https://gitlab.com/fastplatform-[REGION]/metno/-/blob/master/README.md
  - navigator-f3: https://gitlab.com/fastplatform-[REGION]/navigator-f3/-/blob/master/README.md
  - [nimh](https://gitlab.com/fastplatform-bg/nimh/-/blob/master/README.md)
  - [requaferti](https://gitlab.com/fastplatform-be-wal/requaferti/-/blob/master/README.md)
  - sobloo: https://gitlab.com/fastplatform-[REGION]/sobloo/-/blob/master/README.md
  - [visione](https://gitlab.com/fastplatform-it-21/visione/-/blob/master/README.md)
- core: https://gitlab.com/fastplatform-[REGION]/core/-/blob/master/README.md
- custom
  - [be-wal](https://gitlab.com/fastplatform-be-wal/be-wal/-/blob/master/README.md)
  - [bg](https://gitlab.com/fastplatform-bg/bg/-/blob/master/README.md)
  - [gr](https://gitlab.com/fastplatform-gr/gr/-/blob/master/README.md)
  - [it-21](https://gitlab.com/fastplatform-it-21/it-21/-/blob/master/README.md)
  - [ro](https://gitlab.com/fastplatform-ro/ro/-/blob/master/README.md)
  - [sk](https://gitlab.com/fastplatform-sk/sk/-/blob/master/README.md)
- farmer-mobile-app: https://gitlab.com/fastplatform-[REGION]/farmer-mobile-app/-/blob/master/README.md

### User Documentation

Also keep in mind that the backend comes with an extensive user documentation, available at:

https://portal.beta.[REGION].fastplatform.eu/en/docs/

This documentation is automatically generated from the original source files written in English, and is available in 10 other languages:

- Bulgarian
- French
- German
- Greek
- Italian
- Spanish
- Estonian
- Spanish
- Romanian
- Slovak

 We are using [DeepL](https://www.deepl.com/translator) for the automatic translation.

 The English source markdown (.md) files can be found here: https://gitlab.com/fastplatform-[REGION]/core/-/tree/master/services/web/backend/docs/src.

## Advanced topics

### Development involving interaction between services

Sometimes while developing a service, you need to have some other service running in the background, and expose its endpoint through the unified GraphQL endpoint of the main Hasura instance.

Example: `regulatory/nitrate` requires an instance of `core/event/farm` to be running and accessible as a remote schema inside Hasura.

To do that:
- Launch first `core/event/farm` in the background (in bash, cd to the `services/core/event/farm` directory, and run `make start`)
- Modify your local [remote_schemas.yaml](metadata/remote_schemas.yaml) file to add the remote schema of `core/event/farm`:
    ```yaml
    - name: core-event-farm
    definition:
        url_from_env: REMOTE_SCHEMA_URL_CORE_EVENT_FARM
        timeout_seconds: 60
    ```
- Apply the schema: 
    ```bash
    make apply
    ```
- Reload the metadata from the Hasura server by accessing the "Hasura Metadata Actions" page in the Hasura UI: check "Reload all remote schemas", then click the "Reload" button.

> Finally, keep in mind that you might also need to add some specific environment variables to the `metadata.env` file, because environment variables is how we define the URLs of the various remote schemas. 

Example: Let's say we are running a local instance of the `regulatory/nitrate` service for region `be-wal` on port **48002**.

The URL endpoint of this service is declared in an environment variable called `REMOTE_SCHEMA_URL_BE_WAL_REGULATORY_NITRATE`. When running the service locally, the value of this variable should be set for example to `https://localhost.fastplatform.eu:48002/graphql` (for https).

To do achieve this, add the following line to the `metadata.env` file:

```env
REMOTE_SCHEMA_URL_BE_WAL_REGULATORY_NITRATE=https://localhost.fastplatform.eu:48002/graphql
```

Now:

```bash
# Apply the changes to your local instance of Hasura:
make apply

# Restart the service
make stop
make starts
```

### Optional Services

1. Start the media server
> If you want to use media features (file upload and file serving) you need to start the media server located in the project *web/media*.
> If you don't start the web media, you won't be able to upload or view files through the Admin portal.

2. Start monitoring the distributed requests
```bash
make start-tracing
```
> __Jaeger UI will be available at [http://localhost:16686/]()__

### https and authentication
The authentication endpoint (used by the Hasura API Gateway) is exposed at http://localhost.fastplatform.eu:8000/authentication/hasura/.

> If you wish to run the server locally behind https, then you need a domain name and the corresponding certificates.
> You can check the target `starts` in the `Makefile` to see how you can specify the certificate and private key of your local development server. Note that you will also need to add your domain to the `ALLOWED_HOSTS` setting of Django, in the [fastplatform/settings/base.py]() file.