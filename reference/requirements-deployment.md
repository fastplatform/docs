# Deploy the FaST platform infrastructure to another Cloud provider

This document is a list of resources needed to deploy the FaST platform infrastructure on a Cloud provider other than Flexible Engine.

### VPC

A VPC (Virtual Private Cloud) is a logically isolated virtual network and must be provisioned.

###  Public IPs

At least 2 public IPs must be provisioned. One for the FaST services and one for the ```Jumbox``` virtual machine.

### Subnets

The VPC must be divided into one or more subnets according to the desired topology.

### Nat Gateway

A Nat Gateway must be provisoned to enable the virtual machines in the VPC to access the public Internet behind a single IP. 

### Load Balancer

A Load Balancer must be provisioned to distribute traffic, from the public Internet, among multiple endpoints in the VPC.

### S3 Buckets

- one bucket to store backups: ```fastplatform-<iso-code>-backup```
- one bucket to store private data: ```fastplatform-<iso-code>-private```
- one bucket to store public data: ```fastplatform-<iso-code>-public```

The ```fastplatform-<iso-code>-backup``` and ```fastplatform-<iso-code>-private``` buckets must be encrypted, and dedicated service accounts must be created to access them.

### Kubernetes Cluster

A Kubernetes cluster in version ```1.19.10``` must be provisioned with at least 4 nodes with the following specifications:
- CPU : 8 CPUs
- RAM : 32 Go

### Jumpbox

The ```Jumpbox``` virtual machine must be provisoned with the following specifications:
- CPU : 2 CPUs
- RAM : 2 Go
