# Orchestration

Maximum automation was sought in the design of the FaST platform so that it is autonomous, highly available and scalable. This autonomy drastically reduces the need for manual operations and therefore the size of the dedicated Ops team.

## Kubernetes

The services of the FaST platform are deployed in [Docker](https://www.docker.com) containers and orchestrated on a [Kubernetes](https://kubernetes.io) cluster. Kubernetes is an orchestration tool that automates the deployment, the management, the scaling, and the networking of containers.

## Istio Service Mesh

The microservice approach decomposes an application into its essential functions. Each function is called a service, and can be built and deployed independently. The [Istio](https://istio.io/) Service Mesh is used to control, secure and monitor the traffic between microservices.

Istio helps reduce the complexity of these deployments, and automatically secure the services through managed authentication, authorization, and encryption of communication between services. It applies policies and ensure that they are enforced, and that resources are fairly distributed among consumers with rich automatic tracing, monitoring, and logging of all the services. This helps to observe what's happening inside the cluster.

The manifests used to deploy Istio are available [here](https://gitlab.com/fastplatform/ops/kustomize-bases/-/tree/master/istio/1.9.8).

## Knative Serving

[Knative](https://knative.dev/) is as a serverless framework and is used to build, deploy, and manage the serverless workloads. It means that every microservice is deployed and managed by Knative. It brings the following capabilities:
- Higher level abstraction, easy to reason about the object model
- Seamless autoscaling based on HTTP requests
- Gradual rollouts for new revisions
- Integration of networking and service mesh automatically

The manifests used to deploy Knative are available [here](https://gitlab.com/fastplatform/ops/kustomize-bases/-/tree/master/knative/0.25.1).

## Operators

The [Operator](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/) pattern in the Kubernetes ecosystem is the way to automate resource management on Kubernetes as much as possible. It allows the creation of new Kubernetes native resources that describe high-level configurations that can be interpreted and operated by a service (the operator), which is itself deployed on the cluster.

In order to achieve maximum automation, all components of the technical stack deployed on Kubernetes are managed by operators. The manifests used to deploy the operators are available [here](https://gitlab.com/fastplatform/ops/kustomize-bases/-/tree/master/operators)
