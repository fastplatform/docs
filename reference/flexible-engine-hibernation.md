# Hibernation of the Flexible Engine resources

Some services deployed on Flexible Engine can be put into hibernation. In our case, we will focus on hibernating virtual machines and Kubernetes clusters.

## ECS / Jumpbox

The jumpbox is a virtual machine. It can be found and selected in the ```Elastic Cloud Server``` section of the Flexible Engine console.

<img src="images/obs-fe/fe-console/hibernation-awakening/jumpbox-running.png" width="600"/>

To **hibernate** the virtual machine, click on the ```stop``` button in the upper right corner. The status will change from ```Running``` to ```Stopped```.

To **awake** the virtual machine click on the ```start``` button. The status will change from ```Stopped``` to ```Running```.

## CCE / Kubernetes 

Only one CCE  Kubernetes cluster is deployed per Member State. It can be found in the ```Cloud Container Engine``` section of the Flexible Engine console in ```Resource Management``` > ```Clusters```.

<img src="images/obs-fe/fe-console/hibernation-awakening/select-cce.png" width="600"/>

To **hibernate** the CCE Kubernetes cluster, click on ```More``` > ```Hibernate```. Don't forget to select the ```Stop all the nodes in the cluster``` option. The status will change from ```Available``` to ```Hibernation```.

To **awake** the CCE Kubernetes cluster, click on ```More``` > ```Wake```. The status will change from ```Hibernation``` to ```Available```.
