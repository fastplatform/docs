# Backend Data model

- [Backend Data model](#backend-data-model)
  - [Data model generation](#data-model-generation)
  - [Farm](#farm)
  - [External](#external)
  - [Addons](#addons)

## Data model generation

The backend data model is generated using [django-model2puml](https://pypi.org/project/django-model2puml/).

Don't forget to set your `LOCALE` to the desired language for documentation.

```bash
# cd to your backend location
cd …/core/web/backend
make build-datamodel
```

This tool only generates `.puml` files, not directly visible in gitlab. For easier preview, we wrap this command in a custom Django Command and generate an `.md` file instead.

In this document, we present a summarized version of the data model, centered around a few central Django Apps.

For a complete description of the data model, see the [Full Data Model](/reference/full-data-model.md) document.

Also, each django app has its own `data_model.puml` document, located at the root folder for the app.

## Farm

```plantuml

@startuml
class "add_ons.Provider" as add_ons.Provider #e8f4d6 {
    provider
    --
    --
}


class "add_ons.ProviderMember" as add_ons.ProviderMember #e8f4d6 {
    provider member
    --
    --
}
add_ons.ProviderMember *-- add_ons.Provider


class "add_ons.AddOn" as add_ons.AddOn #e8f4d6 {
    add-on
    --
    --
}
add_ons.AddOn *-- add_ons.Provider
add_ons.AddOn *--* farm.Holding


class "add_ons.APIAccess" as add_ons.APIAccess #e8f4d6 {
    API access
    --
    --
}
add_ons.APIAccess *-- add_ons.Provider
add_ons.APIAccess *--* farm.Holding


class "add_ons.AddOnAPIKey" as add_ons.AddOnAPIKey #e8f4d6 {
    add-on API key
    --
    --
}
add_ons.AddOnAPIKey *-- add_ons.AddOn


class "add_ons.AddOnSubscription" as add_ons.AddOnSubscription #e8f4d6 {
    add-on subscription
    --
    --
}
add_ons.AddOnSubscription *-- add_ons.AddOn
add_ons.AddOnSubscription *-- farm.Holding



class "farm.Holding" as farm.Holding #dcd6f4 {
    farm
    --
    --
}


class "farm.HoldingCampaign" as farm.HoldingCampaign #dcd6f4 {
    farm campaign
    --
    --
}
farm.HoldingCampaign *-- farm.Holding
farm.HoldingCampaign *-- farm.Campaign


class "farm.UserRelatedParty" as farm.UserRelatedParty #dcd6f4 {
    farm member
    --
    --
}
farm.UserRelatedParty *-- farm.Holding


class "farm.Campaign" as farm.Campaign #dcd6f4 {
    campaign
    --
    --
}


class "farm.Site" as farm.Site #dcd6f4 {
    site
    --
    --
}
farm.Site *-- farm.HoldingCampaign


class "farm.Plot" as farm.Plot #dcd6f4 {
    plot
    --
    --
}
farm.Plot *-- farm.Site


class "farm.PlotPlantVariety" as farm.PlotPlantVariety #dcd6f4 {
    plot plant variety
    --
    --
}
farm.PlotPlantVariety *-- farm.Plot
farm.PlotPlantVariety *-- fieldbook.PlantVariety
farm.PlotPlantVariety *-- fieldbook.IrrigationMethod


class "farm.Constraint" as farm.Constraint #dcd6f4 {
    constraint
    --
    --
}
farm.Constraint *-- farm.Plot


class "fieldbook.IrrigationMethod" as fieldbook.IrrigationMethod #e7f4d6 {
    irrigation method
    --
    --
}
fieldbook.IrrigationMethod *-- fieldbook.IrrigationMethod


class "fieldbook.DeterminationMethod" as fieldbook.DeterminationMethod #e7f4d6 {
    determination method
    --
    --
}
fieldbook.DeterminationMethod *-- fieldbook.DeterminationMethod


class "fieldbook.ChemicalElement" as fieldbook.ChemicalElement #e7f4d6 {
    chemical element
    --
    --
}
fieldbook.ChemicalElement *-- fieldbook.ChemicalElement


class "fieldbook.FertilizerType" as fieldbook.FertilizerType #e7f4d6 {
    fertilizer type
    --
    --
}
fieldbook.FertilizerType *-- fieldbook.FertilizerType


class "fieldbook.RegisteredFertilizer" as fieldbook.RegisteredFertilizer #e7f4d6 {
    registered fertilizer
    --
    --
}
fieldbook.RegisteredFertilizer *-- fieldbook.FertilizerType



class "fieldbook.RegisteredFertilizerElement" as fieldbook.RegisteredFertilizerElement #e7f4d6 {
    fertilizer element
    --
    --
}
fieldbook.RegisteredFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.RegisteredFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.RegisteredFertilizerElement *-- fieldbook.RegisteredFertilizer


class "fieldbook.CustomFertilizer" as fieldbook.CustomFertilizer #e7f4d6 {
    custom fertilizer
    --
    --
}
fieldbook.CustomFertilizer *-- fieldbook.FertilizerType

fieldbook.CustomFertilizer *-- farm.Holding


class "fieldbook.CustomFertilizerElement" as fieldbook.CustomFertilizerElement #e7f4d6 {
    fertilizer element
    --
    --
}
fieldbook.CustomFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.CustomFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.CustomFertilizerElement *-- fieldbook.CustomFertilizer


class "fieldbook.FertilizationPlan" as fieldbook.FertilizationPlan #e7f4d6 {
    fertilization plan
    --
    --
}
fieldbook.FertilizationPlan *-- farm.Plot
fieldbook.FertilizationPlan *-- add_ons.AddOn


class "fieldbook.PlantSpecies" as fieldbook.PlantSpecies #e7f4d6 {
    plant species
    --
    --
}
fieldbook.PlantSpecies *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantSpeciesGroup" as fieldbook.PlantSpeciesGroup #e7f4d6 {
    plant species group
    --
    --
}
fieldbook.PlantSpeciesGroup *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantVariety" as fieldbook.PlantVariety #e7f4d6 {
    plant variety
    --
    --
}
fieldbook.PlantVariety *-- fieldbook.PlantSpecies


class "messaging.Queue" as messaging.Queue #d6f4eb {
    Queue
    --
    --
}


class "messaging.Ticket" as messaging.Ticket #d6f4eb {
    ticket
    --
    --
}
messaging.Ticket *-- messaging.Queue
messaging.Ticket *-- farm.Holding


class "messaging.TicketMessage" as messaging.TicketMessage #d6f4eb {
    ticket message
    --
    --
}
messaging.TicketMessage *-- messaging.Ticket


class "messaging.Broadcast" as messaging.Broadcast #d6f4eb {
    broadcast
    --
    --
}


class "messaging.BroadcastUserReadStatus" as messaging.BroadcastUserReadStatus #d6f4eb {
    Broadcast user read status
    --
    --
}
messaging.BroadcastUserReadStatus *-- messaging.Broadcast


class "messaging.TicketUserReadStatus" as messaging.TicketUserReadStatus #d6f4eb {
    Ticket user read status
    --
    --
}
messaging.TicketUserReadStatus *-- messaging.Ticket
messaging.TicketUserReadStatus *-- messaging.TicketMessage



class "soil.SoilDerivedObject" as soil.SoilDerivedObject #f4d6dc {
    estimated soil properties
    --
    --
}
soil.SoilDerivedObject *-- farm.Plot


class "soil.DerivedObservation" as soil.DerivedObservation #f4d6dc {
    estimated soil properties observation
    --
    --
}
soil.DerivedObservation *-- soil.SoilDerivedObject
soil.DerivedObservation *-- soil.ObservableProperty


class "soil.SoilSite" as soil.SoilSite #f4d6dc {
    soil sample
    --
    --
}
soil.SoilSite *-- farm.Holding
soil.SoilSite *-- soil.SoilInvestigationPurpose


class "soil.Observation" as soil.Observation #f4d6dc {
    soil sample observation
    --
    --
}
soil.Observation *-- soil.SoilSite
soil.Observation *-- soil.ObservableProperty


class "soil.ObservableProperty" as soil.ObservableProperty #f4d6dc {
    observable property
    --
    --
}
soil.ObservableProperty *-- soil.PhenomenonType



class "soil.PhenomenonType" as soil.PhenomenonType #f4d6dc {
    soil phenomenon type
    --
    --
}
soil.PhenomenonType *-- soil.PhenomenonType


class "soil.SoilInvestigationPurpose" as soil.SoilInvestigationPurpose #f4d6dc {
    soil investigation purpose
    --
    --
}
soil.SoilInvestigationPurpose *-- soil.SoilInvestigationPurpose


@enduml
```

## External

```plantuml
@startuml
class "external.UnitOfMeasure" as external.UnitOfMeasure #d6edf4 {
    unit of measure
    --
    --
}


class "external.ManagementRestrictionOrRegulationZoneVersion" as external.ManagementRestrictionOrRegulationZoneVersion #d6edf4 {
    management restriction or regulation zone version
    --
    --
}


class "external.ManagementRestrictionOrRegulationZone" as external.ManagementRestrictionOrRegulationZone #d6edf4 {
    management restriction or regulation zone
    --
    --
}
external.ManagementRestrictionOrRegulationZone *-- external.ManagementRestrictionOrRegulationZoneVersion
external.ManagementRestrictionOrRegulationZone *-- external.SpecialisedZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.ZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.EnvironmentalDomain


class "external.ZoneTypeCode" as external.ZoneTypeCode #d6edf4 {
    zone type code
    --
    --
}
external.ZoneTypeCode *-- external.ZoneTypeCode


class "external.SpecialisedZoneTypeCode" as external.SpecialisedZoneTypeCode #d6edf4 {
    specialized zone type code
    --
    --
}
external.SpecialisedZoneTypeCode *-- external.SpecialisedZoneTypeCode


class "external.EnvironmentalDomain" as external.EnvironmentalDomain #d6edf4 {
    environmental domain
    --
    --
}
external.EnvironmentalDomain *-- external.EnvironmentalDomain


class "external.SurfaceWaterVersion" as external.SurfaceWaterVersion #d6edf4 {
    surface water version
    --
    --
}


class "external.SurfaceWater" as external.SurfaceWater #d6edf4 {
    surface water
    --
    --
}
external.SurfaceWater *-- external.SurfaceWaterVersion


class "external.WaterCourseVersion" as external.WaterCourseVersion #d6edf4 {
    water course version
    --
    --
}


class "external.WaterCourse" as external.WaterCourse #d6edf4 {
    water course
    --
    --
}
external.WaterCourse *-- external.WaterCourseVersion


class "external.ProtectedSiteVersion" as external.ProtectedSiteVersion #d6edf4 {
    protected site version
    --
    --
}


class "external.ProtectedSite" as external.ProtectedSite #d6edf4 {
    protected site
    --
    --
}
external.ProtectedSite *-- external.ProtectedSiteVersion
external.ProtectedSite *--* external.DesignationType


class "external.DesignationType" as external.DesignationType #d6edf4 {
    designation type
    --
    --
}
external.DesignationType *-- external.DesignationScheme
external.DesignationType *-- external.Designation


class "external.DesignationScheme" as external.DesignationScheme #d6edf4 {
    designation scheme
    --
    --
}
external.DesignationScheme *-- external.DesignationScheme


class "external.Designation" as external.Designation #d6edf4 {
    designation
    --
    --
}
external.Designation *-- external.Designation


class "external.SoilSiteVersion" as external.SoilSiteVersion #d6edf4 {
    soil site version
    --
    --
}


class "external.SoilSite" as external.SoilSite #d6edf4 {
    soil site
    --
    --
}
external.SoilSite *-- external.SoilSiteVersion
external.SoilSite *-- external.SoilInvestigationPurpose


class "external.Observation" as external.Observation #d6edf4 {
    observation
    --
    --
}
external.Observation *-- external.SoilSite
external.Observation *-- external.ObservableProperty


class "external.ObservableProperty" as external.ObservableProperty #d6edf4 {
    observable property
    --
    --
}
external.ObservableProperty *-- external.PhenomenonType
external.ObservableProperty *-- external.UnitOfMeasure


class "external.PhenomenonType" as external.PhenomenonType #d6edf4 {
    phenomenon type
    --
    --
}
external.PhenomenonType *-- external.PhenomenonType


class "external.SoilInvestigationPurpose" as external.SoilInvestigationPurpose #d6edf4 {
    soil investigation purpose
    --
    --
}
external.SoilInvestigationPurpose *-- external.SoilInvestigationPurpose


class "external.AgriPlotVersion" as external.AgriPlotVersion #d6edf4 {
    agricultural plot version
    --
    --
}


class "external.AgriPlot" as external.AgriPlot #d6edf4 {
    public agricultural plot
    --
    --
}
external.AgriPlot *-- external.AgriPlotVersion


@enduml

```

## Addons

```plantuml
@startuml
class "auth.Permission" as auth.Permission #e8f4d6 {
    permission
    --
    --
}


class "auth.Group" as auth.Group #e8f4d6 {
    group
    --
    --
}
auth.Group *--* auth.Permission


class "add_ons.Provider" as add_ons.Provider #e8f4d6 {
    provider
    --
    --
}
add_ons.Provider *--* authentication.User


class "add_ons.ProviderMember" as add_ons.ProviderMember #e8f4d6 {
    provider member
    --
    --
}
add_ons.ProviderMember *-- add_ons.Provider
add_ons.ProviderMember *-- authentication.User


class "add_ons.AddOn" as add_ons.AddOn #e8f4d6 {
    add-on
    --
    --
}
add_ons.AddOn *-- add_ons.Provider
add_ons.AddOn *--* auth.Group
add_ons.AddOn *--* auth.Permission
add_ons.AddOn *--* farm.Holding


class "add_ons.APIAccess" as add_ons.APIAccess #e8f4d6 {
    API access
    --
    --
}
add_ons.APIAccess *-- add_ons.Provider
add_ons.APIAccess *--* auth.Group
add_ons.APIAccess *--* auth.Permission
add_ons.APIAccess *--* farm.Holding


class "add_ons.AddOnAPIKey" as add_ons.AddOnAPIKey #e8f4d6 {
    add-on API key
    --
    --
}
add_ons.AddOnAPIKey *-- add_ons.AddOn
add_ons.AddOnAPIKey *-- authentication.User


class "add_ons.AddOnSubscription" as add_ons.AddOnSubscription #e8f4d6 {
    add-on subscription
    --
    --
}
add_ons.AddOnSubscription *-- add_ons.AddOn
add_ons.AddOnSubscription *-- farm.Holding
add_ons.AddOnSubscription *-- authentication.User


class "authentication.FederatedProvider" as authentication.FederatedProvider #ded6f4 {
    federated identity provider
    --
    --
}


class "authentication.FederatedProviderLogEntry" as authentication.FederatedProviderLogEntry #ded6f4 {
    federated provider log entry
    --
    --
}
authentication.FederatedProviderLogEntry *-- authentication.FederatedProvider


class "authentication.Country" as authentication.Country #ded6f4 {
    country
    --
    --
}
authentication.Country *-- authentication.Language
authentication.Country *--* authentication.Language


class "authentication.Region" as authentication.Region #ded6f4 {
    region
    --
    --
}
authentication.Region *-- authentication.Country
authentication.Region *-- authentication.Language
authentication.Region *--* authentication.Language


class "authentication.Language" as authentication.Language #ded6f4 {
    language
    --
    --
}


class "authentication.User" as authentication.User #ded6f4 {
    User
    --
    --
}
authentication.User *-- authentication.Region
authentication.User *-- authentication.Language
authentication.User *--* auth.Permission
authentication.User *--* auth.Group


class "authentication.Group" as authentication.Group #ded6f4 {
    group
    --
    --
}
authentication.Group *--* auth.Permission


class "authentication.TermsAndConditionsConsentRequest" as authentication.TermsAndConditionsConsentRequest #ded6f4 {
    terms & conditions consent request
    --
    --
}


class "authentication.UserTermsAndConditionsConsent" as authentication.UserTermsAndConditionsConsent #ded6f4 {
    user terms & conditions consent
    --
    --
}
authentication.UserTermsAndConditionsConsent *-- authentication.User
authentication.UserTermsAndConditionsConsent *-- authentication.TermsAndConditionsConsentRequest


class "authentication.UserIACSConsent" as authentication.UserIACSConsent #ded6f4 {
    user IACS consent
    --
    --
}
authentication.UserIACSConsent *-- authentication.User


class "authentication.OIDCClient" as authentication.OIDCClient #ded6f4 {
    OIDC Client
    --
    --
}
authentication.OIDCClient *-- authentication.User


class "authentication.OIDCCode" as authentication.OIDCCode #ded6f4 {
    OIDC Authorization code
    --
    --
}
authentication.OIDCCode *-- authentication.User


class "authentication.OIDCToken" as authentication.OIDCToken #ded6f4 {
    OIDC Token
    --
    --
}
authentication.OIDCToken *-- authentication.User


class "authentication.LoginAccessAttempt" as authentication.LoginAccessAttempt #ded6f4 {
    Login attempt
    --
    --
}


class "authentication.LoginAccessLog" as authentication.LoginAccessLog #ded6f4 {
    Login access log
    --
    --
}


class "authentication.UserSession" as authentication.UserSession #ded6f4 {
    User session
    --
    --
}
authentication.UserSession *-- authentication.User


class "farm.Holding" as farm.Holding #dcd6f4 {
    farm
    --
    --
}


class "farm.HoldingCampaign" as farm.HoldingCampaign #dcd6f4 {
    farm campaign
    --
    --
}
farm.HoldingCampaign *-- farm.Holding
farm.HoldingCampaign *-- farm.Campaign


class "farm.UserRelatedParty" as farm.UserRelatedParty #dcd6f4 {
    farm member
    --
    --
}
farm.UserRelatedParty *-- farm.Holding
farm.UserRelatedParty *-- authentication.User


class "farm.Campaign" as farm.Campaign #dcd6f4 {
    campaign
    --
    --
}


class "farm.Site" as farm.Site #dcd6f4 {
    site
    --
    --
}
farm.Site *-- farm.HoldingCampaign


class "farm.Plot" as farm.Plot #dcd6f4 {
    plot
    --
    --
}
farm.Plot *-- farm.Site


class "farm.PlotPlantVariety" as farm.PlotPlantVariety #dcd6f4 {
    plot plant variety
    --
    --
}
farm.PlotPlantVariety *-- farm.Plot


class "farm.Constraint" as farm.Constraint #dcd6f4 {
    constraint
    --
    --
}
farm.Constraint *-- farm.Plot


@enduml

```