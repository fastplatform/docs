# Glossary


|               |                                                                                                                                                                                                                                           |
|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cluster       | a group of similar objects (pods, nodes,...) that are running together to provide a service.                                                                                                                                               |
| ConfigMap     | an API object used to store non-confidential data in key-value pairs. It allows to decouple environment-specific configuration from the container images, so that the applications are easily portable.                                   |
| Container     | a standard unit of software that packages up code and all its dependencies, so the application runs quickly and reliably from one computing environment to another.                                                                         |
| DevOps        | a set of practices that combines software development (Dev) and IT operations (Ops).                                                                                                                                                       |
| Elastic       | match the resources allocated with actual amount of resources needed at any given point in time.                                                                                                                                           |
| GitOps        | using Git everywhere from development to deployment.                                                                                                                                                                                       |
| Ingress       | provide load balancing, SSL termination and name-based virtual hosting.                                                                                                                                                                    |
| Kustomize     | a standalone tool to customize Kubernetes objects through a kustomization file.                                                                                                                                                            |
| Micro-service | an architectural style that structures an application as a collection of services that are Loosely couple, independently deployable, Organized around business capabilities, Highly maintainable and testable.                                |
| Namespace     | a virtual cluster backed by the same physical cluster.                                                                                                                                                                                     |
| Node          | Kubernetes runs the workload by placing containers into Pods to run on Nodes. A node may be a virtual or physical machine, depending on the cluster. Each node contains the services necessary to run Pods , managed by the control plane. |
| Openstack     | a cloud operating system that controls large pools of compute, storage, and networking resources throughout a datacenter, all managed and provisioned through APIs with common authentication mechanisms.                                  |
| Pod           | a Pod is the basic execution unit of a Kubernetes application--the smallest and simplest unit in the Kubernetes object model that is created or deployed. A Pod represents processes running on the cluster.                               |
| Proxy         | a server application or appliance that acts as an intermediary for requests from clients seeking resources from servers that provide those resources.                                                                                      |
| ReplicaSet    | a ReplicaSet's purpose is to maintain a stable set of replica Pods running at any given time. As such, it is often used to guarantee the availability of a specified number of identical Pods.                                             |
| Scalable      | handles the changing needs of an application within the confines of the infrastructure via statically adding or removing resources to meet applications demands if needed.                                                                 |
| Serverless    | a cloud computing execution model in which the cloud provider runs the server, and dynamically manages the allocation of machine resources.                                                                                                |
| Service       | an application composed of micro-services                                                                                                                                                                                                  |
| Service Mesh  | a dedicated infrastructure layer for facilitating service-to-service communications between microservices, often using a sidecar proxy.                                                                                                    |
| Sobloo        | reference to the cloud provider (DIAS)                                                                                                                                                                                                     |
| StatefullSet  | manages the deployment and scaling of a set of Pods , and provides guarantees about the ordering and uniqueness of these Pods.                                                                                                             |

# Acronyms
|       |                                            |
|-------|--------------------------------------------|
| API   | Application Programming Interface          |
| AS    | AutoScaling                                |
| CCE   | Cloud Container Engine                     |
| CI/CD | Continuous Integration/Continuous Delivery |
| ECS   | Elastic Cloud Server                       |
| ELB   | Elastic Load Balancer                      |
| EVS   | Elastic Volume Storage                     |
| HTTP  | HyperText Transfer Protocol                |
| k8s   | Kube, kubernetes                           |
| NACL  | Network Access Control List                |
| OBS   | Bject Storage                              |
| PVC   | ersistence Volume Claims                   |
| PR    | Pull Request                               |
| VPC   | Virtual Private Cloud                      |
| WAF   | Web Application Firewall                   |
