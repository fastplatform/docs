# Standalone deployment of the [FaST Platform](https://fastplatform.eu): infrastructure requirements

## Introduction

In the first stage of the project, the foundation of the [FaST Platform](https://fastplatform.eu) was developed on the basis of the IaaS services provided by the [Sobloo DIAS](https://sobloo.eu/). The [Sobloo DIAS](https://sobloo.eu/) is a consortium which includes Airbus and whose cloud provider member is [OBS (Orange Business Services)](https://www.orange-business.com/en). The [OBS](https://www.orange-business.com/en) cloud services, used by the [FaST Platform](https://fastplatform.eu), are themselves based on [OpenStack](https://www.openstack.org/).

The second stage of the project also includes an option for each participating Paying Agency to deploy the [FaST Platform](https://fastplatform.eu) on a *suitable* infrastructure, possibly external to [Sobloo](https://sobloo.eu/), and owned and maintained by each participating Paying Agency ("Option C", below).

<img src="standalone-deployment/deployment-options.svg" width="400"/>

**This document presents the technical prerequisites for each Paying Agency to setup their infrastructure to be ready to receive the deployment of the [FaST Platform](https://fastplatform.eu).**

> Notes:
> - This document assumes that the reader has a good knowledge of all aspects of the orchestration of workloads on cloud infrastructures and of the Kubernetes orchestration technology in particular.
> - In this document, we have transposed the components used from the [OBS](https://www.orange-business.com/en) cloud provider into a list of resources and services to be made available. It is up to each Paying Agency to make those services available within their infrastructure.
> - The initial choice of the [Sobloo DIAS](https://sobloo.eu/) (and therefore the [OBS](https://www.orange-business.com/en) cloud provider) was made during the first stage of the project following a preliminary benchmark of all 5 DIAS, based on a scoring grid. Where appropriate, the benchmark criteria will be mentioned in the description of the requirement, to add context.
> - Where possible, a link to the more detailed documentation of the [OBS](https://www.orange-business.com/en) cloud provider will also be added to the requirements.

## Table of Contents

0. [Organization](#0-organization)
1. [Kubernetes cluster (CNCF-compliant)](#1-kubernetes-cluster-cncf-compliant)
2. [Operations](#2-operations)
3. [Object storage service (AWS S3 compliant)](#3-object-storage-service-aws-s3-compliant)
4. [Protections against cyber attacks](#4.-protections-against-cyber-attacks)
5. [Declarative management of infrastructure resources (optional)](#5-declarative-management-of-infrastructure-resources-optional)

## 0. Organization

### Deployement on Sobloo ("Option B")

PwC operates the FaST infrastructure (on Sobloo) and services on behalf of the Paying Agency. PwC is responsible for deployment of new versions, monitoring, maintenance and any recovery procedures. PwC also covers all infrastructure costs until May 31, 2022. 

> Discussions are underway within the European Commission regarding the sustainability of OBS FaST Cloud environments beyond Stage 2. 

### Deployment on a *suitable* infrastructure, external to Sobloo ("Option C")

The choice of deploying the FaST Platform on another (internal) infrastructure is synonymous with a transfer of responsibilities. 

#### Required skills

The Paying Agency must have the necessary skills to deploy and run the FaST Platform internally. With the experience we have in operating the FaST Platform, we recommend two technical profiles:
1. **Infrastructure and Operations Engineer** - His/her role focuses on the orchestration and operation of resources to deploy an operational Kubernetes cluster. He/she will use the available resources of the Paying Agency to meet the prerequisites defined below (see 1. Kubernetes cluster). He/she will be responsible for the Kubernetes cluster and the underlying infrastructure.
2. **Kubernetes Expert with a DevOps focus** - Working directly with current provider PwC, his/her role will be to own and deploy the Kubernetes manifest bundles of FaST services that will be delivered for each new release. In addition to the YAML manifests, the associated Docker images will also be delivered. He/she will be responsible for the services deployed on the Kubernetes cluster.

#### Responsibility assignment matrix (RACI model)

The Paying Agency must assume all operational responsibilities.

| Activities | European Commission | European Commission - IT Provider (PwC) | Paying Agency | Paying Agency - IT Provider |
|-------|-----|---------------------|---------------|------------------|
| Infrastructure provisioning and maintenance (+ recovery) | I | C | A | R |
| Provisioning and maintenance of a compliant Kubernetes cluster (+ recovery) | I | C | A | R |
| Provisioning and maintenance of external compliant PostgreSQL databases (if applicable) (+ recovery) | I | C | A | R |
| Delivery of new versions of FaST (Kubernetes manifests + Docker images) | A | R | I | I |
| First deployment of FaST services | I | C | A | R |
| Go live (release) of each new FaST versions | I | C | A | R |
| Monitoring of FaST services (+ recovery)| I | C | A | R |
| Providing documentation (infrastructure requirements, deployment instructions, a detailed glossary of deployed FaST resources/services) | A | R | I | I |
 
The RACI diagram above is defined according to the principles defined on the referenced [Wikipedia page](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix):
* **R** = Responsible (also recommender) / Those who do the work to complete the task.
* **A** = Accountable (also approver or final approving authority) The one ultimately answerable for the correct and thorough completion of the deliverable or task.
* **C** = Consulted (sometimes consultant or counsel) / Those whose opinions are sought, typically subject-matter experts.
* **I** = Informed (also informee) Those who are kept up-to-date on progress, often only on completion of the task or deliverable.

> The Paying Agency and its IT provider are responsible for the 24-hour availability of FaST services deployed on its infrastructure, security, and hosted data.

## 1. Kubernetes cluster (CNCF-compliant)

<img src="standalone-deployment/kubernetes.png" width="40"/> 

The [FaST Platform](https://fastplatform.eu) relies on a [Kubernetes](https://kubernetes.io/) cluster to orchestrate and manage the lifecycle of all (micro)services including internal communication and persistent storage. Therefore, our continuous integration and deployment pipelines produce [Docker](https://www.docker.com/) containers and the associated declarative [YAML deployment manifests](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

### Kubernetes requirements

The Paying Agency must set up and make available a [Kubernetes](https://kubernetes.io/) cluster meeting the following specifications:
- Version: [1.19.10](https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG/CHANGELOG-1.19.md#v1156)
- Topology:
  - The nodes must be distributed over 3 (three) independent availability zones, and labeled accordingly. Labeling is essential to allow the orchestrator to deploy service replicas in a highly available manner.
- Control Plane:
  - The [Kubernetes](https://kubernetes.io/) cluster must have a minimum of 3 (three) *master* nodes.
- Nodes:
  - The [Kubernetes](https://kubernetes.io/) cluster must have a minimum of 3 (three) *worker* nodes.
- Autoscaling:
  - The *worker* nodes are grouped in one or more node pools. The number of nodes must automatically scale according to the current load (CPU & RAM).
- Network:
  - A basic CNI (Container Network Interface) plugin must be used to provide IP addressing and allow communication between pods in the cluster. The number of IPs must not be a limit (number of potential pods > 1000).
- Storage:
  - The available StorageClass(es) must allow the dynamic creation of persistent volumes based on physical block storage. The physical hard drives must be of type SATA and SSD.
- Load Balancer:
  - [Kubernetes](https://kubernetes.io/) native "Service" resource must support the life cycle management of a Load Balancer dynamically (provision, configuration, decommission). The configuration must include the association of a public or private IP address and the configuration of its backend pool on one of the node pools of the [Kubernetes](https://kubernetes.io/) cluster.

YAML deployment manifests describe the target state of the resources to be deployed. Here is the exhaustive list of [Kubernetes](https://kubernetes.io/) native resources that must be manageable on the cluster: 

| API Group | Versions | Resources |
| --------- | -------- | --------- |
| apps | v1 | DaemonSet <br> Deployment <br> ReplicaSet <br> StatefulSet |
| autoscaling | v1, v2beta1, v2beta2 | HorizontalPodAutoscaler |
| batch | v1beta1 <br> v1 | CronJob <br> Job |
| extensions | v1beta1 | DaemonSet <br> Deployment <br> Ingress <br> NetworkPolicy <br> PodSecurityPolicy <br> ReplicaSet |
| policy | v1beta1 | PodDisruptionBudget <br> PodSecurityPolicy |
| "default" | v1 | Binding <br> ComponentStatus <br> ConfigMap <br> Endpoints <br> Event <br> LimitRange <br> Namespace <br> Node <br> PersistentVolumeClaim <br> PersistentVolume <br> Pod <br> PodTemplate <br> ReplicationController <br> ResourceQuota <br> Secret <br> ServiceAccount <br> Service |

> Notes:
> - All the specifications described above are mandatory.
> - The implementation of an overlay network implementing the Kubernetes NetworkPolices features is not mandatory.

The installation of the components of a [Kubernetes](https://kubernetes.io/) cluster is the responsibility of the candidate Paying Agency to deploy the [FaST Platform](https://fastplatform.eu) on its own infrastructure. The Paying Agency is also responsible for the operational maintenance of the [Kubernetes](https://kubernetes.io/) cluster and for its availability.

<img src="standalone-deployment/kubernetes-components.svg" width="1000" style="background-color: #FFFFFF"/>
<p align="center">Diagram of a <a href="https://kubernetes.io/">Kubernetes</a> cluster with all the components tied together.</p>

<img src="standalone-deployment/kubernetes-ha.png" width="1000" style="background-color: #FFFFFF"/>
<p align="center">A High-Availability Control Plane.</p>

### 1.1 System requirements for Kubernetes

[Kubernetes](https://kubernetes.io/) clusters deployed and managed by the [OBS](https://www.orange-business.com/en) cloud provider rely on physical and network resources for provisioning virtual machines (*master* and *worker* nodes).

We recommend below minimum specifications for each underlying hardware. These specifications can be adjusted (up) by a knowledgeable Paying Agency, as required by the volume of users to be supported by the deployment. 

> **Minimum template for *master* and *worker* nodes**
> - Type: General Purpose
> - vCPUS: 8
> - RAM: 32GB
> - Disks: SSD. In sufficient number and as recommended in the Kubernetes best-practices.

> **Private network**
> - Subnet: 1
> - CIDR block: /16
> - IP assignment: DHCP
> - DNS server: 1 (internal + external address resolution)
> - Internal Gateway: 1
> - Load Balancer: Static or dynamic provisioning with public or private addressing

### 1.2 [CNCF](https://www.cncf.io/) Compliance

<img src="standalone-deployment/sonobuoy.svg" width="200"/> 

The [CNCF (Cloud Native Computing Foundation)](https://www.cncf.io/) was created by Google and works to promote cloud-native microservices architectures by supporting an ecosystem of open source projects, including [Kubernetes](https://kubernetes.io/).

[Sonobuoy](https://sonobuoy.io/) is one of these open source projects and is [the official validation and certification tool](cncf.io/certification/software-conformance/) for [Kubernetes](https://kubernetes.io/) setups. It guarantees users the provision of a compliant [Kubernetes](https://kubernetes.io/) cluster. All Cloud providers offering an off-the-shelf [Kubernetes](https://kubernetes.io/) must go through this certifications.

The Kubernetes cluster provided under the responsibility of the Paying Agency, **must** be validated by the [Sonobuoy](https://sonobuoy.io/) tool.

### 1.3 Conformance procedure

Here is the compliance procedure as it will be executed on the [Kubernetes](https://kubernetes.io/) cluster provided by the Paying Agency.

1. Download the [Sonobuoy CLI](https://sonobuoy.io/) for your platform

> Linux: https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.15.4/sonobuoy_0.15.4_linux_amd64.tar.gz

> OSX: https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.15.4/sonobuoy_0.15.4_darwin_amd64.tar.gz

> Windows: https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.15.4/sonobuoy_0.15.4_windows_amd64.tar.gz

2. Execute SONOBUOY in certified-conformance mode

```bash
$> ./sonobuoy run --mode non-disruptive-conformance --wait --skip-preflight
```

3. Get the results

```bash
results=$(sonobuoy retrieve)
sonobuoy results $results
```

> If some compliance tests have failed and no remediation can be taken by the Paying Agency, a common review will be done to determine on a case-by-case basis whether each non-compliance is blocking or not.

### 1.4 Service Mesh (Istio)

> Istio version 1.9.8 must be installed and ready to use on the Kubernetes cluster.

We use the Istio mesh to manage and control communications between deployed services as well as inbound and outbound traffic. We also rely on some Envoy proxy features to apply custom security filtering rules.

<img src="standalone-deployment/istio.svg" width="500" style="background-color: #FFFFFF"/>
<p align="center">
Istio architecture
</p>

The lifecycle of Istio settings applied to each service is managed directly by the Knative Serveless framework (see 1.5) which completely abstracts the orchestration logic. The Istio version and the installed components must be compatible with the Knative version used.

**Gateways** 

The istio installation must include at least the two standard gateways 
- ```istio-ingressgateway``` (incoming traffic from outside)
- ```cluster-local-gateway``` (incoming traffic from local pods outside the mesh)

```istio-ingressgateway``` must be accessible from a public IP. One way to do this is to configure a LoadBalancer (e.g. via a native Kubernetes LoadBalancer Service if available) with a public IP address and whose backend nodes are nodes of the Kubernetes cluster. Other methods exists and can be used.

> It is the responsibility of the  Paying Agency to install Istio in 1.9.8. High availability of Istio components are also the responsibility of the Paying Agency.

The official documentation is available on the Istio project [website](https://istio.io/). Istio setup is also explained on the Knative [repository](https://github.com/knative/serving). If needed the Kubernetes installation manifests as used for the FaST Platform online can be provided.

### 1.5 Serverless Framework (Knative)

> Knative Serving version 0.25.1 must be installed and ready to use on the Kubernetes cluster. Knative 0.25.1 is compatible with Kubernetes v1.19.10 and Istio 1.9.8.

We use Knative Serving as a framework to orchestrate all stateless microservices. Knative manages all the underlying resources from the network (ingress, certificates, routing, policies, etc ...) to the workloads (deployments, pods, etc ...) with strategic features like rolling update and automatic scaling (even down to zero). Knative is deeply integrated with Istio.

<img src="standalone-deployment/knative.png" width="500" style="background-color: #FFFFFF"/>
<p align="center">
Underlying Knative objects
</p>

**Domain**

The base domain declared in the Knative configurations must be associated with the public IP of the ```istio-ingressgateway``` Gateway.

For each Knative service, the DNS alias used to configure Ingress resourses must be determined by the following custom template:

```yaml
  domainTemplate: |-
    {{if index .Annotations "service.knative.kubernetes.io/name" -}}
      {{- index .Annotations "service.knative.kubernetes.io/name" -}}
    {{else -}}
      {{- .Name -}}
    {{end -}}
    {{if index .Annotations "service.knative.kubernetes.io/subname" -}}
      {{if ne (index .Annotations "service.knative.kubernetes.io/subname") "false" -}}
        .{{- index .Annotations "service.knative.kubernetes.io/subname" -}}
      {{end -}}
    {{else -}}
      .{{- .Namespace -}}
    {{end -}}
    .{{.Domain}}
```

As an example, with a base domain ```be-wal.fastplatform.eu```, the Knative service below will be configured to be accessible via the DNS alias ``map.beta.be-wal.fastplatform.eu``:

```yaml
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  annotations:
    service.knative.kubernetes.io/name: map
    service.knative.kubernetes.io/subname: beta
  name: mapproxy-server
  namespace: <any-namespace>
spec:
  template:
    spec:
        image: ...
```

The FaST Platform exposes the following public endpoints:

- `https://internal.api.{subname}.{knative-base-domain}``
- `https://api.{subname}.{knative-base-domain}``
- `https://idp-authentication.{subname}.{knative-base-domain}``
- `https://map.{subname}.{knative-base-domain}``
- `https://portal.{subname}.{knative-base-domain}``
- `https://media.{subname}.{knative-base-domain}``
- `https://web-vector-tiles-external.{subname}.{knative-base-domain}``
- `https://web-vector-tiles-fastplatform.{subname}.{knative-base-domain}``

> For all these public endpoints, SSL certificates must be provided.

**SSL Certificates**

Knative can automatically generate SSL certificates using the Cert Manager service (see 1.6) and [Let's Encrypt](https://letsencrypt.org/). Each Paying Agency can choose between a wildcard certificate, generating a certificate per Knative service or using an external certificate.

**Configurations**

The following Knative configurations must be active:
| Knative configuration | values |
| --------------------- | ------ |
| config-certmanager | If needed, specify the active issuer to be used for automatic generation of SSL certificates per Knative service. |
| config-domain | Specify the base domain used to expose the knative services (associated to the public IP of the ```ìstio-ingressgateway``` Gateway). This base domain will be used to determine the full DNS alias for each service. |
| config-istio | Specify the Istio Ingress Gateway aliases to be used for external inbound traffic and traffic from off-mesh pods. |
| config-network | If needed, enable automatic SSL certificate generation with the following options: autoTLS=Enabled, certificate.class=cert-manager.certificate.networking.knative.dev, domainTemplate=```(cf. above domainTemplate)``` 

> It is the responsibility of the  Paying Agency to install Knative Serving in v0.25.1.. High availability of Knative components are also the responsibility of the Paying Agency.

The official documentation is available on the Knative project [website](https://knative.dev/docs/). Knative setup is also explained on the Knative [repository](https://github.com/knative/serving). If needed the Kubernetes installation manifests as used for the FaST Platform online can be provided.

### 1.6 Cert Manager

Several endpoints and therefore several DNS aliases are required to expose the FaST Platform over the internet. All the endpoints are exposed in a secure manner (https). The management of the necessary SSL certificates can be delegated to the cert-manager service but is not mandatory.

[cert-manager](https://cert-manager.io/) adds certificates and certificate issuers as resource types in Kubernetes clusters, and simplifies the process of obtaining, renewing and using those certificates. It can issue certificates from a variety of supported sources, including Let’s Encrypt, HashiCorp Vault, and Venafi as well as private PKI. It ensures certificates are valid and up to date, and attempts to renew certificates at a configured time before expiry.

<img src="standalone-deployment/cert-manager.svg" width="700" style="background-color: #FFFFFF"/>
<p align="center">
cert-manager diagram
</p>

> It is the responsibility of the Paying Agency to provide SSL certificates. It can be automatic or not. If needed, the Kubernetes installation manifests as used for the FaST Platform online can be provided.

### 1.7 PostgreSQL

FaST Platform services require several PostgreSQL databases for persistence. Each Paying Agency has two options for managing these PostgreSQL databases:
1. PostgreSQL databases deployed on Kubernetes alongside the services (default)
2. External PostgreSQL databases provisioned and deployed separately

Deploying Postgres databases on Kubernetes is the default option. In this case, all ready-to-use deployment elements will be provided to the Paying Agency to orchestrate and manage High Available Postgres clusters. The Paying Agency will nevertheless have to take ownership of these elements and will be responsible for them.

If the Paying Agency chooses the second option and deploys the Postgres databases separetly, the Paying Agency will be provided with the required specifications only. In particular, as of version v1.1.0 of the FaST Platform, two types of Postgres setup must be available:
- PostgreSQL: 12.7
- PostgreSQL: 12.7 + PostGIS: 2.5

A superuser, as well as a "service" user, must be created on each Postgres instance. The SQL initialization script will be provided.

**Crunchy PostgreSQL Operator**

> For the first option only, Crunchy Postgres Operator version 4.7 must be installed and ready to use on the Kubernetes cluster.

<img src="standalone-deployment/crunchy-pgo.png" width="700" style="background-color: #FFFFFF"/>
<p align="center">
Architecture diagram of the Crunchy Postgres Operator
</p>

The Kubernetes manifests to setup the Crunchy Postgres Operator will be provided. The official documentation is available on the Crunchy PostgreSQL Operator project [website](https://www.crunchydata.com/products/crunchy-postgresql-operator/). Kubernetes manifests for deploying Postgres clusters (managed by the operator) will be provided alongside those for the platform's microservices.

## 2 Operations

The deployment of the different versions of the FaST Platform can be done in two ways depending of the access of the Kubernetes cluster provided by the Paying Agency (see 1.):
- via a dedicated pipeline on [GitLab.com](https://gitlab.com)
- via the offline deployment of a bundle of Kubernetes manifests and associated Docker images

**Dedicated pipeline on [GitLab.com](https://gitlab.com)**

This option requires access to the Paying Agency's Kubernetes cluster API from the Internet and from [GitLab.com](https://gitlab.com). Once the credentials of the Kubernetes cluster are registered on [GitLab.com](https://gitlab.com), a dedicated pipeline will be set up and instantiated automatically for each new version of the FaST Platform. To deploy a new version on the Kubernetes cluster, a simple manual activation of the pipeline will be needed. Thus, the Paying Agency can take advantage of the DevOps assets of the FaST Platform already in place to operate iterative deployments.

A Kubernetes account with sufficient rights on the target `namespace` is a prerequisite. Docker images can be used directly from the existing FaST repository or pushed to a registry of the Paying Agency beforehand.

**Offline deployment**

If the Paying Agency cannot expose the Kubernetes cluster administration API over the Internet, the only option is offline manual deployment of YAML manifests and Docker images. A pipeline on [GitLab.com](https://gitlab.com) will be set up to produce artifacts containing the YAML manifests and the Docker images to be deployed.

> The deployment operation will be under the responsibility of the Paying Agency.

The YAML manifests bundle will be customizable enough to take into account the standard specificities of an internal IT infrastructure and organization. This parameterization will be reviewed jointly with the Paying Agency, within the limits of the common FaST technical infrastructure definition.

## 3. Object storage service (AWS S3 compliant)

<img src="standalone-deployment/s3.png" width="100"/>

The S3 storage system of the [OBS](https://www.orange-business.com/en) cloud provider is used to store and serve the static elements of the web interfaces of the administration portal and the mobile application. It is also used to store and serve tiled Sentinel images and derived products such as NDVI indexes.

A candidate Paying Agency **must** provide a S3 service meeting the following specifications:

- REST API
  - Must be Compatible with Amazon AWS S3 APIs.
- Buckets
  - Features must be available to manage several separate buckets.
- High Availability
  - Must be aware of infrastructure availability zones to ensure service availability.
- Public Endpoint
  - An API access endpoint must be accessible from the public Internet (with valid SSL certificates / HTTPS) and resolvable from a dedicated DNS alias.
- Access Domain Name
  - One dedicated DNS alias must be available per bucket.
- Web UI
  - A graphical user interface must be available for managing the S3 service (Buckets, RBAC, CORS Rules, etc.).
- Authentification
  - Must be compatible with Signature Version 3 and 4. Must be connected to an identity provider system with user group management capabilities.
- Role Based Access Control
  - Multi-level access management: Private, Public Read, Public Read & Write. Authorization management must be applicable to a single user or a group of users.
- CORS
  - Cross-origin resource sharing (CORS) rules must be configurable to facilitate cross-domain requests among clients.

## 4. Protections against cyber attacks

It is the responsibility of the Paying Agency to put in place security measures to protect and monitor their infrastructure from external attacks. 

The [Sobloo DIAS](https://sobloo.eu/) deployment of the FaST Platform uses off-the-shelf services provided by [OBS](https://www.orange-business.com/en) to add security measures against denial of service attacks and application request tampering (forgery). These are basic protections that the each Paying Agency must take on to ensure at least a first layer of security for its dedicated deployment of the FaST Platform.

The following is a description of the features of the [OBS](https://www.orange-business.com/en) cloud provider services we used.

### 4.1 Anti-DDoSS

The anti-distributed denial of service (Anti-DDoS) aims to provide precise capabilities for defending DDoS attacks, such as challenge collapsar (CC) attacks, SYN flood, and User Datagram Protocol (UDP) flood, for tenants by encapsulating professional anti-DDoS device functions. The Anti-DDoS service can defend tenants’ public IP addresses against traffic attacks and application-layer CC attacks.

Anti-DDoS service must provides at least the following functions:
- Defends against traffic attacks and application-layer CC attacks. 
- Allows tenants to customize Anti-DDoS policies. 
- Allows tenants to select public IP addresses to be defended. 
- Provides real-time monitoring reports. 

Link to the official documentation of the [OBS](https://www.orange-business.com/en) cloud provider: [https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/anti-ddos/](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/anti-ddos/)

### 4.2 Web Application Firewall

WAF (web application firewall) is a security service for checking and blocking web attacks. It can solve SQLi, XSS, CMDi and other types of web attacks, that websites face.

Link to the official documentation of the [OBS](https://www.orange-business.com/en) cloud provider: [https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/web-application-firewall/](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/web-application-firewall/)

## 5. Declarative management of infrastructure resources (optional)

The [Sobloo DIAS](https://sobloo.eu/) deployment of the FaST Platform is operated under a *GitOps* approach for both the deployment of infrastructure and the application workloads.

The single source of truth of the configuration of the deployed infrastructure resources is stored and versioned in a `git` repository.

Each modification of the target state of the infrastructure goes through a modification of the state declared in the versioning system and a commit pushed on the `master` branch by a *Merge Request* procedure. This process enables us to follow with high precision the description of the infrastructure elements without having to directly access the resources themselves.

The orchestration of infrastructure resources in an *Infrastructure-As-Code* philosophy, versioned in `git`, is a best practice we encourage each Paying Agency to adopt. It allows for greater control and is essential for future third party collaborations.

<img src="https://gitlab.com/fastplatform/docs/-/raw/master/reference/infrastructure/images/gitops-logic.png" width="1000" style="background-color: #FFFFFF"/>
<p align="center">
GitOps (Infra as Code) implementation of the <a href="https://fastplatform.eu">FaST Platform</a> based on <a href="https://www.terraform.io/">Terraform</a> and <a href="https://www.runatlantis.io/">Atlantis</a> tools.
</p>

This process was implemented for the [Sobloo DIAS](https://sobloo.eu/) deployment using tools like [Terraform](https://www.terraform.io/) and [Atlantis](https://www.runatlantis.io/).

We highly recommended to use the building blocks already implemented in the project, with the help of the associated [documentation](https://gitlab.com/fastplatform/docs/-/blob/master/reference/ci-cd.md#infrastructure-as-code-gitops).
